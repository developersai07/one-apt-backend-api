import "reflect-metadata";
import { createConnection } from "typeorm";
//import * as express from "express";
import * as bodyParser from "body-parser";
import * as fileUpload from "express-fileupload";
import { getRepository } from "typeorm";
import { AssociationSubscription } from "./entity/AssociationSubscription";
const express = require('express');
const fs = require('fs');
import routes from "./routes";
import http = require("http");
var bodyparser = require('body-parser');
var multer = require('multer');
var upload = multer();
const cron = require("node-cron");
createConnection().then(async connection => {
  const app = express();
  // app.use(bodyParser.json());
  // app.use(bodyParser.urlencoded({ extended: true }));
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(bodyParser.urlencoded({ limit: '50mb', extended: false }));

  // for parsing multipart/form-data
  //app.use(upload.single('image')); 
  app.use(express.static('public'));

  app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept,Authorization");
    res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
    next();
  });
  const fileUpload = require('express-fileupload');
  app.use(fileUpload());
  app.post('/ok', (req, res) => {
    res.json({ "message": "Welcome to EasyNotes application. Take notes quickly. Organize and keep track of all your notes." });
  });

  var router = express.Router();
  app.post("/associationEnrollemnt2", function (req, res, next) {
    console.log(req.files);
    res.send("ok");
  });
  app.use("/", routes);


  // const fileUpload = require('express-fileupload');
  // const app = express();

  // default options
  //app.use(fileUpload());

  app.get("/test", function(req , res ) {

res.send("App Works")

  })


  //   // let sampleFile = req.file.sampleFile;

  //   // sampleFile.mv('/uploads/filename.jpg', function(err) {
  //   //   if (err)
  //   //     return res.status(500).send(err);

  //   //   res.send('File uploaded!');
  //   // });
  //   console.log("oksaads");

  //   res.send('File uploaded!');
  // });


  cron.schedule("* * * * *", async function () {
    var cr_date = new Date();
    cr_date.getDate();
    cr_date.getFullYear();
    cr_date.getMonth();

    var crMonth = cr_date.getMonth() + 1;
    var convertedString = cr_date.getFullYear() + '-' + crMonth + '-' + cr_date.getDate();
    const asSubRep = await getRepository(AssociationSubscription)
    const associationSubscriptionrep = await getRepository(AssociationSubscription)
      .createQueryBuilder("assSub")
      .where("assSub.SUCBSCRIPTION_END_DATE = :endDate && assSub.IS_ACTIVE = true", {
        endDate: convertedString,

      }).getMany();


    if (associationSubscriptionrep.length > 0) {
      for (var i = 0; i < associationSubscriptionrep.length; i++) {
        var assSubId = associationSubscriptionrep[i].id;
        var assSubRepObj = await asSubRep.findOne(assSubId);
        assSubRepObj.IS_ACTIVE = false;
        asSubRep.save(assSubRepObj);
      }

    }


  });

  app.listen(3000);


}).catch(error => console.log(error));