import { Router } from 'express';
import SecurityController from '../Controllers/SecurityController';
import { checkJwt } from "../middlewares/checkJwt";
const path = require('path');
const router = Router();

router.post("/add",[checkJwt], SecurityController.add,  function (req, res, next) {
    console.log(req.files);
      if (req.files.security_proof) {
        var ext = req.files.security_proof.name || ''.split('.');
        let sampleFile = req.files.security_proof;
        var ext = req.files.security_proof.name.substr(req.files.security_proof.name.lastIndexOf('.') + 1);
        let securityGovProof = req.body.securityId + '.' + ext + '';
        if (securityGovProof) {
          sampleFile.mv("./uploads/security/govProof/" + securityGovProof + "", function (err) {
            if (err) {
              return res.status(500).send(err);
            } else {
              req.body.sec_gov_proof = securityGovProof;
              next();
            }
          });
        }
      } else {
          res.send(req.body.data)
        //next();
      }
    },
    
    SecurityController.updateFile
    );
 router.get("/getAll/:id",SecurityController.getAll);
 router.get("/securityProof/:id", function (req, res) {
  var fs = require("fs");
  var fileLocation = "../../uploads/security/govProof/" + req.params.id + "";

  const dirPath = path.join(__dirname, '../../uploads/security/govProof/' + req.params.id + '');
  fs.readFile(fileLocation, function (error, data) {
      if (error) {
          console.log(error);
      }
      res.sendFile(dirPath);
  });
});

 router.post("/update",[checkJwt],SecurityController.update,
 function (req, res, next) {
  console.log(req.files);
    if (req.files.security_proof) {
      var ext = req.files.security_proof.name || ''.split('.');
      let sampleFile = req.files.security_proof;
      var ext = req.files.security_proof.name.substr(req.files.security_proof.name.lastIndexOf('.') + 1);
      let securityGovProof = req.body.securityId + '.' + ext + '';
      if (securityGovProof) {
        sampleFile.mv("./uploads/security/govProof/" + securityGovProof + "", function (err) {
          if (err) {
            return res.status(500).send(err);
          } else {
            req.body.sec_gov_proof = securityGovProof;
            next();
          }
        });
      }
    } else {
        res.send(req.body.data)
      //next();
    }
  },
  
  SecurityController.updateFile
 );

    
export default router;