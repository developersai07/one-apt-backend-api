var express = require('express');
var router = express.Router();
const path = require('path');
import CommonAreaTicketController from "../Controllers/CommonAreaTicketController";
import { checkJwt } from "../middlewares/checkJwt";
//import { checkJwt } from "../middlewares/checkJwt";

//const router = Router();
//Login route
router.post("/addCommonTicket", CommonAreaTicketController.raiseCommonTicket,
  function (req, res, next) {
    if (req.files.ComticketImage) {
      console.log("gile ptresrermr, ")
      var ext = req.files.ComticketImage.name || ''.split('.');
      let sampleFile = req.files.ComticketImage;
      var ext = req.files.ComticketImage.name.substr(req.files.ComticketImage.name.lastIndexOf('.') + 1);
      let comTicketImg = req.body.comTicketId + '.' + ext + '';
      if (comTicketImg) {
        console.log("no file ptresrermr, ")
        sampleFile.mv("./uploads/comTicketImg/" + comTicketImg + "", function (err) {
          if (err) {
            console.log(err)
            return res.status(500).send(err);
          }
          else {
            req.body.comTicketImg = comTicketImg;
            next();
          }

        },



        );


      }

    }
    else {

      res.send(req.body.data)
      //next();
    }
  },
  CommonAreaTicketController.updateFileName,
);
router.get("/getComTicketByAsscoiation/:associationId", CommonAreaTicketController.getComTicketByAsscoiation);
router.post("/mapCommonVendorAction", CommonAreaTicketController.mapCommonVendorAction,
  CommonAreaTicketController.checkPrimaryInternalVendor,
  CommonAreaTicketController.checkSecondaryInternalVendor,
  CommonAreaTicketController.checkThirdInternalVendor,
  CommonAreaTicketController.checkFourthInternalVendor,
  CommonAreaTicketController.checkFifthInternalVendor,
  CommonAreaTicketController.checkFirstPriorThirdPartyVendor,
  CommonAreaTicketController.checkSecondPriorThirdPartyVendor,
  CommonAreaTicketController.checkThirdPriorThirdPartyVendor,
  CommonAreaTicketController.checkFourthPriorThirdPartyVendor,
  CommonAreaTicketController.checkFifthPriorThirdPartyVendor,
);

router.get("/mapAssociationComTicketAction/:comticketid/:appUserid/:status/:associationId",
  CommonAreaTicketController.actionComTicketByassociation
  , CommonAreaTicketController.checkPrimaryInternalVendor,
  CommonAreaTicketController.checkSecondaryInternalVendor,
  CommonAreaTicketController.checkThirdInternalVendor,
  CommonAreaTicketController.checkFourthInternalVendor,
  CommonAreaTicketController.checkFifthInternalVendor,
  CommonAreaTicketController.checkFirstPriorThirdPartyVendor,
  CommonAreaTicketController.checkSecondPriorThirdPartyVendor,
  CommonAreaTicketController.checkThirdPriorThirdPartyVendor,
  CommonAreaTicketController.checkFourthPriorThirdPartyVendor,
  CommonAreaTicketController.checkFifthPriorThirdPartyVendor,
)
router.get('/getcomTicketsByMonth/:id/:year', CommonAreaTicketController.getcomTicketsByMonth);
router.get('/getcomTicketsByMonthAll/:year', CommonAreaTicketController.getcomTicketsByMonthAll);
router.get('/getcomTicketsByMonthVendorAssociation/:id/:year', CommonAreaTicketController.getcomTicketsByMonthVendorAssociation);
router.get("/mapAssociationComTicketAction/:id/:status", CommonAreaTicketController.actionWorkComTicketByassociation);

router.get("/comImage/:id", function (req, res) {
  var fs = require("fs");
  var fileLocation = "../../uploads/comTicketImg/" + req.params.id + "";

  const dirPath = path.join(__dirname, '../../uploads/comTicketImg/' + req.params.id + '');
  fs.readFile(fileLocation, function (error, data) {
    if (error) {
      console.log(error);
    }
    res.sendFile(dirPath);
  });
})

router.get("/appUserComTicket/:id", CommonAreaTicketController.appUserComTicket);
router.get("/appVendorComTicket/:id", CommonAreaTicketController.appVendorComTicket);
router.get("/appVendorPendingComTicket/:id", CommonAreaTicketController.appVendorPendingComTicket);
router.get("/appVendorComTicketCompleted/:id", CommonAreaTicketController.appVendorComTicketCompleted);
router.get("/workCompleteVerify/:id/:userid", CommonAreaTicketController.workCompleteVerify);
router.get('/getcomTicketsByAssociation/:id', CommonAreaTicketController.getcomTicketsByAssociation);
router.get("/getCommonAreaTicketsByVendorAssociation/:id", CommonAreaTicketController.getCommonAreaTicketsByVendorAssociation)
router.get("/getTicketDetails", CommonAreaTicketController.getTicketDetails);
export default router;