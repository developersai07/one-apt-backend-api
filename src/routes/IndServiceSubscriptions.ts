import { Router } from "express";
import { checkJwt } from "../middlewares/checkJwt";
import IndServiceSubscriptionController from "../Controllers/IndServiceSubscriptionController";

const router = Router();

router.post("/addIndServiceSubscription", [checkJwt], IndServiceSubscriptionController.addIndServiceSubscription);
router.get("/allIndServiceSubscription", [checkJwt], IndServiceSubscriptionController.getAllIndServiceSubscription);
router.post("/deleteIndServiceSubscription", IndServiceSubscriptionController.deleteIndServiceSubscription);
router.post("/updateIndServiceSubscription", IndServiceSubscriptionController.updateIndServiceSubscription);

export default router;