import { Router } from "express";
import vendorRequestedController from "../Controllers/vendorRequestedController";
import { checkJwt } from "../middlewares/checkJwt";
//import { checkJwt } from "../middlewares/checkJwt";

const router = Router();
//Login route
router.post("/request", vendorRequestedController.request);
router.get("/requestedList/:vassociatinid", vendorRequestedController.getVendorsRequests);
router.get("/associationrequestedList/:associatinid", vendorRequestedController.getassociationVendorsRequests);
router.get("/acceptAssociation/:vassociatinid/:associatinid/:id", vendorRequestedController.acceptAssociation);
router.get("/rejectAssociation/:vassociatinid/:associatinid/:id", vendorRequestedController.rejectAssociation);
router.get("/getMappedAssociations/:id", vendorRequestedController.getMappedAssociations);
router.get("/getAssociationIndTickets/:id/:vid", vendorRequestedController.getAssociationIndTickets);
router.get("/getAppartmentAssociationComTickets/:id", vendorRequestedController.getAppartmentAssociationComTickets);

router.get("/getIndTicketDetails/:id", vendorRequestedController.getIndTicketDetails);
router.get("/getAssociationComTickets/:id/:vid", vendorRequestedController.getAssociationComTickets);
router.get("/getComTicketDetails/:id", vendorRequestedController.getComTicketDetails);
router.post("/AssignIndTicket", vendorRequestedController.AssignIndTicket);
router.post("/AssignComTicket", vendorRequestedController.AssignComTicket);
router.get("/getAssociationData", vendorRequestedController.getAssociationData);
router.get("/getAppartmentAssociationIndTickets/:id", vendorRequestedController.getAppartmentAssociationIndTickets);
router.post("/AssociationAssignIndTicket", vendorRequestedController.AssociationAssignIndTicket);
router.post("/AssignComTicketByAssociation", vendorRequestedController.AssignComTicketByAssociation);

export default router;