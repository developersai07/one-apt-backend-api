import { Router } from "express";
import FlatOwnerController from "../Controllers/FlatOwnerController";
import { checkJwt } from "../middlewares/checkJwt";
const path = require('path');
//import { checkJwt } from "../middlewares/checkJwt";

const router = Router();

router.post("/sampleFile", [checkJwt], function (req, res, next) {
    console.log(req.files);
    res.send("success");
});

router.post("/add", [checkJwt], FlatOwnerController.ownerAdd,
    function (req, res, next) {
        if(req.files)
        {
            if (req.files.owner_pic) {
                var ext = req.files.owner_pic.name || ''.split('.');
                let imageFile = req.files.owner_pic;
                var ext = req.files.owner_pic.name.substr(req.files.owner_pic.name.lastIndexOf('.') + 1);
                let flatOwnerImage = req.body.id + '.' + ext + '';
                if (flatOwnerImage) {
                    imageFile.mv("./uploads/flatOwnerImage/" + flatOwnerImage + "", function (err) {
                        if (err) {
                            return res.status(500).send(err);
                        } else {
                            req.body.owner_pic_name = flatOwnerImage;
                            console.log(req.body.owner_pic_name);
                            next();
                        }
                    });
                }
            } else {
                next();
            }

        }
        else {
            res.send(req.body.data)
        }
       
    }, FlatOwnerController.flatOwnerImage
);

router.get("/flatOwnerImage/:id", function (req, res) {
    var fs = require("fs");
    var fileLocation = "../../uploads/flatOwnerImage/" + req.params.id + "";

    const dirPath = path.join(__dirname, '../../uploads/flatOwnerImage/' + req.params.id + '');
    fs.readFile(fileLocation, function (error, data) {
        if (error) {
            console.log(error);
        }
        res.sendFile(dirPath);
    });
});

router.get("/getAll/:id", [checkJwt], FlatOwnerController.flatOwnergetAll);
router.get("/getById/:id", [checkJwt], FlatOwnerController.flatOwnergetById);
router.post("/delete", [checkJwt], FlatOwnerController.flatOwnerDelete);
router.post("/update", [checkJwt], FlatOwnerController.ownerUpdate,
    function (req, res, next) {
        if(req.files){
            if (req.files.owner_pic) {
                var ext = req.files.owner_pic.name || ''.split('.');
                let imageFile = req.files.owner_pic;
                var ext = req.files.owner_pic.name.substr(req.files.owner_pic.name.lastIndexOf('.') + 1);
                let flatOwnerImage = req.body.id + '.' + ext + '';
                if (flatOwnerImage) {
                    imageFile.mv("./uploads/flatOwnerImage/" + flatOwnerImage + "", function (err) {
                        if (err) {
                            return res.status(500).send(err);
                        } else {
                            req.body.owner_pic_name = flatOwnerImage;
                            console.log(req.body.owner_pic_name);
                            next();
                        }
                    });
                }
            } else {
                next();
            }
        }
        else {
            res.send(req.body.data);

        }
       
    }, FlatOwnerController.flatOwnerImage
);
router.post("/appUpdate",[checkJwt],  FlatOwnerController.ownerAppUpdate,

function (req, res, next) {
    if(req.files){
        if (req.files.owner_pic) {
            var ext = req.files.owner_pic.name || ''.split('.');
            let imageFile = req.files.owner_pic;
            var ext = req.files.owner_pic.name.substr(req.files.owner_pic.name.lastIndexOf('.') + 1);
            let flatOwnerImage = req.body.id + '.' + ext + '';
            if (flatOwnerImage) {
                imageFile.mv("./uploads/flatOwnerImage/" + flatOwnerImage + "", function (err) {
                    if (err) {
                        return res.status(500).send(err);
                    } else {
                        req.body.owner_pic_name = flatOwnerImage;
                        console.log(req.body.owner_pic_name);
                        next();
                    }
                });
            }
        } else {
            next();
        }
    }
    else {
        res.send(req.body.data);

    }
   
}, FlatOwnerController.flatOwnerImage

);
router.post("/appUpdate", [checkJwt],  FlatOwnerController.ownerAppUpdate);
router.get("/associationInformation/:id", [checkJwt], FlatOwnerController.flatOwnerInformation);
router.get("/getflatOwnerTenant/:id", [checkJwt], FlatOwnerController.getflatOwnerTenant);

export default router;