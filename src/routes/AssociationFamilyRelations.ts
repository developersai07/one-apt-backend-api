import { Router } from "express";
import { checkJwt } from "../middlewares/checkJwt";
import AssociationFamilyRelationsController from  "../Controllers/AssociationFamilyRelationsController"


const router = Router();


router.post("/addAssociationFamilyRelations", [checkJwt], AssociationFamilyRelationsController.addAssociationFamilyRelations);

router.post("/addAssociationFamilyRelations", AssociationFamilyRelationsController.addAssociationFamilyRelations);

router.post("/addAppAssociationFamilyRelations", [checkJwt], AssociationFamilyRelationsController.addAppAssociationFamilyRelations);

router.get("/getAllAppAssociationFamilyRelations", [checkJwt], AssociationFamilyRelationsController.getAllAppAssociationFamilyRelations);

router.post("/updateAppAssociationFamilyRelations", [checkJwt], AssociationFamilyRelationsController.updateAppAssociationFamilyRelations);

router.post("/deleteAppAssociationFamilyRelations", [checkJwt], AssociationFamilyRelationsController.deleteAppAssociationFamilyRelations);

export default router;