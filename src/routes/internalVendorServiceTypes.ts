import { Router } from "express";
import VendorindServiceTypeController from "../Controllers/internalvendorindServiceTypeController";
import { checkJwt } from "../middlewares/checkJwt";
//import { checkJwt } from "../middlewares/checkJwt";

const router = Router();
//Login route
router.post("/add",VendorindServiceTypeController.addInternalServiceAmount);
router.post("/update", [checkJwt], VendorindServiceTypeController.updateInternalServiceAmount);
router.post("/delete", [checkJwt], VendorindServiceTypeController.deleteInternalServiceAmount);
router.get("/getByServiceId/:serviceid/:vendorid",VendorindServiceTypeController.getByServiceId);
//router.get("/getServiceTypes/:id",VendorindServiceTypeController.getServiceTypes);

export default router;