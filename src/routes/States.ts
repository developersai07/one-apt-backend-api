import{ Router } from  'express';
import  StatesController  from '../Controllers/StatesController';

const router = Router();
//Login route
router.get("/getAll", StatesController.statesgetall);
router.get("/getStates/:id", StatesController.getStates);


export default router;