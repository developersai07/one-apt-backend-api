import { Router } from "express";
import UserController from "../Controllers/UserController";
import { checkJwt } from "../middlewares/checkJwt";

const router = Router();
//Login route
router.post("/login", UserController.adminLogin);
router.get("/roles/:appUserId", UserController.getappUserRoles);
router.post("/application/login", UserController.applicationLogin);
router.post("/application/forgotPassword", UserController.applicationForgotPassword);
router.post("/application/forgotUserName", UserController.applicationForgotUserName);

router.post("/updatePassword", [checkJwt], UserController.updatePassword);

export default router;