import { Router } from "express";
import VendorAssociationComServiceController  from "../Controllers/VendorAssociationComServiceController";
import { checkJwt } from "../middlewares/checkJwt";
//import { checkJwt } from "../middlewares/checkJwt";
const router = Router();
//Login route
router.post("/addService",[checkJwt], VendorAssociationComServiceController.addService);
router.get("/allAssociationServices/:id",[checkJwt], VendorAssociationComServiceController.allAssociationServices);
router.post("/delete",[checkJwt], VendorAssociationComServiceController.delete);


export default router;