import { Router } from "express";
import AppartmentGrandChildEntityController from "../Controllers/AppartmentGrandChildEntityController";
import { checkJwt } from "../middlewares/checkJwt";
//import { checkJwt } from "../middlewares/checkJwt";

const router = Router();
router.get("/getAll", [checkJwt], AppartmentGrandChildEntityController.getall);
router.get("/getGrandChild/:id", [checkJwt], AppartmentGrandChildEntityController.getGrandChild);
router.post("/add", [checkJwt], AppartmentGrandChildEntityController.add);
router.post("/update", [checkJwt], AppartmentGrandChildEntityController.update);
router.post("/delete", [checkJwt], AppartmentGrandChildEntityController.childEntityDelete);

export default router;