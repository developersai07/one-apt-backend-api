import { Router } from "express";
import AssociationIndServiceController  from "../Controllers/AssociationIndServiceController";
import { checkJwt } from "../middlewares/checkJwt";
//import { checkJwt } from "../middlewares/checkJwt";
const router = Router();
//Login route
router.post("/addService",[checkJwt], AssociationIndServiceController.addService);

router.get("/allAssociationServices/:id",[checkJwt], AssociationIndServiceController.allAssociationServices);
router.post("/delete",[checkJwt], AssociationIndServiceController.delete);


export default router;