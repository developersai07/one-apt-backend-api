import { Router } from 'express';
import CitiesController from '../Controllers/CitiesController';

const router = Router();
//Login route
router.get("/getAll", CitiesController.citiesgetall);
router.get("/getCities/:id", CitiesController.getCities);


export default router;