import { Router } from "express";
import VendorindServiceTypeController from "../Controllers/indServiceTypeController";
import { checkJwt } from "../middlewares/checkJwt";

const router = Router();
//Login route
router.post("/add",VendorindServiceTypeController.addIndServiceType);
router.get("/getServiceTypes/:id",VendorindServiceTypeController.getServiceTypes);
router.post("/update", [checkJwt], VendorindServiceTypeController.updateIndServiceType);

router.post("/delete", [checkJwt], VendorindServiceTypeController.deleteIndServiceType);


export default router;