import { Router } from "express";
import FlatTenantController from "../Controllers/FlatTenantController";
import { checkJwt } from "../middlewares/checkJwt";
const path = require('path');

const router = Router();

router.post("/add", [checkJwt], FlatTenantController.tenantAdd,
    function (req, res, next) {
        console.log(req.body.id);
        console.log("File: ");
        // console.log(req.files);

        if(req.files)
        {
            if (req.files.tenant_pic) {
                var ext = req.files.tenant_pic.name || ''.split('.');
                let imageFile = req.files.tenant_pic;
                var ext = req.files.tenant_pic.name.substr(req.files.tenant_pic.name.lastIndexOf('.') + 1);
                let flatTenantImage = req.body.id + '.' + ext + '';
                if (flatTenantImage) {
                    imageFile.mv("./uploads/flattenantImage/" + flatTenantImage + "", function (err) {
                        if (err) {
                            return res.status(500).send(err);
                        } else {
                            req.body.tenant_pic_name = flatTenantImage;
                            console.log(req.body.tenant_pic_name);
                            next();
                        }
                    });
                }
            } else {
                next();
            }

        }
        else {
            res.send(req.body.data)
        }
       
    }, FlatTenantController.flatTenantImage);

router.get("/flatTenantImage/:id", function (req, res) {
    var fs = require("fs");
    var fileLocation = "../../uploads/flatTenantImage/" + req.params.id + "";

    const dirPath = path.join(__dirname, '../../uploads/flatTenantImage/' + req.params.id + '');
    fs.readFile(fileLocation, function (error, data) {
        if (error) {
            console.log(error);
        }
        res.sendFile(dirPath);
    });
});

router.get("/getAll/:id", [checkJwt], FlatTenantController.flatTenantgetAll);
router.post("/update", [checkJwt], FlatTenantController.tenantUpdate,
function (req, res, next) {
    if(req.files)
    {
        if (req.files.tenant_pic) {
            var ext = req.files.tenant_pic.name || ''.split('.');
            let imageFile = req.files.tenant_pic;
            var ext = req.files.tenant_pic.name.substr(req.files.tenant_pic.name.lastIndexOf('.') + 1);
            let flatTenantImage = req.body.id + '.' + ext + '';
            if (flatTenantImage) {
                imageFile.mv("./uploads/flattenantImage/" + flatTenantImage + "", function (err) {
                    if (err) {
                        return res.status(500).send(err);
                    } else {
                        req.body.tenant_pic_name = flatTenantImage;
                        console.log(req.body.tenant_pic_name);
                        next();
                    }
                });
            }
        } else {
            next();
        }

    }
    else {
        res.send(req.body.data)
    }
   
}, FlatTenantController.flatTenantImage);
router.post("/delete", [checkJwt], FlatTenantController.flatTenantDelete);
router.get("/getById/:id", [checkJwt], FlatTenantController.flatTenantgetById);

router.post("/appUpdate", [checkJwt], FlatTenantController.tenantAppUpdate,
function (req, res, next) {
    if(req.files)
    {
        if (req.files.tenant_pic) {
            var ext = req.files.tenant_pic.name || ''.split('.');
            let imageFile = req.files.tenant_pic;
            var ext = req.files.tenant_pic.name.substr(req.files.tenant_pic.name.lastIndexOf('.') + 1);
            let flatTenantImage = req.body.id + '.' + ext + '';
            if (flatTenantImage) {
                imageFile.mv("./uploads/flattenantImage/" + flatTenantImage + "", function (err) {
                    if (err) {
                        return res.status(500).send(err);
                    } else {
                        req.body.tenant_pic_name = flatTenantImage;
                        console.log(req.body.tenant_pic_name);
                        next();
                    }
                });
            }
        } else {
            next();
        }

    }
    else {
        res.send(req.body.data)
    }
   
}, FlatTenantController.flatTenantImage);


export default router;