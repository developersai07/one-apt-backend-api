import { Router } from "express";
import PlansController from "../Controllers/PlansController";
import { checkJwt } from "../middlewares/checkJwt";
//import { checkJwt } from "../middlewares/checkJwt";

const router = Router();
//Login route
router.get("/getAll", PlansController.plansgetall);
router.post("/update",[checkJwt], PlansController.plansUpdate);
router.post("/delete",[checkJwt], PlansController.plansDelete);
router.post("/add",[checkJwt], PlansController.plansAdd);
router.get("/getNonFree",[checkJwt], PlansController.getNonFree);

export default router;