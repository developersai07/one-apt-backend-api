import { Router } from "express";
import indServcePlanController from "../Controllers/indServicePlansController";
import { checkJwt } from "../middlewares/checkJwt";
//import { checkJwt } from "../middlewares/checkJwt";

const router = Router();
//Login route
router.post("/add", indServcePlanController.addIndServicePlan);
router.get("/getAll", indServcePlanController.getAll);
// router.get("/getindServicePlans/:id", [checkJwt], indServcePlanController.getindServicePlans);
router.get("/getAllIndServicePlans", [checkJwt], indServcePlanController.getAllIndServicePlans);
router.post("/updateIndServicePlans", [checkJwt], indServcePlanController.updateIndServicePlans);
router.post("/deleteIndServicePlans", [checkJwt], indServcePlanController.deleteIndServicePlans);
router.get("/getPlansByService/:serviceId", indServcePlanController.getPlansByService);

export default router;