
import IndividualTicketController from "../Controllers/IndividualTicketController";
var express = require('express');
var router = express.Router();
const path = require('path');

router.post("/addTicket", IndividualTicketController.addTicket, function (req, res, next) {

  if(req.files){
    if (req.files.IndticketImage) {
      var ext = req.files.IndticketImage.name || ''.split('.');
      let sampleFile = req.files.IndticketImage;
      var ext = req.files.IndticketImage.name.substr(req.files.IndticketImage.name.lastIndexOf('.') + 1);
      let indTicketImg = req.body.indTicketId + '.' + ext + '';
      if (indTicketImg) {
        sampleFile.mv("./uploads/indTicketImg/" + indTicketImg + "", function (err) {
          if (err) {
            return res.status(500).send(err);
          }
          else {
            req.body.indTicketImg = indTicketImg;
            next();
          }
  
        },
  
  
  
        );
  
  
      }
  
    }
    else {
      next();
    }
  }
  else {
    next();
  }

},
  IndividualTicketController.updateFileName,
  IndividualTicketController.checkPrimaryInternalVendor,
  IndividualTicketController.checkSecondInternalVendor,
  IndividualTicketController.checkThirdInternalVendor,
  IndividualTicketController.checkFourthInternalVendor,
  IndividualTicketController.checkFifthInternalVendor,
  IndividualTicketController.checkFirstPriorThirdPartyVendor,
  IndividualTicketController.checkSecondPriorThirdPartyVendor,
  IndividualTicketController.checkThirdPriorThirdPartyVendor,
  IndividualTicketController.checkFourthPriorThirdPartyVendor,
  IndividualTicketController.checkFifthPriorThirdPartyVendor,
  //IndividualTicketController.checkInternalVendors,IndividualTicketController.checkThirdPartyVendors
  //IndividualTicketController.indTicketAssignSecondaryVendor,
  //IndividualTicketController.indTicketAssignTeritaryVendor,
);


router.get("/indImage/:id", function (req, res) {
  var fs = require("fs");
  var fileLocation = "../../uploads/indTicketImg/" + req.params.id + "";

  const dirPath = path.join(__dirname, '../../uploads/indTicketImg/' + req.params.id + '');
  fs.readFile(fileLocation, function (error, data) {
    if (error) {
      console.log(error);
    }
    res.sendFile(dirPath);
  });
})

router.get('/vendorIndividualTickets/:id', IndividualTicketController.vendorIndividualTickets);
router.get('/vendorIndividualTicketsCompleted/:id', IndividualTicketController.vendorIndividualTicketsCompleted);
router.get('/vendorIndividualTicketsPendingCompleted/:id', IndividualTicketController.vendorIndividualTicketsPendingCompleted);
router.get('/getindTicketsByMonth/:id/:year', IndividualTicketController.getindTicketsByMonth);

router.get('/getindTicketsByMonthVendorAssociation/:id/:year', IndividualTicketController.getindTicketsByMonthVendorAssociation);

router.get('/getindTicketsByMonthAll/:year', IndividualTicketController.getindTicketsByMonthAll);

router.get('/getindTicketsByAssociation/:id', IndividualTicketController.getindTicketsByAssociation);

router.get('/appUserIndividualTickets/:id', IndividualTicketController.appUserIndividualTickets);
router.get('/vendorIndActionAccept/:id/:vendorID', IndividualTicketController.vendorIndActionAccept);
router.get('/vendorIndActionReject/:id/:vendorID', IndividualTicketController.vendorIndActionReject);
router.post('/vendorIndActionRejectAutoAdding', IndividualTicketController.vendorIndActionReject, IndividualTicketController.checkPrimaryInternalVendor,
  IndividualTicketController.checkSecondInternalVendor,
  IndividualTicketController.checkThirdInternalVendor,
  IndividualTicketController.checkFourthInternalVendor,
  IndividualTicketController.checkFifthInternalVendor,
  IndividualTicketController.checkFirstPriorThirdPartyVendor,
  IndividualTicketController.checkSecondPriorThirdPartyVendor,
  IndividualTicketController.checkThirdPriorThirdPartyVendor,
  IndividualTicketController.checkFourthPriorThirdPartyVendor,
  IndividualTicketController.checkFifthPriorThirdPartyVendor);
router.post('/indTicketsOtpVerify', IndividualTicketController.indTicketsOtpVerify);
router.post('/vendorWorkCompleted', IndividualTicketController.vendorWorkCompleted);
router.post('/ownerWorkCompleted', IndividualTicketController.ownerWorkCompleted);
router.post('/checkInternal_ExtVendors', IndividualTicketController.checkInternalVendors, IndividualTicketController.checkThirdPartyVendors);
router.post('/test', IndividualTicketController.test);
router.post('/resendOtp', IndividualTicketController.resendOtp);
router.get("/getIndivdulaTIcketByVendorAssociation/:id", IndividualTicketController.getIndivdulaTIcketByVendorAssociation)

router.get("/getTicketDetails", IndividualTicketController.getTicketDetails);
router.get("/mailTest", IndividualTicketController.mailTest);
export default router;