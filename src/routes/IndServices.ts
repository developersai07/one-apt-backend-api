import { Request, Response, NextFunction } from "express";
import { Router } from "express";
import IndividualServiceController from "../Controllers/IndividualServiceController";
import { checkJwt } from "../middlewares/checkJwt";
const path = require('path');
//import { checkJwt } from "../middlewares/checkJwt";
const router = Router();
router.post("/associationEnrollemnt2", [checkJwt], function (req, res, next) {
  console.log(req.files);
  res.send("ok");
})
//Login route
router.post("/addService", [checkJwt], IndividualServiceController.addService,
  function (req, res, next) {
  console.log("Files: ");
  console.log(req.files);
    if (req.files.ind_icon) {
      var ext = req.files.ind_icon.name || ''.split('.');
      let sampleFile = req.files.ind_icon;
      var ext = req.files.ind_icon.name.substr(req.files.ind_icon.name.lastIndexOf('.') + 1);
      let indServiceIcon = req.body.indServiceId + '.' + ext + '';
      if (indServiceIcon) {
        sampleFile.mv("./uploads/indServiceIcon/" + indServiceIcon + "", function (err) {
          if (err) {
            return res.status(500).send(err);
          } else {
            req.body.ind_icon_name = indServiceIcon;
            next();
          }
        });
      }
    } else {
      next();
    }
  },

  function (req, res, next) {

    if (req.files.ind_img) {
      var ext = req.files.ind_img.name || ''.split('.');
      let sampleFile = req.files.ind_img;
      var ext = req.files.ind_img.name.substr(req.files.ind_img.name.lastIndexOf('.') + 1);
      let indServiceImage = req.body.indServiceId + '.' + ext + '';
      if (indServiceImage) {
        sampleFile.mv("./uploads/indServiceImage/" + indServiceImage + "", function (err) {
          if (err) {
            return res.status(500).send(err);
          } else {
            req.body.ind_image_name = indServiceImage;
            next();
          }
        });
      }
    } else {
      next();
    }
  },
  IndividualServiceController.updateFileName,
);
router.get("/allIndService", [checkJwt], IndividualServiceController.getAllService);

router.get("/indIcon/:id", function (req, res) {
  var fs = require("fs");
  var fileLocation = "../../uploads/indServiceIcon/" + req.params.id + "";

  const dirPath = path.join(__dirname, '../../uploads/indServiceIcon/' + req.params.id + '');
  fs.readFile(fileLocation, function (error, data) {
    if (error) {
      console.log(error);
    }

    res.sendFile(dirPath);
  });
});


router.get("/indServiceImage/:id", function (req, res) {
  var fs = require("fs");
  var fileLocation = "../../uploads/indServiceImage/" + req.params.id + "";

  const dirPath = path.join(__dirname, '../../uploads/indServiceImage/' + req.params.id + '');


  fs.readFile(fileLocation, function (error, data) {
    if (error) {
      console.log(error);
    }
    res.sendFile(dirPath);
  });
})
router.post("/updateService", [checkJwt], IndividualServiceController.updateService,
  function (req, res, next) {
if(req.files){
  if (req.files.ind_icon) {
    var ext = req.files.ind_icon.name || ''.split('.');
    let sampleFile = req.files.ind_icon;
    var ext = req.files.ind_icon.name.substr(req.files.ind_icon.name.lastIndexOf('.') + 1);
    let indServiceIcon = req.body.indServiceId + '.' + ext + '';
    if (indServiceIcon) {
      console.log("Oksakddjda" + indServiceIcon);
      sampleFile.mv("./uploads/indServiceIcon/" + indServiceIcon + "", function (err) {
        if (err) {
          return res.status(500).send(err);
        } else {
          req.body.ind_icon_name = indServiceIcon;
          next();
        }
      });
    }
  } else {
    next();
  }
}
else {
  next();
}
 
  },

  function (req, res, next) {

    if(req.files){
      if (req.files.ind_img) {
        var ext = req.files.ind_img.name || ''.split('.');
        let sampleFile = req.files.ind_img;
        var ext = req.files.ind_img.name.substr(req.files.ind_img.name.lastIndexOf('.') + 1);
        let indServiceImage = req.body.indServiceId + '.' + ext + '';
        if (indServiceImage) {
          sampleFile.mv("./uploads/indServiceImage/" + indServiceImage + "", function (err) {
            if (err) {
              return res.status(500).send(err);
            } else {
              req.body.ind_image_name = indServiceImage;
              next();
            }
          });
        }
      } else {
        next();
      }


    }
    else {
      next();
    }


  },
  IndividualServiceController.updateFileName,
);
router.post("/delete", [checkJwt],IndividualServiceController.deleteService);
router.get("/plans", [checkJwt],IndividualServiceController.indServicePlans);
router.post("/addSub",[checkJwt], IndividualServiceController.addindServicePlans);
router.post("/updateSub",[checkJwt], IndividualServiceController.updateindServiceSubPlans);
router.get("/getSub/:id", [checkJwt],IndividualServiceController.getindServiceSubPlans);
router.get("/getServiceSub/:id",IndividualServiceController.getindServiceParamSubPlans);
router.get("/getService/:id",IndividualServiceController.getService);
router.post("/deleteSub",[checkJwt], IndividualServiceController.deleteSub);
router.get("/allAppIndService",[checkJwt], IndividualServiceController.getAllIndService);
router.get("/allAppIndService/:id",[checkJwt], IndividualServiceController.getAllIndServiceAssociation);
router.get("/searchService/:id",[checkJwt], IndividualServiceController.searchService);
router.get("/associationVendorByServiceInternal/:associationId/:serviceId", IndividualServiceController.associationVendorByServiceInternal);
router.get("/associationVendorByServiceThirdparty/:associationId/:serviceId", IndividualServiceController.associationVendorByServiceThirdparty);

router.post("/associationVendorPriorityByService", IndividualServiceController.associationVendorPriorityByService);
router.post("/associationVendorPriorityByServiceExternal", IndividualServiceController.associationVendorPriorityByServiceExternal);




export default router;