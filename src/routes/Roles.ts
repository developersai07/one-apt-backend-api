import { Router } from 'express';
import RolesController from '../Controllers/RolesController';


const router = Router();

router.get("/getAll", RolesController.rolesgetall);

export default router;