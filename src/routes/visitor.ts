import { Router } from 'express';
import VendorvisitorCheckInController from '../Controllers/VendorvisitorCheckInController';

import { checkJwt } from "../middlewares/checkJwt";
const router = Router();
const path = require('path');
router.post("/vendor/checkIn",[checkJwt], VendorvisitorCheckInController.checkIn,  function (req, res, next) {
    if (req.files.vv_pic) {
      var ext = req.files.vv_pic.name || ''.split('.');
      let sampleFile = req.files.vv_pic;
      var ext = req.files.vv_pic.name.substr(req.files.vv_pic.name.lastIndexOf('.') + 1);
      let vvCheckin = req.body.data.VENDOR_CHECK_IND_ID + '.' + ext + '';
      if (vvCheckin) {
        sampleFile.mv("./uploads/visitors/vendors/" + vvCheckin + "", function (err) {
          if (err) {
            return res.status(500).send(err);
          }
          else {
            req.body.vvImage = vvCheckin;
            next();
          }
  
        },
  
  
  
        );
  
  
      }
  
    }
    else {
    res.send(req.body.data)
    }
  },  VendorvisitorCheckInController.updateFileName,);

  router.get("/vendor/vendorCheckedIn/:id",[checkJwt], VendorvisitorCheckInController.vendorCheckedIn)
  router.get("/vendor/vendorCheckOut/:id",[checkJwt], VendorvisitorCheckInController.vendorCheckOut)
  router.get("/vendorPic/:id", function (req, res) {
    var fs = require("fs");
    var fileLocation = "../../uploads/visitors/vendors/" + req.params.id + "";
  
    const dirPath = path.join(__dirname, '../../uploads/visitors/vendors/' + req.params.id + '');
    fs.readFile(fileLocation, function (error, data) {
      if (error) {
        console.log(error);
      }
      res.sendFile(dirPath);
    });
  })
  

export default router;