import { Router } from "express";
import { checkJwt } from "../MiddleWares/checkJwt";
import AdvertisementController from "../Controllers/AdvertisementController";
const path = require('path');
const router = Router();

router.post("/addAdvertisement", [checkJwt], AdvertisementController.addAdvertisement,
    function (req, res, next) {
        console.log(req.body.advId);
        if (req.files.adv_image) {
            var ext = req.files.adv_image.name || ''.split('.');
            let sampleFile = req.files.adv_image;
            var ext = req.files.adv_image.name.substr(req.files.adv_image.name.lastIndexOf('.') + 1);
            let advertisementImage = req.body.advId + '.' + ext + '';
            if (advertisementImage) {
                sampleFile.mv("./uploads/advertisementImage/" + advertisementImage + "", function (err) {
                    if (err) {
                        return res.status(500).send(err);
                    } else {
                        req.body.adv_image_name = advertisementImage;
                        next();
                    }
                });
            }
        } else {
            next();
        }
    },
    AdvertisementController.updateFileName,
);

router.get("/advImage/:id", function (req, res) {
    var fs = require("fs");
    var fileLocation = "../../uploads/advertisementImage/" + req.params.id + "";

    const dirPath = path.join(__dirname, '../../uploads/advertisementImage/' + req.params.id + '');
    fs.readFile(fileLocation, function (error, data) {
        if (error) {
            console.log(error);
        }
        res.sendFile(dirPath);
    });
})

router.get("/allAdvertisement", [checkJwt], AdvertisementController.getAllAdvertisement);

router.get("/getAppAllAdvertisement", [checkJwt], AdvertisementController.getAppAllAdvertisement);

router.post("/update", [checkJwt], AdvertisementController.updateAdvertisement,
    function (req, res, next) {

        if (req.files) {
            var ext = req.files.adv_image.name || ''.split('.');
            let sampleFile = req.files.adv_image;
            var ext = req.files.adv_image.name.substr(req.files.adv_image.name.lastIndexOf('.') + 1);
            let advertisementImage = req.body.advId + '.' + ext + '';
            if (advertisementImage) {
                sampleFile.mv("./uploads/advertisementImage/" + advertisementImage + "", function (err) {
                    if (err) {
                        return res.status(500).send(err);
                    } else {
                        req.body.adv_image_name = advertisementImage;
                        next();
                    }
                });
            }
        } else {
            next();
        }
    },
    AdvertisementController.updateFileName,
);

router.post("/delete", [checkJwt], AdvertisementController.deleteAdvertisement);

export default router;