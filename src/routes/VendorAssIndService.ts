import { Router } from "express";
import VendorAssociationIndServiceController  from "../Controllers/VendorAssociationIndServiceController";
import { checkJwt } from "../middlewares/checkJwt";
//import { checkJwt } from "../middlewares/checkJwt";
const router = Router();
//Login route
router.post("/addService",[checkJwt], VendorAssociationIndServiceController.addService);
router.get("/allAssociationServices/:id",[checkJwt], VendorAssociationIndServiceController.allAssociationServices);
router.post("/delete",[checkJwt], VendorAssociationIndServiceController.delete);
//getVendorAssServices

export default router;