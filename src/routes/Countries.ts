import { Router } from "express";
import CountriesController from "../Controllers/CountriesController";
import { checkJwt } from "../middlewares/checkJwt";
//import { checkJwt } from "../middlewares/checkJwt";

const router = Router();
//Login route
router.get("/getAll", CountriesController.countriesgetall);

export default router;