import { Router } from "express";
import CommonAreaServiceController from "../Controllers/CommonAreaServiceController";
import { checkJwt } from "../middlewares/checkJwt";
//import { checkJwt } from "../middlewares/checkJwt";
const path = require('path');
const router = Router();
//Login route
router.post("/addService", [checkJwt], CommonAreaServiceController.addService,
  function (req, res, next) {
    if (req.files.ind_icon) {
      console.log("ojasd");
      console.log(req.files.ind_icon);
      var ext = req.files.ind_icon.name || ''.split('.');
      let sampleFile = req.files.ind_icon;
      var ext = req.files.ind_icon.name.substr(req.files.ind_icon.name.lastIndexOf('.') + 1);
      let comServiceIcon = req.body.comServiceId + '.' + ext + '';

      console.log(comServiceIcon);
      if (comServiceIcon) {
        sampleFile.mv("./uploads/comServiceIcon/" + comServiceIcon + "", function (err) {
          if (err) {
            return res.status(500).send(err);
          } else {
            req.body.com_icon_name = comServiceIcon;
            next();
          }
        });
      }
    } else {
      next();
    }
  },

  function (req, res, next) {

    if (req.files.ind_img) {
      var ext = req.files.ind_img.name || ''.split('.');
      let sampleFile = req.files.ind_img;
      var ext = req.files.ind_img.name.substr(req.files.ind_img.name.lastIndexOf('.') + 1);
      let comServiceImage = req.body.comServiceId + '.' + ext + '';
      if (comServiceImage) {
        sampleFile.mv("./uploads/comServiceImage/" + comServiceImage + "", function (err) {
          if (err) {
            return res.status(500).send(err);
          } else {
            req.body.com_image_name = comServiceImage;
            next();
          }
        });
      }
    } else {
      next();
    }
  },
  CommonAreaServiceController.updateFileName,
);


router.get("/comImage/:id", function (req, res) {
  var fs = require("fs");
  var fileLocation = "../../uploads/comServiceImage/" + req.params.id + "";

  const dirPath = path.join(__dirname, '../../uploads/comServiceImage/' + req.params.id + '');


  fs.readFile(fileLocation, function (error, data) {
    if (error) {
      console.log(error);
    }

    res.sendFile(dirPath);
  });
})



router.get("/comIcon/:id", function (req, res) {
  var fs = require("fs");
  var fileLocation = "../../uploads/comServiceIcon/" + req.params.id + "";

  const dirPath = path.join(__dirname, '../../uploads/comServiceIcon/' + req.params.id + '');


  fs.readFile(fileLocation, function (error, data) {
    if (error) {
      console.log(error);
    }

    res.sendFile(dirPath);
  });
})


//Login route
router.get("/allComService", [checkJwt], CommonAreaServiceController.getAllService);
router.post("/update", [checkJwt], CommonAreaServiceController.updateService,

  function (req, res, next) {

    if (req.files) {
      var ext = req.files.ind_icon.name || ''.split('.');
      let sampleFile = req.files.ind_icon;
      var ext = req.files.ind_icon.name.substr(req.files.ind_icon.name.lastIndexOf('.') + 1);
      let comServiceIcon = req.body.comServiceId + '.' + ext + '';
      if (comServiceIcon) {
        console.log("Oksakddjda" + comServiceIcon);
        sampleFile.mv("./uploads/comServiceIcon/" + comServiceIcon + "", function (err) {
          if (err) {
            return res.status(500).send(err);
          } else {
            req.body.com_icon_name = comServiceIcon;
            next();
          }
        });
      }
    } else {
      next();
    }
  },

  function (req, res, next) {

    if (req.files) {
      var ext = req.files.ind_img.name || ''.split('.');
      let sampleFile = req.files.ind_img;
      var ext = req.files.ind_img.name.substr(req.files.ind_img.name.lastIndexOf('.') + 1);
      let comServiceImage = req.body.comServiceId + '.' + ext + '';
      if (comServiceImage) {
        sampleFile.mv("./uploads/comServiceImage/" + comServiceImage + "", function (err) {
          if (err) {
            return res.status(500).send(err);
          } else {
            req.body.com_image_name = comServiceImage;
            next();
          }
        });
      }
    } else {
      next();
    }
  },
  CommonAreaServiceController.updateFileName,
);
router.post("/delete",[checkJwt], CommonAreaServiceController.deleteService);
router.get("/allAppComService",[checkJwt],CommonAreaServiceController.getAppAllService);
router.get("/allAppComService/:id",[checkJwt],CommonAreaServiceController.getAppAllServiceAssociation);
router.get("/getComServiceById/:id",[checkJwt],CommonAreaServiceController.getComServiceById);

router.get("/searchService/:id",[checkJwt], CommonAreaServiceController.searchService);

export default router;