import { Router } from "express";
import FamilyRelationsController from "../Controllers/FamilyRelationsController";
import { checkJwt } from "../middlewares/checkJwt";

const router = Router();


router.post("/addRelations", [checkJwt], FamilyRelationsController.addRelations);
router.post("/updateRelations", [checkJwt], FamilyRelationsController.updateRelations);
router.post("/addRelation",  FamilyRelationsController.addRelations);
router.post("/updateRelation",  FamilyRelationsController.updateRelations);
router.get("/familyRelationAppGetAll", [checkJwt], FamilyRelationsController.familyRelationAppGetAll);



export default router;


