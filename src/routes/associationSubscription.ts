import { Router } from "express";
import AssociationPlansController  from "../Controllers/AssociationPlansController";
import { checkJwt } from "../middlewares/checkJwt";
//import { checkJwt } from "../middlewares/checkJwt";
const router = Router();
//Login route
router.post("/checkAppartmentAssociation", AssociationPlansController.associationCheckPlan);
router.post("/checkVendorAssociation", AssociationPlansController.vassociationCheckPlan);
router.post("/subAssociatinPlan", AssociationPlansController.subAssociatinPlan);

router.post("/subVendorAssociatinPlan", AssociationPlansController.subVendorAssociatinPlan);


export default router;