import { Router } from "express";
import comTicketAddtionalAmountController from "../Controllers/comTicketAddtionalAmountController";
import { checkJwt } from "../middlewares/checkJwt";
//import { checkJwt } from "../middlewares/checkJwt";

const router = Router();

router.post("/add", comTicketAddtionalAmountController.addTicketAmount);


export default router;