import { Router } from "express";
import AppartmentChildEntityController from "../Controllers/AppartmentChildEntityController";
import { checkJwt } from "../middlewares/checkJwt";
//import { checkJwt } from "../middlewares/checkJwt";

const router = Router();

router.post("/add", AppartmentChildEntityController.childEntityAdd);
router.get("/getAll", AppartmentChildEntityController.childEntitygetAll);
router.post("/update", AppartmentChildEntityController.childEntityUpdate);
router.post("/delete", AppartmentChildEntityController.childEntityDelete);
router.post("/delete", AppartmentChildEntityController.childEntityDelete);
router.get("/getByAppartment/:id", AppartmentChildEntityController.getByAppartment);

export default router;