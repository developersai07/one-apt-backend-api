import { Router } from "express";
import indTicketAddtionalAmountController from "../Controllers/indTicketAddtionalAmountController";
import { checkJwt } from "../middlewares/checkJwt";
//import { checkJwt } from "../middlewares/checkJwt";

const router = Router();

router.post("/add", indTicketAddtionalAmountController.addTicketAmount);


export default router;