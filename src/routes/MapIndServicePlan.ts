import { Router } from "express";
import { checkJwt } from "../middlewares/checkJwt";
import MapIndServicePlanController from "../Controllers/MapIndServicePlanController";

const router = Router();

router.get("/getindServicePlans/:id", [checkJwt], MapIndServicePlanController.getindServicePlans);

router.post("/addNewIndServicePlans", [checkJwt], MapIndServicePlanController.addNewIndServicePlans);

router.post("/deleteIndServicePlan", [checkJwt], MapIndServicePlanController.delete);

export default router;