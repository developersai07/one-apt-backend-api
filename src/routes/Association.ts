import AssociationController from "../Controllers/AssociationController";
import { checkJwt } from "../middlewares/checkJwt";
var multer = require('multer');
var upload = multer({ dest: 'uploads/' });
var type = upload.single('image23');

//import { checkJwt } from "../middlewares/checkJwt";

var express = require('express');
var router = express.Router();
//Login route

router.post("/associationEnrollemnt2", function (req, res, next) {
  console.log(req.files);
  res.send("ok");
})
router.post("/associationEnrollemnt", AssociationController.associationEnrollment,
  function (req, res, next) {

    if(req.files)
    {
      if (req.files.appartmentRegistrationFile) {
        var ext = req.files.appartmentRegistrationFile.name || ''.split('.');
        let sampleFile = req.files.appartmentRegistrationFile;
        let appartementRegistrationFile = req.files.appartmentRegistrationFile.name.e;
        var ext = req.files.appartmentRegistrationFile.name.substr(req.files.appartmentRegistrationFile.name.lastIndexOf('.') + 1);
        let appartmentFileName = req.body.app_registration_number + '-' + req.body.obj.id + '.' + ext + '';
        if (appartmentFileName) {
          console.log("Oksakddjda" + appartmentFileName);
          sampleFile.mv("./uploads/appartmentRegistrationFile/" + appartmentFileName + "", function (err) {
            if (err) {
              return res.status(500).send(err);
            }
            else {
              req.body.appartment_file_loc = appartmentFileName;
              next();
            }
  
          },
  
  
  
          );
        }
  
      }
      else {
        next();
      }

    }
    else {
      next()
    }
 
  },
  AssociationController.updateAppartmentFileLoc,

  function (req, res, next) {

    if(req.files)
    {
      if (req.files.associationRegistrationFile) {

        console.log("Id" + req.body.data.id);
        var ext = req.files.associationRegistrationFile.name || ''.split('.');
        let sampleFile = req.files.associationRegistrationFile;
        var ext = req.files.associationRegistrationFile.name.substr(req.files.associationRegistrationFile.name.lastIndexOf('.') + 1);
        let associationFileFileName = req.body.ass_registraion_number + '-' + req.body.data.id + '.' + ext + '';
        if (associationFileFileName) {
          sampleFile.mv("./uploads/associationRegistrationFile/" + associationFileFileName + "", function (err) {
            if (err) {
              return res.status(500).send(err);
            }
            else {
              req.body.association_file_loc = associationFileFileName;
              next();
            }
  
          },
  
  
  
          );
        }
  
      }
      else {
        res.status(200).json({
          obj: req.body.obj,
          data: req.body.newdata,
          userDetaild: req.body.data,
          roleObj: req.body.roleObj,
          assObj: req.body.assObj
        })
      }


    }
    else {
      res.status(200).json({
        obj: req.body.obj,
        data: req.body.newdata,
        userDetaild: req.body.data,
        roleObj: req.body.roleObj,
        assObj: req.body.assObj
      })
    }

 


    // Use the mv() method to place the file somewhere on your server

  },

  AssociationController.updateassociationFileLoc,

);

router.post("/updateAssociationEnrollment", [checkJwt], AssociationController.updateAssociationEnrollment);

// router.post('/upload', 
//   function (req,res) { 
//    // console.log(req.body);
//     console.log(req.files);
//     res.send("ok");
//   });
router.post("/added", AssociationController.added, function (req, res, next) {
  if (!req.files || Object.keys(req.files).length === 0) {
    return res.status(400).send('No files were uploaded.');
  }

  // The name of the input field (i.e. "sampleFile") is used to retrieve the uploaded file
  let sampleFile = req.files.sampleFile;

  var ext = req.files.sampleFile.name || ''.split('.');
  console.log(ext + "ok");
  var ext2 = req.files.sampleFile.name.substr(req.files.sampleFile.name.lastIndexOf('.') + 1);
  console.log(ext2 + "ok2");
  // Use the mv() method to place the file somewhere on your server
  sampleFile.mv("./uploads/filename.jpg", function (err) {
    if (err)
      return res.status(500).send(err);
    next()

    // res.send('File uploaded!');
  },



  );
},
  AssociationController.updateFileName,


);


router.get("/associationInfo/:id", [checkJwt], AssociationController.associationInfo);
//router.get("/associationGrandChildEntites/:id", [checkJwt], AssociationController.associationGrandChildEntites);

router.get("/associationInformation/:id", [checkJwt], AssociationController.associationInformation);

router.get("/associationByZipCode/:zipcode", AssociationController.associationByZipCode);
router.get("/getAssociationData", AssociationController.getAssociationData);

export default router;