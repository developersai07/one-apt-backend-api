
var nodemailer = require('nodemailer');
var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: 'sharukh.smart.9@gmail.com',
      pass: '9885510085Sharu$1994'
    }
  });
  
class FlatTenantSendMail {
    static flatTenantEnrollmentSendMail = async (name,username,password,email) => {
        var mailOptions = {
            from: 'sharukh.smart.9@gmail.com',
            to: email,
            subject: 'OneApt FlatTenant Login Credentials',
            // text: 'UserName : '+username+' and password:'+password+''
            html: '<table width="100%" style="background:#f7f7f7; border:none; padding:30px;"> <tr> <td style="text-align:center; padding-bottom:30px;"> <img src="./uploads/oneapt_logo.png" style="max-width:200px;"> </td> </tr> <tr> <td> <table width="90%" style="background:#fff; border:1px solid #f7f7f7; padding:20px;" cellpadding="0" cellspacing="0" align="center"> <tr> <td> <p>Dear ' + name + ',</p> <p>Greetings from OneAPT!</p> <br> <p> Please Use this username : <b> '+ username +' </b> and  password   <b> ' + password + ' </b> to login </p> <p> For any questions, please contact us at <a href="mailto:sharukh.smart.9@gmail.com" style="color:#53C5F2">customercare@oneapt.com </a> </p> <br> <p>Thank You,</p> <p> Team OneAPT. </p> </td> </tr> </table> </td> </tr> </table>'
          };
          
        transporter.sendMail(mailOptions, function(error, info){
            if (error) {
              console.log(error);
            } else {
              console.log('Email sent: ' + info.response);
            }
          });

    }

  static flatTenantEnrollmentUserNameSendMail = async (name,username, email) => {
    var mailOptions = {
      from: 'sharukh.smart.9@gmail.com',
      to: email,
      subject: 'OneApt FlatTenant UserName',
      // text: 'UserName : '+username+' and password:'+password+'',
      html: '<table width="100%" style="background:#f7f7f7; border:none; padding:30px;"> <tr> <td style="text-align:center; padding-bottom:30px;"> <img src="./uploads/oneapt_logo.png" style="max-width:200px;"> </td> </tr> <tr> <td> <table width="90%" style="background:#fff; border:1px solid #f7f7f7; padding:20px;" cellpadding="0" cellspacing="0" align="center"> <tr> <td> <p>Dear ' + name + ',</p> <p>Greetings from OneAPT!</p> <br> <p> Please Use This UserName to login <b> ' + username + ' </b> </p> <p> For any questions, please contact us at <a href="mailto:sharukh.smart.9@gmail.com" style="color:#53C5F2">customercare@oneapt.com </a> </p> <br> <p>Thank You,</p> <p> Team OneAPT. </p> </td> </tr> </table> </td> </tr> </table>'
    };

    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        console.log(error);
      } else {
        console.log('Email sent: ' + info.response);
      }
    });

  }

}

export default FlatTenantSendMail;