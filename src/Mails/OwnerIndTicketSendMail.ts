
var nodemailer = require('nodemailer');

var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: 'sharukh.smart.9@gmail.com',
      pass: '9885510085Sharu$1994'
    }
  });
  
class OwnerIndTicketSendMail {
    static IndTicketSendMail = async (ticketId,ticketDescription,email,ticketDate,ticketTime,serviceName,serviceTypeName,quantity,amount,plan,vendorName) => {
        var mailOptions = {
            from: 'sharukh.smart.9@gmail.com',
            to: email,
            subject: 'OneApt Ind Ticket '+ticketId+'',
            // text: 'TicketId : OneApt-ind-'+ticketId+' and ticketDescription:'+ticketDescription+'ticket Date: '+ticketId+' ticketTIme : '+ticketTime+''
            html:`
            <style>
            #indTicketTable {
              font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
              border-collapse: collapse;
              width: 100%;
            }
            
            #indTicketTable td, #customers th {
              padding: 0px;
            }
            
            
            #indTicketTable th {
              padding-top: 0px;
              padding-bottom: 0px;
              text-align: center;
              color: white;
            }
            
            #indTicketTable td {
              padding: 0; 
              margin: 0;
            }
            
            table, th, td {
              height:40px;
            }
            </style>
            <div style="text-align:center;">
            <img src="../../uploads/appLogo/oneAptlogo.png" style="max-width:200px;">
            </div>
      
            <table width="100%" style="background:#f7f7f7; border:none; padding:30px;"> 
 <table id="indTicketTable">
 <tr style="height:2px;">
     <td>TIcket ID</td>
     <td>  <b> # IND-${ticketId} </b></td>
   </tr>

 <tr>
   <td>Description</td>
   <td> <b> ${ticketDescription} </b> </td>
 </tr>
 <tr>
     <td>Date</td>
     <td> <b> ${ticketDate} </b></td>
   </tr>

   <tr>
       <td>Time</td>
       <td> <b> ${ticketTime} </b></td>
     </tr>
     <tr>
         <td>Service</td>
         <td>  <b>${serviceName} </b></td>
       </tr>


       
       <tr>
           <td>Type</td>
           <td>  <b>  ${serviceTypeName} </b> </td>
         </tr>
           <tr >
               <td>Quantity</td>
               <td>  <b> ${quantity} </b> </td>
             </tr>

             <tr>
                 <td>Plan :</td>
                 <td>  ${plan} </b></td>
               </tr>
     

                 <tr>
                     <td>AMOUNT</td>
                     <td>  <b> ${amount}   </b> </td>
                   </tr>
                   <tr>
                   <td>Vendor</td>
                   <td>  <b> ${vendorName}   </b> </td>
                 </tr>
                   
</table> 


 </table>
          
            `
          };
          
        transporter.sendMail(mailOptions, function(error, info){
            if (error) {
              console.log(error);
            } else {
              console.log('Email sent: ' + info.response);
            }
          });

    }


    static IndTicketRejectSendMail = async (ticketId,ticketDescription,email,ticketDate,ticketTime,serviceName,serviceTypeName,quantity,amount,plan,vendorName) => {
      var mailOptions = {
          from: 'sharukh.smart.9@gmail.com',
          to: email,
          subject: 'OneApt Ind Ticket Reject '+ticketId+'',
          // text: 'TicketId : OneApt-ind-'+ticketId+' and ticketDescription:'+ticketDescription+'ticket Date: '+ticketId+' ticketTIme : '+ticketTime+''
          html:`
          <style>
          #indTicketTable {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
          }
          
          #indTicketTable td, #customers th {
            padding: 0px;
          }
          
          
          #indTicketTable th {
            padding-top: 0px;
            padding-bottom: 0px;
            text-align: center;
            color: white;
          }
          
          #indTicketTable td {
            padding: 0; 
            margin: 0;
          }
          
          table, th, td {
            height:40px;
          }
          </style>
          <div style="text-align:center;">
          <img src="../../uploads/appLogo/oneAptlogo.png" style="max-width:200px;">
          </div>
    
          <table width="100%" style="background:#f7f7f7; border:none; padding:30px;"> 
<table id="indTicketTable">
<tr style="height:2px;">
   <td>TIcket ID</td>
   <td>  <b> # IND-${ticketId} </b></td>
 </tr>

<tr>
 <td>Description</td>
 <td> <b> ${ticketDescription} </b> </td>
</tr>
<tr>
   <td>Date</td>
   <td> <b> ${ticketDate} </b></td>
 </tr>

 <tr>
     <td>Time</td>
     <td> <b> ${ticketTime} </b></td>
   </tr>
   <tr>
       <td>Service</td>
       <td>  <b>${serviceName} </b></td>
     </tr>


     
     <tr>
         <td>Type</td>
         <td>  <b>  ${serviceTypeName} </b> </td>
       </tr>
         <tr >
             <td>Quantity</td>
             <td>  <b> ${quantity} </b> </td>
           </tr>

           <tr>
               <td>Plan :</td>
               <td>  ${plan} </b></td>
             </tr>
   

               <tr>
                   <td>AMOUNT</td>
                   <td>  <b> ${amount}   </b> </td>
                 </tr>
                 <tr>
                 <td>Vendor</td>
                 <td>  <b> ${vendorName}   </b> </td>
               </tr>
                 
</table> 


</table>
        
          `
        };
        
      transporter.sendMail(mailOptions, function(error, info){
          if (error) {
            console.log(error);
          } else {
            console.log('Email sent: ' + info.response);
          }
        });

  }

}

export default OwnerIndTicketSendMail;