
var nodemailer = require('nodemailer');
var transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: 'sharukh.smart.9@gmail.com',
    pass: '9885510085Sharu$1994'
  }
});

class AssociationSendMail {

  static associationIndSendMail = async (ticketId,ticketDescription,email,ticketDate,ticketTime,serviceName,serviceTypeName,quantity,amount,plan,vendorName) => {
    var mailOptions = {
        from: 'sharukh.smart.9@gmail.com',
        to: email,
        subject: 'OneApt Ind Ticket '+ticketId+' - Not Assigned',
        // text: 'TicketId : OneApt-ind-'+ticketId+' and ticketDescription:'+ticketDescription+'ticket Date: '+ticketId+' ticketTIme : '+ticketTime+''
        html:`
        <style>
        #indTicketTable {
          font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
          border-collapse: collapse;
          width: 100%;
        }
        
        #indTicketTable td, #customers th {
          padding: 0px;
        }
        
        
        #indTicketTable th {
          padding-top: 0px;
          padding-bottom: 0px;
          text-align: center;
          color: white;
        }
        
        #indTicketTable td {
          padding: 0; 
          margin: 0;
        }
        
        table, th, td {
          height:40px;
        }
        </style>
        <div style="text-align:center;">
        <img src="../../uploads/appLogo/oneAptlogo.png" style="max-width:200px;">
        </div>
  
        <table width="100%" style="background:#f7f7f7; border:none; padding:30px;"> 
<table id="indTicketTable">
<tr style="height:2px;">
 <td>TIcket ID</td>
 <td>  <b> # IND-${ticketId} </b></td>
</tr>

<tr>
<td>Description</td>
<td> <b> ${ticketDescription} </b> </td>
</tr>
<tr>
 <td>Date</td>
 <td> <b> ${ticketDate} </b></td>
</tr>

<tr>
   <td>Time</td>
   <td> <b> ${ticketTime} </b></td>
 </tr>
 <tr>
     <td>Service</td>
     <td>  <b>${serviceName} </b></td>
   </tr>


   
   <tr>
       <td>Type</td>
       <td>  <b>  ${serviceTypeName} </b> </td>
     </tr>
       <tr >
           <td>Quantity</td>
           <td>  <b> ${quantity} </b> </td>
         </tr>

         <tr>
             <td>Plan :</td>
             <td>  ${plan} </b></td>
           </tr>
 

             <tr>
                 <td>AMOUNT</td>
                 <td>  <b> ${amount}   </b> </td>
               </tr>
               <tr>
               <td>Vendor</td>
               <td>  <b> ${vendorName}   </b> </td>
             </tr>
               
</table> 


</table>
      
        `
      };
      
    transporter.sendMail(mailOptions, function(error, info){
        if (error) {
          console.log(error);
        } else {
          console.log('Email sent: ' + info.response);
        }
      });

}


  static associationEnrollmentSendMail = async (firstName,username, password, email) => {
    var mailOptions = {
      from: 'sharukh.smart.9@gmail.com',
      to: email,
      subject: 'OneApt Association Admin Login Credentials',
      // text: 'UserName : '+username+' and password:'+password+'',
      html: '<table width="100%" style="background:#f7f7f7; border:none; padding:30px;"> <tr> <td style="text-align:center; padding-bottom:30px;"> <img src="./uploads/oneapt_logo.png" style="max-width:200px;"> </td> </tr> <tr> <td> <table width="90%" style="background:#fff; border:1px solid #f7f7f7; padding:20px;" cellpadding="0" cellspacing="0" align="center"> <tr> <td> <p>Dear ' + firstName + ',</p> <p>Greetings from OneAPT!</p> <br> <p> Please Use this username : <b> '+ username +' </b> and  password   <b> ' + password + ' </b> to login </p> <p> For any questions, please contact us at <a href="mailto:sharukh.smart.9@gmail.com" style="color:#53C5F2">customercare@oneapt.com </a> </p> <br> <p>Thank You,</p> <p> Team OneAPT. </p> </td> </tr> </table> </td> </tr> </table>'
    };

    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        console.log(error);
      } else {
        console.log('Email sent: ' + info.response);
      }
    });

  }

  static associationEnrollmentUserNameSendMail = async (username,  email) => {
    var mailOptions = {
      from: 'sharukh.smart.9@gmail.com',
      to: email,
      subject: 'OneApt Association UserName',
      // text: 'UserName : '+username+' and password:'+password+'',
      html: '<table width="100%" style="background:#f7f7f7; border:none; padding:30px;"> <tr> <td style="text-align:center; padding-bottom:30px;"> <img src="./uploads/oneapt_logo.png" style="max-width:200px;"> </td> </tr> <tr> <td> <table width="90%" style="background:#fff; border:1px solid #f7f7f7; padding:20px;" cellpadding="0" cellspacing="0" align="center"> <tr> <td> <p>Dear ' + username + ',</p> <p>Greetings from OneAPT!</p> <br> <p> Please Use This UserName to login <b> ' + username + ' </b> </p> <p> For any questions, please contact us at <a href="mailto:sharukh.smart.9@gmail.com" style="color:#53C5F2">customercare@oneapt.com </a> </p> <br> <p>Thank You,</p> <p> Team OneAPT. </p> </td> </tr> </table> </td> </tr> </table>'
    };

    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        console.log(error);
      } else {
        console.log('Email sent: ' + info.response);
      }
    });

  }

}

export default AssociationSendMail;