import {Entity, PrimaryGeneratedColumn,OneToMany,JoinColumn, Column,ManyToOne, OneToOne,CreateDateColumn,UpdateDateColumn} from "typeorm";
//import {APPUSERS} from '../entity/AppUser';
import {IndividualServiceSubscriptions} from './IndServiceSubscriptions';
import {IndividualAreaServices} from './IndividualAreaServices';
@Entity({name: "INDIVIDUAL_SERVICE_MAP_SUBSCRIPTIONS"})
export class IndividualServiceMapSubscriptions {
    @PrimaryGeneratedColumn()
    MAP_SERVICE_SUBSCRIPTION_ID: number;
    @ManyToOne(type => IndividualServiceSubscriptions, indServiceSubscriptions => indServiceSubscriptions.mapSubs)
    @JoinColumn({ name: "SERVICE_SUBSCRIPTION_ID" })
    SERVICE_SUBSCRIPTION_ID: IndividualServiceSubscriptions;
    @ManyToOne(type => IndividualAreaServices, individualAreaServices => individualAreaServices.mapSub)
    @JoinColumn({ name: "IND_SERVICE_ID" })
    IND_SERVICE_ID: IndividualAreaServices;
    @Column()
    AMOUNT: string;
    @Column()
    IS_ACTIVE: Boolean;
    @Column()
    CREATED_BY: string;
    @Column()
    UPDATED_BY: string;
    @CreateDateColumn() 
    public CREATED_DATE: Date;
  
    @UpdateDateColumn() 
    public UPDATED_DATE: Date;


}