import {Entity, PrimaryGeneratedColumn,OneToMany,JoinColumn, Column,ManyToOne, OneToOne,CreateDateColumn,UpdateDateColumn} from "typeorm";
import {IndividualAreaServices} from '../entity/IndividualAreaServices';
import {IND_SERVICE_PLANS} from '../entity/indServicePlan';
@Entity({name: "MAP_IND_SERVICE_PLANS"})
export class MAP_IND_SERVICE_PLANS {
    @PrimaryGeneratedColumn()
    MAP_IND_SERVICE_PLAN_ID: number;
    @ManyToOne(type => IndividualAreaServices, indservice => indservice.IND_SERVICE_ID)
    @JoinColumn({ name: "IND_SERVICE_ID" })
    indServices: IndividualAreaServices;
    @ManyToOne(type => IND_SERVICE_PLANS, indserviceplan => indserviceplan.IND_SERVICE_PLAN_ID)
    @JoinColumn({ name: "IND_SERVICE_PLANS" })
    indServicesPlan: IND_SERVICE_PLANS;
   

}