import {Entity, PrimaryGeneratedColumn,CreateDateColumn,ManyToMany,UpdateDateColumn,OneToMany,JoinColumn, Column,ManyToOne} from "typeorm";;
import {CommonAreaServices} from './CommonAreaService';
import {VENDORASSOCIATION} from "./VendorAssociation";
@Entity({name: "VENDOR_ASSOCIATION_COM_SERVICES"})
export class VENDOR_ASSOCIATION_COM_SERVICES {
    @PrimaryGeneratedColumn()
    VENDOR_ASSOCIATION_COM_SERVICES_ID: number;
    @ManyToOne(type => CommonAreaServices, comService => comService.COM_SERVICE_ID)
    @JoinColumn({ name: "COM_SERVICE_ID" })
    comServices: CommonAreaServices;
    @ManyToOne(type => VENDORASSOCIATION, vendorAssociation => vendorAssociation.id)
    @JoinColumn({ name: "VENDOR_ASSOCIATION_ID" })
    vassociation: VENDORASSOCIATION;
    @Column()
    CREATED_BY: string;
    @Column()
    UPDATED_BY: string;
    @CreateDateColumn() 
    public CREATED_DATE: Date;
    @UpdateDateColumn() 
    public UPDATED_DATE: Date;

}
