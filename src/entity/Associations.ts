import { Entity, PrimaryGeneratedColumn, OneToMany, JoinColumn, Column, ManyToOne, OneToOne, CreateDateColumn, UpdateDateColumn } from "typeorm";
import { Appartment } from '../entity/AppartmentEntity';

@Entity({ name: "ASSOCIATION" })
export class ASSOCIATION {
    @PrimaryGeneratedColumn({ name: "ASSOCIATION_ID" })
    id: number;
    @Column({ name: "ASSOCIATION_NAME" })
    ASSOCIATION_NAME: string;
    @ManyToOne(type => Appartment, appartment => appartment.id)
    @JoinColumn({ name: "APPARTMENT_ID" })
    appatmentInfo: Appartment;
    @Column({ name: "ASSOCIATION_UNIQUE_NUMBER" })
    ASSOCIATION_UNIQUE_NUMBER: string;
    @Column({ name: "CONTACT_PERSON_NAME" })
    CONTACT_PERSON_NAME: string;
    @Column({ name: "PHONE_NUMBER" })
    PHONE_NUMBER: string;
    @Column({ name: "EMAIL" })
    EMAIL: string;
    ///
    @Column({ name: "DATA_RANGE" })
    DATA_RANGE: string;
    @Column({ name: "ZIP_CODE" })
    ZIP_CODE: string;
    @Column({ name: "NUMBER_REPORT_CARDS" })
    NUMBER_REPORT_CARDS: string;
    @Column({ name: "NUMBER_FLAT_HOUSES" })
    NUMBER_FLAT_HOUSES: string;
    @Column({ name: "NUMBER_EMPLOYEES_LOGIN" })
    NUMBER_EMPLOYEES_LOGIN: string;
    @Column({ name: "EMAIL_MODE" })
    EMAIL_MODE: string;
    @Column({ name: "SMS_MODE" })
    SMS_MODE: string;
    @Column({ name: "WHATSAPP_MODE" })
    WHATSAPP_MODE: string;

    @Column({ name: "ACT_REQ_COMMUNICATION" })
    ACT_REQ_COMMUNICATION: string;

    @Column({ name: "ASS_PAYMENT" })
    ASS_PAYMENT: string;









    @Column({ name: "NUMBER_OF_FLATS_HOUSES" })
    NUMBER_OF_FLATS_HOUSES: number;
    @Column({ name: "VISITOR_VEHICLE_ALLOWED" })
    VISITOR_VEHICLE_ALLOWED: string;
    @Column({ name: "PRESIDENT_NAME" })
    PRESIDENT_NAME: string;

    @Column({ name: "PRESIDENT_EMAIL" })
    PRESIDENT_EMAIL: string;


    @Column({ name: "PRESIDENT_PHONE_NUMBER" })
    PRESIDENT_PHONE_NUMBER: string;
    @Column({ name: "PRESIDENT_ADDRESS" })
    PRESIDENT_ADDRESS: string;
    @Column({ name: "NUMBER_OF_PARKING_SLOTS" })
    NUMBER_OF_PARKING_SLOTS: string;

    @Column({ name: "TWO_WHELLER_PARKING_SLOTS" })
    TWO_WHELLER_PARKING_SLOTS: string;
    @Column({ name: "FOUR_WHELLER_PARKING_SLOTS" })
    FOUR_WHELLER_PARKING_SLOTS: string;
    @Column({ name: "AMBULANCE_PARKING" })
    AMBULANCE_PARKING: boolean;
    @Column({ name: "IS_REGISTERED" })
    IS_REGISTERED: boolean;
    @Column({ name: "REGISTRATION_NUMBER", nullable: true })
    REGISTRATION_NUMBER: string;
    @Column({ name: "REGISTRATION_FILE_LOC", nullable: true })
    REGISTRATION_FILE_LOC: string;
    @Column()
    CREATED_BY: string;
    @Column()
    UPDATED_BY: string;
    @CreateDateColumn()
    public CREATED_DATE: Date;
    @UpdateDateColumn()
    public UPDATED_DATE: Date;
}