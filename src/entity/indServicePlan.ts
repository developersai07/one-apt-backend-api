import {Entity, PrimaryGeneratedColumn,OneToMany,JoinColumn, Column,ManyToOne, OneToOne,CreateDateColumn,UpdateDateColumn} from "typeorm";
//import {APPUSERS} from '../entity/AppUser';
@Entity({name: "IND_SERVICE_PLANS"})
export class IND_SERVICE_PLANS {
    @PrimaryGeneratedColumn()
    IND_SERVICE_PLAN_ID: number;
    @Column()
    PLAN: string;
    @Column()
    DAYS: number;
    @Column()
    CREATED_BY: string;

    @Column()
    UPDATED_BY: string;

    @CreateDateColumn() 
    public CREATED_DATE: Date;
  
    @UpdateDateColumn() 
    public UPDATED_DATE: Date;


}