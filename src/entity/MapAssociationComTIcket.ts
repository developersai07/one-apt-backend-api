import {Entity, PrimaryGeneratedColumn,OneToMany,JoinColumn, Column,ManyToOne, OneToOne,CreateDateColumn,UpdateDateColumn} from "typeorm";
import {CommonAreaTickets} from '../entity/CommonAreaTickets';
import {AppUserDetails} from '../entity/AppUserDetails';
@Entity({name: "MAP_ASS_COM_TICKET"})
export class MAP_ASS_COM_TICKET {
    @PrimaryGeneratedColumn()
    MAP_ASS_COM_TICKET_ID: number;
    @ManyToOne(type => CommonAreaTickets, comTicket => comTicket.COM_TICKET_ID)
    @JoinColumn({ name: "COM_TICKET_ID" })
    comTickets: CommonAreaTickets;
    @ManyToOne(type => AppUserDetails, appuser => appuser.APP_USER_ID)
    @JoinColumn({ name: "APP_USER_ID" })
    appUser: AppUserDetails;
    @Column()
    IS_ACCEPTED: boolean;
    @Column()
    IS_REJECTED: boolean;
}