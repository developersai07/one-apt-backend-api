
import {Entity, PrimaryGeneratedColumn,OneToMany,JoinColumn,ManyToMany, Column,ManyToOne, OneToOne,CreateDateColumn,UpdateDateColumn} from "typeorm";
import {VENDORASSOCIATION} from './VendorAssociation';
import {ASSOCIATION_VENDORS} from '../entity/AssociationVendors';
import { ASSOCIATION } from "./Associations";
import {VendorServices} from "./VendorServices";
import { MapIndTicketVendor } from "./MapIndTicketsVendor";
import { MapComTicketVendor } from "./MapComTIcketVendor";
import { ASSOCIATION_VEN_SER_PRIORITY } from "./AssociationVenServPriority";
 
@Entity({name: "VENDOR"})
export class VENDOR {
    @PrimaryGeneratedColumn({name:"VENDOR_ID"})
    id: number;
    @Column({name:"VENDOR_NAME"})
    VENDOR_NAME: string;
    @Column({name:"VENDOR_MOBILE_NUMBER"})
    VENDOR_MOBILE_NUMBER: string;
    @Column({name:"VENDOR_ALT_MOBILE_NUMBER"})
    VENDOR_ALT_MOBILE_NUMBER: string;
    @Column({name:"VENDOR_EMAIL"})
    VENDOR_EMAIL: string;
    @Column({name:"VENDOR_ADDRESS"})
    VENDOR_ADDRESS: string;
    @Column({name:"ZIP_CODE"})
    ZIP_CODE: string;
    @Column({name:"VENDOR_PIC", nullable: true })
    VENDOR_PIC: string;
    @Column({name:"VENDOR_UNIQUE_NUMBER", nullable: true })
    VENDOR_UNIQUE_NUMBER: string;
    @Column({name:"VENDOR_PROOF", nullable: true })
    VENDOR_PROOF: string;
    @Column({name:"IS_ACTIVE"})
    IS_ACTIVE: boolean;
    @ManyToOne(type => VENDORASSOCIATION, vassociation => vassociation.id, {nullable: true})
    @JoinColumn({ name: "VENDOR_ASSOCIATION_ID", })
    vAssociation: VENDORASSOCIATION;
    @ManyToOne(type => ASSOCIATION, association => association.id, {nullable: true})
    @JoinColumn({ name: "ASSOCIATION_ID", })
    asssociation: ASSOCIATION;
    @Column()
    CREATED_BY: string;
    @Column()
    UPDATED_BY: string;

    @OneToMany(type => ASSOCIATION_VENDORS, assVendors => assVendors.vendorInfo)
    @JoinColumn()
    assVendors: ASSOCIATION_VENDORS;

    @OneToMany(type => VendorServices, venService => venService.vendorInfo)
    @JoinColumn()
    vensServices: VendorServices;

    @OneToMany(type => MapIndTicketVendor, mapindTicketVendor => mapindTicketVendor.vendor)
    @JoinColumn()
    mapVendor: MapIndTicketVendor;
    @OneToMany(type => MapComTicketVendor, mapcomTIcketvendor => mapcomTIcketvendor.vendor)
    @JoinColumn()
    mapComVendor: MapComTicketVendor;
    
    @OneToMany(type => ASSOCIATION_VEN_SER_PRIORITY, assvensupe => assvensupe.vendorInfo)
    @JoinColumn()
    assVenSerPriority: ASSOCIATION_VEN_SER_PRIORITY;
    // @ManyTo(type => ASSOCIATION_VENDORS, assVendors => assVendors.id, {nullable: true})
    // assVendors: ASSOCIATION_VENDORS;
    @CreateDateColumn() 
    public CREATED_DATE: Date;  
    @UpdateDateColumn() 
    public UPDATED_DATE: Date;
}