import { Entity, PrimaryGeneratedColumn, ManyToOne, JoinColumn, Column, CreateDateColumn, UpdateDateColumn } from 'typeorm';
import { FamilyRelations } from './FamilyRelations';
import { AppUserDetails } from './AppUserDetails';
import { ASSOCIATION } from './Associations';


@Entity({ name: "ASSOCIATION_FAMILY_RELATIONS" })

export class AssociationFamilyRelations {
    @PrimaryGeneratedColumn()
    ID: number;

    @ManyToOne(type => FamilyRelations, RELATIONS => RELATIONS.ID)
    @JoinColumn({ name: "RELATIONS_ID"})
    familyRelationsEntity: FamilyRelations;

    @ManyToOne(type => AppUserDetails, APP_USER => APP_USER.APP_USER_ID)
    @JoinColumn({ name:"APP_USER_ID"})
    appUserDetailsEntity: AppUserDetails;

    @ManyToOne(type => ASSOCIATION, ASS => ASS.id)
    @JoinColumn({ name: "ASSOCIATION_ID" })
    associationEntity: ASSOCIATION;

    @Column()
    RELATIVE_NAME: string;

    @Column()
    PHONE_NUMBER: string;

    @Column()
    EMAIL_ID: string;

    @Column()
    AGE: number;

    @Column()
    CREATED_BY: string;

    @Column()
    UPDATED_BY: string;

    @CreateDateColumn()
    CREATED_DATE: Date;

    @UpdateDateColumn()
    UPDATED_DATE: Date;
}