import {Entity, PrimaryGeneratedColumn,CreateDateColumn,UpdateDateColumn,OneToMany,JoinColumn, Column, ManyToOne} from "typeorm";;
import {ASSOCIATION} from '../entity/Associations';
@Entity({name: "Security"})
export class Security {
    @PrimaryGeneratedColumn()
    ID: number;
    @Column()
    SECURITY_NAME: string;
    @Column()
    SECURITY_EMAIL: string;
    @Column()
    SECURITY_MOBILE_NUMBER: string;

    @Column()
    IS_SECURITY_HEAD: boolean;

    @Column()
    SECURITY_GOV_PROOF: string;

    @Column({nullable : true})
    SECURITY_PIC: string;

    @Column({nullable : true})
    SECURITY_UNIQUE_NUMBER: string;

    @ManyToOne(type => ASSOCIATION, assoc => assoc.id)
    @JoinColumn({name : "ASSOCIATION_ID"})
    association: ASSOCIATION;

    @Column()
    CREATED_BY: string;

    @Column()
    UPDATED_BY: string;

    @CreateDateColumn() 
    public CREATED_DATE: Date;
  
    @UpdateDateColumn() 
    public UPDATED_DATE: Date;

}
