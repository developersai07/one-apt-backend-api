import{ Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn} from 'typeorm';
import { ColumnMetadata } from 'typeorm/metadata/ColumnMetadata';
@Entity({name: "ADVERTISEMENT"})
export class AdvertisementEntity {
    @PrimaryGeneratedColumn()
    ID: number;

    @Column()
    TITLE: string;
    
    @Column()
    DESCRIPTION: String;
    
    @Column()
    IMAGE: string;
    
    @Column()
    CREATED_BY: string;

    @Column()
    UPDATED_BY: string;

    @CreateDateColumn()
    public CREATED_DATE: Date;

    @UpdateDateColumn()
    public UPDATED_DATE: Date;
}