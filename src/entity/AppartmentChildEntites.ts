import {Entity, PrimaryGeneratedColumn,CreateDateColumn,UpdateDateColumn,OneToMany,JoinColumn, Column, ManyToOne} from "typeorm";
import {Appartment} from "../entity/AppartmentEntity";
import {APPARTMENT_GRAND_CHILD_ENTITIES} from "../entity/AppartmentGrandChildEntity";
@Entity({name: "APPARTMENT_CHILD_ENTITIES"})
export class APPARTMENT_CHILD_ENTITIES {
    @PrimaryGeneratedColumn()
    APPARTMENT_CHILD_ENTITY_ID: number;
    @Column()
    CHILD_NAME: string;
    @ManyToOne(type => Appartment, appartment => appartment.id)
    @JoinColumn({ name: "APPARTMENT_ID" })
    appartmentChildEntiy : Appartment;
    //above removed
    // @OneToMany(type => APPARTMENT_GRAND_CHILD_ENTITIES, appartmentGrandChildEntity => appartmentGrandChildEntity.appartmentChildEntiy)
    // @JoinColumn()
    // appGrandChild: APPARTMENT_GRAND_CHILD_ENTITIES[];

    // @OneToMany(type => APPARTMENT_GRAND_CHILD_ENTITIES, appartmentGrandChildEntity => appartmentGrandChildEntity.appartmentChildEntiy)
    // @JoinColumn()
    // mapSub: APPARTMENT_GRAND_CHILD_ENTITIES[];


    @OneToMany(type => APPARTMENT_GRAND_CHILD_ENTITIES, individualServiceMapSubscriptions => individualServiceMapSubscriptions.APPARTMENT_CHILD_ENTITY_ID)
    @JoinColumn()
    mapSub: APPARTMENT_GRAND_CHILD_ENTITIES[];

    @Column()
    CREATED_BY: string;
    @Column()
    UPDATED_BY: string;
    @CreateDateColumn() 
    public CREATED_DATE: Date;  
    @UpdateDateColumn() 
    public UPDATED_DATE: Date;

}
