
import { Entity, PrimaryGeneratedColumn, OneToMany, JoinColumn, Column, ManyToOne, OneToOne, CreateDateColumn, UpdateDateColumn } from "typeorm";
import { Appartment } from './AppartmentEntity';
import { COUNTRIES } from './Countries';
import { STATES } from './States';
import { CITIES } from './Cities';
import { VENDORREQUESTEDASSOCIATION } from './VendorRequestedAssociations';
import { ASSOCIATION_VENDORS } from "./AssociationVendors";
@Entity({ name: "VENDOR_ASSOCIATION" })
export class VENDORASSOCIATION {
    @PrimaryGeneratedColumn({ name: "VENDOR_ASSOCIATION_ID" })
    id: number;
    @Column({ name: "VENDOR_COMPANY_NAME" })
    VENDOR_COMPANY_NAME: string;
    @Column({ name: "VENDOR_UNIQUE_NUMBER" })
    VENDOR_UNIQUE_NUMBER: string;
    @Column({ name: "CONTACT_PERSON_NAME" })
    CONTACT_PERSON_NAME: string;
    @Column({ name: "PHONE_NUMBER" })
    PHONE_NUMBER: string;
    @Column({ name: "EMAIL" })
    CONTACT_EMAIL: string;
    @Column({ name: "ADDRESS" })
    CONTACT_ADDRESS: string;

    @Column({ name: "VENDOR_CONTACT_PERSON_NAME" })
    VENDOR_CONTACT_PERSON_NAME: string;
    @Column({ name: "VENDOR_PHONE_NUMBER" })
    VENDOR_PHONE_NUMBER: string;
    @Column({ name: "VENDOR_EMAIL" })
    VENDOR_EMAIL: string;
    @Column({ name: "VENDOR_ADDRESS" })
    VENDOR_ADDRESS: string;
    ///
    @Column({ name: "DATA_RANGE" })
    DATA_RANGE: string;
    @Column({ name: "NUMBER_REPORT_CARDS" })
    NUMBER_REPORT_CARDS: string;
    @Column({ name: "NUMBER_EMPLOYEES_LOGIN" })
    NUMBER_EMPLOYEES_LOGIN: string;
    @Column({ name: "EMAIL_MODE" })
    EMAIL_MODE: string;
    @Column({ name: "SMS_MODE" })
    SMS_MODE: string;
    @Column({ name: "WHATSAPP_MODE" })
    WHATSAPP_MODE: string;


    @ManyToOne(type => COUNTRIES, countries => countries.id)
    @JoinColumn({ name: "COUNTRY_ID" })
    country: COUNTRIES;

    @ManyToOne(type => STATES, states => states.cities)
    @JoinColumn({ name: "STATE_ID" })
    states: STATES;

    @ManyToOne(type => CITIES, cities => cities.states)
    @JoinColumn({ name: "CITY_ID" })
    cities: CITIES;

    @Column({ name: "DISTRICT" })
    DISTRICT: string;
    @Column({ name: "PINCODE" })
    PINCODE: string;

    @Column({ name: "LAND_MARK" })
    LAND_MARK: string;
    @Column({ name: "EMPLOYEE_COUNT" })
    EMPLOYEE_COUNT: string;

    @Column({ name: "NUM_CLIENT_CUSTOMERS" })
    NUM_CLIENT_CUSTOMERS: string;
    @Column({ name: "NUM_CLIENT_CUSTOMERS_FOR_REGISTRATION" })
    NUM_CLIENT_CUSTOMERS_FOR_REGISTRATION: string;

    @Column({ name: "ACT_REQ_COMMUNICATION" })
    ACT_REQ_COMMUNICATION: string;
    @Column({ name: "IS_REGISTERED" })
    IS_REGISTERED: boolean;
    @Column({ name: "REGISTRATION_NUMBER", nullable: true })
    REGISTRATION_NUMBER: string;
    @Column({ name: "REGISTRATION_FILE_LOC", nullable: true })
    REGISTRATION_FILE_LOC: string;
    @Column()
    CREATED_BY: string;
    @Column()
    UPDATED_BY: string;
    @CreateDateColumn()
    public CREATED_DATE: Date;
    @UpdateDateColumn()
    public UPDATED_DATE: Date;

    @OneToMany(type => ASSOCIATION_VENDORS, assVendors => assVendors.vendorInfo)
    @JoinColumn()
    assVendors: ASSOCIATION_VENDORS;

}