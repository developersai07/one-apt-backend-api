import {Entity, PrimaryGeneratedColumn,OneToMany,JoinColumn, Column,ManyToOne,CreateDateColumn,UpdateDateColumn, OneToOne, ManyToMany} from "typeorm";
import {Plans} from "../entity/Plans";
import {AppUserDetails} from "../entity/AppUserDetails";

@Entity({name: "ASSOCIATION_SUBSCRIPTIONS"})
export class AssociationSubscription {
    @PrimaryGeneratedColumn({name:"SUBSCRIPTION_ID"})
    id: number;

    @ManyToOne(type => Plans, plan => plan.PlAN_ID)
    @JoinColumn({ name: "PlAN_ID" })
    plan: Plans;

    @Column()
    IS_ASSOCIATION: boolean;
    @Column()
    IS_VENDOR: boolean;
    @ManyToOne(type => AppUserDetails, appuser => appuser.APP_USER_ID)
    @JoinColumn({ name: "APP_USER_ID" })
    appUser: AppUserDetails;

    @Column({type: "date"})
    SUCBSCRIPTION_START_DATE;

    @Column({type: "date"})
    SUCBSCRIPTION_END_DATE;

    @Column()
    SUBSCRIPTION_CREATED_DATE: Date;
    @Column()
    IS_ACTIVE :  Boolean;
    @Column()
    CREATED_BY: string;
    @Column()
    UPDATED_BY: string;
    @CreateDateColumn() 
    public CREATED_DATE: Date;  
    @UpdateDateColumn() 
    public UPDATED_DATE: Date;
 
}