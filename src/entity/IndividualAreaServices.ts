import {Entity, PrimaryGeneratedColumn,OneToMany,JoinColumn, Column,ManyToOne, OneToOne,CreateDateColumn,UpdateDateColumn} from "typeorm";
//import {APPUSERS} from '../entity/AppUser';
import {IndividualServiceMapSubscriptions} from '../entity/IndServiceMapSubscriptions'
@Entity({name: "INDIVIDUAL_AREA_SERVICES"})
export class IndividualAreaServices {
    @PrimaryGeneratedColumn()
    IND_SERVICE_ID: number;
    @Column()
    IND_SERVICE_NAME: string;
    @Column({nullable: true})
    IND_ICON_NAME: string;
    @Column({nullable: true})
    IND_IMAGE_NAME: string;
 
    @Column()
    DESCRIPTION: string;
    @OneToMany(type => IndividualServiceMapSubscriptions, individualServiceMapSubscriptions => individualServiceMapSubscriptions.IND_SERVICE_ID)
    @JoinColumn()
    mapSub: IndividualServiceMapSubscriptions[];
    @Column()
    CREATED_BY: string;

    @Column()
    UPDATED_BY: string;

    @CreateDateColumn() 
    public CREATED_DATE: Date;
  
    @UpdateDateColumn() 
    public UPDATED_DATE: Date;


}