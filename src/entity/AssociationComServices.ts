import {Entity, PrimaryGeneratedColumn,CreateDateColumn,ManyToMany,UpdateDateColumn,OneToMany,JoinColumn, Column,ManyToOne} from "typeorm";;
import {CommonAreaServices} from './CommonAreaService';
import {ASSOCIATION} from "./Associations";
@Entity({name: "ASSOCIATION_COM_SERVICES"})
export class ASSOCIATION_COM_SERVICES {
    @PrimaryGeneratedColumn()
    ASSOCIATION_COM_SERVICES_ID: number;
    @ManyToOne(type => CommonAreaServices, comService => comService.COM_SERVICE_ID)
    @JoinColumn({ name: "COM_SERVICE_ID" })
    comServices: CommonAreaServices;
    @ManyToOne(type => ASSOCIATION, appUserDetails => appUserDetails.id)
    @JoinColumn({ name: "ASSOCIATION_ID" })
    association: ASSOCIATION;
    @Column()
    CREATED_BY: string;
    @Column()
    UPDATED_BY: string;
    @CreateDateColumn() 
    public CREATED_DATE: Date;
    @UpdateDateColumn() 
    public UPDATED_DATE: Date;

}
