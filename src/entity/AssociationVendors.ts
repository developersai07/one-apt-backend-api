import {Entity, PrimaryGeneratedColumn,OneToMany,JoinColumn, Column,ManyToOne, OneToOne,CreateDateColumn,UpdateDateColumn} from "typeorm";
import {ASSOCIATION} from './Associations';
import {VENDOR} from "./Vendors";
import {VendorServices} from "./VendorServices";
import {VENDORASSOCIATION} from "./VendorAssociation";
@Entity({name: "ASSOCIATION_VENDORS"})
export class ASSOCIATION_VENDORS {
    @PrimaryGeneratedColumn()
    id: number;
    @ManyToOne(type => ASSOCIATION, association => association.id)
    @JoinColumn({ name: "ASSOCIATION_ID" })
    association: ASSOCIATION;
    @ManyToOne(type => VENDOR, vendor => vendor.id)
    @JoinColumn({ name: "VENDOR_ID" })
    vendorInfo: VENDOR;
    @Column()
    IS_INTERNAL_VENDOR: boolean;
    @Column()
    IS_THIRD_PARTY_ASSOCIATION: boolean;

    @ManyToOne(type => VENDORASSOCIATION, vassociation => vassociation.id,{nullable: true})
    @JoinColumn({ name: "VENDOR_ASSOCIATION_ID" })
    vendorAssociation: VENDORASSOCIATION;

    
    // @OneToMany(type => VendorServices, vendorService => vendorService.id)
    // @JoinColumn()
    // venServe: VendorServices[];
    @Column()
    CREATED_BY: string;
    @Column()
    UPDATED_BY: string;
    @CreateDateColumn() 
    public CREATED_DATE: Date;  
    @UpdateDateColumn() 
    public UPDATED_DATE: Date;

}