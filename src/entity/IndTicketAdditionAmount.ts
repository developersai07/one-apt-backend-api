import {Entity, PrimaryGeneratedColumn,OneToMany,JoinColumn, Column,ManyToOne, OneToOne,CreateDateColumn,UpdateDateColumn} from "typeorm";
import {IndividualTickets} from '../entity/IndividualTickets';
import {VENDOR} from '../entity/Vendors';
@Entity({name: "IND_TICKETS_ADDITIONAL_AMOUNTS"})
export class IND_TICKETS_ADDITIONAL_AMOUNTS {
    @PrimaryGeneratedColumn()
    IND_TICKETS_ADDITIONAL_AMOUNT_ID: number;
    @Column()
    AMOUNT: number;
    @Column()
    REASON: string;
    @ManyToOne(type => IndividualTickets, indTicikets => indTicikets.IND_TICKET_ID)
    @JoinColumn({ name: "IND_TICKET_ID" })
    indTickets: IndividualTickets;
    @ManyToOne(type => VENDOR, vendor => vendor.id)
    @JoinColumn({ name: "VENDOR_ID" })
    vendor: VENDOR;
    @Column()
    CREATED_BY: string;
    @Column()
    UPDATED_BY: string;

    @CreateDateColumn() 
    public CREATED_DATE: Date;
  
    @UpdateDateColumn() 
    public UPDATED_DATE: Date;


}