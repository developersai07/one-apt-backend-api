import {Entity, PrimaryGeneratedColumn,CreateDateColumn,UpdateDateColumn,OneToMany,JoinColumn, Column} from "typeorm";;
import {UserRoles} from '../entity/UserRoles';
@Entity({name: "Roles"})
export class Roles {
    @PrimaryGeneratedColumn()
    ROLE_ID: number;

    @Column()
    ROLE_NAME: string;

    @OneToMany(type => UserRoles, userRoles => userRoles.role)
    @JoinColumn()
    role: UserRoles[];

    @Column()
    CREATED_BY: string;

    @Column()
    UPDATED_BY: string;

    @CreateDateColumn() 
    public CREATED_DATE: Date;
  
    @UpdateDateColumn() 
    public UPDATED_DATE: Date;

}
