import {Entity, PrimaryGeneratedColumn,OneToMany,JoinColumn, Column,ManyToOne, OneToOne,CreateDateColumn,UpdateDateColumn} from "typeorm";
import {IndividualAreaServices} from '../entity/IndividualAreaServices';
import {IND_SERVICE_TYPES} from '../entity/indServiceType';
import {VENDOR} from '../entity/Vendors';
@Entity({name: "internal_vendors_serviceamount"})
export class INTERNAL_SERVICES_AMOUNT_VENDORS {
    @PrimaryGeneratedColumn()
    INTERNAL_VENDOR_AMOUNT_ID: number;
    @ManyToOne(type => IndividualAreaServices, indservice => indservice.IND_SERVICE_ID)
    @JoinColumn({ name: "IND_SERVICE_ID" })
    indServices: IndividualAreaServices;
    @ManyToOne(type => IND_SERVICE_TYPES, indservicetypes => indservicetypes.IND_SERVICE_TYPE_ID,{nullable : true})
    @JoinColumn({ name: "SERVICE_TYPE_ID" })
    indServicesTypes: IND_SERVICE_TYPES;
    @ManyToOne(type => VENDOR, vendor => vendor.id)
    @JoinColumn({ name: "VENDOR_ID" })
    vendor: VENDOR;
    @Column()
    AMOUNT: number;
    @Column()
    CREATED_BY: string;
    @Column()
    UPDATED_BY: string;
    @CreateDateColumn() 
    public CREATED_DATE: Date;
    @UpdateDateColumn() 
    public UPDATED_DATE: Date;
}