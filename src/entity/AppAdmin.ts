import {Entity, PrimaryGeneratedColumn,CreateDateColumn,UpdateDateColumn,OneToOne, Column,OneToMany} from "typeorm";
import {AppUserDetails} from "../entity/AppUserDetails";
@Entity({name: "APP_ADMIN"})
export class AppAdmin 
{
    @PrimaryGeneratedColumn()
    ID: number;
    @Column()
    FIRST_NAME: string;
    @Column()
    LAST_NAME: string;
    @Column()
    EMAIL: string;
    @Column()
    IS_ACTIVE: boolean;
    @Column({ type: 'bigint' })
    MOBILE_NUMBER: number;
    @Column()
    CREATED_BY: string;
    @Column()
    UPDATED_BY: string;
    @CreateDateColumn() 
    public CREATED_DATE: Date;
    @UpdateDateColumn() 
    public UPDATED_DATE: Date;
    @OneToOne(type => AppUserDetails, appUserDetails => appUserDetails.adminInfo)
    appAdmin: AppUserDetails;
}
