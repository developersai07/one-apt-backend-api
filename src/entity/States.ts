import {Entity, PrimaryGeneratedColumn,OneToMany,JoinColumn, Column,ManyToOne, OneToOne} from "typeorm";
import {AppUserDetails} from "../entity/AppUserDetails";
import {COUNTRIES} from "../entity/Countries";
import {CITIES} from "../entity/Cities";

@Entity({name: "STATES"})
export class STATES {
    @PrimaryGeneratedColumn()
    id: number;
    @Column({name:"name"})
    name: string;
    @ManyToOne(type => COUNTRIES, country => country.states)
    @JoinColumn({ name: "country_id" })
    country: COUNTRIES;

    @OneToMany(type => CITIES, cities => cities.states)
    @JoinColumn()
    cities: CITIES[];

}