import {Entity, PrimaryGeneratedColumn,OneToMany,JoinColumn, Column,ManyToOne, OneToOne} from "typeorm";
import {AppUserDetails} from "../entity/AppUserDetails";
import {STATES} from "../entity/States";

@Entity({name: "COUNTRIES"})
export class COUNTRIES {
    @PrimaryGeneratedColumn()
    id: number;
    @Column({name:"sortname"})
    sortname: string;
    @Column({name:"name"})
    name: string;
    @Column({name:"phonecode"})
    phonecode: string;

    @OneToMany(type => STATES, states => states.country)
    @JoinColumn()
    states: STATES[];

}