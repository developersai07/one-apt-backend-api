import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn } from 'typeorm';

@Entity({ name: "FAMILY_RELATIONS" })

export class FamilyRelations{
    @PrimaryGeneratedColumn()
    ID: number;

    @Column()
    RELATION_NAME: string;

    @Column()
    CREATED_BY: string;

    @Column()
    UPDATED_BY: string;

    @CreateDateColumn()
    public CREATED_DATE: Date;

    @UpdateDateColumn()
    public UPDATED_DATE: Date;
}