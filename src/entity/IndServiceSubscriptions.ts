import {Entity, PrimaryGeneratedColumn,OneToMany,JoinColumn, Column,ManyToOne, OneToOne,CreateDateColumn,UpdateDateColumn} from "typeorm";
//import {APPUSERS} from '../entity/AppUser';
import {IndividualServiceMapSubscriptions} from './IndServiceMapSubscriptions';
@Entity({name: "INDIVIDUAL_SERVICE_SUBSCRIPTIONS"})
export class IndividualServiceSubscriptions {
    @PrimaryGeneratedColumn()
    SERVICE_SUBSCRIPTION_ID: number;
    @Column()
    SERVICE_SUBSCRIPTION_NAME: string;
    @Column()
    IS_TIMES: Boolean;
    @Column()
    IS_ACTIVE: Boolean;
    @Column()
    TIMES: number;
    @Column()
    CREATED_BY: string;
    @Column()
    UPDATED_BY: string;
    @CreateDateColumn() 
    public CREATED_DATE: Date;
  
    @UpdateDateColumn() 
    public UPDATED_DATE: Date;

    @OneToMany(type => IndividualServiceMapSubscriptions, individualServiceMapSubscriptions => individualServiceMapSubscriptions.SERVICE_SUBSCRIPTION_ID)
    @JoinColumn()
    mapSubs: IndividualServiceMapSubscriptions[];
}