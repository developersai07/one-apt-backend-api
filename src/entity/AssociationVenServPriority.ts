import {Entity, PrimaryGeneratedColumn,OneToMany,JoinColumn, Column,ManyToOne, OneToOne,CreateDateColumn,UpdateDateColumn} from "typeorm";
import {ASSOCIATION} from './Associations';
import {VENDOR} from "./Vendors";
import {IndividualAreaServices} from "./IndividualAreaServices";
@Entity({name: "ASSOCIATION_VEN_IND_SER_PRIORITY"})
export class ASSOCIATION_VEN_SER_PRIORITY 
{
    @PrimaryGeneratedColumn()
    id: number;
    @ManyToOne(type => ASSOCIATION, association => association.id)
    @JoinColumn({ name: "ASSOCIATION_ID" })
    association: ASSOCIATION;
    @ManyToOne(type => VENDOR, vendor => vendor.id)
    @JoinColumn({ name: "VENDOR_ID" })
    vendorInfo: VENDOR;
    @ManyToOne(type => IndividualAreaServices, indService => indService.IND_SERVICE_ID)
    @JoinColumn({ name: "IND_SERVICE_ID" })
    indService: IndividualAreaServices;
    @Column()
    PRIORITY: number;
    @Column()
    CREATED_BY: string;
    @Column()
    UPDATED_BY: string;
    @CreateDateColumn() 
    public CREATED_DATE: Date;  
    @UpdateDateColumn() 
    public UPDATED_DATE: Date;

}