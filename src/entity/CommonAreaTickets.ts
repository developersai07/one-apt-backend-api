import {Entity, PrimaryGeneratedColumn,CreateDateColumn,UpdateDateColumn,OneToMany,JoinColumn, Column, Timestamp, ManyToOne} from "typeorm";
import {AppUserDetails} from './AppUserDetails';
import {ASSOCIATION} from './Associations';
import {CommonAreaServices} from './CommonAreaService';
import {IndividualServiceMapSubscriptions} from './IndServiceMapSubscriptions';
import {INDIVIDUAL_TICKET_OTP} from './IndividualTIcketOtp';
import {IND_TICKETS_ADDITIONAL_AMOUNTS} from './IndTicketAdditionAmount';
import {IND_SERVICE_PLANS} from './indServicePlan';
import {IND_SERVICE_TYPES} from './indServiceType';
import {IndividualAreaServices} from './IndividualAreaServices';
import {MAP_ASS_COM_TICKET} from './MapAssociationComTIcket';
import {MapComTicketVendor} from './MapComTIcketVendor';
import { APP_USER_COM_TICKETS } from './AppUserComTicket';
@Entity({name: "COMMON_AREA_TICKETS"})
export class CommonAreaTickets 
{
    @PrimaryGeneratedColumn()
    COM_TICKET_ID: number;
    @Column()
    TICKET_DESCRIPTION: string;
    @Column({type: "date"})
    TICKET_DATE: Date;
    @Column({type: "time"})
    TICKET_TIME;
    @Column({nullable:true})
    QUANTITY: number;
    @Column({nullable:true})
    AMOUNT : number;
    // @ManyToOne(type => AppUserDetails, appUserDeatils => appUserDeatils.APP_USER_ID)
    // @JoinColumn({ name: "APP_USER_ID" })
    // appUserDetails: AppUserDetails;
    @ManyToOne(type => ASSOCIATION, association => association.id)
    @JoinColumn({ name: "ASSOCIATION_ID" })
    association: ASSOCIATION;
    @ManyToOne(type => IndividualAreaServices, indServices => indServices.IND_SERVICE_ID)
    @JoinColumn({ name: "IND_SERVICE_ID" })
    serviceId: IndividualAreaServices;
    @ManyToOne(type => CommonAreaServices, indServices => indServices.COM_SERVICE_ID)
    @JoinColumn({ name: "COM_SERVICE_ID" })
    comService: CommonAreaServices;
       @ManyToOne(type => IND_SERVICE_PLANS, indServicePlan => indServicePlan.IND_SERVICE_PLAN_ID)
    @JoinColumn({ name: "IND_SERVICE_PLAN_ID" })
    indSerPlan: IND_SERVICE_PLANS;
    @ManyToOne(type => IND_SERVICE_TYPES, indServiceType => indServiceType.IND_SERVICE_TYPE_ID,{nullable : true})
    @JoinColumn({ name: "IND_SERVICE_TYPE_ID" })
    indServiceTypes: IND_SERVICE_TYPES;

    @OneToMany(type => MapComTicketVendor, mappComVe => mappComVe.comTickets)
    @JoinColumn()
    mapComVendor: MapComTicketVendor;

    @OneToMany(type => MAP_ASS_COM_TICKET, mapAssCom => mapAssCom.comTickets)
    @JoinColumn()
    mapAssCom: MAP_ASS_COM_TICKET;
    @Column({nullable:true})
    IMG_LOCATION: string;
    @Column()
    STATUS: string;
    @Column()
    IS_ACTIVE: boolean;
    @Column({nullable : true})
    IS_WORK_COMPLETED: boolean;
    @Column({nullable : true})
    IS_USER_VERIFIED: boolean;

    @ManyToOne(type => AppUserDetails, appUserDeatils => appUserDeatils.APP_USER_ID,{
        nullable : true
    })
    @OneToMany(type => APP_USER_COM_TICKETS, appUserComDeatils => appUserComDeatils.comTicket)
    @JoinColumn()
    appUserComTicket: MAP_ASS_COM_TICKET;
    @JoinColumn({ name: "VERIFIED_APP_USER_ID" })
    vappUserDetails: AppUserDetails;
    @Column()
    IS_ASSOCIATION_APPROVED: boolean;
    @Column()
    CREATED_BY: string;
    @Column()
    UPDATED_BY: string;
    @CreateDateColumn() 
    public CREATED_DATE: Date;  
    @UpdateDateColumn() 
    public UPDATED_DATE: Date;
}
