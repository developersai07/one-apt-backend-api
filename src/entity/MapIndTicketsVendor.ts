import {Entity, PrimaryGeneratedColumn,OneToMany,JoinColumn, Column,ManyToOne, OneToOne} from "typeorm";
import {IndividualTickets} from "../entity/IndividualTickets";
import {VENDOR} from './Vendors'

@Entity({name: "MAP_IND_TICKETS_VENDORS"})
export class MapIndTicketVendor {
    @PrimaryGeneratedColumn()
    id: number;
    @ManyToOne(type => IndividualTickets, indTicket => indTicket.IND_TICKET_ID)
    @JoinColumn({ name: "IND_TICKET_ID" })
    indTickets : IndividualTickets;
    @ManyToOne(type => VENDOR, vendor => vendor.id)
    @JoinColumn({ name: "VENDOR_ID" })
    vendor : VENDOR;
    @Column()
    IS_ACTIVE: boolean;
    @Column()
    IS_ACCEPTED: boolean;
 
    @Column()
    IS_REJECTED: boolean;
}