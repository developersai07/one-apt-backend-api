import {Entity, PrimaryGeneratedColumn,OneToMany,JoinColumn, Column,ManyToOne, OneToOne,CreateDateColumn,UpdateDateColumn} from "typeorm";
import {IndividualAreaServices} from '../entity/IndividualAreaServices';
@Entity({name: "IND_SERVICE_TYPES"})
export class IND_SERVICE_TYPES {
    @PrimaryGeneratedColumn()
    IND_SERVICE_TYPE_ID: number;
    @Column()
    TYPE: string;
    @ManyToOne(type => IndividualAreaServices, indservice => indservice.IND_SERVICE_ID)
    @JoinColumn({ name: "IND_SERVICE_ID" })
    indServices: IndividualAreaServices;
    @Column()
    CREATED_BY: string;
    @Column()
    UPDATED_BY: string;

    @CreateDateColumn() 
    public CREATED_DATE: Date;
  
    @UpdateDateColumn() 
    public UPDATED_DATE: Date;


}