import {Entity, PrimaryGeneratedColumn,OneToMany,JoinColumn, Column,ManyToOne, OneToOne} from "typeorm";
import {AppUserDetails} from "../entity/AppUserDetails";
import {Roles} from "../entity/Roles";
import {IndividualAreaServices} from './IndividualAreaServices';
import {VENDOR} from "./Vendors";
@Entity({name: "VENDOR_SERVICES"})
export class VendorServices {
    @PrimaryGeneratedColumn()
    id: number;
    @ManyToOne(type => IndividualAreaServices, indService => indService.IND_SERVICE_ID)
    @JoinColumn({ name: "IND_SERVICE_ID" })
    indService: IndividualAreaServices;
    @ManyToOne(type => VENDOR, vendor => vendor.id)
    @JoinColumn({ name: "VENDOR_ID" })
    vendorInfo: VENDOR;
}