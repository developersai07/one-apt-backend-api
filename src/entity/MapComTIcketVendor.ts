import {Entity, PrimaryGeneratedColumn,OneToMany,JoinColumn, Column,ManyToOne, OneToOne} from "typeorm";
import {CommonAreaTickets} from "../entity/CommonAreaTickets";
import {VENDOR} from './Vendors'

@Entity({name: "MAP_COM_TICKETS_VENDORS"})
export class MapComTicketVendor {
    @PrimaryGeneratedColumn()
    id: number;
    @ManyToOne(type => CommonAreaTickets, comTicket => comTicket.COM_TICKET_ID)
    @JoinColumn({ name: "COM_TICKET_ID" })
    comTickets : CommonAreaTickets;
    @ManyToOne(type => VENDOR, vendor => vendor.id)
    @JoinColumn({ name: "VENDOR_ID" })
    vendor : VENDOR;
    @Column()
    IS_ACTIVE: boolean;
    @Column()
    IS_ACCEPTED: boolean;
 
    @Column()
    IS_REJECTED: boolean;
    @Column({nullable : true})
    IS_WORK_STARTED: boolean;
    @Column({nullable : true})
    IS_WORK_COMPLETED: boolean;
    
}