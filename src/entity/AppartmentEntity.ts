import {Entity, PrimaryGeneratedColumn,OneToMany,JoinColumn, Column,ManyToOne, OneToOne,CreateDateColumn,UpdateDateColumn} from "typeorm";
import {COUNTRIES} from '../entity/Countries';
import {STATES} from '../entity/States';
import {CITIES} from '../entity/Cities';
import {APPARTMENT_CHILD_ENTITIES} from '../entity/AppartmentChildEntites'
@Entity({name: "APPARTMENT"})
export class Appartment {
    @PrimaryGeneratedColumn({name:"APPARTMENT_ID"})
    id: number;
    @Column({name:"APPARTMENT_NAME"})
    APPARTMENT_NAME: string;
    @Column({name:"ADDRESS"})
    ADDRESS: string;
    @ManyToOne(type => COUNTRIES, countries => countries.id)
    @JoinColumn({ name: "COUNTRY_ID" })
    country: COUNTRIES;

    @ManyToOne(type => STATES, states => states.cities)
    @JoinColumn({ name: "STATE_ID" })
    states: STATES;

    @ManyToOne(type => CITIES, cities => cities.states)
    @JoinColumn({ name: "CITY_ID" })
    cities: CITIES;

    @Column({name:"DISTRICT"})
    DISTRICT: string;
    @Column({name:"PINCODE"})
    PINCODE: string;
     @OneToMany(type => APPARTMENT_CHILD_ENTITIES, appartmentChildEntity => appartmentChildEntity.APPARTMENT_CHILD_ENTITY_ID)
     appartmentChildEntity: APPARTMENT_CHILD_ENTITIES;
    @Column({name:"LAND_MARK"})
    LAND_MARK: string;
    @Column({name:"APPARTMENT_UNIQUE_NUMBER"})
    APPARTMENT_UNIQUE_NUMBER: string;
    @Column({name:"REGISTRATION_NUMBER",nullable:true})
    REGISTRATION_NUMBER: string;
    @Column({name:"IS_REGISTERED"})
    IS_REGISTERED: boolean;
    @Column({name:"REGISTRATION_FILE_LOC",nullable:true})
    REGISTRATION_FILE_LOC: string;
    @Column()
    CREATED_BY: string;
    @Column()
    UPDATED_BY: string;
    @CreateDateColumn() 
    public CREATED_DATE: Date;  
    @UpdateDateColumn() 
    public UPDATED_DATE: Date;
    
    

}