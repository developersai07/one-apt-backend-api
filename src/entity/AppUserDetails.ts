import {Entity, PrimaryGeneratedColumn,OneToMany,JoinColumn, Column,ManyToOne, OneToOne} from "typeorm";
import {AppAdmin} from "../entity/AppAdmin";
import {UserRoles} from "../entity/UserRoles";
import {ASSOCIATION} from '../entity/Associations';
import { VENDORASSOCIATION } from "./VendorAssociation";
import {FlatOwners} from "./FlatOwners";
import {FlatTenants} from "./FlatTenants";
import {VENDOR} from "./Vendors";
import {Security} from "./Security";
//import {APPUSERS} from '../entity/AppUser';
@Entity({name: "APP_USER_DETAILS"})
export class AppUserDetails {
    @PrimaryGeneratedColumn()
    APP_USER_ID: number;
    @Column()
    IS_ACTIVE: boolean;
    @Column()
    USER_NAME: string;
    @Column()
    PASSWORD: string;
    @JoinColumn()
    appPrivacy: AppAdmin;
    @OneToOne(type => AppAdmin, appAdmin => appAdmin.appAdmin,{ nullable: true })
    @JoinColumn()
    adminInfo: AppAdmin;
    @OneToOne(type => ASSOCIATION, association => association.id,{nullable: true})
    @JoinColumn()
    association: ASSOCIATION;
    @OneToOne(type => VENDORASSOCIATION, vendorassociation => vendorassociation.id,{nullable: true})
    @JoinColumn()
    vendorAssociation: VENDORASSOCIATION;
    @OneToOne(type => FlatOwners, flatOwner => flatOwner.FLAT_OWNER_ID,{nullable: true})
    @JoinColumn({name:"FLAT_OWNER"})
    flatOwner: FlatOwners;
    @OneToOne(type => FlatTenants, flatTenant => flatTenant.TENANT_ID,{nullable: true})
    @JoinColumn({name:"FLAT_TENANT"})
    flatTenant: FlatTenants;
    @OneToOne(type => VENDOR, vendor => vendor.id,{nullable: true})
    @JoinColumn({name:"vendor"})
    vendor: VENDOR;
    @OneToOne(type => Security, security => security.ID,{nullable: true})
    @JoinColumn({name:"security"})
    security: Security;
    @OneToMany(type => UserRoles, userRoles => userRoles.appUserInfo, { nullable: true })
    @JoinColumn()
    appRolesInfo: UserRoles[];

}