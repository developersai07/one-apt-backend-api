import {Entity, PrimaryGeneratedColumn,OneToMany,JoinColumn, Column,ManyToOne, OneToOne} from "typeorm";
import {STATES} from "../entity/States";

@Entity({name: "CITIES"})
export class CITIES {
    @PrimaryGeneratedColumn()
    id: number;
    @Column({name:"name"})
    name: string;
    @ManyToOne(type => STATES, states => states.cities)
    @JoinColumn({ name: "state_id" })
    states: STATES;
}