import {Entity, PrimaryGeneratedColumn,OneToMany,JoinColumn, Column,ManyToOne, OneToOne} from "typeorm";
import {AppUserDetails} from "../entity/AppUserDetails";
import {Roles} from "../entity/Roles";

@Entity({name: "USER_ROLES"})
export class UserRoles {
    @PrimaryGeneratedColumn()
    id: number;
    @ManyToOne(type => Roles, roles => roles.role)
    @JoinColumn({ name: "ROLE_ID" })
    role: Roles;
    @ManyToOne(type => AppUserDetails, appUserDetails => appUserDetails.appRolesInfo)
    @JoinColumn({ name: "APPUSERID" })
    appUserInfo: AppUserDetails;
}