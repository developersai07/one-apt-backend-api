import {Entity, PrimaryGeneratedColumn,CreateDateColumn,UpdateDateColumn,OneToMany,JoinColumn, Column} from "typeorm";;
@Entity({name: "PLANS"})
export class Plans {
    @PrimaryGeneratedColumn()
    PlAN_ID: number;
    @Column()
    PLAN_NAME: string;
    @Column()
    IS_ACTIVE: boolean;
    @Column()
    IS_TRIAL: boolean;
    @Column()
    AMOUNT: number;
    @Column()
    DAYS: number;
    @Column()
    CREATED_BY: string;
    @Column()
    UPDATED_BY: string;
    @CreateDateColumn() 
    public CREATED_DATE: Date;  
    @UpdateDateColumn() 
    public UPDATED_DATE: Date;

}
