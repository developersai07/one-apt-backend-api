import {Entity, PrimaryGeneratedColumn,CreateDateColumn,UpdateDateColumn,OneToMany,JoinColumn, Column, Timestamp, ManyToOne} from "typeorm";
import {AppUserDetails} from './AppUserDetails';
import {ASSOCIATION} from './Associations';
import {IndividualAreaServices} from './IndividualAreaServices';
import {IndividualServiceMapSubscriptions} from './IndServiceMapSubscriptions';
import {INDIVIDUAL_TICKET_OTP} from './IndividualTIcketOtp';
import {IND_TICKETS_ADDITIONAL_AMOUNTS} from './IndTicketAdditionAmount';
import {MapIndTicketVendor} from './MapIndTicketsVendor';
import {IND_SERVICE_PLANS} from './indServicePlan';
import {IND_SERVICE_TYPES} from './indServiceType';
@Entity({name: "INDIVIDUAL_SERVICE_TICKETS"})
export class IndividualTickets 
{
    @PrimaryGeneratedColumn()
    IND_TICKET_ID: number;
    @Column()
    TICKET_DESCRIPTION: string;
    @Column({type: "date"})
    TICKET_DATE: Date;
    @Column({type: "time"})
    TICKET_TIME;
    @Column({nullable:true})
    QUANTITY: number;
    @Column({nullable:true})
    AMOUNT : number;
    @ManyToOne(type => AppUserDetails, appUserDeatils => appUserDeatils.APP_USER_ID)
    @JoinColumn({ name: "APP_USER_ID" })
    appUserDetails: AppUserDetails;
    @ManyToOne(type => ASSOCIATION, association => association.id)
    @JoinColumn({ name: "ASSOCIATION_ID" })
    association: ASSOCIATION;
    @ManyToOne(type => IndividualAreaServices, indServices => indServices.IND_SERVICE_ID)
    @JoinColumn({ name: "IND_SERVICE_ID" })
    serviceId: IndividualAreaServices;
       @ManyToOne(type => IND_SERVICE_PLANS, indServicePlan => indServicePlan.IND_SERVICE_PLAN_ID)
    @JoinColumn({ name: "IND_SERVICE_PLAN_ID" })
    indSerPlan: IND_SERVICE_PLANS;
    @ManyToOne(type => IND_SERVICE_TYPES, indServiceType => indServiceType.IND_SERVICE_TYPE_ID,{nullable : true})
    @JoinColumn({ name: "IND_SERVICE_TYPE_ID" })
    indServiceTypes: IND_SERVICE_TYPES;
    // @ManyToOne(type => IndividualServiceMapSubscriptions, indServicesMapSub => indServicesMapSub.MAP_SERVICE_SUBSCRIPTION_ID)
    // @JoinColumn({ name: "SERVICE_SUBSCRIPTION_ID" })
    // indSerMapSub: IndividualServiceMapSubscriptions;
    @OneToMany(type => INDIVIDUAL_TICKET_OTP, indTicketOtp => indTicketOtp.indTickets)
    @JoinColumn()
    indTicketOtp: INDIVIDUAL_TICKET_OTP;
    @OneToMany(type => MapIndTicketVendor, mappIndVe => mappIndVe.indTickets)
    @JoinColumn()
    mapIndVendor: MapIndTicketVendor;
    @OneToMany(type => IND_TICKETS_ADDITIONAL_AMOUNTS, inddAddtionAMount => inddAddtionAMount.indTickets)
    @JoinColumn()
    indAddtionAmount: IND_TICKETS_ADDITIONAL_AMOUNTS;

    @Column()
    IMG_LOCATION: string;
    @Column()
    STATUS: string;
    @Column()
    IS_ACTIVE: boolean;
    @Column()
    CREATED_BY: string;
    @Column()
    UPDATED_BY: string;
    @CreateDateColumn() 
    public CREATED_DATE: Date;  
    @UpdateDateColumn() 
    public UPDATED_DATE: Date;
}
