import {Entity, PrimaryGeneratedColumn,CreateDateColumn,ManyToMany,UpdateDateColumn,OneToMany,JoinColumn, Column,ManyToOne} from "typeorm";;
import {IndividualAreaServices} from './IndividualAreaServices';
import {VENDORASSOCIATION} from "./VendorAssociation";
@Entity({name: "VENDOR_ASSOCIATION_IND_SERVICES"})
export class VENDOR_ASSOCIATION_IND_SERVICES {
    @PrimaryGeneratedColumn()
    VENDOR_ASSOCIATION_IND_SERVICES_ID: number;
    @ManyToOne(type => IndividualAreaServices, indService => indService.IND_SERVICE_ID)
    @JoinColumn({ name: "IND_SERVICE_ID" })
    indServices: IndividualAreaServices;
    @ManyToOne(type => VENDORASSOCIATION, vendorAssociation => vendorAssociation.id)
    @JoinColumn({ name: "VENDOR_ASSOCIATION_ID" })
    vassociation: VENDORASSOCIATION;
    @Column()
    CREATED_BY: string;
    @Column()
    UPDATED_BY: string;
    @CreateDateColumn() 
    public CREATED_DATE: Date;
    @UpdateDateColumn() 
    public UPDATED_DATE: Date;

}
