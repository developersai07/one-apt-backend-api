import {Entity, PrimaryGeneratedColumn,CreateDateColumn,UpdateDateColumn,OneToOne,JoinColumn, Column,OneToMany, ManyToOne} from "typeorm";
import {IndividualAreaServices} from "../entity/IndividualAreaServices";
import {CommonAreaServices} from "../entity/CommonAreaService";
import {APPARTMENT_CHILD_ENTITIES} from "../entity/AppartmentChildEntites";
import {APPARTMENT_GRAND_CHILD_ENTITIES} from "../entity/AppartmentGrandChildEntity";
import {ASSOCIATION} from "../entity/Associations";
import {VENDOR} from "../entity/Vendors";
@Entity({name: "VENDOR_CHECKIN"})
export class VENDOR_CHECKIN 
{
    @PrimaryGeneratedColumn()
    VENDOR_CHECK_IND_ID: number;
    
    @Column()
    AREA: string;
    @ManyToOne(type => IndividualAreaServices, indService => indService.IND_SERVICE_ID,{ nullable: true })
    @JoinColumn({name:"IND_AREA_SERVICES"})
    indService: IndividualAreaServices;
    @ManyToOne(type => VENDOR, vendor => vendor.id,{ nullable: true })
    @JoinColumn({name:"VENDOR"})
    vendor: VENDOR;
    @ManyToOne(type => CommonAreaServices, comService => comService.COM_SERVICE_ID,{ nullable: true })
    @JoinColumn({name:"COM_AREA_SERVICES"})
    comService: CommonAreaServices;
    @ManyToOne(type => APPARTMENT_CHILD_ENTITIES, appChildEntites => appChildEntites.APPARTMENT_CHILD_ENTITY_ID,{ nullable: true })
    @JoinColumn({name:"APP_CHILD"})
    appChildEntity: APPARTMENT_CHILD_ENTITIES;
    @ManyToOne(type => APPARTMENT_GRAND_CHILD_ENTITIES, appGrandChildEntites => appGrandChildEntites.APPARTMENT_GRAND_CHILD_ENTITY_ID,{ nullable: true })
    @JoinColumn({name:"APP_GRAND_CHILD"})
    appGrandChild: APPARTMENT_GRAND_CHILD_ENTITIES;
    @ManyToOne(type => ASSOCIATION, association => association.id)
    @JoinColumn({name:"ASSOCIATION_ID"})
    association: ASSOCIATION;
    @Column({type: 'datetime', nullable: true, default: null})
    CHECK_IN_TIME: Date;
    @Column({type: 'datetime', nullable: true, default: null})
    CHECK_OUT_TIME: Date;
    @Column({ nullable: true})
    IS_CHECKED_IN: Boolean;
    @Column({ nullable: true})
    IS_CHECKED_OUT: Boolean;
    @Column()
    PERSONS: number;
    @Column({nullable : true})
    VV_PIC: number;
    @Column()
    CREATED_BY: string;
    @Column()
    UPDATED_BY: string;
    @CreateDateColumn() 
    public CREATED_DATE: Date;
    @UpdateDateColumn() 
    public UPDATED_DATE: Date;
  
}
