import {Entity, PrimaryGeneratedColumn,CreateDateColumn,UpdateDateColumn,OneToMany,ManyToMany,JoinColumn, Column, ManyToOne} from "typeorm";
import {APPARTMENT_CHILD_ENTITIES} from "../entity/AppartmentChildEntites";
@Entity({name: "APPARTMENT_GRAND_CHILD_ENTITIES"})
export class APPARTMENT_GRAND_CHILD_ENTITIES {
    @PrimaryGeneratedColumn()
    APPARTMENT_GRAND_CHILD_ENTITY_ID: number;
    @Column()
    GARND_CHILD_NAME: string;
    // @ManyToOne(type => APPARTMENT_CHILD_ENTITIES, appartmentChildEntity => appartmentChildEntity.mapSub,
    //     // { onDelete: "CASCADE" }
    //     )
    // @JoinColumn({ name: "APPARTMENT_CHILD_ENTITY_ID" })
    // appartmentChildEntiy : APPARTMENT_CHILD_ENTITIES;


    @ManyToOne(type => APPARTMENT_CHILD_ENTITIES, individualAreaServices => individualAreaServices.mapSub,
        {nullable: true})
    @JoinColumn({ name: "APPARTMENT_CHILD_ENTITY_ID" })
    APPARTMENT_CHILD_ENTITY_ID: APPARTMENT_CHILD_ENTITIES;
    @Column()
    CREATED_BY: string;
    @Column()
    UPDATED_BY: string;
    @CreateDateColumn() 
    public CREATED_DATE: Date;  
    @UpdateDateColumn() 
    public UPDATED_DATE: Date;

}
