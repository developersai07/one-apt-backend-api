import {Entity, PrimaryGeneratedColumn,OneToMany,JoinColumn, Column,ManyToOne, OneToOne,CreateDateColumn,UpdateDateColumn} from "typeorm";
//import {APPUSERS} from '../entity/AppUser';
@Entity({name: "COMMON_AREA_SERVICES"})
export class CommonAreaServices {
    @PrimaryGeneratedColumn()
    COM_SERVICE_ID: number;
    @Column()
    COM_SERVICE_NAME: string;
    @Column()
    DESCRIPTION: string;
    @Column({nullable:true})
    COM_ICON_NAME: string;
    @Column({nullable:true})
    COM_IMAGE_NAME: string;
    @Column()
    CREATED_BY: string;

    @Column()
    UPDATED_BY: string;

    @CreateDateColumn() 
    public CREATED_DATE: Date;
  
    @UpdateDateColumn() 
    public UPDATED_DATE: Date;


}