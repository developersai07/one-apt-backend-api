import {Entity, PrimaryGeneratedColumn,OneToMany,JoinColumn, Column,ManyToOne, OneToOne,CreateDateColumn,UpdateDateColumn} from "typeorm";
import {CommonAreaTickets} from '../entity/CommonAreaTickets';
import {VENDOR} from '../entity/Vendors';
@Entity({name: "COM_TICKETS_ADDITIONAL_AMOUNTS"})
export class COM_TICKETS_ADDITIONAL_AMOUNTS {
    @PrimaryGeneratedColumn()
    COM_TICKETS_ADDITIONAL_AMOUNT_ID: number;
    @Column()
    AMOUNT: number;
    @Column()
    REASON: string;
    @ManyToOne(type => CommonAreaTickets, comTicket => comTicket.comService)
    @JoinColumn({ name: "COM_TICKET_ID" })
    comTickets: CommonAreaTickets;
    @ManyToOne(type => VENDOR, vendor => vendor.id)
    @JoinColumn({ name: "VENDOR_ID" })
    vendor: VENDOR;
    @Column()
    CREATED_BY: string;
    @Column()
    UPDATED_BY: string;

    @CreateDateColumn() 
    public CREATED_DATE: Date;
  
    @UpdateDateColumn() 
    public UPDATED_DATE: Date;


}