import {Entity, PrimaryGeneratedColumn,CreateDateColumn,UpdateDateColumn,OneToMany,JoinColumn, Column, Timestamp, ManyToOne} from "typeorm";
import {AppUserDetails} from './AppUserDetails';
import {ASSOCIATION} from './Associations';
import {IndividualTickets} from './IndividualTickets';

@Entity({name: "INDIVIDUAL_TICKET_OTP"})
export class INDIVIDUAL_TICKET_OTP 
{
    @PrimaryGeneratedColumn()
    IND_TICKET_ID_OTP: number;
    @Column()
    OTP_NUMBER: string;
    @Column({type: "date"})
    OTP_DATE: Date;
    @Column()
    IS_ACTIVE: boolean;
    @Column()
    IS_VERIFIED: boolean;
    @ManyToOne(type => IndividualTickets, indTickets => indTickets.IND_TICKET_ID)
    @JoinColumn({ name: "IND_TICKET_ID" })
    indTickets: IndividualTickets;
    @Column()
    IS_WORK_COMPLETED: boolean;
    @Column()
    IS_USER_VERIFIED: boolean;
    
 
  
}
