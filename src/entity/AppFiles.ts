import {Entity, PrimaryGeneratedColumn,CreateDateColumn,UpdateDateColumn,OneToMany,JoinColumn, Column} from "typeorm";;

@Entity({name: "APP_FILE"})
export class APP_FILE {
    @PrimaryGeneratedColumn()
    FILE_ID: number;
    @Column()
    FILE_NAME: string;
    @Column()
    FILE_LOC: string;
    @Column()
    CREATED_BY: string;
    @Column()
    UPDATED_BY: string;
    @CreateDateColumn() 
    public CREATED_DATE: Date;
    @UpdateDateColumn() 
    public UPDATED_DATE: Date;

}
