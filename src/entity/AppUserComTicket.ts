import {Entity, PrimaryGeneratedColumn,CreateDateColumn,ManyToOne,UpdateDateColumn,OneToMany,JoinColumn, Column} from "typeorm";;
import {CommonAreaTickets} from '../entity/CommonAreaTickets';
import {AppUserDetails} from '../entity/AppUserDetails';
@Entity({name: "APP_USER_COM_TICKETS"})
export class APP_USER_COM_TICKETS {
    @PrimaryGeneratedColumn()
    APP_USER_COM_TICKET_ID: number;
    @ManyToOne(type => CommonAreaTickets, comTicket => comTicket.COM_TICKET_ID)
    @JoinColumn({ name: "COM_TICKET_ID" })
    comTicket: CommonAreaTickets;
    @ManyToOne(type => AppUserDetails, appUser => appUser.APP_USER_ID)
    @JoinColumn({ name: "APP_USER_ID" })
    appUser : AppUserDetails;
    @Column()
    IS_ACTIVE: boolean;
}
