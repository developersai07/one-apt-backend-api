import {Entity, PrimaryGeneratedColumn,CreateDateColumn,ManyToMany,UpdateDateColumn,OneToMany,JoinColumn, Column,ManyToOne} from "typeorm";;
import {IndividualAreaServices} from './IndividualAreaServices';
import {ASSOCIATION} from "./Associations";
@Entity({name: "ASSOCIATION_IND_SERVICES"})
export class ASSOCIATION_IND_SERVICES {
    @PrimaryGeneratedColumn()
    ASSOCIATION_IND_SERVICES_ID: number;
    @ManyToOne(type => IndividualAreaServices, indService => indService.IND_SERVICE_ID)
    @JoinColumn({ name: "IND_SERVICE_ID" })
    indServices: IndividualAreaServices;
    @ManyToOne(type => ASSOCIATION, appUserDetails => appUserDetails.id)
    @JoinColumn({ name: "ASSOCIATION_ID" })
    association: ASSOCIATION;
    @Column()
    CREATED_BY: string;
    @Column()
    UPDATED_BY: string;
    @CreateDateColumn() 
    public CREATED_DATE: Date;
    @UpdateDateColumn() 
    public UPDATED_DATE: Date;

}
