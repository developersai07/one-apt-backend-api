import {Entity, PrimaryGeneratedColumn,CreateDateColumn,UpdateDateColumn,OneToMany,JoinColumn, Column, ManyToOne} from "typeorm";
import {Appartment} from "../entity/AppartmentEntity";
import {ASSOCIATION} from "../entity/Associations";
import {APPARTMENT_CHILD_ENTITIES} from "../entity/AppartmentChildEntites";
import {APPARTMENT_GRAND_CHILD_ENTITIES} from "../entity/AppartmentGrandChildEntity";
@Entity({name: "FLAT_TENANTS"})
export class FlatTenants {
    @PrimaryGeneratedColumn()
    TENANT_ID: number;
    @Column()
    TENANT_FIRST_NAME: string;
    @Column()
    TENANT_LAST_NAME: string;
    @Column()
    TENANT_EMAIL: string;
    @Column()
    TENANT_MOBILE_NUMBER: string;
    @Column()
    IS_ACTIVE: boolean;
    @Column()
    IS_FINANCIAL_ACCEESS: boolean;
    @Column({nullable : true})
    TENANT_PIC: string;
    @Column()
    TENANT_ADDRESS: string;
    @CreateDateColumn() 
    public TENANT_CREATED_DATE: Date;  
    @ManyToOne(type => Appartment, appartment => appartment.id)
    @JoinColumn({ name: "APPARTMENT_ID" })
    appartmentEntiy : Appartment;
    @ManyToOne(type => ASSOCIATION, association => association.id)
    @JoinColumn({ name: "ASSOCIATION_ID" })
    associationEntity : ASSOCIATION;
    @ManyToOne(type => APPARTMENT_CHILD_ENTITIES, appartmentChildEntity => appartmentChildEntity.APPARTMENT_CHILD_ENTITY_ID)
    @JoinColumn({ name: "APPARTMENT_CHILD_ENTITY" })
    associationChildEntity : APPARTMENT_CHILD_ENTITIES;
    @ManyToOne(type => APPARTMENT_GRAND_CHILD_ENTITIES, appartmentGrandChild => appartmentGrandChild.APPARTMENT_GRAND_CHILD_ENTITY_ID)
    @JoinColumn({ name: "APPARTMENT_GRAND_CHILD_ENTITY" })
    associationGrandChildEntity : APPARTMENT_GRAND_CHILD_ENTITIES;
    @Column()
    CREATED_BY: string;
    @Column()
    UPDATED_BY: string;
    @CreateDateColumn() 
    public CREATED_DATE: Date;  
    @UpdateDateColumn() 
    public UPDATED_DATE: Date;

}
