import {Entity, PrimaryGeneratedColumn,OneToMany,JoinColumn, Column,ManyToOne, OneToOne,CreateDateColumn,UpdateDateColumn} from "typeorm";
import {IndividualAreaServices} from '../entity/IndividualAreaServices';
import {VENDORASSOCIATION} from '../entity/VendorAssociation';
import {IND_SERVICE_TYPES} from '../entity/indServiceType';
//import {APPUSERS} from '../entity/AppUser';
@Entity({name: "IND_SER_TYPES_VENDOR_ASSOCIATION"})
export class IND_SER_TYPES_VENDOR_ASSOCIATION {
    @PrimaryGeneratedColumn()
    IND_SER_TYPES_VENDOR_ASS_ID: number;
    @Column()
    AMOUNT: number;
    @ManyToOne(type => IndividualAreaServices, indservice => indservice.IND_SERVICE_ID)
    @JoinColumn({ name: "IND_SERVICE_ID" })
    indServices: IndividualAreaServices;

    @ManyToOne(type => IND_SERVICE_TYPES, indservicetypes => indservicetypes.IND_SERVICE_TYPE_ID)
    @JoinColumn({ name: "IND_SERVICE_TYPE_ID" })
    indServicetypes: IND_SERVICE_TYPES;

    @ManyToOne(type => VENDORASSOCIATION, vassociation => vassociation.id)
    @JoinColumn({ name: "VENDOR_ASSOCIATION_ID" })
    vassociation: VENDORASSOCIATION;

    
    @Column()
    CREATED_BY: string;

    @Column()
    UPDATED_BY: string;

    @CreateDateColumn() 
    public CREATED_DATE: Date;
  
    @UpdateDateColumn() 
    public UPDATED_DATE: Date;


}