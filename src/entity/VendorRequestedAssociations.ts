
import {Entity, PrimaryGeneratedColumn,OneToMany,JoinColumn, Column,ManyToOne, OneToOne,CreateDateColumn,UpdateDateColumn} from "typeorm";
import {ASSOCIATION} from './Associations';
import {VENDORASSOCIATION} from './VendorAssociation';
@Entity({name: "VENDOR_REQUESTED_ASSOCIATION"})
export class VENDORREQUESTEDASSOCIATION 
{   
@PrimaryGeneratedColumn()
id: number;

@ManyToOne(type => VENDORASSOCIATION, vassociaitonn => vassociaitonn.id)
@JoinColumn({ name: "VENDOR_ASSOCIATION_ID" })
vassociation: VENDORASSOCIATION;
@ManyToOne(type => ASSOCIATION, association => association.id)
@JoinColumn({ name: "ASSOCIATION_ID" })
association: ASSOCIATION;
@Column()
IS_ACCEPTED: boolean;
@Column()
IS_REJECTED: boolean;
@Column()
DESCRIPTION : string;
@Column()
CREATED_BY: string;
@Column()
UPDATED_BY: string;
@CreateDateColumn() 
public CREATED_DATE: Date;  
@UpdateDateColumn() 
public UPDATED_DATE: Date;
}