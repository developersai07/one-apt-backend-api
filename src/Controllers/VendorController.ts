import { Request, Response } from "express";
import * as jwt from "jsonwebtoken";
import { getRepository } from "typeorm";
import { ASSOCIATION } from "../entity/Associations";
import { COUNTRIES } from "../entity/Countries";
import { STATES } from "../entity/States";
import { CITIES } from "../entity/Cities";
import { Appartment } from "../entity/AppartmentEntity";
import { AppUserDetails } from '../entity/AppUserDetails';
import { Roles } from '../entity/Roles';
import { UserRoles } from "../entity/UserRoles";
import { Plans } from "../entity/Plans";
import { AssociationSubscription } from "../entity/AssociationSubscription";
import VendorSendMail from "../Mails/VendorSendMail";
import { VENDORASSOCIATION } from "../entity/VendorAssociation";
import { ASSOCIATION_VENDORS } from "../entity/AssociationVendors";
import { FlatTenants } from "../entity/FlatTenants";
import { VENDOR } from "../entity/Vendors";
import { VendorServices } from "../entity/VendorServices";
import { IndividualAreaServices } from "../entity/IndividualAreaServices";
var nodemailer = require('nodemailer');


var transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: 'sharukh.smart.9@gmail.com',
    pass: '9885510085Sharu$1994'
  }
});

class VendorController {
  static vendorEnrollment = async (req: Request, res: Response, next) => {

    console.log("adding route  asasd");
    const countriesrep = getRepository(COUNTRIES);
    const statesrep = getRepository(STATES);
    const citiessrep = getRepository(CITIES);
    const appUserRepository = getRepository(AppUserDetails);
    const rolesrep = getRepository(Roles);
    const userRolesRepository = getRepository(UserRoles);
    const plansRepository = getRepository(Plans);
    const associationSubscriptionrep = getRepository(AssociationSubscription);

    const vendorAssociationrep = getRepository(VENDORASSOCIATION);

    const contryobj = await countriesrep.findOne(req.body.country_id);
    const statesobj = await statesrep.findOne(req.body.states_id);
    const citiesobj = await citiessrep.findOne(req.body.cities_id);

    const vendorAssociation = new VENDORASSOCIATION();
    vendorAssociation.CONTACT_PERSON_NAME = req.body.contact_person_name;
    vendorAssociation.CONTACT_EMAIL = req.body.email;
    vendorAssociation.PHONE_NUMBER = req.body.phone_number;
    vendorAssociation.CONTACT_ADDRESS = req.body.address;
    vendorAssociation.country = contryobj;
    vendorAssociation.states = statesobj;
    vendorAssociation.cities = citiesobj;
    vendorAssociation.DISTRICT = req.body.distrcit;
    vendorAssociation.LAND_MARK = req.body.landmark;
    vendorAssociation.PINCODE = req.body.pin_code;
    vendorAssociation.IS_REGISTERED = req.body.vendor_registered;
    vendorAssociation.REGISTRATION_NUMBER = req.body.vendor_registraion_number;
    vendorAssociation.REGISTRATION_FILE_LOC = req.body.vendor_registraion_file;
    vendorAssociation.VENDOR_COMPANY_NAME = req.body.company_name;

    vendorAssociation.EMPLOYEE_COUNT = req.body.emp_num;

    vendorAssociation.NUM_CLIENT_CUSTOMERS = req.body.num_clients_customers;
    vendorAssociation.VENDOR_UNIQUE_NUMBER = "12231231234";

    vendorAssociation.VENDOR_CONTACT_PERSON_NAME = req.body.vendor_name;
    vendorAssociation.VENDOR_EMAIL = req.body.vendor_email;
    vendorAssociation.VENDOR_PHONE_NUMBER = req.body.vendor_mobile;
    vendorAssociation.VENDOR_ADDRESS = req.body.vendor_address;
    vendorAssociation.DATA_RANGE = req.body.data_range;
    vendorAssociation.NUMBER_REPORT_CARDS = req.body.no_of_report_cards;
    vendorAssociation.NUMBER_EMPLOYEES_LOGIN = req.body.no_of_employee_login;
    vendorAssociation.NUM_CLIENT_CUSTOMERS_FOR_REGISTRATION = req.body.no_of_clients_customers_for_registration;
    vendorAssociation.EMAIL_MODE = req.body.email_mode;
    vendorAssociation.SMS_MODE = req.body.sms_mode;
    vendorAssociation.WHATSAPP_MODE = req.body.whatsApp_mode;
    vendorAssociation.ACT_REQ_COMMUNICATION = req.body.act_req_communication;
    vendorAssociation.UPDATED_BY = 'User';
    vendorAssociation.CREATED_BY = 'User';

    const vendorAssociationObj = vendorAssociationrep.create(vendorAssociation);
    vendorAssociationrep.save(vendorAssociationObj).then((async data => {
      var vendorId = data.id;

      var today = new Date();
      var dda = String(today.getDate()).padStart(2, '0');
      var mma = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
      var yyyya = today.getFullYear();
      var hrsa = today.getHours();
      var minuta = today.getMinutes();
      var secondsa = today.getSeconds();
      var stringDatea = 'OP' + mma + '' + dda + '' + yyyya + '' + hrsa + '' + minuta + '' + secondsa + '' + data.id;

      const vendorUpObj = await vendorAssociationrep.findOne(data.id);
      vendorUpObj.VENDOR_UNIQUE_NUMBER = stringDatea;
      vendorAssociationrep.save(vendorUpObj).then((data => {

        var vendorObj = data;
        var vendTring = req.body.vendor_name.charAt(0);
        var compTring = req.body.company_name.charAt(0);
        var vendpTring = req.body.contact_person_name.charAt(2);
        var email3Tring = req.body.vendor_email.charAt(3);
        var email4Tring = req.body.vendor_email.charAt(4);
        var username = '' + vendTring + '' + compTring + '' + data.id + '' + vendpTring + '' + email3Tring + '' + email4Tring + '';
        var password = Math.floor(100000000 + Math.random() * 900000000) + '';

        const appUser = new AppUserDetails();
        appUser.IS_ACTIVE = true;
        appUser.USER_NAME = username;
        appUser.PASSWORD = password;
        appUser.adminInfo = null;
        appUser.vendorAssociation = vendorUpObj;
        const appuserObj = appUserRepository.create(appUser);

        appUserRepository.save(appuserObj).then((async data => {

          const appUsrUpobj = await appUserRepository.findOne(data.APP_USER_ID);
          appUsrUpobj.USER_NAME = data.USER_NAME + '' + data.APP_USER_ID + '';
          username = appUsrUpobj.USER_NAME;
          appUserRepository.save(appUsrUpobj);
          console.log("adding route");
          const appUserResObj = data;
          var roleId = 3;
          const roleObj = await rolesrep.findOne(roleId);
          //  const roleObj =   rolesrep.findOne(2);

          const userRole = new UserRoles();
          userRole.role = roleObj;
          userRole.appUserInfo = data;
          const userObj = userRolesRepository.create(userRole);

          userRolesRepository.save(userObj).then(async (data) => {


            const planObj = await plansRepository.findOne(req.body.plan);
            var Cr_date = new Date();
            var date = Cr_date.setDate(Cr_date.getDate() + planObj.DAYS);
            var subEndDate = new Date(date)
            const assocSub = new AssociationSubscription();
            assocSub.plan = planObj;
            assocSub.appUser = appUserResObj;
            assocSub.IS_ASSOCIATION = false;
            assocSub.IS_VENDOR = true;
            assocSub.IS_ACTIVE = true;
            assocSub.SUCBSCRIPTION_START_DATE = new Date();
            assocSub.SUBSCRIPTION_CREATED_DATE = new Date();
            assocSub.SUCBSCRIPTION_END_DATE = subEndDate;
            assocSub.UPDATED_BY = 'User';
            assocSub.CREATED_BY = 'User';
            //assocSub.UPDATED_BY = 'User';
            const assSubObj = associationSubscriptionrep.create(assocSub);

            associationSubscriptionrep.save(assSubObj).then((data) => {
              VendorSendMail.vendorEnrollmentSendMail(req.body.vendor_name,username, password, req.body.email);

              req.body.data = data;
              req.body.roleObj = roleObj;
              req.body.assSubObj = assSubObj;
              req.body.vendorObj = vendorObj;





              next();

              //     res.status(200).json({
              //         userDetaild:data,
              //         roleObj:roleObj,
              //         assObj: assSubObj
              // })
            })


          }).catch((error) => {
            console.log(error);
            res.status(500).send(error)
          })



        })).catch((error) => {
          console.log(error);
          res.status(500).send(error)
        })


      })).catch((error) => {
        console.log(error);
        res.status(500).send(error)
      })


    })).catch((error => {
      console.log(error);
      res.status(500).send(error)
    }))

  }


  static updateVendorEnrollemnt = async (req: Request, res: Response, next) => {
    console.log("updateVendorEnrollemnt");
    const countriesrep = getRepository(COUNTRIES);
    const statesrep = getRepository(STATES);
    const citiessrep = getRepository(CITIES);
    

    const vendorAssociationrep = getRepository(VENDORASSOCIATION);

     const contryobj = await countriesrep.findOne(req.body.country);
    const statesobj = await statesrep.findOne(req.body.state);
    const citiesobj = await citiessrep.findOne(req.body.city);
    const vendorObj = await vendorAssociationrep.findOne(req.body.vendor_id);
    vendorObj.VENDOR_COMPANY_NAME = req.body.vendor_company_name;
    vendorObj.CONTACT_PERSON_NAME = req.body.contact_person_name;
    vendorObj.CONTACT_EMAIL = req.body.email;
    vendorObj.PHONE_NUMBER = req.body.phone_number;
    vendorObj.CONTACT_ADDRESS = req.body.vendor_contact_person_address;
    vendorObj.country = contryobj;
    vendorObj.states = statesobj;
    vendorObj.cities = citiesobj;
    vendorObj.DISTRICT = req.body.distrcit;
    vendorObj.LAND_MARK = req.body.landmark;
    vendorObj.PINCODE = req.body.pin_code;
    // vendorObj.IS_REGISTERED = req.body.vendor_registered;
    // vendorObj.REGISTRATION_NUMBER = req.body.vendor_registraion_number;
    // vendorObj.REGISTRATION_FILE_LOC = req.body.vendor_registraion_file;
    vendorObj.VENDOR_COMPANY_NAME = req.body.company_name;

    vendorObj.EMPLOYEE_COUNT = req.body.emp_num;

    vendorObj.NUM_CLIENT_CUSTOMERS = req.body.num_client_customers;
    // vendorObj.VENDOR_UNIQUE_NUMBER = "12231231234";

    vendorObj.VENDOR_CONTACT_PERSON_NAME = req.body.vendor_contact_person_name;
    vendorObj.VENDOR_EMAIL = req.body.vendor_contact_person_email;
    vendorObj.VENDOR_PHONE_NUMBER = req.body.vendor_contact_person_phone_number;
    vendorObj.VENDOR_ADDRESS = req.body.address;
    vendorObj.DATA_RANGE = req.body.data_range;
    vendorObj.NUMBER_REPORT_CARDS = req.body.number_of_report_cards;
    vendorObj.NUMBER_EMPLOYEES_LOGIN = req.body.num_emp_login;
    vendorObj.NUM_CLIENT_CUSTOMERS_FOR_REGISTRATION = req.body.num_client_customers_for_registration;
    vendorObj.EMAIL_MODE = req.body.email_mode;
    vendorObj.SMS_MODE = req.body.sms_mode;
    vendorObj.WHATSAPP_MODE = req.body.whatsApp_mode;
    vendorObj.ACT_REQ_COMMUNICATION = req.body.act_req_communication;
    vendorObj.UPDATED_BY = req.body.updated_by;
    vendorObj.UPDATED_DATE = new Date();
    vendorAssociationrep.save(vendorObj).then(async (data) => {
      res.send(data);

    }).catch((error) => {
      res.send(error);
    });
  }


  static updateVendorFileLoc = async (req: Request, res: Response, next) => {

    if (req.body.vendor_file_loc) {

      const vendorRep = getRepository(VENDORASSOCIATION);

      console.log("ok");
      console.log(req.body.vendorObj.id);

      const vendorObj = await vendorRep.findOne(req.body.vendorObj.id);
      vendorObj.REGISTRATION_FILE_LOC = req.body.vendor_file_loc;

      await vendorRep.save(vendorObj);
      res.status(200).json({
        userDetaild: req.body.data,
        roleObj: req.body.roleObj,
        assObj: req.body.assSubObj,
        vendorObj: req.body.vendorObj
      })

    }
    else {
      next();
    }



  }


  static getAllVendors = async (req: Request, res: Response, next) => {

    const vendor = getRepository(VENDOR);

    vendor.find({}).then((data) => {
      res.send(data);
    }).catch((error) => {
      res.status(500).send(error);
    })


  }

  static updateVendor = async (req: Request, res: Response, next) => {
    const vendorrep = getRepository(VENDOR);
    const VendorServicesrep = getRepository(VendorServices);
    const individualServiceRep = getRepository(IndividualAreaServices);
    const vendorOb = await vendorrep.findOne(req.body.id);

    vendorOb.VENDOR_NAME = req.body.vendor_name;
    //vendorOb.in_house_trd_team = req.body.in_house_3rd_team;
    vendorOb.VENDOR_MOBILE_NUMBER = req.body.contact_number;
    //vendorOb.VENDOR_EMAIL = req.body.email_id;
    vendorOb.VENDOR_ALT_MOBILE_NUMBER = req.body.alt_mobile_number;
    vendorOb.ZIP_CODE = req.body.zip_code;
    vendorOb.VENDOR_ADDRESS = req.body.address;
    vendorOb.UPDATED_BY = req.body.created_by;

    const vendorSerRep = getRepository(VendorServices);

    const result = await vendorSerRep.query('DELETE FROM vendor_services WHERE VENDOR_ID = ' + req.body.id + '');

    var services = [];

    services = req.body.services;
    for (var i = 0; i < services.length; i++) {
      var serviceId = services[i];
      console.log("serviceid")
      console.log(serviceId)

      if (serviceId != ',') {
        const indServObj = await individualServiceRep.findOne(serviceId);
        const vendorServiceObj = new VendorServices();
        vendorServiceObj.indService = indServObj;
        vendorServiceObj.vendorInfo = vendorOb;
        const vendorServiceUpObj = await VendorServicesrep.create(vendorServiceObj);
        VendorServicesrep.save(vendorServiceUpObj).then((data) => {
        })
      }

    }

    vendorrep.save(vendorOb).then((data) => {

      req.body.vendor_id = req.body.id;
      next();

    }).catch((error) => {
      res.status(500).send(error)
    })







  }


  static vendorgetById = async (req: Request, res: Response) => {
    const vendor = getRepository(VENDOR);
    vendor.findOne({
      where: {
        id: req.params.id
      },
      relations: ['asssociation'],
    }).then((data) => {
      res.send(data);
    }
    ).catch((error) => {
      res.send(error)
    });
  }




  static updateAppVendor = async (req: Request, res: Response, next) => {
    const vendorrep = getRepository(VENDOR);
    const vendorOb = await vendorrep.findOne(req.body.id);
    vendorOb.VENDOR_NAME = req.body.first_name;
    // vendorOb.in_house_trd_team = req.body.in_house_3rd_team;
    vendorOb.VENDOR_MOBILE_NUMBER = req.body.mobile_number;
    vendorOb.VENDOR_EMAIL = req.body.email;
    // vendorOb.VENDOR_ALT_MOBILE_NUMBER = req.body.alt_mobile_number;
    // vendorOb.ZIP_CODE = req.body.zip_code;
    // vendorOb.VENDOR_ADDRESS = req.body.address;
    vendorOb.UPDATED_BY = req.body.updated_by;

    vendorrep.save(vendorOb).then((data) => {
      req.body.data = data;
      // res.send(data);

      // req.body.vendor_id = req.body.id;
      next();

    }).catch((error) => {
      res.status(500).send(error)
    });
  }
  static addVendor = async (req: Request, res: Response, next) => {
    const vendor = getRepository(VENDOR);
    const vendorAssociation = getRepository(VENDORASSOCIATION);
    const VendorServicesrep = getRepository(VendorServices);
    const individualServiceRep = getRepository(IndividualAreaServices);
    const appUserDetailRep = getRepository(AppUserDetails);
    const rolesrep = getRepository(Roles);
    const userRolesRepository = getRepository(UserRoles);

    const verndorAssObj = await vendorAssociation.findOne(req.body.vAssociationId);
    const vendorObj = new VENDOR();
    vendorObj.IS_ACTIVE = true;
    vendorObj.VENDOR_NAME = req.body.vendor_name;
    //vendorObj.in_house_trd_team = req.body.in_house_3rd_team;
    vendorObj.VENDOR_MOBILE_NUMBER = req.body.contact_number;
    vendorObj.VENDOR_EMAIL = req.body.email_id;
    vendorObj.VENDOR_ALT_MOBILE_NUMBER = req.body.alt_mobile_number;
    vendorObj.ZIP_CODE = req.body.zip_code;
    vendorObj.VENDOR_ADDRESS = req.body.address;
    vendorObj.IS_ACTIVE = true;
    vendorObj.CREATED_BY = req.body.created_by;
    vendorObj.UPDATED_BY = req.body.created_by;
    vendorObj.vAssociation = verndorAssObj;

    const vendNewObj = vendor.create(vendorObj);
    vendor.save(vendNewObj).then(async (data) => {



      var vendorId = data.id;
      var vendorObj = data;
      const verndorAssObj = await vendor.findOne(vendorId);
      var today = new Date();
      var dd = String(today.getDate()).padStart(2, '0');
      var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
      var yyyy = today.getFullYear();
      var hrs = today.getHours();
      var minut = today.getMinutes();
      var seconds = today.getSeconds();
      var stringDate = 'OPV' + mm + '' + dd + '' + yyyy + '' + hrs + '' + minut + '' + vendorId;
      var vendorUniqueNumbr = stringDate;

      verndorAssObj.VENDOR_UNIQUE_NUMBER = vendorUniqueNumbr;

      vendor.save(verndorAssObj).then(async (uPObj) => {

        console.log("hello")
        var services = [];
        services = req.body.services;
        for (var i = 0; i < services.length; i++) {
          var serviceId = services[i];
          if (serviceId != ',') {
            const indServObj = await individualServiceRep.findOne(serviceId);
            const vendorServiceObj = new VendorServices();
            vendorServiceObj.indService = indServObj;
            vendorServiceObj.vendorInfo = vendorObj;
            const vendorServiceUpObj = await VendorServicesrep.create(vendorServiceObj);
            VendorServicesrep.save(vendorServiceUpObj).then((data) => {
            })


          }

        }
        var vendTring = req.body.vendor_name.charAt(0);
        var compTring = req.body.email_id.charAt(0);
        var vencNumberTring = req.body.contact_number.charAt(2);
        var venaNumberTring = req.body.alt_mobile_number.charAt(2);
        var vendorId = data.id;
        var username = '' + vendTring + '' + compTring + '' + data.id + '' + vencNumberTring + '' + venaNumberTring + '' + vendorId + '';
        var password = Math.floor(100000000 + Math.random() * 900000000) + '';
        const appUserDeails = new AppUserDetails();
        appUserDeails.IS_ACTIVE = true;
        appUserDeails.USER_NAME = username;
        appUserDeails.PASSWORD = password;
        appUserDeails.vendor = vendorObj;
        const appUserRep = await appUserDetailRep.create(appUserDeails);

        appUserDetailRep.save(appUserRep).then((async appUserRep => {
          console.log(appUserRep);
          const appUserResObj = data;
          var roleId = 6;
          const roleObj = await rolesrep.findOne(roleId);
          const userRole = new UserRoles();
          userRole.role = roleObj;
          userRole.appUserInfo = appUserRep;
          const userObj = userRolesRepository.create(userRole);
          userRolesRepository.save(userObj).then((userRoleObj) => {


            VendorSendMail.vendorSendMail(req.body.vendor_name,username, password, req.body.email_id);
            req.body.userRoleObj = userRoleObj;
            req.body.appUserRep = appUserRep;
            req.body.vendorObj = vendorObj;
            req.body.vendor_id = vendorObj.id;
            next();

          }).catch((error) => {
            console.log(error);
            res.status(500).send(error)
          })
        })).catch((error) => {
          console.log(error);
          res.status(500).send(error)
        })

      }).catch((error) => {
        console.log(error);
        res.status(500).send(error)
      })
    }).catch((error) => {
      console.log(error);
      res.status(500).send(error)
    })

  }


  //add vendor by associa5ion


  static addVendorByAssociation = async (req: Request, res: Response, next) => {
    const vendor = getRepository(VENDOR);
    const vendorAssociation = getRepository(VENDORASSOCIATION);
    const associationrep = getRepository(ASSOCIATION);
    const VendorServicesrep = getRepository(VendorServices);
    const individualServiceRep = getRepository(IndividualAreaServices);
    const appUserDetailRep = getRepository(AppUserDetails);
    const rolesrep = getRepository(Roles);
    const userRolesRepository = getRepository(UserRoles);
    const vendorRep = getRepository(VENDOR);
    const associatonRep = getRepository(ASSOCIATION);
    const vassociatonRep = getRepository(ASSOCIATION_VENDORS);

    const associationObj = await associationrep.findOne(req.body.associationId);

    const vendorEmailObj = await vendorRep.findOne({
      where: {
        VENDOR_EMAIL: req.body.email_id
      }
    })

    if (vendorEmailObj) {
      res.json({
        msg: "Vendor Email Exists",

      });
    }
    else {
      const vendorObj = new VENDOR();
      vendorObj.IS_ACTIVE = true;
      vendorObj.VENDOR_NAME = req.body.vendor_name;
      //vendorObj.in_house_trd_team = req.body.in_house_3rd_team;
      vendorObj.VENDOR_MOBILE_NUMBER = req.body.contact_number;
      vendorObj.VENDOR_EMAIL = req.body.email_id;
      vendorObj.VENDOR_ALT_MOBILE_NUMBER = req.body.alt_mobile_number;
      vendorObj.ZIP_CODE = req.body.zip_code;
      vendorObj.VENDOR_ADDRESS = req.body.address;
      vendorObj.IS_ACTIVE = true;
      vendorObj.CREATED_BY = req.body.created_by;
      vendorObj.UPDATED_BY = req.body.created_by;
      vendorObj.asssociation = associationObj;

      const vendNewObj = vendor.create(vendorObj);
      vendor.save(vendNewObj).then(async (data) => {



        var vendorId = data.id;
        var vendorObj = data;
        const verndorAssObj = await vendor.findOne(vendorId);
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();
        var hrs = today.getHours();
        var minut = today.getMinutes();
        var seconds = today.getSeconds();
        var stringDate = 'OPV' + mm + '' + dd + '' + yyyy + '' + hrs + '' + minut + '' + vendorId;
        var vendorUniqueNumbr = stringDate;

        verndorAssObj.VENDOR_UNIQUE_NUMBER = vendorUniqueNumbr;

        vendor.save(verndorAssObj).then(async (uPObj) => {

          console.log("hello")
          var services = [];
          services = req.body.services;
          for (var i = 0; i < services.length; i++) {
            var serviceId = services[i];
            if(serviceId != ','){
              const indServObj = await individualServiceRep.findOne(serviceId);
              const vendorServiceObj = new VendorServices();
              vendorServiceObj.indService = indServObj;
              vendorServiceObj.vendorInfo = vendorObj;
              const vendorServiceUpObj = await VendorServicesrep.create(vendorServiceObj);
              VendorServicesrep.save(vendorServiceUpObj).then((data) => {
              })
            }
         
          }
          var vendTring = req.body.vendor_name.charAt(0);
          var compTring = req.body.email_id.charAt(0);
          var vencNumberTring = req.body.contact_number.charAt(2);
          var venaNumberTring = req.body.alt_mobile_number.charAt(2);
          var vendorId = data.id;
          var username = '' + vendTring + '' + compTring + '' + data.id + '' + vencNumberTring + '' + venaNumberTring + '' + vendorId + '';
          var password = Math.floor(100000000 + Math.random() * 900000000) + '';
          const appUserDeails = new AppUserDetails();
          appUserDeails.IS_ACTIVE = true;
          appUserDeails.USER_NAME = username;
          appUserDeails.PASSWORD = password;
          appUserDeails.vendor = vendorObj;
          const appUserRep = await appUserDetailRep.create(appUserDeails);

          appUserDetailRep.save(appUserRep).then((async appUserRep => {
            console.log(appUserRep);
            const appUserResObj = data;
            var roleId = 6;
            const roleObj = await rolesrep.findOne(roleId);
            const userRole = new UserRoles();
            userRole.role = roleObj;
            userRole.appUserInfo = appUserRep;
            const userObj = userRolesRepository.create(userRole);
            userRolesRepository.save(userObj).then(async (userRoleObj) => {
              // var vendor_id = vendorObj.id;

              //const vendorObj = await vendorRep.findOne(vendorObj.id);
              const associationObj = await associatonRep.findOne(req.body.associationId);

              const vendorAssociation = new ASSOCIATION_VENDORS();
              vendorAssociation.association = associationObj;
              vendorAssociation.vendorInfo = vendorObj;
              vendorAssociation.IS_INTERNAL_VENDOR = true;
              vendorAssociation.IS_THIRD_PARTY_ASSOCIATION = false;
              vendorAssociation.CREATED_BY = req.body.created_by;
              vendorAssociation.UPDATED_BY = req.body.created_by;
              const vAssObj = await vassociatonRep.create(vendorAssociation);
              const vObj = await vassociatonRep.save(vAssObj);

              VendorSendMail.vendorSendMail(req.body.vendor_name,username, password, req.body.email_id);
              req.body.userRoleObj = userRoleObj;
              req.body.appUserRep = appUserRep;
              req.body.vendorObj = vendorObj;
              req.body.vendor_id = vendorObj.id;
              next();
            }).catch((error) => {
              console.log(error);
              res.status(500).send(error)
            })
          })).catch((error) => {
            console.log(error);
            res.status(500).send(error)
          })

        }).catch((error) => {
          console.log(error);
          res.status(500).send(error)
        })
      }).catch((error) => {
        console.log(error);
        res.status(500).send(error)
      })

    }


  }

  static getVendorInfo = async (req: Request, res: Response, next) => {

    const vassociation = getRepository(VENDORASSOCIATION);
    const mapVendorAssObj = await vassociation.findOne(req.params.id);
    res.send(mapVendorAssObj);

  }
  static getVendorEnrolledInfo = async (req: Request, res: Response, next) => {

    const vassociation = getRepository(VENDORASSOCIATION);
    const mapVendorAssObj = await vassociation.findOne({
      where: {
        id: req.params.id
      },
      relations: ['country', 'states', 'cities', 'assVendors']
    }).then((data) => {
      console.log(data);
      res.send(data);
    }).catch((error) => {
      res.status(500).send(error);
    });
    res.send(mapVendorAssObj);

  }

  static getVendorData = async (req: Request, res: Response, next) => {

    console.log(req.params.id)

    const vendor = getRepository(VENDOR);
    const mapVendorObj = await vendor.findOne({
      where: {
        id: req.params.id
      },
      relations: ['vAssociation', 'asssociation']
    }).then((data) => {
      console.log(data)

      res.send(data);
    }).catch((error) => {
      console.log(error)
      res.status(500).send(error)
    });
  }


static externalVendorByAssociation = async (req: Request, res: Response,next) => {

  console.log(req.params.id)

  //res.send(req.params.id)

  const vendor = getRepository(VENDOR);
 const mapVendorObj = await vendor.find({
  where:{
    vAssociation: req.params.id
  }
 }).then((data)=>{
  console.log(data)
  var count = data.length;

  res.json({
    count : count
  });
 }).catch((error)=>{
   console.log(error)
   res.status(500).send(error)
 })

}

static inteternalVendorByAssociation = async (req: Request, res: Response,next) => {

  console.log(req.params.id)

  //res.send(req.params.id)

  const vendor = getRepository(VENDOR);
 const mapVendorObj = await vendor.find({
  where:{
    asssociation: req.params.id
  }
 }).then((data)=>{
  console.log(data)
  var count = data.length;

  res.json({
    count : count
  });
 }).catch((error)=>{
   console.log(error)
   res.status(500).send(error)
 });
}

static checkVendorExists = async (req: Request, res: Response, next) => {
  const vendorRep = getRepository(VENDOR);
  const vendorObj = await vendorRep.findOne({
    where : {
      VENDOR_UNIQUE_NUMBER : req.body.visitorCode
    }
  });

  res.send(vendorObj)
}

  static vendorsServices = async (req: Request, res: Response, next) => {
    const vendorserv = getRepository(VendorServices);
    const mapVendorObj = await vendorserv.find({
      where: {
        vendorInfo: req.params.id
      },
      relations: ['indService']
    }).then((data) => {
      console.log(data)

      res.send(data);
    }).catch((error) => {
      console.log(error)
      res.status(500).send(error)
    });
  }
  static appupdateFileName = async (req: Request, res: Response, next) => {
    const vendorRep = getRepository(VENDOR);
    const vendorObj = await vendorRep.findOne(req.body.vendor_id);
    if (req.body.vendor_image_file_name) {
      vendorObj.VENDOR_PIC = req.body.vendor_image_file_name
    }
    vendorRep.save(vendorObj).then((data) => {
      console.log(data);
      res.send(data);
    }).catch((error) => {
      res.status(500).send(error);
    })
  }
  static updateFileName = async (req: Request, res: Response, next) => {
    const vendorRep = getRepository(VENDOR);
    const vendorObj = await vendorRep.findOne(req.body.vendor_id);
    if (req.body.vendor_image_file_name) {
      vendorObj.VENDOR_PIC = req.body.vendor_image_file_name
    }
    if (req.body.vendor_gov_proof_file_name) {
      vendorObj.VENDOR_PROOF = req.body.vendor_gov_proof_file_name
    }

    vendorRep.save(vendorObj).then((data) => {
      console.log(data);
      res.send(data);
    }).catch((error) => {
      res.status(500).send(error);
    })

  }

  static deleteService = async (req: Request, res: Response) => {
    const vendorRep = getRepository(VENDOR);
    const results = await vendorRep.delete(req.body.id);

    // const users = await connection.manager.find(User);
    res.send(results);
  }


  static vendorZipCode = async (req: Request, res: Response) => {
    const vendorRep = getRepository(VENDOR);
    const results = await vendorRep.find(
      {
        where: {
          ZIP_CODE: req.params.id
        }
      }
    ).then((data) => {
      res.send(data);
    }).catch((error) => {
      res.status(500).send(error);
    })

  }

  static getassociationVendors = async (req: Request, res: Response) => {

    console.log("hii");
    const vendorRep = getRepository(VENDOR);
    vendorRep.find({
      where: {
        asssociation: req.params.associationId,

      },
      relations: ["vensServices", "vensServices.indService"]
    }).then((data) => {
      res.send(data)
    }).catch((error) => {
      res.status(500).send(error)
    })

  }


  static getAllVendorsAssociation = async (req: Request, res: Response) => {

    console.log("hii");
    const vendorRep = getRepository(VENDOR);
    vendorRep.find({
      where: {
        vAssociation: req.params.vassociationId
      }
    }).then((data) => {
      res.send(data)
    }).catch((error) => {
      res.status(500).send(error)
    })

  }

  static getAllInternalVendorsAssociationByService = async (req: Request, res: Response) => {

    const loadedPost = await getRepository(VENDOR)
      .createQueryBuilder("vendor")
      .innerJoinAndSelect("vendor.vensServices", "venSerObj", "venSerObj.vendorInfo = vendor.id")
      .where("vendor.asssociation = :associationId && venSerObj.IND_SERVICE_ID = :serviceId ", {
        serviceId: req.params.serviceId,
        associationId: req.params.associationId
      })
      .getMany();

    res.send(loadedPost);

  }
  static getAllVendorsAssociationByService = async (req: Request, res: Response) => {

    const loadedPost = await getRepository(VENDOR)
      .createQueryBuilder("vendor")
      .innerJoinAndSelect("vendor.vensServices", "venSerObj", "venSerObj.vendorInfo = vendor.id")
      .where("vendor.vAssociation = :associationId && venSerObj.IND_SERVICE_ID = :serviceId ", {
        serviceId: req.params.serviceId,
        associationId: req.params.vassociationId
      })
      .getMany();

    res.send(loadedPost);

  }

  static getInternalVendorsByService = async (req: Request, res: Response) => {

    const loadedPost = await getRepository(VENDOR)
      .createQueryBuilder("vendor")
      .innerJoinAndSelect("vendor.vensServices", "venSerObj", "venSerObj.vendorInfo = vendor.id")
      .where("vendor.asssociation = :associationId && venSerObj.IND_SERVICE_ID = :serviceId ", {
        serviceId: req.params.serviceId,
        associationId: req.params.associationId
      })
      .getMany();

    res.send(loadedPost);

  }

  static getAssociationVendors = async (req: Request, res: Response) => {

    const asVendRep = getRepository(ASSOCIATION_VENDORS);
    asVendRep.find({
      where: {
        association: req.params.associationId
      },
      relations: ['vendorInfo']
    }).then((data) => {
      res.send(data)
    }).catch((error) => {
      res.status(500).send(error)
    })
  }

  static vendorZipCodeNotin = async (req: Request, res: Response) => {

    const vendorReposit = getRepository(VENDOR);
    // const loadedPost = await getRepository(ASSOCIATION_VENDORS)
    // .createQueryBuilder("ass_vendors")
    // .leftJoinAndSelect("ass_vendors.vendorInfo", "vendorEntity", "vendorEntity.id = ass_vendors.VENDOR_ID")
    // .where("vendorEntity.ZIP_CODE = :zipcode &&  ass_vendors.ASSOCIATION_ID IS NULL ", { zipcode: req.params.id })
    // .getMany();

    const loadedPost = await getRepository(VENDOR)
      .createQueryBuilder("vendor")
      .leftJoinAndSelect("vendor.assVendors", "assVendorsObj", "assVendorsObj.vendorInfo = vendor.VENDOR_ID")
      .where("vendor.ZIP_CODE = :zipcode && (assVendorsObj.ASSOCIATION_ID IS NULL  || assVendorsObj.ASSOCIATION_ID != :assId) ", {
        zipcode: req.params.id,
        assId: req.params.associationId
      })
      .getMany();

    console.log("hii")
    console.log(req.params.associationId);
    console.log(req.params.id);

    res.send(loadedPost);
    //   const vendorRep = getRepository(VENDOR);
    //   const results = await vendorRep.find(
    //     {
    // where:{
    //   ZIP_CODE : req.params.id
    // }
    //     }
    //     ).then((data)=>{
    //       res.send(data);
    //     }).catch((error)=>{
    //       res.status(500).send(error);
    //     })

  }


  static vendorMap = async (req: Request, res: Response) => {
    const vendorRep = getRepository(VENDOR);
    const associatonRep = getRepository(ASSOCIATION);
    const vassociatonRep = getRepository(ASSOCIATION_VENDORS);
    const vendorObj = await vendorRep.findOne(req.body.id);
    const associationObj = await associatonRep.findOne(req.body.associationId);
    const vendorAssociation = new ASSOCIATION_VENDORS()
    vendorAssociation.association = associationObj;
    vendorAssociation.vendorInfo = vendorObj;
    vendorAssociation.CREATED_BY = req.body.appUserId;
    vendorAssociation.UPDATED_BY = req.body.appUserId;
    const vAssObj = await vassociatonRep.create(vendorAssociation);
    vassociatonRep.save(vAssObj).then((data) => {
      res.send(data)
    }).catch((error) => [
      res.status(500).send(error)
    ])
  }





  static vendorRemove = async (req: Request, res: Response) => {

    const vassociatonRep = getRepository(ASSOCIATION_VENDORS);
    const vassociationObj = await vassociatonRep.delete(req.body.id);
    res.send(vassociationObj);
  }

}

export default VendorController;