import { Request, Response } from "express";
import * as jwt from "jsonwebtoken";
import { getRepository } from "typeorm";
import { FlatOwners } from "../entity/FlatOwners";
import { Appartment } from "../entity/AppartmentEntity";
import { ASSOCIATION } from "../entity/Associations";
import { APPARTMENT_CHILD_ENTITIES } from "../entity/AppartmentChildEntites";
import { APPARTMENT_GRAND_CHILD_ENTITIES } from "../entity/AppartmentGrandChildEntity";
import { AppUserDetails } from "../entity/AppUserDetails";
import { Roles } from "../entity/Roles";
import { UserRoles } from "../entity/UserRoles";
import FlatOwnerSendMail from "../Mails/FlatOwnerSendMail";
import { FlatTenants } from "../entity/FlatTenants";

class FlatOwnerController {

    static ownerAdd = async (req: Request, res: Response, next) => {
        const rolesRepository = getRepository(Roles);
        const userRolesRepository = getRepository(UserRoles);
        const appUserDetailsRepository = getRepository(AppUserDetails);
        const flatOwnerRepository = getRepository(FlatOwners);
        const appartmentRepository = getRepository(Appartment);
        const associationRepository = getRepository(ASSOCIATION);
        const appartmentChildRepository = getRepository(APPARTMENT_CHILD_ENTITIES);
        const appartmentGrandChildRepository = getRepository(APPARTMENT_GRAND_CHILD_ENTITIES);

        const flatOwnerRepositoryObject = await flatOwnerRepository
            .createQueryBuilder("flatOwner")
            // .where("indTikOtp.IND_TICKET_ID = :ticketId AND indTikOtp.OTP_DATE =  :otp_date AND indTikOtp.IS_VERIFIED = true  AND indTikOtp.IS_WORK_COMPLETED = true  ", {
            .where("flatOwner.associationEntity = :associationId AND flatOwner.associationChildEntity = :appartmentChildId AND flatOwner.associationGrandChildEntity = :appartmentGrandChildId ", {
                associationId: req.body.associationId,
                appartmentChildId: req.body.appartmentChildId,
                appartmentGrandChildId: req.body.appartmentGrandChildId,
            }).getOne();

            const flatOwnerRepositoryEmailObject = await flatOwnerRepository
            .createQueryBuilder("flatOwner")
            // .where("indTikOtp.IND_TICKET_ID = :ticketId AND indTikOtp.OTP_DATE =  :otp_date AND indTikOtp.IS_VERIFIED = true  AND indTikOtp.IS_WORK_COMPLETED = true  ", {
            .where("flatOwner.OWNER_EMAIL = :email", {
                email: req.body.owner_email,
            }).getOne();
            
        console.log("flatOwnerRepository: ");
        console.log(flatOwnerRepositoryObject);

        if (flatOwnerRepositoryObject) {

            res.json({
                msg: "Owner Already Exists",
                data: flatOwnerRepositoryObject
            });

        }
        else if (flatOwnerRepositoryEmailObject){
            res.json({
                msg: "Owner Email Exists",
                data: flatOwnerRepositoryObject
            });
            
        } else {
            console.log("New Flat Owner");
            const appartmentObj = await appartmentRepository.findOne(req.body.appartmentId);
            const associationObj = await associationRepository.findOne(req.body.associationId);
            const appartmentChildObj = await appartmentChildRepository.findOne(req.body.appartmentChildId);
            const appartmentGrandChildObj = await appartmentGrandChildRepository.findOne(req.body.appartmentGrandChildId);
            // const planObj = await plansRepository.findOne(req.body.id);
            // const mergedUser = indServiceRepository.merge(user,req.body);
            const flatOwner = new FlatOwners();
            flatOwner.OWNER_FIRST_NAME = req.body.owner_first_name;
            flatOwner.OWNER_LAST_NAME = req.body.owner_last_name;
            flatOwner.OWNER_EMAIL = req.body.owner_email;
            flatOwner.OWNER_MOBILE_NUMBER = req.body.owner_mobile_number;
var isAssociation ;
            if(req.body.is_asscoation == 'true'){
                isAssociation = true
            }
            else {
                isAssociation = false
            }


            var isFinancialAccess; 

            if(req.body.is_financial_access == 'true'){
                isFinancialAccess = true
            }
            else {
                isFinancialAccess = false
            }
 


            flatOwner.IS_ASSOCIATION = isAssociation;
            flatOwner.IS_FINANCIAL_ACCEESS = isFinancialAccess;
            // flatOwner.OWNER_PIC = req.body.owner_pic;
            flatOwner.OWNER_ADDRESS = req.body.owner_address;
            // flatOwner.appartmentEntiy = req.body.owner_address;
            flatOwner.IS_ACTIVE = true;
            flatOwner.appartmentEntiy = appartmentObj;
            flatOwner.associationEntity = associationObj;
            flatOwner.associationChildEntity = appartmentChildObj;
            flatOwner.associationGrandChildEntity = appartmentGrandChildObj;
            flatOwner.CREATED_BY = req.body.created_by;
            flatOwner.UPDATED_BY = req.body.updated_by;
            const flatOwnerObj = await flatOwnerRepository.create(flatOwner);

            flatOwnerRepository.save(flatOwnerObj).then(async (data) => {
                var flatOwnerResObj = data;
                var assTring = req.body.owner_first_name.charAt(0);
                var appTring = req.body.owner_last_name.charAt(0);
                var username = data.FLAT_OWNER_ID + '' + appTring + '' + assTring + '' + req.body.appartmentId + '' + req.body.associationId + '' + req.body.appartmentChildId + '' + req.body.appartmentGrandChildId + '';
                var password = Math.floor(100000000 + Math.random() * 900000000) + '';
                const appUser = new AppUserDetails();

                appUser.USER_NAME = username;
                appUser.PASSWORD = password;
                appUser.flatOwner = data;
                appUser.IS_ACTIVE = true;
                const appUserObj = await appUserDetailsRepository.create(appUser);

                appUserDetailsRepository.save(appUserObj).then((async data => {

                    const appUsrUpobj = await appUserDetailsRepository.findOne(data.APP_USER_ID);
                    appUsrUpobj.USER_NAME = data.USER_NAME + '' + data.APP_USER_ID + '';
                    appUserDetailsRepository.save(appUsrUpobj);
                    var appUserObjres = data;
                    const rolesObj = await rolesRepository.findOne(4);

                    const userRoleObj = new UserRoles();
                    userRoleObj.appUserInfo = appUserObj;
                    userRoleObj.role = rolesObj;
                    const UserRoleObj = await userRolesRepository.create(userRoleObj);
                    userRolesRepository.save(UserRoleObj).then((data) => {
                        FlatOwnerSendMail.flatOwnerEnrollmentSendMail(req.body.owner_first_name,username, password, req.body.owner_email);
                        // res.status(200).json({
                        //     flatOwner: flatOwnerResObj,
                        //     data: appUserObjres,
                        //     roleObj: data,
                        // })
                        req.body.id = flatOwnerResObj.FLAT_OWNER_ID;
                        req.body.flatOwnerResObj = flatOwnerResObj;
                        req.body.appUserObjres = appUserObjres;
                        req.body.data = data;
                        next();
                    }).catch((err) => {
                        res.status(500).send(err);
                    })


                })).catch((err) => {
                    res.status(500).send(err);
                })
                // req.body.id = data.FLAT_OWNER_ID;
                // req.body.data = data;

                // next();
                //res.status(200).send(data)
            }).catch((err) => {
                res.status(500).send(err);
            });
        }



    }

    static flatOwnerImage = async (req: Request, res: Response, next) => {
        const flatOwnerRepository = getRepository(FlatOwners);

        const flatownerObj = await flatOwnerRepository.findOne(req.body.id);
        if (req.body.owner_pic_name) {
            flatownerObj.OWNER_PIC = req.body.owner_pic_name;
        }
        flatOwnerRepository.save(flatownerObj).then((data) => {
            var flatOwnerResObj = data;
            res.status(200).json({
                flatOwner: flatOwnerResObj,
                // data: appUserObjres,
                // roleObj: data,
            })
        }).catch((error) => {
            res.status(500).send(error);
        })
    }


    static flatOwnergetAll = async (req: Request, res: Response) => {
        console.log("flatOwnergetAll");
        const flatOwners = getRepository(FlatOwners);
        flatOwners.find({
            where : {
                associationEntity : req.params.id
            },
            relations: ['appartmentEntiy', 'associationEntity', 'associationChildEntity', 'associationGrandChildEntity'],
        }).then((data) => {
            res.send(data)
        }
        ).catch((error) => {
            res.send(error)
        });
    }
    static flatOwnergetById = async (req: Request, res: Response) => {
        const flatOwners = getRepository(FlatOwners);
        flatOwners.findOne({
            where: {
                FLAT_OWNER_ID: req.params.id
            },
            relations: ['appartmentEntiy', 'associationEntity', 'associationChildEntity', 'associationGrandChildEntity'],
        }).then((data) => {
            res.send(data);           
        }
        ).catch((error) => {
            res.send(error)
        });
    }

    static flatOwnerDelete = async (req: Request, res: Response) => {

        const flatOwnerRepository = getRepository(FlatOwners);

        const result = await flatOwnerRepository.query('DELETE FROM FLAT_OWNERS WHERE FLAT_OWNER_ID = ' + req.body.id + '');

        res.send(result);
        // const appUserDetailsRepository = getRepository(AppUserDetails);

        // console.log("ok")
        // console.log(req.body.id )
        // const appUserResult = await appUserDetailsRepository.findOne({
        //     where: { flatOwner: req.body.id }
        // });
        // console.log(appUserResult);
        // appUserResult.flatOwner = null;
        // appUserResult.appRolesInfo
        // console.log(appUserResult)
        // var appUserId = appUserResult.APP_USER_ID; 
        // const df = await appUserDetailsRepository.save(appUserResult);
        // const dfz = await appUserDetailsRepository.delete(appUserId);

        // const flatOwnersReporsity = getRepository(FlatOwners);
        // const flatOwnerresults = await flatOwnersReporsity.findOne(req.body.id);

        // flatOwnerresults.appartmentEntiy = null;
        // flatOwnerresults.associationEntity = null;
        // flatOwnerresults.associationChildEntity = null;
        // flatOwnerresults.associationGrandChildEntity = null;
        // const results = await flatOwnersReporsity.save(flatOwnerresults);
        // const result = await flatOwnersReporsity.delete(req.body.id);
        // res.send(result);
    }

    static ownerUpdate = async (req: Request, res: Response, next) => {
        // const appartmentChildEntity = getRepository(APPARTMENT_CHILD_ENTITIES);
        // const appGrandChildEntity = getRepository(APPARTMENT_GRAND_CHILD_ENTITIES);
        
        const flatOwnerRepository = getRepository(FlatOwners);
        const appartmentRepository = getRepository(Appartment);
        const associationRepository = getRepository(ASSOCIATION);
        const appartmentChildRepository = getRepository(APPARTMENT_CHILD_ENTITIES);
        const appartmentGrandChildRepository = getRepository(APPARTMENT_GRAND_CHILD_ENTITIES);

        const flatOwnerObj = await flatOwnerRepository.findOne(req.body.id);
        const appartmentObj = await appartmentRepository.findOne(req.body.appartmentId);
        const associationObj = await associationRepository.findOne(req.body.associationId);
        const appartmentChildObj = await appartmentChildRepository.findOne(req.body.appartmentChildId);
        const appartmentGrandChildObj = await appartmentGrandChildRepository.findOne(req.body.appartmentGrandChildId);

        // const flatOwnerRepositoryObject = await flatOwnerRepository
        // .createQueryBuilder("flatOwner")
        // // .where("indTikOtp.IND_TICKET_ID = :ticketId AND indTikOtp.OTP_DATE =  :otp_date AND indTikOtp.IS_VERIFIED = true  AND indTikOtp.IS_WORK_COMPLETED = true  ", {
        // .where("flatOwner.associationEntity = :associationId AND flatOwner.associationChildEntity = :appartmentChildId AND flatOwner.associationGrandChildEntity = :appartmentGrandChildId ", {
        //     associationId: req.body.associationId,
        //     appartmentChildId: req.body.appartmentChildId,
        //     appartmentGrandChildId: req.body.appartmentGrandChildId,
        // }).getOne();


        const flatOwnerRepositoryEmailObject = await flatOwnerRepository
        .createQueryBuilder("flatOwner")
        // .where("indTikOtp.IND_TICKET_ID = :ticketId AND indTikOtp.OTP_DATE =  :otp_date AND indTikOtp.IS_VERIFIED = true  AND indTikOtp.IS_WORK_COMPLETED = true  ", {
        .where("flatOwner.OWNER_EMAIL = :email", {
            email: req.body.owner_email,
        }).getOne();

        // if (flatOwnerRepositoryObject) {

        //     res.json({
        //         msg: "Owner Already Exists",
        //         data: flatOwnerRepositoryObject
        //     });

        // }
        // else
        
        if (flatOwnerRepositoryEmailObject){
            res.json({
                msg: "Owner Email Exists",
              //  data: flatOwnerRepositoryObject
            });
            
        }
        else {



        // const childEntityObj = await appartmentChildEntity.findOne(req.body.childId);
        // const grandChildObj = await appGrandChildEntity.findOne(req.body.id);
        flatOwnerObj.OWNER_FIRST_NAME = req.body.owner_first_name;
        flatOwnerObj.OWNER_LAST_NAME = req.body.owner_last_name;
       // flatOwnerObj.OWNER_EMAIL = req.body.owner_email;
        flatOwnerObj.OWNER_MOBILE_NUMBER = req.body.owner_mobile_number;
        var isAssociation ;
            if(req.body.is_asscoation == 'true'){
                isAssociation = true
            }
            else {
                isAssociation = false
            }


            var isFinancialAccess; 

            if(req.body.is_financial_access == 'true'){
                isFinancialAccess = true
            }
            else {
                isFinancialAccess = false
            }
 
        flatOwnerObj.IS_ASSOCIATION = isAssociation;
        flatOwnerObj.IS_FINANCIAL_ACCEESS = isFinancialAccess;
        //flatOwnerObj.OWNER_PIC = req.body.owner_pic;
        flatOwnerObj.OWNER_ADDRESS = req.body.owner_address;
        // flatOwner.appartmentEntiy = req.body.owner_address;
        flatOwnerObj.IS_ACTIVE = true;
        flatOwnerObj.appartmentEntiy = appartmentObj;
        flatOwnerObj.associationEntity = associationObj;
        flatOwnerObj.associationChildEntity = appartmentChildObj;
        flatOwnerObj.associationGrandChildEntity = appartmentGrandChildObj;
        // flatOwnerObj.CREATED_BY = req.body.created_by;
        flatOwnerObj.UPDATED_BY = req.body.created_by;
        // grandChildObj.APPARTMENT_CHILD_ENTITY_ID = childEntityObj;
        // grandChildObj.GARND_CHILD_NAME = req.body.grandChildName;
        // grandChildObj.UPDATED_BY = req.body.created_by;
        // appGrandChildEntity.save(grandChildObj).then((data) => {
        //     res.send(data)
        // }
        // ).catch((error) => {
        //     res.send(error)
        // });
        flatOwnerRepository.save(flatOwnerObj).then(async (data) => {
            // res.send(data)
            req.body.id = flatOwnerObj.FLAT_OWNER_ID;
            req.body.flatOwnerObj = flatOwnerObj;
            req.body.data = data;
            next();
        }
        ).catch((error) => {
            res.send(error)
        });

        }



    }

    static ownerAppUpdate = async (req: Request, res: Response, next) => {
        console.log("ownerAppUpdate")
        const flatOwnerRepository = getRepository(FlatOwners);
        // const appartmentRepository = getRepository(Appartment);
        const associationRepository = getRepository(ASSOCIATION);

        const flatOwnerObj = await flatOwnerRepository.findOne(req.body.id);
        // const appartmentObj = await appartmentRepository.findOne(req.body.appartmentId);
        const associationObj = await associationRepository.findOne(req.body.associationId);
    
        flatOwnerObj.OWNER_FIRST_NAME = req.body.first_name;
        flatOwnerObj.OWNER_LAST_NAME = req.body.last_name;
        flatOwnerObj.OWNER_EMAIL = req.body.email;
        flatOwnerObj.OWNER_MOBILE_NUMBER = req.body.mobile_number;
        // flatOwnerObj.OWNER_PIC = req.body.owner_pic;
        //flatOwnerObj.OWNER_ADDRESS = req.body.address;
        //flatOwnerObj.IS_ACTIVE = true;
        // flatOwnerObj.appartmentEntiy = appartmentObj;
        flatOwnerObj.associationEntity = associationObj;
        flatOwnerObj.UPDATED_BY = req.body.updated_by;
        flatOwnerRepository.save(flatOwnerObj).then(async (data) => {
            console.log(data);
            // req.body.id = flatOwnerResObj.FLAT_OWNER_ID;
            // req.body.flatOwnerResObj = flatOwnerResObj;
            // req.body.appUserObjres = appUserObjres;
            req.body.data = data;
            next();
         //   res.send(data);
            // req.body.id = flatOwnerObj.FLAT_OWNER_ID;
            // req.body.flatOwnerObj = flatOwnerObj;
            // req.body.data = data;
            // next();
        }
        ).catch((error) => {
            console.log(error);
            res.send(error)
        });
    }

    static getflatOwnerTenant = async (req: Request, res: Response) => {
        const flatOwnerRepository = getRepository(FlatOwners);
        const flatTenantRepository = getRepository(FlatTenants);
var flatOwnerObj = await flatOwnerRepository.find({
    where : {
        associationEntity : req.params.id
    }
})
var flatTenantObj = await flatTenantRepository.find({
    where : {
        associationEntity :req.params.id
    }
})

flatOwnerObj.length;

res.json({
    flatOwner : flatOwnerObj.length,
    flatTenant : flatTenantObj.length
})


    }

    static flatOwnerInformation = async (req: Request, res: Response) => {
        const flatOwnerRepository = getRepository(FlatOwners);
        flatOwnerRepository.find({
            where: {
                FLAT_OWNER_ID: req.params.id
            }
        }).then((data) => {
            console.log(data);
            res.send(data);
        }).catch((error) => {
            res.send(error);
        });
    }

}

export default FlatOwnerController;