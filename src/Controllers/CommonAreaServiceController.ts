import { Request, Response } from "express";
import * as jwt from "jsonwebtoken";
import { getRepository } from "typeorm";
import { CommonAreaServices } from "../entity/CommonAreaService";
import { ASSOCIATION_COM_SERVICES } from "../entity/AssociationComServices";

class CommonAreaServiceController {
    static addService = async (req: Request, res: Response, next) => {
        const comServiceSubRepository = getRepository(CommonAreaServices);
        comServiceSubRepository.find({
            where: {
                COM_SERVICE_NAME: req.body.serviceName
            }
        }).then((data) => {
console.log(data)

            if(data.length > 0){

                res.status(201).send({
                    message: "Service Already Available",
                    obj: data
                })

            }
            else {
                const comService = new CommonAreaServices();
                comService.COM_SERVICE_NAME = req.body.serviceName;
                comService.DESCRIPTION = req.body.description;
                comService.CREATED_BY = req.body.created_by;
                comService.UPDATED_BY = req.body.created_by;
        
                const comSObj = comServiceSubRepository.create(comService);
                comServiceSubRepository.save(comSObj).then((data) => {
                    req.body.comServiceId = data.COM_SERVICE_ID;
                    req.body.data = data;
        
                    next();
                    //    res.send(data)
                }
                ).catch((error) => {
                    res.send(error)
                });


            }

        })
       // const comServiceSubRepository = getRepository(CommonAreaServices);
      
    }

    static getAllService = async (req: Request, res: Response) => {
        const comServiceSubRepository = getRepository(CommonAreaServices);
        const results = await comServiceSubRepository.find({}).then(
            data => {
                res.send(data);
            }
        ).catch(err => {
            console.log(err);
        });
    }

    static getAppAllService = async (req: Request, res: Response) => {
        const comServiceSubRepository = getRepository(CommonAreaServices);
        const results = await comServiceSubRepository.find({}).then(
            data => {
                res.send(data);
            }
        ).catch(err => {
            console.log(err);
        });
    }
    static getComServiceById = async (req: Request, res: Response) => {
        const comServiceSubRepository = getRepository(CommonAreaServices);
        const results = await comServiceSubRepository.findOne({
            where : {
                COM_SERVICE_ID : req.params.id   
            }
        }).then(
            data => {
                res.send(data);
            }
        ).catch(err => {
            console.log(err);
        });
    }

    static getAppAllServiceAssociation = async (req: Request, res: Response) => {
        const asscomServiceSubRepository = getRepository(ASSOCIATION_COM_SERVICES);
        const results = await asscomServiceSubRepository.find({
            relations:['comServices']
        }).then(
            data => {
                res.send(data);
            }
        ).catch(err => {
            console.log(err);
        });
    }

    static searchService = async (req: Request, res: Response) => {
        const associationindServiceRepository = getRepository(ASSOCIATION_COM_SERVICES);
        const loadedPost = await getRepository(ASSOCIATION_COM_SERVICES)
        .createQueryBuilder("association_services")
        .leftJoinAndSelect("association_services.comServices", "comService", "comService.COM_SERVICE_ID = association_services.comServices")
        .where("COM_SERVICE_NAME like :id", { id: '%' +  req.params.id + '%' })
        .getMany();
        res.status(200).send(loadedPost);
    }

    static updateService = async (req: Request, res: Response) => {
        const comServiceSubRepository = getRepository(CommonAreaServices);
        const comService = await comServiceSubRepository.findOne(req.body.serviceId);
        // const mergedUser = indServiceRepository.merge(user,req.body);
        comService.COM_SERVICE_NAME = req.body.serviceName;
        comService.UPDATED_BY = req.body.last_updated_by;

        comServiceSubRepository.save(comService).then(data => {
            res.send(data);
        }
        ).catch(err => {
            console.log(err);
        });
    }


    static updateFileName = async (req: Request, res: Response, next) => {
        const comServiceRepository = getRepository(CommonAreaServices);
        const comService = await comServiceRepository.findOne(req.body.comServiceId);
        if (req.body.com_image_name) {
            comService.COM_IMAGE_NAME = req.body.com_image_name;
        }
        if (req.body.com_icon_name) {
            comService.COM_ICON_NAME = req.body.com_icon_name;
        }
        comServiceRepository.save(comService).then(data => {
            res.send(data);
        }).catch((error) => {
            res.status(500).send(error)
        });
    }
 

    static deleteService = async (req: Request, res: Response) => {
        const comServiceSubRepository = getRepository(CommonAreaServices);
        const results = await comServiceSubRepository.delete(req.body.id);

        // const users = await connection.manager.find(User);
        res.send(results);
    }
}

export default CommonAreaServiceController;