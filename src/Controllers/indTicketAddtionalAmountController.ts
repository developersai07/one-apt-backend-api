import { Request, Response } from "express";
import * as jwt from "jsonwebtoken";
import { getRepository } from "typeorm";
import { IND_TICKETS_ADDITIONAL_AMOUNTS } from "../entity/IndTicketAdditionAmount";
import { IndividualTickets } from "../entity/IndividualTickets";
import { VENDOR } from "../entity/Vendors";

class indTicketAddtionalAmountController {
    static addTicketAmount = async (req: Request, res: Response) => {
        const  indTicketAdditionalAmount = getRepository(IND_TICKETS_ADDITIONAL_AMOUNTS);
        const  indTicketRep = getRepository(IndividualTickets);
        const  vendorRep = getRepository(VENDOR);
     const indTicketObj = await   indTicketRep.findOne(req.body.indTicketId);
     const vendorObj = await   vendorRep.findOne(req.body.vendorId);

        

     const indTicketAmount    = new IND_TICKETS_ADDITIONAL_AMOUNTS();
     indTicketAmount.AMOUNT = req.body.amount;
     indTicketAmount.REASON = req.body.reason;
     
     indTicketAmount.CREATED_BY = req.body.created_by;
     indTicketAmount.UPDATED_BY = req.body.created_by;
     indTicketAmount.indTickets =indTicketObj;
     indTicketAmount.vendor = vendorObj;

     indTicketAdditionalAmount.save(indTicketAmount).then((data)=>{
         res.send(data)
     }).catch((error)=>{

console.log(error)
         res.status(500).send(error)
     })



        
    }



}

export default indTicketAddtionalAmountController;