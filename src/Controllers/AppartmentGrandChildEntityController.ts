import { Request, Response } from "express";
import * as jwt from "jsonwebtoken";
import { getRepository } from "typeorm";
import { APPARTMENT_GRAND_CHILD_ENTITIES } from "../entity/AppartmentGrandChildEntity";
import { APPARTMENT_CHILD_ENTITIES } from "../entity/AppartmentChildEntites";
import { Appartment } from "../entity/AppartmentEntity";

class AppartmentGrandChildEntityController {
    
    static getall = async (req: Request, res: Response) => {
        const appGrandChildEntity = getRepository(APPARTMENT_GRAND_CHILD_ENTITIES);
        appGrandChildEntity.find({
            relations: ['APPARTMENT_CHILD_ENTITY_ID'],
        }).then((data) => {
            res.send(data)
        }
        ).catch((error) => {
            res.send(error)
        });
    }

    static getGrandChild = async (req: Request, res: Response) => {
        const appGrandChildEntity = getRepository(APPARTMENT_GRAND_CHILD_ENTITIES);
        const result = await appGrandChildEntity.find({
            // relations: ['country'],
            where: { APPARTMENT_CHILD_ENTITY_ID: req.params.id }
        })
        res.send(result);
    }

    static add = async (req: Request, res: Response) => {
        const appartmentChildEntity = getRepository(APPARTMENT_CHILD_ENTITIES);
        const appGrandChildEntity = getRepository(APPARTMENT_GRAND_CHILD_ENTITIES);
        const childEntityObj = await appartmentChildEntity.findOne(req.body.childId);

        const appGrandChildObj = new APPARTMENT_GRAND_CHILD_ENTITIES();
        appGrandChildObj.APPARTMENT_CHILD_ENTITY_ID = childEntityObj;
        appGrandChildObj.GARND_CHILD_NAME = req.body.grandChildName;
        appGrandChildObj.CREATED_BY = req.body.created_by;
        appGrandChildObj.UPDATED_BY = req.body.created_by;
        const appGrandChildObjC = await appGrandChildEntity.create(appGrandChildObj);
        appGrandChildEntity.save(appGrandChildObjC).then((data) => {
            res.send(data)
        }
        ).catch((error) => {
            res.send(error)
        });
    }


    static update = async (req: Request, res: Response) => {
        const appartmentChildEntity = getRepository(APPARTMENT_CHILD_ENTITIES);
        const appGrandChildEntity = getRepository(APPARTMENT_GRAND_CHILD_ENTITIES);
        const childEntityObj = await appartmentChildEntity.findOne(req.body.childId);
        const grandChildObj = await appGrandChildEntity.findOne(req.body.id);
        grandChildObj.APPARTMENT_CHILD_ENTITY_ID = childEntityObj;
        grandChildObj.GARND_CHILD_NAME = req.body.grandChildName;
        grandChildObj.UPDATED_BY = req.body.created_by;
        appGrandChildEntity.save(grandChildObj).then((data) => {
            res.send(data)
        }
        ).catch((error) => {
            res.send(error)
        });
    }

    static childEntityDelete = async (req: Request, res: Response) => {
        const appartmentGrandChildEntity = getRepository(APPARTMENT_GRAND_CHILD_ENTITIES);
        const results = await appartmentGrandChildEntity.findOne(req.body.id);


        results.APPARTMENT_CHILD_ENTITY_ID = null;

        //  console.log(results);
        const results2 = await appartmentGrandChildEntity.save(results);
        const resd = await appartmentGrandChildEntity.delete(req.body.id);
        res.send(resd);
    }


}



export default AppartmentGrandChildEntityController;