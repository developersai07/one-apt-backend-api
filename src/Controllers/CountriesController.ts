import { Request, Response } from "express";
import * as jwt from "jsonwebtoken";
import { getRepository } from "typeorm";
import { COUNTRIES } from "../entity/Countries";

class CountriesController {
    static countriesgetall = async (req: Request, res: Response) => {
        const countriesrep = getRepository(COUNTRIES);
        const results = await countriesrep.find({
            // relations: ['countries']
        }).then(
            data => {
                console.log(data);
                res.send(data);
            }).catch(err => {
                console.log(err);
            });
    }



}

export default CountriesController;