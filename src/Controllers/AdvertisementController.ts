import { Request, Response } from "express";
import * as jwt from "jsonwebtoken";
import { getRepository } from "typeorm";
import { AdvertisementEntity } from "../entity/Advertisement";

class AdvertisementController {
    static addAdvertisement = async (req: Request, res: Response, next) => {
        console.log("addAdvertisement");
        const advertisementRepository = getRepository(AdvertisementEntity);
        const adv = new AdvertisementEntity();
        adv.TITLE = req.body.title;
        adv.DESCRIPTION = req.body.description;
        adv.CREATED_BY = req.body.created_by;
        adv.UPDATED_BY = req.body.created_by;

        const advObj = advertisementRepository.create(adv);
        advertisementRepository.save(advObj).then((data) => {
            req.body.advId = data.ID;
            req.body.data = data;

            next();
            //    res.send(data)
        }
        ).catch((error) => {
            res.send(error)
        });
    }

    static updateAdvertisement = async (req: Request, res: Response, next) => {
        console.log("update")
        const advertisementRepository = getRepository(AdvertisementEntity);
        const adv = await advertisementRepository.findOne(req.body.advId);
        adv.TITLE = req.body.title;
        adv.UPDATED_BY = req.body.last_updated_by;
        advertisementRepository.save(adv).then(data => {
            req.body.advId = data.ID;
            req.body.data = data;

            next();
            // res.send(data);
        }
        ).catch(err => {
            console.log(err);
        });
    }

    static updateFileName = async (req: Request, res: Response, next) => {
        const advertisementRepository = getRepository(AdvertisementEntity);
        const adv = await advertisementRepository.findOne(req.body.advId);
        if (req.body.adv_image_name) {
            adv.IMAGE = req.body.adv_image_name;
        }
        advertisementRepository.save(adv).then(data => {
            res.send(data);
        }).catch((error) => {
            res.status(500).send(error)
        });
    }

    static getAllAdvertisement = async (req: Request, res: Response) => {
        const advertisementRepository = getRepository(AdvertisementEntity);
        const results = await advertisementRepository.find({}).then(
            data => {
                res.send(data);
            }
        ).catch(err => {
            console.log(err);
        });
    }
    static getAppAllAdvertisement = async (req: Request, res: Response) => {
        const advertisementRepository = getRepository(AdvertisementEntity);
        const results = await advertisementRepository.find({}).then(
            data => {
                res.send(data);
            }
        ).catch(err => {
            console.log(err);
        });
    }

    

    static deleteAdvertisement = async (req: Request, res: Response) => {
        console.log("delete: " + req.body.id);
        const advertisementRepository = getRepository(AdvertisementEntity);
        const results = await advertisementRepository.delete(req.body.id);
        res.send(results);
    }

}
export default AdvertisementController;