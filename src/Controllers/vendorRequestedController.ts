import { Request, Response } from "express";
import * as jwt from "jsonwebtoken";
import { getRepository, AdvancedConsoleLogger } from "typeorm";
import { VENDORREQUESTEDASSOCIATION } from "../entity/VendorRequestedAssociations";
import { ASSOCIATION } from "../entity/Associations";
import { VENDORASSOCIATION } from "../entity/VendorAssociation";
import { VENDOR } from "../entity/Vendors";
import { ASSOCIATION_VENDORS } from "../entity/AssociationVendors";
import { IndividualTickets } from '../entity/IndividualTickets'
import { MapIndTicketVendor } from '../entity/MapIndTicketsVendor'
import { CommonAreaTickets } from "../entity/CommonAreaTickets";
import { MapComTicketVendor } from "../entity/MapComTIcketVendor";
import { IND_SER_TYPES_VENDOR_ASSOCIATION } from '../entity/IndSerTypesVendorAssociation';
import {INTERNAL_SERVICES_AMOUNT_VENDORS} from '../entity/InternalVendorServiceAmount';
import { AppUserDetails } from "../entity/AppUserDetails";
import { FlatOwners } from "../entity/FlatOwners";
import { FlatTenants } from "../entity/FlatTenants";
import OwnerIndTicketSendMail from "../Mails/OwnerIndTicketSendMail";
import ownerComTicketSendMail from "../Mails/OwnerComTicketSendMail";
import {IND_SERVICE_PLANS} from '../entity/indServicePlan';
import vendorSendMail from "../Mails/VendorSendMail";
class vendorRequestedController {
    static request = async (req: Request, res: Response, next) => {
        const association = getRepository(ASSOCIATION);
        const vassociation = getRepository(VENDORASSOCIATION);

        const vrequestedAssociation = getRepository(VENDORREQUESTEDASSOCIATION);
        const associationObj = await association.findOne(req.body.associationid);
        const vassociationObj = await vassociation.findOne(req.body.vassociationid);
        const vasscoiation = new VENDORREQUESTEDASSOCIATION();
        vasscoiation.DESCRIPTION = req.body.description;
        vasscoiation.IS_ACCEPTED = false;
        vasscoiation.IS_REJECTED = false;
        vasscoiation.CREATED_BY = req.body.created_by;
        vasscoiation.UPDATED_BY = req.body.created_by;
        vasscoiation.association = associationObj;
        vasscoiation.vassociation = vassociationObj;

        const vreqAssObj = await vrequestedAssociation.create(vasscoiation);

        vrequestedAssociation.save(vreqAssObj).then((data) => {
            res.send(data)
        }).catch((error) => {
            res.status(500).send(error)
        })


    }

    static getMappedAssociations = async (req: Request, res: Response, next) => {

        const assVendorRep = getRepository(ASSOCIATION_VENDORS);
        assVendorRep.find({
            where: {
                vendorAssociation: req.params.id
            }, relations: ["association"]
        }).then((data) => {
            res.send(data)
        })

    }

    static AssociationAssignIndTicket = async (req: Request, res: Response, next) => {
        const mapInd = await getRepository(MapIndTicketVendor).query("SELECT * FROM map_ind_tickets_vendors WHERE IND_TICKET_ID = '"+req.body.indTicketId+"' AND VENDOR_ID ='"+req.body.vendorId+"' AND IS_ACTIVE = true ");

        if(mapInd.length > 0){
            console.log("xya")
        res.json({
            msg : "Assigning to same vendor"
        })
        }

        else {

            const appUserDetailRep = getRepository(AppUserDetails);

            const indTicketRepository = getRepository(IndividualTickets);
        
            const flatOwnersRep = getRepository(FlatOwners);
            const flatTenantsRep = getRepository(FlatTenants);
   const indOpObj = await indTicketRepository.findOne({
    where : {
        IND_TICKET_ID : req.body.indTicketId
    },

    relations: ["appUserDetails","association","serviceId","indSerPlan","indServiceTypes","association","appUserDetails.flatOwner","appUserDetails.flatTenant"]
})


const appUser = await appUserDetailRep.findOne(

    {
        where: {
            APP_USER_ID: indOpObj.appUserDetails.APP_USER_ID
        },
        relations: ["flatOwner", "flatTenant"]
    }

    // req.body.appUserId
);

var email;

if (appUser.flatOwner) {
    console.log("flatOwner")
    var flatOwnerId = appUser.flatOwner.FLAT_OWNER_ID;
    const flatOwnerObj = await flatOwnersRep.findOne(flatOwnerId);
    email = flatOwnerObj.OWNER_EMAIL;
    req.body.appUserEmail = email;
    req.body.appUserName =   flatOwnerObj.OWNER_FIRST_NAME ;
    req.body.appUserChildName =   flatOwnerObj.associationChildEntity.CHILD_NAME ;
    req.body.appUserGrandchild =  flatOwnerObj.associationGrandChildEntity.GARND_CHILD_NAME;

}

if (appUser.flatTenant) {
    console.log("flatTenant")
    var flatTenantId = appUser.flatTenant.TENANT_ID;
    const flatTenantObj = await flatTenantsRep.findOne(flatTenantId);
    email = flatTenantObj.TENANT_EMAIL;
    req.body.appUserEmail = email;
    req.body.appUserName =   flatTenantObj.TENANT_FIRST_NAME ;
    req.body.appUserChildName =   flatTenantObj.associationChildEntity.CHILD_NAME ;
    req.body.appUserGrandchild =  flatTenantObj.associationGrandChildEntity.GARND_CHILD_NAME;

}

            const prioryOneElVendor = await getRepository(MapIndTicketVendor).query("UPDATE map_ind_tickets_vendors SET IS_ACTIVE= false  WHERE IND_TICKET_ID = '" + req.body.indTicketId + "' ")
            const vendorRep = await getRepository(VENDOR);
            const individualTicketRep = await getRepository(IndividualTickets);
            const vendorObj = await vendorRep.findOne(req.body.vendorId);
            const indObj = await individualTicketRep.findOne(req.body.indTicketId,{
                relations:["serviceId","indServiceTypes"]
            });
            const mapIndVendorRep = await getRepository(MapIndTicketVendor);
            console.log(indObj)
            const mapVendorObj = new MapIndTicketVendor();
            mapVendorObj.vendor = vendorObj;
            mapVendorObj.indTickets = indObj;
            mapVendorObj.IS_ACCEPTED = false;
            mapVendorObj.IS_REJECTED = false;
            mapVendorObj.IS_ACTIVE = true;
            var samapVendorObj = await mapIndVendorRep.create(mapVendorObj);
            mapIndVendorRep.save(samapVendorObj).then(async (data) => {

                if (indObj.indServiceTypes) {

                    console.log("jkkjkjsf")
                    req.body.serviceId

                    const interServAmountVendor = await getRepository(INTERNAL_SERVICES_AMOUNT_VENDORS)
                        .createQueryBuilder("interServVendor")
                        .where("interServVendor.IND_SERVICE_ID =  :indServiceId AND interServVendor.SERVICE_TYPE_ID = :indServiceTypeId AND VENDOR_ID = :vendorId ", {
                            indServiceId: indObj.serviceId.IND_SERVICE_ID,
                            indServiceTypeId: indObj.indServiceTypes.IND_SERVICE_TYPE_ID,
                            vendorId: req.body.vendorId
                        }).getOne().then(async (data) => {

                            var amount = data.AMOUNT;

                            if (indObj.QUANTITY) {

                                amount = amount * indObj.QUANTITY;
                            }

                        if(indObj.indSerPlan.PLAN == 'Monthly'){
                        const indServPlanRep = getRepository(IND_SERVICE_PLANS);
                        const indServiceMapSub = await indServPlanRep.findOne(indObj.indSerPlan.IND_SERVICE_PLAN_ID);
                        var days =  indServiceMapSub.DAYS;
                        indObj.AMOUNT = amount * days;
                        }
                        else {
                        indObj.AMOUNT = amount;
                        }

                            var esult = await individualTicketRep.save(indObj);


                            
                    var appUserDetails = req.body.appUserName+'-'+req.body.appUserChildName+'-'+ req.body.appUserGrandchild; 
                    vendorSendMail.vendoIndSendMail(indObj.IND_TICKET_ID, indObj.TICKET_DESCRIPTION,   vendorObj.VENDOR_EMAIL, indObj.TICKET_DATE, indObj.TICKET_TIME,   indObj.serviceId.IND_SERVICE_NAME, indObj.indServiceTypes.TYPE, indObj.QUANTITY ,amount,indObj.indSerPlan.PLAN,indObj.association.ASSOCIATION_NAME,  appUserDetails)
                              
                    OwnerIndTicketSendMail.IndTicketSendMail(indObj.IND_TICKET_ID, indObj.TICKET_DESCRIPTION,   vendorObj.VENDOR_EMAIL, indObj.TICKET_DATE, indObj.TICKET_TIME,   indObj.serviceId.IND_SERVICE_NAME, indObj.indServiceTypes.TYPE, indObj.QUANTITY ,amount,indObj.indSerPlan.PLAN,vendorObj.VENDOR_NAME);
                        }).catch((error) => {
                            console.log(error)
                        });




                }
                else {

                    const interServAmountVendor = await getRepository(INTERNAL_SERVICES_AMOUNT_VENDORS)
                        .createQueryBuilder("interServVendor")
                        .where("interServVendor.IND_SERVICE_ID =  :indServiceId  AND VENDOR_ID = :vendorId", {
                            indServiceId:indObj.serviceId.IND_SERVICE_ID,
                            vendorId: req.body.vendorId
                        }).getOne().then(async (data) => {
                            var amount = data.AMOUNT;
                            if (indObj.QUANTITY) {

                                amount = amount * indObj.QUANTITY;
                            }

                            indObj.AMOUNT = amount;

                            var esult = await individualTicketRep.save(indObj);

                                         
                    var appUserDetails = req.body.appUserName+'-'+req.body.appUserChildName+'-'+ req.body.appUserGrandchild; 
                    vendorSendMail.vendoIndSendMail(indObj.IND_TICKET_ID, indObj.TICKET_DESCRIPTION,   vendorObj.VENDOR_EMAIL, indObj.TICKET_DATE, indObj.TICKET_TIME,   indObj.serviceId.IND_SERVICE_NAME, indObj.indServiceTypes.TYPE, indObj.QUANTITY ,amount,indObj.indSerPlan.PLAN,indObj.association.ASSOCIATION_NAME,  appUserDetails)
                              
                    OwnerIndTicketSendMail.IndTicketSendMail(indObj.IND_TICKET_ID, indObj.TICKET_DESCRIPTION,   vendorObj.VENDOR_EMAIL, indObj.TICKET_DATE, indObj.TICKET_TIME,   indObj.serviceId.IND_SERVICE_NAME, indObj.indServiceTypes.TYPE, indObj.QUANTITY ,amount,indObj.indSerPlan.PLAN,vendorObj.VENDOR_NAME);
                        }).catch((error) => {
                            console.log(error)
                        });


                }



            res.send(samapVendorObj)    

            });


        }



    }

    static AssignIndTicket = async (req: Request, res: Response, next) => {
        const mapInd = await getRepository(MapIndTicketVendor).query("SELECT * FROM map_ind_tickets_vendors WHERE IND_TICKET_ID = '"+req.body.indTicketId+"' AND VENDOR_ID ='"+req.body.vendorId+"' AND IS_ACTIVE = true ")
        console.log(req.body.vendorId)
console.log(mapInd)

if(mapInd.length > 0){
    console.log("xya")
res.json({
    msg : "Assigning to same vendor"
})
}
else {

    const appUserDetailRep = getRepository(AppUserDetails);

    const indTicketRepository = getRepository(IndividualTickets);

    const flatOwnersRep = getRepository(FlatOwners);
    const flatTenantsRep = getRepository(FlatTenants);

    const associationRepository = getRepository(ASSOCIATION);

   const indOpObj = await indTicketRepository.findOne({
        where : {
            IND_TICKET_ID : req.body.indTicketId
        },

        relations: ["appUserDetails","association","serviceId","indSerPlan","indServiceTypes","association","appUserDetails.flatOwner","appUserDetails.flatTenant"]
    })


    const appUser = await appUserDetailRep.findOne(

        {
            where: {
                APP_USER_ID: indOpObj.appUserDetails.APP_USER_ID
            },
            relations: ["flatOwner", "flatTenant"]
        }

        // req.body.appUserId
    );

    var email;

    if (appUser.flatOwner) {
        console.log("flatOwner")
        var flatOwnerId = appUser.flatOwner.FLAT_OWNER_ID;
        const flatOwnerObj = await flatOwnersRep.findOne(flatOwnerId);
        email = flatOwnerObj.OWNER_EMAIL;
        req.body.appUserEmail = email;
        req.body.appUserName =   flatOwnerObj.OWNER_FIRST_NAME ;
        req.body.appUserChildName =   flatOwnerObj.associationChildEntity.CHILD_NAME ;
        req.body.appUserGrandchild =  flatOwnerObj.associationGrandChildEntity.GARND_CHILD_NAME;

    }

    if (appUser.flatTenant) {
        console.log("flatTenant")
        var flatTenantId = appUser.flatTenant.TENANT_ID;
        const flatTenantObj = await flatTenantsRep.findOne(flatTenantId);
        email = flatTenantObj.TENANT_EMAIL;
        req.body.appUserEmail = email;
        req.body.appUserName =   flatTenantObj.TENANT_FIRST_NAME ;
        req.body.appUserChildName =   flatTenantObj.associationChildEntity.CHILD_NAME ;
        req.body.appUserGrandchild =  flatTenantObj.associationGrandChildEntity.GARND_CHILD_NAME;

    }

    console.log("abashdabhd")
    const prioryOneElVendor = await getRepository(MapIndTicketVendor).query("UPDATE map_ind_tickets_vendors SET IS_ACTIVE= false  WHERE IND_TICKET_ID = '" + req.body.indTicketId + "' ")
    const vendorRep = await getRepository(VENDOR);
    const individualTicketRep = await getRepository(IndividualTickets);
    const vendorObj = await vendorRep.findOne(req.body.vendorId);
    const indObj = await individualTicketRep.findOne(req.body.indTicketId,{
        relations:["serviceId","indServiceTypes"]
    });
    const mapIndVendorRep = await getRepository(MapIndTicketVendor);
    console.log(indObj)
    const mapVendorObj = new MapIndTicketVendor();
    mapVendorObj.vendor = vendorObj;
    mapVendorObj.indTickets = indObj;
    mapVendorObj.IS_ACCEPTED = false;
    mapVendorObj.IS_REJECTED = false;
    mapVendorObj.IS_ACTIVE = true;
    var samapVendorObj = await mapIndVendorRep.create(mapVendorObj);
    mapIndVendorRep.save(samapVendorObj).then(async (data) => {
        if (indObj.indServiceTypes) {
            const interServAmountVendorAssociation = await getRepository(IND_SER_TYPES_VENDOR_ASSOCIATION)
                .createQueryBuilder("interServAmountVendorAssociation")
                .where("interServAmountVendorAssociation.IND_SERVICE_ID =  :indServiceId AND interServAmountVendorAssociation.IND_SERVICE_TYPE_ID = :indServiceTypeId AND VENDOR_ASSOCIATION_ID = :vendorAsociationId", {
                    indServiceId: indObj.serviceId.IND_SERVICE_ID,
                    indServiceTypeId: indObj.indServiceTypes.IND_SERVICE_TYPE_ID,
                    vendorAsociationId: req.body.vAssociationId
                }).getOne().then(async (data) => {
                    var amount = data.AMOUNT;


                    if (indObj.QUANTITY) {
                        var quantity = indObj.QUANTITY;

                        amount = amount * quantity;
                    }

                  if(indObj.indSerPlan.PLAN == 'Monthly'){
     const indServPlanRep = getRepository(IND_SERVICE_PLANS);
        const indServiceMapSub = await indServPlanRep.findOne(indObj.indSerPlan.IND_SERVICE_PLAN_ID);
       var days =  indServiceMapSub.DAYS;
       indObj.AMOUNT = amount * days;
  
                                }
                                else {

                                    indObj.AMOUNT = amount;
                                    console.log(amount + "amount")
    
                                }


                    var esult = await individualTicketRep.save(indObj);

                    var appUserDetails = req.body.appUserName+'-'+req.body.appUserChildName+'-'+ req.body.appUserGrandchild; 
                    vendorSendMail.vendoIndSendMail(indObj.IND_TICKET_ID, indObj.TICKET_DESCRIPTION,   vendorObj.VENDOR_EMAIL, indObj.TICKET_DATE, indObj.TICKET_TIME,   indObj.serviceId.IND_SERVICE_NAME, indObj.indServiceTypes.TYPE, indObj.QUANTITY ,amount,indObj.indSerPlan.PLAN,indObj.association.ASSOCIATION_NAME,  appUserDetails)
                              
                    OwnerIndTicketSendMail.IndTicketSendMail(indObj.IND_TICKET_ID, indObj.TICKET_DESCRIPTION,   vendorObj.VENDOR_EMAIL, indObj.TICKET_DATE, indObj.TICKET_TIME,   indObj.serviceId.IND_SERVICE_NAME, indObj.indServiceTypes.TYPE, indObj.QUANTITY ,amount,indObj.indSerPlan.PLAN,vendorObj.VENDOR_NAME);
                }).catch((error) => {
                    console.log(error);
                });




        }
        else {

            const interServAmountVendorAssociation = await getRepository(IND_SER_TYPES_VENDOR_ASSOCIATION)
                .createQueryBuilder("interServAmountVendorAssociation")
                .where("interServAmountVendorAssociation.IND_SERVICE_ID =  :indServiceId AND VENDOR_ASSOCIATION_ID = :vendorAsociationId", {
                    indServiceId:  indObj.serviceId.IND_SERVICE_ID,
                    vendorAsociationId: req.body.vAssociationId
                }).getOne().then(async (data) => {
                    var amount = data.AMOUNT;


                    if (req.body.quantity) {

                        amount = amount * req.body.quantity;
                    }

                    // var esult = await individualTicketRep.save(indObj);
                    if(indObj.indSerPlan.PLAN == 'Monthly'){
                        const indServPlanRep = getRepository(IND_SERVICE_PLANS);
                           const indServiceMapSub = await indServPlanRep.findOne(indObj.indSerPlan.IND_SERVICE_PLAN_ID);
                          var days =  indServiceMapSub.DAYS;
                          indObj.AMOUNT = amount * days;
                     
                                                   }
                                                   else {
                   
                                                       indObj.AMOUNT = amount;
                                                       console.log(amount + "amount")
                       
                                                   }

                    var esult = await individualTicketRep.save(indObj);
                }).catch((error) => {
                    console.log(error);
                });

        }




        res.send(samapVendorObj);


    

    })
}

     

    }

    static AssignComTicketByAssociation = async (req: Request, res: Response, next) => {

        const commonTIcketRep = await getRepository(CommonAreaTickets);
        const mapCom = await getRepository(MapIndTicketVendor).query("SELECT * FROM map_com_tickets_vendors WHERE COM_TICKET_ID = '"+req.body.comTicketId+"' AND VENDOR_ID ='"+req.body.vendorId+"' AND IS_ACTIVE = true ")
        console.log(req.body.vendorId)
        if(mapCom.length > 0){
            console.log("xya")
        res.json({
            msg : "Assigning to same vendor"
        })
        }

        else {
            const prioryOneElVendor = await getRepository(MapComTicketVendor).query("UPDATE map_com_tickets_vendors SET IS_ACTIVE= false  WHERE COM_TICKET_ID = '" + req.body.comTicketId + "' ")
            const vendorRep = await getRepository(VENDOR);
            const commonTicketRep = await getRepository(CommonAreaTickets);

            
    const comObjAss= await commonTIcketRep.findOne({
        where : {
           COM_TICKET_ID : req.body.comTicketId
        },
       relations : ["association","serviceId","comService","indSerPlan","indServiceTypes"]
   })
            const vendorObj = await vendorRep.findOne(req.body.vendorId);
            const comObj = await commonTicketRep.findOne(req.body.comTicketId,{
                relations:["serviceId","indServiceTypes"]
            });
            const mapComVendorRep = await getRepository(MapComTicketVendor);
            console.log(comObj)
            const mapVendorObj = new MapComTicketVendor();
            mapVendorObj.vendor = vendorObj;
            mapVendorObj.comTickets = comObj;
            mapVendorObj.IS_ACCEPTED = false;
            mapVendorObj.IS_REJECTED = false;
            mapVendorObj.IS_ACTIVE = true;
            var samapVendorObj = await mapComVendorRep.create(mapVendorObj);
            mapComVendorRep.save(samapVendorObj).then(async (data) => {


                if (comObj.indServiceTypes.IND_SERVICE_TYPE_ID) {
                    req.body.serviceId

                    const interServAmountVendor = await getRepository(INTERNAL_SERVICES_AMOUNT_VENDORS)
                        .createQueryBuilder("interServVendor")
                        .where("interServVendor.IND_SERVICE_ID =  :indServiceId AND interServVendor.SERVICE_TYPE_ID = :indServiceTypeId AND VENDOR_ID = :vendorId ", {
                            indServiceId: comObj.serviceId.IND_SERVICE_ID,
                            indServiceTypeId: comObj.indServiceTypes.IND_SERVICE_TYPE_ID,
                            vendorId: req.body.vendorId
                        }).getOne().then(async (data) => {

                            var amount = data.AMOUNT;


                            if (comObj.QUANTITY) {

                                amount = amount * comObj.QUANTITY;
                            }

                            comObj.AMOUNT = amount;
                            console.log(amount + "amount")

                            var esult = await commonTIcketRep.save(comObj);

                            
                            vendorSendMail.vendoComSendMail(comObjAss.COM_TICKET_ID,comObjAss.TICKET_DESCRIPTION,vendorObj.VENDOR_EMAIL,comObjAss.TICKET_DATE,comObjAss.TICKET_TIME,comObjAss.comService.COM_SERVICE_NAME,comObjAss.serviceId.IND_SERVICE_NAME,
                                comObjAss.indServiceTypes.TYPE,comObjAss.QUANTITY,comObjAss.AMOUNT,comObjAss.indSerPlan.PLAN,comObjAss.association.ASSOCIATION_NAME)

                            ownerComTicketSendMail.ComTicketSendMail(comObjAss.COM_TICKET_ID,comObjAss.TICKET_DESCRIPTION,comObjAss.association.EMAIL,comObjAss.TICKET_DATE,comObjAss.TICKET_TIME,comObjAss.comService.COM_SERVICE_NAME,comObjAss.serviceId.IND_SERVICE_NAME,comObjAss.indSerPlan.PLAN,
                                comObjAss.indServiceTypes.TYPE,comObjAss.QUANTITY,comObjAss.AMOUNT,vendorObj.VENDOR_NAME)
                        }).catch((error) => {
                            console.log(error)
                        });




                }
                else {

                    const interServAmountVendor = await getRepository(INTERNAL_SERVICES_AMOUNT_VENDORS)
                        .createQueryBuilder("interServVendor")
                        .where("interServVendor.IND_SERVICE_ID =  :indServiceId  AND VENDOR_ID = :vendorId", {
                            indServiceId: comObj.serviceId.IND_SERVICE_ID,
                            vendorId: req.body.vendorId
                        }).getOne().then(async (data) => {
                            var amount = data.AMOUNT;


                            if (comObj.QUANTITY) {

                                amount = amount * comObj.QUANTITY;
                            }

                            comObj.AMOUNT = amount;

                            var esult = await commonTIcketRep.save(comObj);
                        }).catch((error) => {
                            console.log(error)
                        });


                }





            })



            res.send(samapVendorObj)


        }

    }


    static AssignComTicket = async (req: Request, res: Response, next) => {
        const commonTIcketRep = await getRepository(CommonAreaTickets);
        const mapCom = await getRepository(MapIndTicketVendor).query("SELECT * FROM map_com_tickets_vendors WHERE COM_TICKET_ID = '"+req.body.comTicketId+"' AND VENDOR_ID ='"+req.body.vendorId+"' AND IS_ACTIVE = true ")
        console.log(req.body.vendorId)

        if(mapCom.length > 0){
            console.log("xya")
        res.json({
            msg : "Assigning to same vendor"
        })
        }
        else {
            const prioryOneElVendor = await getRepository(MapComTicketVendor).query("UPDATE map_com_tickets_vendors SET IS_ACTIVE= false  WHERE COM_TICKET_ID = '" + req.body.comTicketId + "' ")
            const vendorRep = await getRepository(VENDOR);
            const commonTicketRep = await getRepository(CommonAreaTickets);

            const comObjAss= await commonTIcketRep.findOne({
                where : {
                   COM_TICKET_ID : req.body.comTicketId
                },
               relations : ["association","serviceId","comService","indSerPlan","indServiceTypes"]
           })
            const vendorObj = await vendorRep.findOne(req.body.vendorId);
            const comObj = await commonTicketRep.findOne(req.body.comTicketId,{
                relations:["serviceId","indServiceTypes"]
            });
            const mapComVendorRep = await getRepository(MapComTicketVendor);
            console.log(comObj)
            const mapVendorObj = new MapComTicketVendor();
            mapVendorObj.vendor = vendorObj;
            mapVendorObj.comTickets = comObj;
            mapVendorObj.IS_ACCEPTED = false;
            mapVendorObj.IS_REJECTED = false;
            mapVendorObj.IS_ACTIVE = true;
            var samapVendorObj = await mapComVendorRep.create(mapVendorObj);
            mapComVendorRep.save(samapVendorObj).then(async (data) => {


                if (comObj.indServiceTypes.IND_SERVICE_TYPE_ID) {
             
                        const interServAmountVendor = await getRepository(INTERNAL_SERVICES_AMOUNT_VENDORS)
                            .createQueryBuilder("interServVendor")
                            .where("interServVendor.IND_SERVICE_ID =  :indServiceId AND interServVendor.SERVICE_TYPE_ID = :indServiceTypeId AND VENDOR_ID = :vendorId ", {
                                indServiceId: comObj.serviceId.IND_SERVICE_ID,
                                indServiceTypeId: comObj.indServiceTypes.IND_SERVICE_TYPE_ID,
                                vendorId: req.body.vendorId
                            }).getOne().then(async (data) => {

                                var amount = data.AMOUNT;


                                if (comObj.QUANTITY) {

                                    amount = amount * comObj.QUANTITY;
                                }

                                comObj.AMOUNT = amount;
                                console.log(amount + "amount")

                                var esult = await commonTIcketRep.save(comObj);

                                vendorSendMail.vendoComSendMail(comObjAss.COM_TICKET_ID,comObjAss.TICKET_DESCRIPTION,vendorObj.VENDOR_EMAIL,comObjAss.TICKET_DATE,comObjAss.TICKET_TIME,comObjAss.comService.COM_SERVICE_NAME,comObjAss.serviceId.IND_SERVICE_NAME,
                                    comObjAss.indServiceTypes.TYPE,comObjAss.QUANTITY,comObjAss.AMOUNT,comObjAss.indSerPlan.PLAN,comObjAss.association.ASSOCIATION_NAME)

                                ownerComTicketSendMail.ComTicketSendMail(comObjAss.COM_TICKET_ID,comObjAss.TICKET_DESCRIPTION,comObjAss.association.EMAIL,comObjAss.TICKET_DATE,comObjAss.TICKET_TIME,comObjAss.comService.COM_SERVICE_NAME,comObjAss.serviceId.IND_SERVICE_NAME,comObjAss.indSerPlan.PLAN,
                                    comObjAss.indServiceTypes.TYPE,comObjAss.QUANTITY,comObjAss.AMOUNT,vendorObj.VENDOR_NAME)
                            }).catch((error) => {
                                console.log(error)
                            });




                }
                else {

                    const interServAmountVendorAssociation = await getRepository(IND_SER_TYPES_VENDOR_ASSOCIATION)
                        .createQueryBuilder("interServAmountVendorAssociation")
                        .where("interServAmountVendorAssociation.IND_SERVICE_ID =  :indServiceId AND VENDOR_ASSOCIATION_ID = :vendorAsociationId", {
                            indServiceId: comObj.serviceId.IND_SERVICE_ID,
                            vendorAsociationId:  req.body.vAssociationId
                        }).getOne().then(async (data) => {
                            var amount = data.AMOUNT;


                            if (req.body.quantity) {

                                amount = amount * req.body.quantity;
                            }

                            // var esult = await individualTicketRep.save(indObj);

                            comObj.AMOUNT = amount;

                            var esult = await commonTicketRep.save(comObj);
                        }).catch((error) => {
                            console.log(error);
                        });

                }
                res.send(samapVendorObj);
    
            })


        }


    }

    static getAppartmentAssociationIndTickets = async (req: Request, res: Response, next) => {
   const indTicketVendor = await getRepository(IndividualTickets)
        const indTicketObj = await indTicketVendor.find({
            where: {
                association: req.params.id
            },
            relations: ['indTicketOtp', 'mapIndVendor', 'mapIndVendor.vendor', 'indServiceTypes', 'indSerPlan', 'indAddtionAmount', 'serviceId', 'association', 'appUserDetails', 'appUserDetails.flatOwner', 'appUserDetails.flatTenant', 'appUserDetails.flatOwner.associationChildEntity', 'appUserDetails.flatOwner.associationGrandChildEntity', 'appUserDetails.flatTenant.associationChildEntity', 'appUserDetails.flatTenant.associationGrandChildEntity'],
            order: {
                IND_TICKET_ID: "DESC"
            }
        })

        res.send(indTicketObj)


    }
    static getAssociationIndTickets = async (req: Request, res: Response, next) => {

        // var indTicketObj = await getRepository(IndividualTickets)
        // .createQueryBuilder("indTickets")
        // .leftJoinAndSelect("indTickets.mapIndVendor","indVendor","indTickets.mapIndVendor = indVendor.indTickets")
        // .leftJoinAndSelect("indVendor.vendor","ven","indVendor.vendor = ven.id")
        // .leftJoinAndSelect("indTickets.indServiceTypes","serType","indTickets.indServiceTypes = serType.IND_SERVICE_TYPE_ID")
        // .leftJoinAndSelect("indTickets.indSerPlan","serPlan","indTickets.indSerPlan = serPlan.IND_SERVICE_PLAN_ID")
        // .leftJoinAndSelect("indTickets.indAddtionAmount","indAddAmount","indTickets.indAddAmount = indAddAmount.indTickets")
        // .leftJoinAndSelect("indTickets.serviceId","indService","indService.IND_SERVICE_ID = indTickets.serviceId")
        // .leftJoinAndSelect("indTickets.association","assoc","assoc.id = indTickets.association")
        // .leftJoinAndSelect("indTickets.appUserDetails","appUser","appUser.APP_USER_ID = indTickets.appUserDetails")
        // .leftJoinAndSelect("appUser.flatOwner","flatOwner","flatOwner.FLAT_OWNER_ID = appUser.flatOwner")
        // .leftJoinAndSelect("appUser.flatTenant","flatTenant","flatTenant.TENANT_ID = appUser.flatTenant")
        // .leftJoinAndSelect("flatOwner.associationChildEntity","ownerChildEntity","ownerChildEntity.APPARTMENT_CHILD_ENTITY_ID = flatOwner.associationChildEntity")
        // .leftJoinAndSelect("flatOwner.associationGrandChildEntity","ownerGrandChildEntity","ownerGrandChildEntity.APPARTMENT_GRAND_CHILD_ENTITY_ID = flatOwner.associationGrandChildEntity")

        // .leftJoinAndSelect("flatTenant.associationChildEntity","tenantChildEntity","tenantChildEntity.APPARTMENT_CHILD_ENTITY_ID = flatTenant.associationChildEntity")
        // .leftJoinAndSelect("flatTenant.associationGrandChildEntity","tenantGrandChildEntity","tenantGrandChildEntity.APPARTMENT_GRAND_CHILD_ENTITY_ID = flatTenant.associationGrandChildEntity")
        // .where("indTickets.association = :associationid  AND ven.VENDOR_ASSOCIATION_ID IS NOT NULL;", {
        //     associationid: req.params.associatinid,
        // })
        // .getMany();

        //const indTicketVendor = await getRepository(IndividualTickets)
        // const indTicketObj = await indTicketVendor.find({
        //     where: {
        //         association: req.params.id
        //     },
        //     relations: ['indTicketOtp', 'mapIndVendor', 'mapIndVendor.vendor', 'indServiceTypes', 'indSerPlan', 'indAddtionAmount', 'serviceId', 'association', 'appUserDetails', 'appUserDetails.flatOwner', 'appUserDetails.flatTenant', 'appUserDetails.flatOwner.associationChildEntity', 'appUserDetails.flatOwner.associationGrandChildEntity', 'appUserDetails.flatTenant.associationChildEntity', 'appUserDetails.flatTenant.associationGrandChildEntity'],
        //     order: {
        //         IND_TICKET_ID: "DESC"
        //     }
        // })
        var indTicketObj = await getRepository(IndividualTickets).query(`SELECT * FROM INDIVIDUAL_SERVICE_TICKETS 
        INNER JOIN map_ind_tickets_vendors ON  map_ind_tickets_vendors.IND_TICKET_ID = INDIVIDUAL_SERVICE_TICKETS.IND_TICKET_ID INNER JOIN vendor ON  vendor.VENDOR_ID = map_ind_tickets_vendors.VENDOR_ID
        WHERE INDIVIDUAL_SERVICE_TICKETS.ASSOCIATION_ID = ${req.params.id} AND vendor.VENDOR_ASSOCIATION_ID =${req.params.vid} AND map_ind_tickets_vendors.IS_ACTIVE = true  GROUP BY INDIVIDUAL_SERVICE_TICKETS.IND_TICKET_ID  ORDER BY INDIVIDUAL_SERVICE_TICKETS.IND_TICKET_ID DESC`)
        res.send(indTicketObj);

    }

    static getIndTicketDetails = async (req: Request, res: Response, next) => {

     

        const indTicketVendor = await getRepository(IndividualTickets)
        const indTicketObj = await indTicketVendor.findOne({
            where: {
                IND_TICKET_ID: req.params.id
            },
            relations: ['indTicketOtp', 'mapIndVendor', 'mapIndVendor.vendor', 'indServiceTypes', 'indSerPlan', 'indAddtionAmount', 'serviceId', 'association', 'appUserDetails', 'appUserDetails.flatOwner', 'appUserDetails.flatTenant', 'appUserDetails.flatOwner.associationChildEntity', 'appUserDetails.flatOwner.associationGrandChildEntity', 'appUserDetails.flatTenant.associationChildEntity', 'appUserDetails.flatTenant.associationGrandChildEntity'],
            order: {
                IND_TICKET_ID: "DESC"
            }
        })

        res.send(indTicketObj);

    }

    static getComTicketDetails = async (req: Request, res: Response, next) => {
    const commonTIcketRep = await getRepository(CommonAreaTickets)
        const comTicketObj = await commonTIcketRep.findOne({
            where: {
                COM_TICKET_ID: req.params.id
            },
            relations: ['mapComVendor', 'mapComVendor.vendor', 'indServiceTypes', 'indSerPlan', 'serviceId', 
                'association', 'appUserComTicket', 'appUserComTicket.appUser', 'appUserComTicket.appUser.flatOwner', 'appUserComTicket.appUser.flatOwner.associationChildEntity', 'appUserComTicket.appUser.flatOwner.associationGrandChildEntity', 'appUserComTicket.appUser.flatTenant', 'appUserComTicket.appUser.flatTenant.associationChildEntity', 'appUserComTicket.appUser.flatTenant.associationGrandChildEntity'],
            order: {
                COM_TICKET_ID: "DESC"
            }
        })
        console.log(comTicketObj)
       res.send(comTicketObj);


    }

    static getAppartmentAssociationComTickets = async (req: Request, res: Response, next) => {
        var comTicketObj = await getRepository(CommonAreaTickets).query(`SELECT * FROM COMMON_AREA_TICKETS 
       WHERE ASSOCIATION_ID = ${req.params.id} ORDER BY COMMON_AREA_TICKETS.COM_TICKET_ID DESC`)
        res.send(comTicketObj);

        // const commonTIcketRep = await getRepository(CommonAreaTickets)
        // const comTicketObj = await commonTIcketRep.find({
        //     where: {
        //         association: req.params.id
        //     },
        //     relations: ['mapComVendor', 'mapComVendor.vendor', 'indServiceTypes', 'indSerPlan', 'serviceId', 
        //         'association', 'appUserComTicket', 'appUserComTicket.appUser', 'appUserComTicket.appUser.flatOwner', 'appUserComTicket.appUser.flatOwner.associationChildEntity', 'appUserComTicket.appUser.flatOwner.associationGrandChildEntity', 'appUserComTicket.appUser.flatTenant', 'appUserComTicket.appUser.flatTenant.associationChildEntity', 'appUserComTicket.appUser.flatTenant.associationGrandChildEntity'],
        //     order: {
        //         COM_TICKET_ID: "DESC"
        //     }
        // })
        // console.log(comTicketObj)
      //  res.send(comTicketObj);

    }

    static getAssociationComTickets = async (req: Request, res: Response, next) => {
        var comTicketObj = await getRepository(CommonAreaTickets).query(`SELECT * FROM COMMON_AREA_TICKETS 
        INNER JOIN map_com_tickets_vendors ON  map_com_tickets_vendors.COM_TICKET_ID = COMMON_AREA_TICKETS.COM_TICKET_ID INNER JOIN vendor ON  vendor.VENDOR_ID = map_com_tickets_vendors.VENDOR_ID
        WHERE COMMON_AREA_TICKETS.ASSOCIATION_ID = ${req.params.id} AND vendor.VENDOR_ASSOCIATION_ID =${req.params.vid} AND map_com_tickets_vendors.IS_ACTIVE = true  GROUP BY COMMON_AREA_TICKETS.COM_TICKET_ID  ORDER BY COMMON_AREA_TICKETS.COM_TICKET_ID DESC`)
        res.send(comTicketObj);

        // const commonTIcketRep = await getRepository(CommonAreaTickets)
        // const comTicketObj = await commonTIcketRep.find({
        //     where: {
        //         association: req.params.id
        //     },
        //     relations: ['mapComVendor', 'mapComVendor.vendor', 'indServiceTypes', 'indSerPlan', 'serviceId', 
        //         'association', 'appUserComTicket', 'appUserComTicket.appUser', 'appUserComTicket.appUser.flatOwner', 'appUserComTicket.appUser.flatOwner.associationChildEntity', 'appUserComTicket.appUser.flatOwner.associationGrandChildEntity', 'appUserComTicket.appUser.flatTenant', 'appUserComTicket.appUser.flatTenant.associationChildEntity', 'appUserComTicket.appUser.flatTenant.associationGrandChildEntity'],
        //     order: {
        //         COM_TICKET_ID: "DESC"
        //     }
        // })
        // console.log(comTicketObj)
      //  res.send(comTicketObj);

    }

    static getVendorsRequests = async (req: Request, res: Response, next) => {
        const association = getRepository(ASSOCIATION);
        const vassociation = getRepository(VENDORASSOCIATION);

        const vrequestedAssociation = getRepository(VENDORREQUESTEDASSOCIATION);

        vrequestedAssociation.find({
            where: {
                vassociation: req.params.vassociatinid
            }

        }).then((data) => {
            res.send(data)
        }).catch((error) => {
            res.status(500).send(error)
        })

    }


    static getassociationVendorsRequests = async (req: Request, res: Response, next) => {
        const association = getRepository(ASSOCIATION);
        const vassociation = getRepository(VENDORASSOCIATION);

        const vrequestedAssociation = getRepository(VENDORREQUESTEDASSOCIATION);

        vrequestedAssociation.find({
            where: {
                association: req.params.associatinid
            },
            relations: ['vassociation']

        }).then((data) => {
            res.send(data)
        }).catch((error) => {
            res.status(500).send(error)
        })
    }

    static acceptAssociation = async (req: Request, res: Response, next) => {
        const association = getRepository(ASSOCIATION);
        const vassociation = getRepository(VENDORASSOCIATION);
        const associationVendors = getRepository(ASSOCIATION_VENDORS);
        const vendor = getRepository(VENDOR);
        const vrequestedAssociation = getRepository(VENDORREQUESTEDASSOCIATION);
        const avendor = await association.findOne(req.params.associatinid)


        vrequestedAssociation.findOne(req.params.id).then((data) => {
            data.IS_ACCEPTED = true;
            vrequestedAssociation.save(data).then(async (data) => {
                var vassobj = data;

                var vendorAssId = req.params.vassociatinid;

                var vendorAssociationObj = await vassociation.findOne(vendorAssId)


                var assVendors = new ASSOCIATION_VENDORS;
                // console.log(vendorAsscoauron+"vendorAsscoauron")
                assVendors.CREATED_BY = 'User';
                assVendors.UPDATED_BY = 'User';
                assVendors.vendorAssociation = vendorAssociationObj;
                assVendors.association = avendor;
                assVendors.IS_INTERNAL_VENDOR = false;
                assVendors.IS_THIRD_PARTY_ASSOCIATION = true;
                const vendorObj = await associationVendors.create(assVendors);
                const result = await associationVendors.save(vendorObj);
                res.send(vassobj);
            }).catch((error) => {
                res.status(500).send(error)
            })


        }).catch((error) => {
            res.status(500).send(error)
        })



    }


    static rejectAssociation = async (req: Request, res: Response, next) => {
        const association = getRepository(ASSOCIATION);
        const vassociation = getRepository(VENDORASSOCIATION);
        const associationVendors = getRepository(ASSOCIATION_VENDORS);
        const vendor = getRepository(VENDOR);
        const vrequestedAssociation = getRepository(VENDORREQUESTEDASSOCIATION);
        const avendor = await association.findOne(req.params.associatinid)


        vrequestedAssociation.findOne(req.params.id).then((data) => {
            data.IS_ACCEPTED = false;
            data.IS_REJECTED = true;
            vrequestedAssociation.save(data).then(async (data) => {
                var vassobj = data;

                var vendorAssId = req.params.vassociatinid;

                const result = await vendor.find({
                    where: {
                        VENDOR_ASSOCIATION_ID: vendorAssId
                    }
                }).then(async (data) => {
                    var vendors = [];
                    vendors = data;

                    for (var i = 0; i < vendors.length; i++) {
                        var vendorid = vendors[i].id;
                        var queriedObj = await getRepository(ASSOCIATION_VENDORS)
                            .createQueryBuilder("assVendors")
                            .where("assVendors.ASSOCIATION_ID = :associationid  AND assVendors.VENDOR_ID =:vendorId", {
                                associationid: req.params.associatinid,
                                vendorId: vendorid
                            })
                            .getOne();
                        var asvendorId = queriedObj.id;
                        var assDelteObj = associationVendors.delete(asvendorId);
                    }
                })

                res.send(vassobj);

            }).catch((error) => {
                res.status(500).send(error)
            })


        }).catch((error) => {
            res.status(500).send(error)
        })



    }

    static getAssociationData = async (req: Request, res: Response, next) => {
        const associationCount = await getRepository(VENDORASSOCIATION).query("SELECT COUNT(*) as vendorAssCount From vendor_association");
     const vendorCount = await getRepository(VENDOR).query("SELECT  COUNT(*) as vendorCount FROM vendor WHERE `vendor`.`VENDOR_ASSOCIATION_ID` IS NOT NULL  ");
     res.json({
      associationCount : associationCount,
      vendorCount : vendorCount
     })

  }


}
export default vendorRequestedController;