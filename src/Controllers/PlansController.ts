import { Request, Response } from "express";
import * as jwt from "jsonwebtoken";
import { getRepository } from "typeorm";
import { Plans } from "../entity/Plans";

class PlansController {
    static plansgetall = async (req: Request, res: Response) => {
        console.log("plansgetall")
        const  plansRepository = getRepository(Plans);
        plansRepository.find({}).then((data)=>{
            console.log(data);
            res.send(data)
        }).catch((error)=>{
                res.send(error) 
            });      
    }

    static getNonFree = async (req: Request, res: Response) => {
        console.log("plansgetall")
        const  plansRepository = getRepository(Plans);
        plansRepository.find({
            where : {
                IS_TRIAL : false
            }
        }).then((data)=>{
            console.log(data);
            res.send(data)
        }).catch((error)=>{
                res.send(error) 
            });      
    }

    static plansUpdate = async (req: Request, res: Response) => {
        const  plansRepository = getRepository(Plans);
        const planObj = await plansRepository.findOne(req.body.id);
        // const mergedUser = indServiceRepository.merge(user,req.body);
        planObj.PLAN_NAME = req.body.planName;
        planObj.DAYS = req.body.days;
        planObj.AMOUNT = req.body.amount;
        planObj.IS_ACTIVE  = req.body.is_active;
        planObj.IS_TRIAL  = req.body.is_trial;
        planObj.UPDATED_BY  = req.body.updated_by;
        plansRepository.save(planObj).then(data=>{
         res.send(data);
        }).catch((error)=>{
          res.status(500).send(error)
        });
    
}
static plansDelete = async (req: Request, res: Response) => {
    const  plansRepository = getRepository(Plans);
    const results = await plansRepository.delete(req.body.id);
    res.send(results);

}
static plansAdd = async (req: Request, res: Response) => {
    const  plansRepository = getRepository(Plans);
    const planObj = new Plans();
    planObj.PLAN_NAME = req.body.planName;
    planObj.DAYS = req.body.days;
    planObj.AMOUNT = req.body.amount;
    planObj.IS_ACTIVE  = req.body.is_active;
    planObj.IS_TRIAL  = req.body.is_trial;
    planObj.CREATED_BY  = req.body.created_by;
    planObj.UPDATED_BY  = req.body.created_by;
    const plan = plansRepository.create(planObj);
    plansRepository.save(plan).then((data)=>{
         res.send(data)
     }
         ).catch((error)=>{
             res.send(error) 
         });

}


}

export default PlansController;