import { Request, Response } from "express";
import { getRepository } from "typeorm";
import { FamilyRelations } from "../entity/FamilyRelations";

class FamilyRelationsController {

    static addRelations = async ( req: Request, res: Response, next ) => {
        console.log("addRelations");
        const familyRelatiosRepository = getRepository(FamilyRelations);
        const relation = new FamilyRelations();
        relation.RELATION_NAME = req.body.relation_name;
        relation.CREATED_BY = req.body.created_by;
        relation.UPDATED_BY = req.body.created_by;

        const relationobj = familyRelatiosRepository.create(relation);
        familyRelatiosRepository.save(relationobj).then((data) => {
            req.body.data = data;
            next();
            console.log(data);
            res.send(data);

        }).catch((error) => {
            console.log(error);
            res.send(error);
        });
    }

    static updateRelations = async ( req: Request, res: Response, next ) => {
        console.log("updateRelations");
        const familyRelatiosRepository = getRepository(FamilyRelations);
        const relation = await familyRelatiosRepository.findOne(req.body.relation_id);
        relation.RELATION_NAME = req.body.relation_name;
        relation.UPDATED_BY = req.body.last_updated_by;
        familyRelatiosRepository.save(relation).then((data) => {
            res.send(data);
        }).catch((error) => {
            console.log(error);
            res.send(error);
        });

    }

    static familyRelationAppGetAll = async (req: Request, res: Response) => {
        const familyRelatiosRepository = getRepository(FamilyRelations);
        const results = await familyRelatiosRepository.find({
            // relations: ['countries']
        }).then(
            data => {
                console.log(data);
                res.send(data);
            }).catch(err => {
                console.log(err);
            });
    }

}

export default FamilyRelationsController;