import { Request, Response } from "express";
import * as jwt from "jsonwebtoken";
import { getRepository } from "typeorm";
import { AssociationSubscription } from "../entity/AssociationSubscription";
import { Plans } from "../entity/Plans";
import { AppUserDetails } from '../entity/AppUserDetails';

class AssociationPlansController {
    static associationCheckPlan = async (req: Request, res: Response) => {

        const assRep = getRepository(AssociationSubscription);
        const assRepO = await assRep.createQueryBuilder("associationSub")
            .where("associationSub.APP_USER_ID  = :assId   && associationSub.IS_ACTIVE = true && associationSub.IS_ASSOCIATION = true", {
                assId: req.body.appUserId
            }).getMany();

        console.log(req.body.appUserId)
        console.log(assRepO)
        if (assRepO.length > 0) {
            res.send(true)
        }
        else {
            res.send(false)
        }
    }

    static vassociationCheckPlan = async (req: Request, res: Response) => {
        console.log("vassociationCheckPlan");

        const assRep = await getRepository(AssociationSubscription)
            .createQueryBuilder("assSub")
            .where("assSub.APP_USER_ID = :assId && assSub.IS_ACTIVE = true && assSub.IS_VENDOR = true", {
                assId: req.body.appUserId

            }).getMany();
        // const assRep = getRepository(AssociationSubscription);
        // const assRepO = await assRep.createQueryBuilder("associationSub")
        //     .where("associationSub.APP_USER_ID  = :assId && associationSub.IS_ACTIVE = true && associationSub.IS_VENDOR = true ", {
        //         assId: req.body.appUserId
        //     }).getMany();

        console.log("vasscheck")
        console.log(req.body.appUserId)
        console.log(assRep)

        if (assRep.length > 0) {

            console.log("isjfsjf")
            res.send(true)
        }
        else {
            res.send(false)
        }
        //console.log(data);
        //  res.send(data);

    }

    static subAssociatinPlan = async (req: Request, res: Response) => {
        const associationSubscriptionrep = getRepository(AssociationSubscription);
        const appUserRepository = getRepository(AppUserDetails);
        const plansRepository = getRepository(Plans);
        const planObj = await plansRepository.findOne(req.body.plan);
        const appUserResObj = await appUserRepository.findOne(req.body.plan)
        var Cr_date = new Date();
        var date = Cr_date.setDate(Cr_date.getDate() + planObj.DAYS);
        var subEndDate = new Date(date)
        const assocSub = new AssociationSubscription();
        assocSub.plan = planObj;
        assocSub.appUser = appUserResObj;
        assocSub.IS_ASSOCIATION = true;
        assocSub.IS_VENDOR = false;
        assocSub.IS_ACTIVE = true;
        assocSub.SUCBSCRIPTION_START_DATE = new Date();
        assocSub.SUBSCRIPTION_CREATED_DATE = new Date();
        assocSub.SUCBSCRIPTION_END_DATE = subEndDate;
        assocSub.UPDATED_BY = 'User';
        assocSub.CREATED_BY = 'User';
        assocSub.UPDATED_BY = 'User';
        const assSubObj = associationSubscriptionrep.create(assocSub);

        associationSubscriptionrep.save(assSubObj).then((data) => {
            res.send(assSubObj);
        }).catch((error) => {
            res.status(500).send(error)
        })



    }


    static subVendorAssociatinPlan = async (req: Request, res: Response) => {

        console.log("subVendorAssociatinPlan")
        const associationSubscriptionrep = getRepository(AssociationSubscription);
        const appUserRepository = getRepository(AppUserDetails);
        const plansRepository = getRepository(Plans);
        const planObj = await plansRepository.findOne(req.body.plan);
        const appUserResObj = await appUserRepository.findOne(req.body.appUserId)
        var Cr_date = new Date();
        var date = Cr_date.setDate(Cr_date.getDate() + planObj.DAYS);
        var subEndDate = new Date(date)
        const assocSub = new AssociationSubscription();
        assocSub.plan = planObj;
        assocSub.appUser = appUserResObj;
        assocSub.IS_ASSOCIATION = false;
        assocSub.IS_VENDOR = true;
        assocSub.IS_ACTIVE = true;
        assocSub.SUCBSCRIPTION_START_DATE = new Date();
        assocSub.SUBSCRIPTION_CREATED_DATE = new Date();
        assocSub.SUCBSCRIPTION_END_DATE = subEndDate;
        assocSub.UPDATED_BY = 'User';
        assocSub.CREATED_BY = 'User';
        assocSub.UPDATED_BY = 'User';
        const assSubObj = associationSubscriptionrep.create(assocSub);

        associationSubscriptionrep.save(assSubObj).then((data) => {
            res.send(assSubObj);
        }).catch((error) => {
            console.log(error)
            res.status(500).send(error)
        })


    }



}

export default AssociationPlansController;