import { Request, Response } from "express";
import * as jwt from "jsonwebtoken";
import { getRepository } from "typeorm";
import { IndividualAreaServices } from "../entity/IndividualAreaServices";
import { AppUserDetails } from "../entity/AppUserDetails";
import { VENDOR_ASSOCIATION_IND_SERVICES } from "../entity/VendorAssociationIndServices";
import {VENDORASSOCIATION} from "../entity/VendorAssociation";
class AssociationIndServiceController {
  
static addService = async (req: Request, res: Response) => {
    const  vassIndServiceRep = getRepository(VENDOR_ASSOCIATION_IND_SERVICES);
    const  indServiceRep = getRepository(IndividualAreaServices);
    const  vendorassociationRep = getRepository(VENDORASSOCIATION);
    const  appUserRep = getRepository(AppUserDetails);
   const IndServiceObj = await indServiceRep.findOne(req.body.indServiceId);
   const appUserObj = await appUserRep.findOne(req.body.appUserId);
   const vassociationObj = await vendorassociationRep.findOne(req.body.vendorAssociationId);
    const VassociationIndService = new VENDOR_ASSOCIATION_IND_SERVICES();
    VassociationIndService.indServices = IndServiceObj;
    VassociationIndService.vassociation = vassociationObj;
    VassociationIndService.CREATED_BY  = req.body.created_by;
    VassociationIndService.UPDATED_BY  = req.body.created_by;
    const vassociationIndServiceObj = vassIndServiceRep.create(VassociationIndService);
    vassIndServiceRep.save(vassociationIndServiceObj).then((data)=>{
         res.send(data)
     }
         ).catch((error)=>{
             res.send(error) 
         });

}

  
static allAssociationServices = async (req: Request, res: Response) => {
    const  vassIndServiceRep = getRepository(VENDOR_ASSOCIATION_IND_SERVICES);
    vassIndServiceRep.find({
        relations: ['indServices'],
        where: { appUser : req.params.id }
    }).then((data)=>{
        res.send(data)
    }
        ).catch((error)=>{
            res.send(error) 
        });

}
static delete = async (req: Request, res: Response) => {
    const  vassIndServiceRep = getRepository(VENDOR_ASSOCIATION_IND_SERVICES);
    vassIndServiceRep.delete(req.body.id).then((data)=>{
    res.send(data)
}
    ).catch((error)=>{
        res.send(error) 
    });

}


}

export default AssociationIndServiceController;