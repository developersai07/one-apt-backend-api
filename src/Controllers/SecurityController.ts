import { Request, Response } from "express";
import { getRepository } from "typeorm";
import { Security } from "../entity/Security";
import { ASSOCIATION } from "../entity/Associations";
import { read } from "fs";
import { AppUserDetails } from "../entity/AppUserDetails";
import { Roles } from "../entity/Roles";
import { UserRoles } from "../entity/UserRoles";
import SecuritySendMail from "../Mails/SecuritySendMail";
class SecurityController {

    static add = async (req: Request, res: Response,next) => {
        const rolesRepository = getRepository(Roles);
        const securityRep = getRepository(Security);
        const associationRep = getRepository(ASSOCIATION);
        const appUserRep = getRepository(AppUserDetails);
    const associationObj = await     associationRep.findOne(req.body.associationId);
    const userRolesRepository = getRepository(UserRoles);
    const securityobj = new    Security();
    securityobj.SECURITY_NAME = req.body.name;
    securityobj.SECURITY_EMAIL = req.body.email;
    securityobj.SECURITY_MOBILE_NUMBER = req.body.mobileNumber;
    var isHead ;

    if(req.body.is_security_head == 'true'){
        isHead = true;
    }
    else {
        isHead = false;
    }
    securityobj.IS_SECURITY_HEAD = isHead;
    securityobj.CREATED_BY = req.body.appUserId;
    securityobj.UPDATED_BY = req.body.appUserId;
    securityobj.association =associationObj;
    console.log("appUserDetailsx");

   
    securityRep.save(securityobj).then((data)=>
    {
        var assTring = req.body.name.charAt(0);
        var appTring = req.body.email.charAt(4);
        var mobNum = req.body.mobileNumber.charAt(5);

        console.log("mobNum");
       var username = data.ID + '' + assTring + '' + appTring + '' + req.body.associationId + ''+mobNum+'';
       var password = Math.floor(100000000 + Math.random() * 900000000) + '';
      const   appUserDetails = new AppUserDetails();
      appUserDetails.IS_ACTIVE = true;
      appUserDetails.USER_NAME = username;
      appUserDetails.PASSWORD = password;
      appUserDetails.security = data;

      console.log("appUserDetails");

      console.log(appUserDetails);

      appUserRep.save(appUserDetails).then(async (appUserObj)=>{

        var userName = appUserObj.USER_NAME;
        userName = userName+''+appUserObj.APP_USER_ID;

        var newUserName = userName;
        var today = new Date();
        var dda = String(today.getDate()).padStart(2, '0');
        var mma = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyya = today.getFullYear();
        var hrsa = today.getHours();
        var minuta = today.getMinutes();
        var secondsa = today.getSeconds();
        var stringDatea = 'OPS' + mma + '' + dda + '' + yyyya + '' + req.body.associationId + '' + appUserObj.APP_USER_ID;

        const securityObj = await  securityRep.findOne(data.ID);
        securityObj.SECURITY_UNIQUE_NUMBER = stringDatea;
     const secsdO = await   securityRep.save(securityObj);


      const appUserObjUp = await  appUserRep.findOne(appUserObj.APP_USER_ID);

      appUserObjUp.USER_NAME = userName;

      const awaitedObj = await appUserRep.save(appUserObjUp);

      const rolesObj = await rolesRepository.findOne(7);
      const userRoleObj = new UserRoles();
                    userRoleObj.appUserInfo = appUserObj;
                    userRoleObj.role = rolesObj;
                    const UserRoleObj = await userRolesRepository.save(userRoleObj);

                    SecuritySendMail.securityEnrollmentSendMail(req.body.name,newUserName, password, req.body.email);

                    req.body.data = data;
                    req.body.securityId = data.ID
                next()

      }).catch((error)=>{
          console.log(error)
      })


   
    }).catch((error)=>{

        console.log(error)
      // res.status(500).send(error);
    })


        // const results = await rolesrep.find().then(
        //     data => {
        //         console.log(data);
        //         res.send(data);
        //     }).catch(err => {
        //         console.log(err);
        //     });
    }

    static  updateFile = async (req: Request, res: Response,next) =>{
        const securityRep = getRepository(Security);
      const securityObj = await  securityRep.findOne(req.body.id);
      securityObj.SECURITY_GOV_PROOF = req.body.sec_gov_proof;

      securityRep.save(securityObj).then((data)=>{
         res.send(req.body.data)
      })
    }

    static  getAll = async (req: Request, res: Response,next) =>{
        const securityRep = getRepository(Security);
        const securityObj = await  securityRep.find({
            where : {
                association : req.params.id
            }
        });
       res.send(securityObj)

    }

    static  update = async (req: Request, res: Response,next) =>{
        const securityRep = getRepository(Security);
     const securityObj = await   securityRep.findOne(req.body.id);
     securityObj.SECURITY_NAME = req.body.name;
    // securityObj.SECURITY_EMAIL = req.body.email;
     securityObj.SECURITY_MOBILE_NUMBER = req.body.mobileNumber;
     var isHead ;
     if(req.body.is_security_head == 'true'){
         isHead = true;
     }
     else {
         isHead = false;
     }
     securityObj.IS_SECURITY_HEAD = isHead;
     securityObj.UPDATED_BY = req.body.appUserId;

     console.log(securityObj)

     console.log("kadjkas")

     securityRep.save(securityObj).then((data)=>
     {
        req.body.data = data;
                    req.body.securityId = data.ID
                next()
     })
 

    }

}

export default SecurityController;