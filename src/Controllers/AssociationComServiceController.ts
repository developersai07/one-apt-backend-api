import { Request, Response } from "express";
import * as jwt from "jsonwebtoken";
import { getRepository } from "typeorm";
import { CommonAreaServices } from "../entity/CommonAreaService";
import { AppUserDetails } from "../entity/AppUserDetails";
import { ASSOCIATION_COM_SERVICES } from "../entity/AssociationComServices";
import {ASSOCIATION} from "../entity/Associations";

class AssociationIndServiceController {
  
static addService = async (req: Request, res: Response) => {
    const  assComServiceRep = getRepository(ASSOCIATION_COM_SERVICES);
    const  associationRep = getRepository(ASSOCIATION);
    const  comServiceRep = getRepository(CommonAreaServices);
    const  appUserRep = getRepository(AppUserDetails);
   const ComServiceObj = await comServiceRep.findOne(req.body.comServiceId);
   const associationObj = await associationRep.findOne(req.body.associationId);
    const associationComService = new ASSOCIATION_COM_SERVICES();
    associationComService.comServices = ComServiceObj;
    associationComService.association = associationObj;
    associationComService.CREATED_BY  = req.body.created_by;
    associationComService.UPDATED_BY  = req.body.created_by;
    const associationComServiceObj = assComServiceRep.create(associationComService);
    assComServiceRep.save(associationComServiceObj).then((data)=>{
         res.send(data)
     }
         ).catch((error)=>{
             res.send(error) 
         });

}

static allAssociationServices = async (req: Request, res: Response) => {
    const  assComServiceRep = getRepository(ASSOCIATION_COM_SERVICES);
    assComServiceRep.find({
        relations: ['comServices'],
        where: { appUser : req.params.id }
    }).then((data)=>{
        res.send(data)
    }
        ).catch((error)=>{
            res.send(error) 
        });

}

static delete = async (req: Request, res: Response) => {
    const  assComServiceRep = getRepository(ASSOCIATION_COM_SERVICES);
    assComServiceRep.delete(req.body.id).then((data)=>{
    res.send(data)
}
    ).catch((error)=>{
        res.send(error) 
    });

}




}

export default AssociationIndServiceController;