import { Request, Response } from "express";
import * as jwt from "jsonwebtoken";
import { getRepository } from "typeorm";
import {VENDOR_CHECKIN } from "../entity/VendorCheckIn";
import {IndividualAreaServices } from "../entity/IndividualAreaServices";
import {CommonAreaServices } from "../entity/CommonAreaService";
import {APPARTMENT_CHILD_ENTITIES } from "../entity/AppartmentChildEntites";
import {APPARTMENT_GRAND_CHILD_ENTITIES } from "../entity/AppartmentGrandChildEntity";
import {ASSOCIATION} from '../entity/Associations';
import {VENDOR} from '../entity/Vendors';
class VendorvisitorCheckInController {
 

static checkIn = async (req: Request, res: Response,next) => {

    console.log("sfjsabdhabd")
    const  vendorChekInRep = getRepository(VENDOR_CHECKIN);
    const  indServiceRep = getRepository(IndividualAreaServices);
    const  comServiceRep = getRepository(CommonAreaServices);
    const  appChildEntityRep = getRepository(APPARTMENT_CHILD_ENTITIES);
    const  appGrnadChildEntityRep = getRepository(APPARTMENT_GRAND_CHILD_ENTITIES);
    const  associationRep = getRepository(ASSOCIATION);
    const  vendorRep = getRepository(VENDOR);
    const associationObj = await associationRep.findOne(req.body.associationId);
   const vendorObj = await vendorRep.findOne(req.body.vendorId)
    const vCheckIn = new VENDOR_CHECKIN();
    if( req.body.serArea == 'Individual'){
     const indServiceObj = await   indServiceRep.findOne(req.body.indService);
     const appChildObj = await   appChildEntityRep.findOne(req.body.block);
     const appGrandChildObj = await   appGrnadChildEntityRep.findOne(req.body.flats);    
        vCheckIn.indService = indServiceObj;
        vCheckIn.appChildEntity = appChildObj;
        vCheckIn.appGrandChild = appGrandChildObj;
    }
    if( req.body.serArea == 'Common'){
        const comServiceObj = await   comServiceRep.findOne(req.body.comService);
        vCheckIn.comService = comServiceObj;
    }
    vCheckIn.AREA = req.body.serArea;
    vCheckIn.PERSONS = req.body.persons;
    vCheckIn.CHECK_IN_TIME = new Date();
    vCheckIn.IS_CHECKED_IN = true;
    vCheckIn.IS_CHECKED_OUT = false;
    vCheckIn.CREATED_BY = req.body.created_by;
    vCheckIn.UPDATED_BY = req.body.created_by;
    vCheckIn.association = associationObj;
    vCheckIn.vendor = vendorObj;
    
    vendorChekInRep.save(vCheckIn).then((data)=>{
        req.body.data = data
        next()
    }).catch((error)=>{
        console.log(error)
        res.status(500).send(error)
    })

}

static updateFileName = async (req: Request, res: Response,next) => {
    const  vendorChekInRep = getRepository(VENDOR_CHECKIN);
  const vendorCObj = await  vendorChekInRep.findOne(req.body.data.VENDOR_CHECK_IND_ID);
  vendorCObj.VV_PIC = req.body.vvImage;
  vendorChekInRep.save(vendorCObj).then((data)=>{
      res.send(req.body.data)
  }).catch((error)=>{
    return res.status(500).send(error);
  })
}





static vendorCheckedIn = async (req: Request, res: Response,next) => {

    const vChckedIn = await getRepository(VENDOR_CHECKIN)
    .createQueryBuilder("vChekcedIn")
    .leftJoinAndSelect("vChekcedIn.vendor", "vendorobj", "vendorobj.id = vChekcedIn.vendor")
    .where("vChekcedIn.association =  :id AND vChekcedIn.IS_CHECKED_IN  = true AND  vChekcedIn.IS_CHECKED_OUT  = false ",{
         id:   req.params.id,
    })
    .getMany();

    res.send(vChckedIn)
}

static vendorCheckOut = async (req: Request, res: Response,next) => {

    const  vendorChekInRep = getRepository(VENDOR_CHECKIN);
    const vendorCObj = await  vendorChekInRep.findOne(req.params.id);
    vendorCObj.IS_CHECKED_OUT = true;
    vendorCObj.CHECK_OUT_TIME = new Date();
    vendorChekInRep.save(vendorCObj).then((data)=>{
        res.send(data)
    }).catch((error)=>{
      return res.status(500).send(error);
    })

   
}

}

export default VendorvisitorCheckInController;