import { Request, Response } from "express";
import * as jwt from "jsonwebtoken";
import { getRepository } from "typeorm";
import { IND_SERVICE_TYPES } from "../entity/indServiceType";
import { IndividualAreaServices } from "../entity/IndividualAreaServices";

class indServcePlanController {
    static addIndServiceType = async (req: Request, res: Response, next) => {
        const indServceTypeRep = getRepository(IND_SERVICE_TYPES);
        const indAreaServices = getRepository(IndividualAreaServices);

        const indServiceIdObj = await indAreaServices.findOne(req.body.serviceId);

        const prioryOnesrvicetype = await getRepository(IND_SERVICE_TYPES)
            .createQueryBuilder("srvicetype")
            .where("srvicetype.TYPE =  :serviceTypeName AND srvicetype.IND_SERVICE_ID = :service ", {
                serviceTypeName: req.body.serviceTypeName,
                service: req.body.serviceId,
            }).getCount();

        if (prioryOnesrvicetype > 0) {
            
            res.status(201).send({
                message: "Service Type Already Available"
            })
        } else {
            const indType = new IND_SERVICE_TYPES();
            indType.TYPE = req.body.serviceTypeName;
            indType.indServices = indServiceIdObj;
            indType.CREATED_BY = req.body.created_by;
            indType.UPDATED_BY = req.body.created_by;
            //indPlan.CREATED_DATE = craetedDate;
            //  indPlan.UPDATED_DATE = updatedDate;

            const indServiceTypeObj = indServceTypeRep.create(indType);
            indServceTypeRep.save(indServiceTypeObj).then((data) => {
                res.send(data)
            }
            ).catch((error) => {
                res
                console.log(error)
                res.send(error)
            });
        }
       



    }

    static updateIndServiceType = async (req: Request, res: Response) => {
        console.log("updateindServiceType");
        const indServceTypeRep = getRepository(IND_SERVICE_TYPES);

  

        const indAreaServices = getRepository(IndividualAreaServices);

        const indServiceIdObj = await indAreaServices.findOne(req.body.serviceId);

        console.log(req.body.serviceTypeName);
        console.log(req.body.serviceId);

        const prioryOnesrvicetype = await getRepository(IND_SERVICE_TYPES).createQueryBuilder("srvicetype")
            .where("srvicetype.TYPE =  :serviceTypeName AND srvicetype.IND_SERVICE_ID = :service ", {
                serviceTypeName: req.body.serviceTypeName,
                service: req.body.serviceId,
            }).getCount();



        if (prioryOnesrvicetype > 0){
            console.log("Service Type Already Available");
            res.status(201).send({
                message: "Service Type Already Available"
            })
            }
            else {

            const indServiceType = await indServceTypeRep.findOne({
                where: {
                    IND_SERVICE_TYPE_ID: req.body.id
                }
            });
            indServiceType.TYPE = req.body.serviceTypeName;
            indServiceType.UPDATED_BY = req.body.updated_by;
            indServceTypeRep.save(indServiceType).then(data => {
                console.log(data);
                res.send(data);
            }
            ).catch(err => {
                console.log(err);
            });
            }
  
    }

    static getAllIndServiceTypes = async (res: Response, req: Request) => {
        // console.log("ok");
        // console.log(req.params.id);
        // res.send(2);
        console.log("**********");
        console.log("******" + req.params.id);
        const indServceTypeRep = getRepository(IND_SERVICE_TYPES);
        indServceTypeRep.find({
            relations: ['indServices'],
            where: {
                indServices: req.params.id
            }
        }).then((data) => {
            res.send(data)
        }
        ).catch((error) => {
            console.log(error)
            res.send(error)
        });
    }

    static deleteIndServiceType = async (req: Request, res: Response) => {
        console.log(req.body.id);
        const indServceTypeRep = getRepository(IND_SERVICE_TYPES);
        const results = await indServceTypeRep.delete({
            IND_SERVICE_TYPE_ID: req.body.id,
            indServices: req.body.serviceId
        });
        res.send(results);
    }



    static getServiceTypes = async (req: Request, res: Response, next) => {
        const indServceTypeRep = getRepository(IND_SERVICE_TYPES);
        indServceTypeRep.find({
            relations: ['indServices'],
            where: {
                indServices: req.params.id
            }
        }).then((data) => {
            res.send(data);
        }).catch((error) => {
            res.status(500).send(error)
        })

    }


}
export default indServcePlanController;