import { Request, Response } from "express";
import * as jwt from "jsonwebtoken";
import { getRepository } from "typeorm";
import { IndividualAreaServices } from "../entity/IndividualAreaServices";
import { AppUserDetails } from "../entity/AppUserDetails";
import { ASSOCIATION_IND_SERVICES } from "../entity/AssociationIndServices";
import {ASSOCIATION} from "../entity/Associations";
class AssociationIndServiceController {
  
static addService = async (req: Request, res: Response) => {
    const  assIndServiceRep = getRepository(ASSOCIATION_IND_SERVICES);
    const  indServiceRep = getRepository(IndividualAreaServices);
    const  associationRep = getRepository(ASSOCIATION);
    const  appUserRep = getRepository(AppUserDetails);
   const IndServiceObj = await indServiceRep.findOne(req.body.indServiceId);
   const appUserObj = await appUserRep.findOne(req.body.appUserId);
   const associationObj = await associationRep.findOne(req.body.associationId);
    const associationIndService = new ASSOCIATION_IND_SERVICES();
    associationIndService.indServices = IndServiceObj;
    associationIndService.association = associationObj;
    associationIndService.CREATED_BY  = req.body.created_by;
    associationIndService.UPDATED_BY  = req.body.created_by;
    const associationIndServiceObj = assIndServiceRep.create(associationIndService);
    assIndServiceRep.save(associationIndServiceObj).then((data)=>{
         res.send(data)
     }
         ).catch((error)=>{
             res.send(error) 
         });

}

  
static allAssociationServices = async (req: Request, res: Response) => {
    const  assIndServiceRep = getRepository(ASSOCIATION_IND_SERVICES);
    assIndServiceRep.find({
        relations: ['indServices'],
        where: { appUser : req.params.id }
    }).then((data)=>{
        res.send(data)
    }
        ).catch((error)=>{
            res.send(error) 
        });

}
static delete = async (req: Request, res: Response) => {
    const  assIndServiceRep = getRepository(ASSOCIATION_IND_SERVICES);
  assIndServiceRep.delete(req.body.id).then((data)=>{
    res.send(data)
}
    ).catch((error)=>{
        res.send(error) 
    });

}


}

export default AssociationIndServiceController;