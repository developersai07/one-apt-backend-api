import { Request, Response, response } from "express";
import * as jwt from "jsonwebtoken";
import { getRepository } from "typeorm";
import { IND_SERVICE_PLANS } from "../entity/indServicePlan";
import { MAP_IND_SERVICE_PLANS } from "../entity/MapIndServicePlan";


class indServceTypeController {
    static addIndServicePlan = async (req: Request, res: Response, next) => {
        console.log("addIndServicePlan34532");
        const indServceiPlanRep = getRepository(IND_SERVICE_PLANS);
        indServceiPlanRep.find({
            where: {
                PLAN: req.body.planName
            }
        }).then((data) => {
            if (data.length > 0) {
                console.log(data.length);
                console.log(data);
                res.status(201).send({
                    message: "Service Already Available",
                    obj: data
                })
            } else {
                var craetedDate = new Date("30-3-2020");
                var updatedDate = new Date("30-3-2020");
                const indPlan = new IND_SERVICE_PLANS();
                indPlan.PLAN = req.body.planName;
                indPlan.DAYS = req.body.times;
                indPlan.CREATED_BY = req.body.created_by;
                indPlan.UPDATED_BY = req.body.created_by;
                //indPlan.CREATED_DATE = craetedDate;
                //  indPlan.UPDATED_DATE = updatedDate;

                const planObj = indServceiPlanRep.create(indPlan);
                indServceiPlanRep.save(planObj).then((data) => {
                    res.send(data)
                }
                ).catch((error) => {
                    console.log(error)
                    res.send(error)
                });
            }
        }).catch((error) => {
            console.log(error)
            res.send(error)
        });
    }

    static updateIndServicePlans = async (req: Request, res: Response) => {
        console.log("updateIndServicePlans");
        const indServceiPlanRep = getRepository(IND_SERVICE_PLANS);
        const indPlan = await indServceiPlanRep.findOne(req.body.id);
        indPlan.PLAN = req.body.planName;
        indPlan.DAYS = req.body.times;
        indPlan.UPDATED_BY = req.body.updated_by;
        indServceiPlanRep.save(indPlan).then(data => {
            console.log(data);
            res.send(data);
        }
        ).catch(err => {
            console.log(err);
        });

    }

    static deleteIndServicePlans = async (req: Request, res: Response) => {
        const indServceiPlanRep = getRepository(IND_SERVICE_PLANS);
        // const advertisementRepository = getRepository(MAP_IND_SERVICE_PLANS);
        const results = await indServceiPlanRep.delete(req.body.id);
        res.send(results);
    }

    static getAll = async (req: Request, res: Response, next) => {
        const indServceiPlanRep = getRepository(IND_SERVICE_PLANS);
        indServceiPlanRep.find({}).then((data) => {
            res.send(data)
        }
        ).catch((error) => {
           // console.log(error)
            res.send(error)
        });
    }

    static getPlansByService = async (req: Request, res: Response, next) => {
        const mapindServceiPlanRep = getRepository(MAP_IND_SERVICE_PLANS);
        mapindServceiPlanRep.find({
            where : {
                indServices : req.params.serviceId
            },
            relations:['indServices','indServicesPlan']
        }).then((data) => {
         res.send(data)
        }
        ).catch((error) => {
            console.log(error)
            res.send(error)
        });
    }

    static getAllIndServicePlans = async (req: Request, res: Response) => {
        console.log("**********");
        const indServceiPlanRep = getRepository(IND_SERVICE_PLANS);
        indServceiPlanRep.find({}).then((data) => {
            res.send(data)
        }
        ).catch((error) => {
            console.log(error)
            res.send(error)
        });
    }
}
export default indServceTypeController;