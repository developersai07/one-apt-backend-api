import { Request, Response } from "express";
import * as jwt from "jsonwebtoken";
import { getRepository } from "typeorm";
import { CITIES } from "../entity/Cities";

class StatesController {
    static citiesgetall = async (req: Request, res: Response) => {

        const citiesrep = getRepository(CITIES);
        const results = await citiesrep.find({
            relations: ['states']
        }).then(
            data => {
                console.log(data);
                res.send(data);
            }).catch(err => {
                console.log(err);
            });
    }

    static getCities = async (req: Request, res: Response) => {
        const citiesrep = getRepository(CITIES);
        const result = await citiesrep.find({
            // relations: ['country'],
            where: { states: req.params.id }
        }
        )
        res.send(result);
    }



}

export default StatesController;