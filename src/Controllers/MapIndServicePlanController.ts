import { Request, Response, response } from "express";
import { getRepository } from "typeorm";
import { MAP_IND_SERVICE_PLANS } from "../entity/MapIndServicePlan";


class MapIndServicePlanController {

    static getindServicePlans = async (req: Request, res: Response) => {
        console.log("**********");
        console.log(req.params.id);
        const mapIndServicePlansRepository = getRepository(MAP_IND_SERVICE_PLANS);
        mapIndServicePlansRepository.find({
            relations: ['indServicesPlan'],
            where: {
                indServices: req.params.id
            }
        }).then((data) => {
            console.log(data);
            res.send(data);
        }).catch((error) => {
            console.log(error);
            res.send(error);
        });
    }


    static addNewIndServicePlans = async (req: Request, res: Response) => {
        const mapIndServicePlansRepository = getRepository(MAP_IND_SERVICE_PLANS);
        mapIndServicePlansRepository.find({
            where: {
                indServices: req.body.indServiceId,
                indServicesPlan: req.body.indServicePlanId
            }
        }).then((data) => {
            console.log(data.length);
            if (data.length > 0) {
                res.status(201).send({
                    message: "Plan is active",
                    obj: data
                })
            } else {
                const mapIndSerPlansObj = new MAP_IND_SERVICE_PLANS()

                mapIndSerPlansObj.indServices = req.body.indServiceId;
                mapIndSerPlansObj.indServicesPlan = req.body.indServicePlanId;

                const mapIndServicePlan = mapIndServicePlansRepository.create(mapIndSerPlansObj);
                mapIndServicePlansRepository.save(mapIndServicePlan).then((data) => {
                    console.log(data);
                    res.send(data);
                }).catch((error) => {
                    console.log(error);
                    res.send(error);
                });
            }
        }).catch((error) => {
            console.log(error);
            res.send(error);
        });
    }

    static delete = async (req: Request, res: Response) => {
        console.log("--------------");
        console.log("delete: " + req.body.map_ind_service_plan_id);
        const advertisementRepository = getRepository(MAP_IND_SERVICE_PLANS);
        // const results = await advertisementRepository.delete(req.body.id);
        const results = await advertisementRepository.delete({
            MAP_IND_SERVICE_PLAN_ID: req.body.map_ind_service_plan_id,
            indServicesPlan: req.body.ind_service_plan_id
        }).then(() =>{
            res.send(results);
        }).catch((error)=>{
            response.send(error);
        });
    }

}

export default MapIndServicePlanController;