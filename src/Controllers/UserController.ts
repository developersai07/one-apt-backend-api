import { Request, Response, response } from "express";
import * as jwt from "jsonwebtoken";
import { getRepository } from "typeorm";
import { AppUserDetails } from "../entity/AppUserDetails";
import { UserRoles } from "../entity/UserRoles";
import config from "../config/config";
import { AppAdmin } from "../entity/AppAdmin";
import { ASSOCIATION } from "../entity/Associations";
import { VENDOR } from "../entity/Vendors";
import { FlatOwners } from "../entity/FlatOwners";
import { FlatTenants } from "../entity/FlatTenants";
import FlatOwnerSendMail from "../Mails/FlatOwnerSendMail";
import AssociationSendMail from "../Mails/AssociationSendMail";
import VendorSendMail from "../Mails/VendorSendMail";
import FlatTenantSendMail from "../Mails/FlatTenantsSendMail";
import { VENDORASSOCIATION } from "../entity/VendorAssociation";
class UserController {

    static updatePassword =async(req: Request, res: Response) => {
        console.log('updatePassword');
        const appUserRep = getRepository(AppUserDetails);
        console.log(req.body.appuserId);
        console.log(req.body.password);
        const userObj = await appUserRep.findOne(req.body.appuserId);
        console.log(userObj);
        userObj.PASSWORD = req.body.password;
        appUserRep.save(userObj).then( async (data) => {
            console.log(data);
            res.send(data);
        }).catch((error) => {
            res.send(error);
        })
    }
    static adminLogin = async (req: Request, res: Response) => {
        const appUserRepository = getRepository(AppUserDetails);
        const user = await appUserRepository.findOne(
            {
                relations: ['adminInfo', 'association', 'association.appatmentInfo', 'vendorAssociation', 'flatOwner', 'flatTenant', 'appRolesInfo', 'appRolesInfo.role', 'vendor'
                ],
                where: {
                    USER_NAME: req.body.username
                }
            });
        console.log(user);
        if (user) {

            if (user.PASSWORD == req.body.password) {

                if (user.IS_ACTIVE == true) {
                    user.PASSWORD = null;
                    var token = jwt.sign({ id: user.APP_USER_ID }, config.jwtSecret, {
                        expiresIn: 86400 // expires in 24 hours
                    });
                    res.status(200).json({
                        user: user,
                        jwtToken: token
                    });
                }
                else
                    res.status(500).json({
                        message: "In-Active"
                    });

            } else {

                res.status(200).send({
                    message: "Incorrect  Password Login"
                });
            }
        } else {
            res.status(200).send({
                message: "Incorrect User Login"
            });
        }
    }

    static applicationLogin = async (req: Request, res: Response) => {
        const appUserRepository = getRepository(AppUserDetails);
        const user = await appUserRepository.findOne(
            {
                relations: ['adminInfo', 'association', 'association.appatmentInfo', 'vendorAssociation', 'flatOwner', 'flatTenant', 'appRolesInfo', 'appRolesInfo.role', 'flatOwner.associationEntity', 'flatTenant.associationEntity', 'vendor', 'vendor.asssociation', 'vendor.vAssociation','security','security.association','security.association.appatmentInfo' ],
                where: {
                    USER_NAME: req.body.username
                }
            });
        if (user) {

            if (user.PASSWORD == req.body.password)

                if (user.IS_ACTIVE == true) {
                    user.PASSWORD = null;
                    var token = jwt.sign({ id: user.APP_USER_ID }, config.jwtSecret, {
                        expiresIn: 86400 // expires in 24 hours
                    });
                    res.status(200).json({
                        user: user,
                        jwtToken: token
                    });
                }
                else
                    res.status(500).json({
                        message: "In-Active"
                    });

            else

                res.status(200).send({
                    message: "Incorrect Login"
                });

        }
        else {
            res.status(200).send({
                message: "Incorrect Login"
            });
        }
    }

    static getappUserRoles = async (req: Request, res: Response) => {
        const userRolesRepository = getRepository(UserRoles);
        const results = await userRolesRepository.find({
            relations: ['role'],
            where: {
                appUserInfo: req.params.appUserId
            }
        }).then(
            data => {
                res.send(data);
            }
        ).catch(err => {
            res.status(500).send(err);
        });
    }

    static applicationForgotPassword = async (req: Request, res: Response) => {
        const appUserRepository = getRepository(AppUserDetails);
        const superAdminRepository = getRepository(AppAdmin);
        const associationAdminRepository = getRepository(ASSOCIATION);
        const vendorAdminRepository = getRepository(VENDORASSOCIATION);
        const vendorRepository = getRepository(VENDOR);
        const flatOwnerRepository = getRepository(FlatOwners);
        const flatTenantRepository = getRepository(FlatTenants);

        const id = req.body.role;
        if (id == '1') {
            

        } else if (id == '2') {
            const association = await associationAdminRepository.findOne({
                where: {
                    EMAIL: req.body.email
                }
            }).then(data => {
                const assoObj = data;
                console.log(assoObj);
                var name = data.ASSOCIATION_NAME
                const userID = data.id;
                const appUser = appUserRepository.findOne({
                    where: {
                        association: userID
                    }
                }).then(user => {
                    const username = user.USER_NAME;
                    const password = user.PASSWORD;
                    AssociationSendMail.associationEnrollmentSendMail(name,username, password, req.body.email);
                    res.status(200).json({
                        message: 'Success',
                        association: assoObj,
                    })
                }).catch(error => {
                    console.log(error);
                    res.send(error);
                });
            }).catch(error => {
                console.log(error);
                // res.status(404).send(error);
                res.status(404).json({
                    message: 'Check Your Email',
                    error: error
                });
            });
        } else if (id == '3') {
            const vendorAssociation = await vendorAdminRepository.findOne({
                where: {
                    VENDOR_EMAIL: req.body.email
                }
            }).then(data => {
                const vendorAssociationObj = data;
                var vname = data.VENDOR_COMPANY_NAME
                console.log(vendorAssociationObj);
                const userID = data.id;
                const appUser = appUserRepository.findOne({
                    where: {
                        vendorAssociation: userID
                    }
                }).then(user => {
                    const username = user.USER_NAME;
                    const password = user.PASSWORD;
                    VendorSendMail.vendorEnrollmentSendMail(vname,username, password, req.body.email);
                    res.status(200).json({
                        message: 'Success',
                        vendorAssociation: vendorAssociationObj,
                    })
                }).catch(error => {

                    res.send(error);
                });
            }).catch(error => {
                console.log(error);
                // res.status(404).send(error);
                res.status(404).json({
                    message: 'Check Your Email',
                    error: error
                });
            });

        } else if (id == '4') {
            const flatOwner = await flatOwnerRepository.findOne({
                where: {
                    OWNER_EMAIL: req.body.email
                }
            }).then(data => {
                const flatOwnerObj = data;
                var flatOwnerName = data.OWNER_FIRST_NAME
                console.log(flatOwnerObj);
                const userID = data.FLAT_OWNER_ID;
                const appUser = appUserRepository.findOne({
                    where: {
                        flatOwner: userID
                    }
                }).then(user => {
                    const username = user.USER_NAME;
                    const password = user.PASSWORD;
                    FlatOwnerSendMail.flatOwnerEnrollmentSendMail(flatOwnerName,username, password, req.body.email);
                    res.status(200).json({
                        message: 'Success',
                        flatOwner: flatOwnerObj,
                    })
                }).catch(error => {
                    
                    res.send(error);
                });
            }).catch(error => {
                console.log(error);
                // res.status(404).send(error);
                res.status(404).json({
                    message: 'Check Your Email',
                    error: error
                });
            });
        } else if(id == '5'){
            const flatTenent = await flatTenantRepository.findOne({
                where: {
                    TENANT_EMAIL: req.body.email
                }
            }).then(data => {
                const flatOwnerObj = data;
                var flatTenantName = data.TENANT_FIRST_NAME
                console.log(flatOwnerObj);
                const userID = data.TENANT_ID;
                const appUser = appUserRepository.findOne({
                    where: {
                        flatTenant: userID
                    }
                }).then(user => {
                    const username = user.USER_NAME;
                    const password = user.PASSWORD;
                    FlatTenantSendMail.flatTenantEnrollmentSendMail(flatTenantName,username, password, req.body.email);
                    res.status(200).json({
                        message: 'Success',
                        flatTenant: flatOwnerObj,
                    })
                }).catch(error => {

                    res.send(error);
                });
            }).catch(error => {
                console.log(error);
                // res.status(404).send(error);
                res.status(404).json({
                    message: 'Check Your Email',
                    error: error
                });
            });
            
        } else if (id == '6') {
            const vendor = await vendorRepository.findOne({
                where: {
                    VENDOR_EMAIL: req.body.email
                }
            }).then(data => {
                const vendorObj = data;
                var vendorName = data.VENDOR_NAME
                console.log(vendorObj);
                const userID = data.id;
                const appUser = appUserRepository.findOne({
                    where: {
                        vendor: userID
                    }
                }).then(user => {
                    const username = user.USER_NAME;
                    const password = user.PASSWORD;
                    VendorSendMail.vendorSendMail(vendorName,username, password, req.body.email);
                    res.status(200).json({
                        message: 'Success',
                        vendor: vendorObj,
                    })
                }).catch(error => {

                    res.send(error);
                });
            }).catch(error => {
                console.log(error);
                // res.status(404).send(error);
                res.status(404).json({
                    message: 'Check Your Email',
                    error: error
                });
            });

        } else {
            console.log("else");
            res.status(404).json({
                message: 'Nothing'
            })
        }

    }


    static applicationForgotUserName = async (req: Request, res: Response) => {
        const appUserRepository = getRepository(AppUserDetails);
        const superAdminRepository = getRepository(AppAdmin);
        const associationAdminRepository = getRepository(ASSOCIATION);
        const vendorAdminRepository = getRepository(VENDORASSOCIATION);
        const vendorRepository = getRepository(VENDOR);
        const flatOwnerRepository = getRepository(FlatOwners);
        const flatTenantRepository = getRepository(FlatTenants);

        const id = req.body.role;
        if (id == '1') {


        } else if (id == '2') {
            const association = await associationAdminRepository.findOne({
                where: {
                    EMAIL: req.body.email
                }
            }).then(data => {
                const assoObj = data;
                console.log(assoObj);
                const userID = data.id;
                const appUser = appUserRepository.findOne({
                    where: {
                        association: userID
                    }
                }).then(user => {
                    const username = user.USER_NAME;
                    AssociationSendMail.associationEnrollmentUserNameSendMail(username, req.body.email);
                    res.status(200).json({
                        message: 'Success',
                        association: assoObj,
                    })
                }).catch(error => {
                    console.log(error);
                    res.send(error);
                });
            }).catch(error => {
                console.log(error);
                // res.status(404).send(error);
                res.status(404).json({
                    message: 'Check Your Email',
                    error: error
                });
            });
        } else if (id == '3') {
            const vendorAssociation = await vendorAdminRepository.findOne({
                where: {
                    VENDOR_EMAIL: req.body.email
                }
            }).then(data => {
                const vendorAssociationObj = data;
                var vendorAssName = data.VENDOR_COMPANY_NAME
                console.log(vendorAssociationObj);
                const userID = data.id;
                const appUser = appUserRepository.findOne({
                    where: {
                        vendorAssociation: userID
                    }
                }).then(user => {
                    const username = user.USER_NAME;
                    VendorSendMail.vendorEnrollmentUserNameSendMail(vendorAssName,username, req.body.email);
                    res.status(200).json({
                        message: 'Success',
                        vendorAssociation: vendorAssociationObj,
                    })
                }).catch(error => {

                    res.send(error);
                });
            }).catch(error => {
                console.log(error);
                // res.status(404).send(error);
                res.status(404).json({
                    message: 'Check Your Email',
                    error: error
                });
            });

        } else if (id == '4') {
            const flatOwner = await flatOwnerRepository.findOne({
                where: {
                    OWNER_EMAIL: req.body.email
                }
            }).then(data => {
                const flatOwnerObj = data;
             var firstName =   data.OWNER_FIRST_NAME
                console.log(flatOwnerObj);
                const userID = data.FLAT_OWNER_ID;
                const appUser = appUserRepository.findOne({
                    where: {
                        flatOwner: userID
                    }
                }).then(user => {
                    const username = user.USER_NAME;
                    FlatOwnerSendMail.flatOwnerEnrollmentUserNameSendMail(firstName,username, req.body.email);
                    res.status(200).json({
                        message: 'Success',
                        flatOwner: flatOwnerObj,
                    })
                }).catch(error => {

                    res.send(error);
                });
            }).catch(error => {
                console.log(error);
                // res.status(404).send(error);
                res.status(404).json({
                    message: 'Check Your Email',
                    error: error
                });
            });
        } else if (id == '5') {
            const flatTenent = await flatTenantRepository.findOne({
                where: {
                    TENANT_EMAIL: req.body.email
                }
            }).then(data => {
                const flatOwnerObj = data;
                console.log(flatOwnerObj);
                const userID = data.TENANT_ID;
                var tenantName = data.TENANT_FIRST_NAME
                const appUser = appUserRepository.findOne({
                    where: {
                        flatTenant: userID
                    }
                }).then(user => {
                    const username = user.USER_NAME;
                    FlatTenantSendMail.flatTenantEnrollmentUserNameSendMail(tenantName,username, req.body.email);
                    res.status(200).json({
                        message: 'Success',
                        flatTenant: flatOwnerObj,
                    })
                }).catch(error => {

                    res.send(error);
                });
            }).catch(error => {
                console.log(error);
                // res.status(404).send(error);
                res.status(404).json({
                    message: 'Check Your Email',
                    error: error
                });
            });

        } else if (id == '6') {
            const vendor = await vendorRepository.findOne({
                where: {
                    VENDOR_EMAIL: req.body.email
                }
            }).then(data => {
                const vendorObj = data;
           var vendorName =     data.VENDOR_NAME
                console.log(vendorObj);
                const userID = data.id;
                const appUser = appUserRepository.findOne({
                    where: {
                        vendor: userID
                    }
                }).then(user => {
                    const username = user.USER_NAME;
                    VendorSendMail.vendorUserNameSendMail(vendorName,username, req.body.email);
                    res.status(200).json({
                        message: 'Success',
                        vendor: vendorObj,
                    })
                }).catch(error => {
                    res.send(error);
                });
            }).catch(error => {
                console.log(error);
                // res.status(404).send(error);
                res.status(404).json({
                    message: 'Check Your Email',
                    error: error
                });
            });

        } else {
            console.log("else");
            res.status(404).json({
                message: 'Nothing'
            })
        }

    }
}


export default UserController;