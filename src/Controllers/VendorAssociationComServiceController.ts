import { Request, Response } from "express";
import * as jwt from "jsonwebtoken";
import { getRepository } from "typeorm";
import { CommonAreaServices } from "../entity/CommonAreaService";
import { AppUserDetails } from "../entity/AppUserDetails";
import { VENDOR_ASSOCIATION_COM_SERVICES } from "../entity/VendorAssociationComServices";
import {VENDORASSOCIATION} from "../entity/VendorAssociation";
class AssociationComServiceController {
  
static addService = async (req: Request, res: Response) => {
    const  vassComServiceRep = getRepository(VENDOR_ASSOCIATION_COM_SERVICES);
    const  comServiceRep = getRepository(CommonAreaServices);
    const  vendorassociationRep = getRepository(VENDORASSOCIATION);
    const  appUserRep = getRepository(AppUserDetails);
   const ComServiceObj = await comServiceRep.findOne(req.body.comServiceId);
   const appUserObj = await appUserRep.findOne(req.body.appUserId);
   const vassociationObj = await vendorassociationRep.findOne(req.body.vendorAssociationId);
    const VassociationComService = new VENDOR_ASSOCIATION_COM_SERVICES();
    VassociationComService.comServices = ComServiceObj;
    VassociationComService.vassociation = vassociationObj;
    VassociationComService.CREATED_BY  = req.body.created_by;
    VassociationComService.UPDATED_BY  = req.body.created_by;
    const vassociationIndServiceObj = vassComServiceRep.create(VassociationComService);
    vassComServiceRep.save(vassociationIndServiceObj).then((data)=>{
         res.send(data)
     }
         ).catch((error)=>{
             res.send(error) 
         });

}

  
static allAssociationServices = async (req: Request, res: Response) => {
    const  vassComServiceRep = getRepository(VENDOR_ASSOCIATION_COM_SERVICES);
    vassComServiceRep.find({
        relations: ['comServices'],
        where: { appUser : req.params.id }
    }).then((data)=>{
        res.send(data)
    }
        ).catch((error)=>{
            res.send(error) 
        });

}
static delete = async (req: Request, res: Response) => {
    const  vassComServiceRep = getRepository(VENDOR_ASSOCIATION_COM_SERVICES);
    vassComServiceRep.delete(req.body.id).then((data)=>{
    res.send(data)
}
    ).catch((error)=>{
        res.send(error) 
    });

}


}

export default AssociationComServiceController;