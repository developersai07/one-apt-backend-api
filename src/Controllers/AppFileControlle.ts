import { Request, Response } from "express";
import * as jwt from "jsonwebtoken";
import { getRepository } from "typeorm";
import {APP_FILE } from "../entity/AppFiles";

class AppFileController {
 

static appFIleAdd = async (req: Request, res: Response) => {
    const  appFileRep = getRepository(APP_FILE);
   const appFIleObj =  await appFileRep.findOne(req.body.id);
   appFIleObj.FILE_LOC = req.body.file_loc;
   appFIleObj.UPDATED_BY  = req.body.created_by;
    appFileRep.save(appFIleObj).then((data)=>{
         res.send(data)
     }
         ).catch((error)=>{
             res.send(error) 
         });

}
static getAll = async (req: Request, res: Response) => {
    const  appFileRep = getRepository(APP_FILE);
   const appFIleres =  await appFileRep.find({});
   res.send(appFIleres)
}


}

export default AppFileController;