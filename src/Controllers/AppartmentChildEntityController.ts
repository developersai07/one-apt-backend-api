import { Request, Response } from "express";
import * as jwt from "jsonwebtoken";
import { getRepository } from "typeorm";
import { APPARTMENT_CHILD_ENTITIES } from "../entity/AppartmentChildEntites";
import { Appartment } from "../entity/AppartmentEntity";

class AppartmentChildEntityController {
  

static childEntityAdd = async (req: Request, res: Response) => {
    const  appartmentChildEntity = getRepository(APPARTMENT_CHILD_ENTITIES);
    const  appartmentEntity = getRepository(Appartment);
  const appartmentObj = await   appartmentEntity.findOne( req.body.appartmentId);
    const childEntity = new APPARTMENT_CHILD_ENTITIES();
    childEntity.CHILD_NAME = req.body.childName;
    childEntity.CREATED_BY = req.body.created_by;
    childEntity.UPDATED_BY = req.body.created_by;
    childEntity.appartmentChildEntiy = appartmentObj;

    const child = appartmentChildEntity.create(childEntity);
    appartmentChildEntity.save(child).then((data)=>{
         res.send(data)
     }
         ).catch((error)=>{
             res.send(error) 
         });

}


static childEntitygetAll = async (req: Request, res: Response) => {
        const  appartmentChildEntity = getRepository(APPARTMENT_CHILD_ENTITIES);
        appartmentChildEntity.find({
            relations: ['appartmentChildEntiy'],
        }).then((data)=>
        {
            res.send(data)
        }
            ).catch((error)=>{
            res.send(error) 
        });
}


static getByAppartment = async (req: Request, res: Response) => {
  const  appartmentChildEntity = getRepository(APPARTMENT_CHILD_ENTITIES);
  appartmentChildEntity.find({
    where : {
      appartmentChildEntiy : req.params.id
    },
      relations: ['appartmentChildEntiy'],
  }).then((data)=>
  {
      res.send(data)
  }
      ).catch((error)=>{
      res.send(error) 
  });
}


static childEntityUpdate = async (req: Request, res: Response) => {
    const  appartmentChildEntity = getRepository(APPARTMENT_CHILD_ENTITIES);
  const appChildUpObj = await  appartmentChildEntity.findOne(req.body.id);
  appChildUpObj.CHILD_NAME = req.body.childName;
  appartmentChildEntity.save(appChildUpObj).then((data)=>{
    res.send(data)
  }).catch((error)=>{
    res.send(error) 
});
}

static childEntityDelete = async (req: Request, res: Response) => {
    const  appartmentChildEntity =   getRepository(APPARTMENT_CHILD_ENTITIES);
    const results = await appartmentChildEntity.delete(req.body.id);
     res.send(results);
   }



}

export default AppartmentChildEntityController;