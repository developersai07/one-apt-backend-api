import { Request, Response, response } from "express";
import * as jwt from "jsonwebtoken";
import { getRepository } from "typeorm";
import { IND_SERVICE_TYPES } from "../entity/indServiceType";
import { INTERNAL_SERVICES_AMOUNT_VENDORS } from "../entity/InternalVendorServiceAmount";
import { IndividualAreaServices } from "../entity/IndividualAreaServices";
import { VENDOR } from "../entity/Vendors";

class internalServiceAmopuntController {
  static addInternalServiceAmount = async (req: Request, res: Response, next) => {
    const indServceTypeRep = getRepository(IND_SERVICE_TYPES);
    const indAreaServiceRep = getRepository(IndividualAreaServices);
    const internalVendormountRep = getRepository(INTERNAL_SERVICES_AMOUNT_VENDORS);
    const vendorRep = getRepository(VENDOR);

    const indServiceIdObj = await indAreaServiceRep.findOne(req.body.serviceId);
    const serviceTypeObj = await indServceTypeRep.findOne(req.body.serviceTypeId);
    const vendorObj = await vendorRep.findOne(req.body.vendorId);
    const internalVendorAmountobj = await internalVendormountRep.findOne({
      where: {
        indServicesTypes: req.body.serviceTypeId
      }
    }).then((data) => {
      if (data) {
        res.status(201).json({
          message: "Amount Already Added to Selected Service Type",
          obj: data
        });
      } else {
        const interAmountType = new INTERNAL_SERVICES_AMOUNT_VENDORS();
        interAmountType.indServices = indServiceIdObj;
        console.log("-----");
        console.log(serviceTypeObj);
        console.log("indServiceIdObj");
        console.log(indServiceIdObj);
        if (req.body.serviceTypeId) {
          interAmountType.indServicesTypes = serviceTypeObj;
        }
        interAmountType.AMOUNT = req.body.amount;
        interAmountType.vendor = vendorObj;
        interAmountType.CREATED_BY = req.body.created_by;
        interAmountType.UPDATED_BY = req.body.created_by;
        console.log(interAmountType);
        internalVendormountRep.save(interAmountType).then((data) => {
          res.send(data)
        }
        ).catch((error) => {
          console.log(error)
          res.send(error)
        });
      }
    }).catch((error) => {
      console.log(error)
      res.send(error)
    });





  }

  static updateInternalServiceAmount = async (req: Request, res: Response) => {
    console.log("updateInternalServiceAmount");
    const internalVendorAmountRep = getRepository(INTERNAL_SERVICES_AMOUNT_VENDORS);
    const internalVendorAmountobj = await internalVendorAmountRep.findOne({
      where: {
        INTERNAL_VENDOR_AMOUNT_ID: req.body.id
      }
    });
    console.log(internalVendorAmountobj);
    internalVendorAmountobj.AMOUNT = req.body.amount;
    internalVendorAmountobj.UPDATED_BY = req.body.updated_by;
    internalVendorAmountRep.save(internalVendorAmountobj).then((data) => {
      console.log(data);
      res.send(data);
    }).catch((error) => {
      res.send(error);
    });

  }

  static deleteInternalServiceAmount = async (req: Request, res: Response) => {
    console.log("delete: " + req.body.id);
    const internalVendorAmountRep = getRepository(INTERNAL_SERVICES_AMOUNT_VENDORS);
    const internalVendorAmountobj = await internalVendorAmountRep.find({
      where: {
        INTERNAL_VENDOR_AMOUNT_ID: req.body.id
      }
    });
    const results = await internalVendorAmountRep.delete(req.body.id);
    res.send(results);
  }


  static getByServiceId = async (req: Request, res: Response, next) => {

    const internalVendorSerRep = getRepository(INTERNAL_SERVICES_AMOUNT_VENDORS);

    const result = await internalVendorSerRep.query('SELECT *  FROM internal_vendors_serviceamount LEFT JOIN   ind_service_types ON internal_vendors_serviceamount.IND_SERVICE_ID = ind_service_types.IND_SERVICE_ID WHERE internal_vendors_serviceamount.IND_SERVICE_ID = ' + req.params.serviceid + ' AND internal_vendors_serviceamount.VENDOR_ID = ' + req.params.vendorid + ' GROUP BY  internal_vendors_serviceamount.INTERNAL_VENDOR_AMOUNT_ID');
    console.log("--------------------");
    console.log(result);

    res.send(result)
  }

}
export default internalServiceAmopuntController;