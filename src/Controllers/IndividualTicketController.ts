import { Request, Response } from "express";
import * as jwt from "jsonwebtoken";
import { getRepository } from "typeorm";
import { IndividualAreaServices } from "../entity/IndividualAreaServices";
import { IndividualTickets } from "../entity/IndividualTickets";
import { ASSOCIATION } from "../entity/Associations";
import { AppUserDetails } from "../entity/AppUserDetails";
import { IndividualServiceMapSubscriptions } from "../entity/IndServiceMapSubscriptions";
import OwnerIndTicketSendMail from "../Mails/OwnerIndTicketSendMail";
import vendorSendMail from "../Mails/VendorSendMail";
import { FlatOwners } from "../entity/FlatOwners";
import { MapIndTicketVendor } from '../entity/MapIndTicketsVendor'
import { VENDOR } from "../entity/Vendors";
import { ASSOCIATION_VENDORS } from "../entity/AssociationVendors";
import { INDIVIDUAL_TICKET_OTP } from "../entity/IndividualTIcketOtp";
import { IND_SERVICE_PLANS } from '../entity/indServicePlan';
import { IND_SERVICE_TYPES } from '../entity/indServiceType';
import { INTERNAL_SERVICES_AMOUNT_VENDORS } from '../entity/InternalVendorServiceAmount';
import { IND_SER_TYPES_VENDOR_ASSOCIATION } from '../entity/IndSerTypesVendorAssociation';
import { Plans } from "../entity/Plans";
import { FlatTenants } from "../entity/FlatTenants";
import assSendMail from "../Mails/AssociationSendMail"

class IndividualTicketController {
    static addTicket = async (req: Request, res: Response, next) => {
        const associationRepository = getRepository(ASSOCIATION);
        const individualAreaServiceRepository = getRepository(IndividualAreaServices);
        const individualServiceMapSub = getRepository(IndividualServiceMapSubscriptions);
        const indTicketOtpRep = getRepository(INDIVIDUAL_TICKET_OTP);
        const indTicketRepository = getRepository(IndividualTickets);
        const indServPlanRep = getRepository(IND_SERVICE_PLANS);
        const indSerTypeRep = getRepository(IND_SERVICE_TYPES);
        const appUserDetailRep = getRepository(AppUserDetails);
        const flatOwnersRep = getRepository(FlatOwners);
        const flatTenantsRep = getRepository(FlatTenants);
        const association = await associationRepository.findOne(req.body.associationId);
        const indService = await individualAreaServiceRepository.findOne(req.body.serviceId);
        //   const indServiceMapSub = await individualServiceMapSub.findOne(req.body.service_subscription_id);
        const indServiceMapSub = await indServPlanRep.findOne(req.body.service_subscription_id);

        console.log(req.body.service_types_id + "req.body.service_types_id")
        const indSerTypeSub = await indSerTypeRep.findOne(req.body.service_types_id);

        const appUser = await appUserDetailRep.findOne(

            {
                where: {
                    APP_USER_ID: req.body.appUserId
                },
                relations: ["flatOwner", "flatTenant"]
            }

            // req.body.appUserId
        );
        var spliitedDate = req.body.ticket_date.split('T');
        var spliDate = spliitedDate[0];
        req.body.ticket_date = spliDate;
        var spliitedTIme = req.body.ticket_time.split('T');
        var newDtime = spliitedTIme[1];
        var newtime = newDtime.split('.');
        var ticketTime = newtime[0];
        req.body.ticket_time = ticketTime;
        var email;
        req.body.associationName = association.ASSOCIATION_NAME;
        req.body.associationEmail = association.EMAIL;
        if (appUser.flatOwner) {
            console.log("flatOwner")
            var flatOwnerId = appUser.flatOwner.FLAT_OWNER_ID;
            const flatOwnerObj = await flatOwnersRep.findOne({
                where : {
                    FLAT_OWNER_ID  : flatOwnerId
                },
                relations : ["associationChildEntity","associationGrandChildEntity"]
            });
            email = flatOwnerObj.OWNER_EMAIL;
            req.body.appUserEmail = email;
            req.body.appUserName =   flatOwnerObj.OWNER_FIRST_NAME ;
            req.body.appUserChildName =   flatOwnerObj.associationChildEntity.CHILD_NAME ;
            req.body.appUserGrandchild =  flatOwnerObj.associationGrandChildEntity.GARND_CHILD_NAME;

        }

        if (appUser.flatTenant) {
            console.log("flatTenant")
            var flatTenantId = appUser.flatTenant.TENANT_ID;
            const flatTenantObj = await flatTenantsRep.findOne({
                where : {
                    TENANT_ID : flatTenantId
                },
                relations : ["associationChildEntity","associationGrandChildEntity"]
            });
            email = flatTenantObj.TENANT_EMAIL;
            req.body.appUserEmail = email;
            req.body.appUserName =   flatTenantObj.TENANT_FIRST_NAME ;
            req.body.appUserChildName =   flatTenantObj.associationChildEntity.CHILD_NAME ;
            req.body.appUserGrandchild =  flatTenantObj.associationGrandChildEntity.GARND_CHILD_NAME;

        }

        const indTicket = new IndividualTickets();
        indTicket.TICKET_DESCRIPTION = req.body.description;
        indTicket.TICKET_DATE = spliDate;
        indTicket.TICKET_TIME = ticketTime;
        if(req.body.quantity != 'undefined'){

            indTicket.QUANTITY = req.body.quantity;
        }
        else {
            req.body.quantity = null
        }
    
        indTicket.IS_ACTIVE = true;
        indTicket.STATUS = 'Pending';
        indTicket.association = association;
        indTicket.serviceId = indService;
        indTicket.indSerPlan = indServiceMapSub;
        indTicket.indServiceTypes = indSerTypeSub;
        indTicket.appUserDetails = appUser;
        indTicket.CREATED_BY = req.body.appUserId;
        indTicket.UPDATED_BY = req.body.appUserId;
        indTicket.IMG_LOCATION = "No";
        const indTicketObj = indTicketRepository.create(indTicket);
        indTicketRepository.save(indTicketObj).then(async (data) => {
            var randomNumber = Math.floor(100000 + Math.random() * 900000) + '';
            const newIndTicketOtp = new INDIVIDUAL_TICKET_OTP();

            newIndTicketOtp.OTP_DATE = spliDate;
            newIndTicketOtp.OTP_NUMBER = randomNumber;
            newIndTicketOtp.IS_ACTIVE = true;
            newIndTicketOtp.IS_VERIFIED = false;
            newIndTicketOtp.IS_VERIFIED = false;
            newIndTicketOtp.IS_USER_VERIFIED = false;
            newIndTicketOtp.IS_WORK_COMPLETED = false;
            newIndTicketOtp.indTickets = indTicketObj;

            const indTIcketObj = await indTicketOtpRep.save(newIndTicketOtp);
            req.body.appUserserviceName = indService.IND_SERVICE_NAME;
            req.body.appUserserviceTypeName = indSerTypeSub.TYPE;
            req.body.appUserQuantity = req.body.quantity;
            req.body.appUserPlan =indServiceMapSub.PLAN



            // OwnerIndTicketSendMail.IndTicketSendMail(data.IND_TICKET_ID, req.body.description, email, req.body.ticket_date, req.body.ticket_time);
            req.body.indTicketId = data.IND_TICKET_ID;
            req.body.data = data;
            next();
            // res.send(data)
        }
        ).catch((error) => {
            res.send(error)
        });
    }





    static updateFileName = async (req: Request, res: Response, next) => {
        const indTicketRep = getRepository(IndividualTickets);
        const indTicket = await indTicketRep.findOne(req.body.indTicketId);
        if (req.body.indTicketImg) {
            indTicket.IMG_LOCATION = req.body.indTicketImg;
        }
        indTicketRep.save(indTicket).then(data => {
            next()
            //   res.send(data);
        }).catch((error) => {
            res.status(500).send(error)
        });
    }

    static indTicketAssignPrimaryVendor = async (req: Request, res: Response, next) => {

        const mapIndVendorRep = await getRepository(MapIndTicketVendor);
        const vendorRep = await getRepository(VENDOR);
        const individualTicketRep = await getRepository(IndividualTickets);
        var date = "2020-03-23T" + req.body.ticket_time + ".573Z";
        var postTime = getPostTIme(date);
        var preTime = getPreTime(date);

        //checking priority 1 
        const prioryOneVendor = await getRepository(VENDOR)
            .createQueryBuilder("vendor")
            .leftJoinAndSelect("vendor.assVendors", "assVendor", "assVendor.vendorInfo = vendor.id")
            .leftJoinAndSelect("vendor.vensServices", "venService", "venService.vendorInfo = vendor.id")
            .leftJoinAndSelect("vendor.mapVendor", "mapVendors", "mapVendors.vendor = vendor.id")
            .leftJoinAndSelect("mapVendors.indTickets", "indTicket", "indTicket.IND_TICKET_ID = mapVendors.IND_TICKET_ID")
            // .where("assVendor.ASSOCIATION_ID =  :id AND venService.IND_SERVICE_ID = :service  AND indTicket.TICKET_DATE = :ticket_date  AND (indTicket.TICKET_TIME  BETWEEN '"+preTime+"' AND '"+postTime+"')",{ id:   req.body.associationId,
            //     service: req.body.serviceId,
            //     ticket_date: req.body.ticket_date,
            // })
            .where("assVendor.ASSOCIATION_ID =  :id AND venService.IND_SERVICE_ID = :service ", {
                id: req.body.associationId,
                service: req.body.serviceId,
            })
            .getOne();
        if (prioryOneVendor) {
            var vendorId = prioryOneVendor.id;
            //res.send(prioryOneVendor);
            const vendorObj = await vendorRep.findOne(vendorId);
            const indObj = await individualTicketRep.findOne(req.body.indTicketId)
            const mapVendorObj = new MapIndTicketVendor();
            mapVendorObj.vendor = vendorObj;
            mapVendorObj.indTickets = indObj;
            mapVendorObj.IS_ACCEPTED = false;
            mapVendorObj.IS_REJECTED = false;
            mapVendorObj.IS_ACTIVE = true;
            var samapVendorObj = mapIndVendorRep.create(mapVendorObj);

            mapIndVendorRep.save(samapVendorObj).then((mapIndObj) => {
                res.send(req.body.data);
            }).catch((error) => {
                res.status(500).send(error)
            });
        }
        else {
            //     const prioryVendor = await getRepository(VENDOR)
            //     .createQueryBuilder("vendor")
            //     .leftJoinAndSelect("vendor.assVendors", "assVendor", "assVendor.vendorInfo = vendor.id")
            //     .leftJoinAndSelect("vendor.vensServices", "venService", "venService.vendorInfo = vendor.id")
            //     .leftJoinAndSelect("vendor.mapVendor", "mapVendors", "mapVendors.vendor = vendor.id")
            //     .where("assVendor.ASSOCIATION_ID =  :id AND venService.IND_SERVICE_ID = :service ",{ id:   req.body.associationId,
            //         service: req.body.serviceId,
            //     })
            //     .getOne();
            //     var vendorId = prioryVendor.id;

            //  const vendorObj = await  vendorRep.findOne(vendorId);
            //   const indObj = await individualTicketRep.findOne( req.body.indTicketId)

            //  const mapVendorObj = new MapIndTicketVendor();
            //  mapVendorObj.vendor = vendorObj;
            //  mapVendorObj.indTickets = indObj;
            //  mapVendorObj.IS_ACCEPTED = false;
            //  mapVendorObj.IS_REJECTED = false;
            //  mapVendorObj.IS_ACTIVE = true;
            //  var samapVendorObj = mapIndVendorRep.create(mapVendorObj);

            //  mapIndVendorRep.save(samapVendorObj).then((mapIndObj)=>{
            //     res.send(req.body.data);
            //  }).catch((error) => {
            //     res.status(500).send(error)
            // });

            res.status(500).send("not able to assign vendor")
        }

    }

    static test = async (req: Request, res: Response, next) => {
        var date = "2020-03-23T" + req.body.ticket_time + ".573Z";
        var postTime = getPostTIme(date);
        var preTime = getPreTime(date);
        console.log(req.body.ticket_date)
        const prioryOneVendor = await getRepository(VENDOR).query("SELECT * FROM vendor LEFT JOIN map_ind_tickets_vendors ON   map_ind_tickets_vendors.VENDOR_ID = vendor.VENDOR_ID LEFT JOIN individual_service_tickets ON    individual_service_tickets.IND_TICKET_ID = map_ind_tickets_vendors.IND_TICKET_ID LEFT JOIN association_vendors ON   association_vendors.`VENDOR_ASSOCIATION_ID` = vendor.`VENDOR_ASSOCIATION_ID` LEFT JOIN vendor_services ON   vendor_services.`VENDOR_ID` = vendor.`VENDOR_ID` LEFT JOIN association_ven_ind_ser_priority ON   association_ven_ind_ser_priority.`VENDOR_ID` = vendor.`VENDOR_ID` WHERE association_vendors.`ASSOCIATION_ID` = 1 AND association_ven_ind_ser_priority.`PRIORITY` = 1  AND association_ven_ind_ser_priority.`ASSOCIATION_ID` = 1 AND individual_service_tickets.TICKET_DATE = '2020-03-28' AND (individual_service_tickets.TICKET_TIME   BETWEEN '21:18:53' AND '21:20:53') GROUP BY vendor.`VENDOR_ID`")
        // .createQueryBuilder("vendor")
        // .leftJoinAndSelect("vendor.assVendors", "assVendor", "assVendor.vendorInfo = vendor.id")
        // .leftJoinAndSelect("vendor.vensServices", "venService", "venService.vendorInfo = vendor.id")
        // .leftJoinAndSelect("vendor.mapVendor", "mapVendors", "mapVendors.vendor = vendor.id")
        // .leftJoinAndSelect("mapVendors.indTickets", "indTicket", "indTicket.IND_TICKET_ID = mapVendors.IND_TICKET_ID")
        // .leftJoinAndSelect("vendor.assVenSerPriority", "assSerPriority", "assSerPriority.vendorInfo = vendor.id")
        // .where("assVendor.ASSOCIATION_ID =  :id AND venService.IND_SERVICE_ID = :service  AND indTicket.TICKET_DATE = :ticket_date AND assSerPriority.PRIORITY = 1  AND  assVendor.IS_INTERNAL_VENDOR = false AND  assVendor.IS_THIRD_PARTY_VENDOR = true AND (indTicket.TICKET_TIME   BETWEEN '"+preTime+"' AND '"+postTime+" ')",{ id:   req.body.associationId,
        //     service: req.body.serviceId,
        //     ticket_date: req.body.ticket_date,
        // }).getOne();

        // if(prioryOneVendor){

        //     res.send(prioryOneVendor)
        //             //   next()
        // }
        // else {
        //     res.send(prioryOneVendor)
        // }
        res.send(prioryOneVendor)

    }

    //check 1 internavl vendor

    static checkPrimaryInternalVendor = async (req: Request, res: Response, next) => {
        const mapIndVendorRep = await getRepository(MapIndTicketVendor);
        const vendorRep = await getRepository(VENDOR);
        const individualTicketRep = await getRepository(IndividualTickets);

        // const indServPlanRep = getRepository(IND_SERVICE_PLANS);

        // const indServiceMapSub = await indServPlanRep.findOne(req.body.service_subscription_id);

        var date = "2020-03-23T" + req.body.ticket_time + ".573Z";
        var postTime = getPostTIme(date);
        var preTime = getPreTime(date);
        console.log(req.body.ticket_date)
        const prioryOneVendor = await getRepository(VENDOR)
            .createQueryBuilder("vendor")
            .leftJoinAndSelect("vendor.assVendors", "assVendor", "assVendor.vendorInfo = vendor.id")
            .leftJoinAndSelect("vendor.vensServices", "venService", "venService.vendorInfo = vendor.id")
            .leftJoinAndSelect("vendor.mapVendor", "mapVendors", "mapVendors.vendor = vendor.id")
            .leftJoinAndSelect("mapVendors.indTickets", "indTicket", "indTicket.IND_TICKET_ID = mapVendors.IND_TICKET_ID")
            .leftJoinAndSelect("vendor.assVenSerPriority", "assSerPriority", "assSerPriority.vendorInfo = vendor.id")
            .where("assVendor.ASSOCIATION_ID =  :id AND venService.IND_SERVICE_ID = :service  AND assSerPriority.PRIORITY = 1  AND  assVendor.IS_INTERNAL_VENDOR = true AND indTicket.STATUS = 'Pending'   AND   indTicket.TICKET_DATE = :ticket_date AND (indTicket.TICKET_TIME   BETWEEN '" + preTime + "' AND '" + postTime + " ') ", {
                id: req.body.associationId,
                service: req.body.serviceId,
                ticket_date: req.body.ticket_date,
            }).getOne();
console.log("priorOneVendor")
            console.log(prioryOneVendor);




        if (prioryOneVendor) {
            next()
        }
        else {

            const prioryOneElVendor = await getRepository(VENDOR)
                .createQueryBuilder("vendor")
                .leftJoinAndSelect("vendor.assVendors", "assVendor", "assVendor.vendorInfo = vendor.id")
                .leftJoinAndSelect("vendor.vensServices", "venService", "venService.vendorInfo = vendor.id")
                .leftJoinAndSelect("vendor.mapVendor", "mapVendors", "mapVendors.vendor = vendor.id")
                .leftJoinAndSelect("mapVendors.indTickets", "indTicket", "indTicket.IND_TICKET_ID = mapVendors.IND_TICKET_ID")
                .leftJoinAndSelect("vendor.assVenSerPriority", "assSerPriority", "assSerPriority.vendorInfo = vendor.id")
                .where("assVendor.ASSOCIATION_ID =  :id AND venService.IND_SERVICE_ID = :service   AND assVendor.IS_INTERNAL_VENDOR = true AND   assSerPriority.PRIORITY = 1 ", {
                    id: req.body.associationId,
                    service: req.body.serviceId,
                }).getOne();

            console.log(prioryOneElVendor)
            if (prioryOneElVendor != undefined) {
                var vendorId = prioryOneElVendor.id;
                const vendorObj = await vendorRep.findOne(vendorId);
                const indObj = await individualTicketRep.findOne(
                    {
                        where : {
                            IND_TICKET_ID :  req.body.indTicketId
                        },

                        relations : ["indSerPlan"]
                    }
                   
                    )
                console.log(indObj)
                const mapVendorObj = new MapIndTicketVendor();
                mapVendorObj.vendor = vendorObj;
                mapVendorObj.indTickets = indObj;
                mapVendorObj.IS_ACCEPTED = false;
                mapVendorObj.IS_REJECTED = false;
                mapVendorObj.IS_ACTIVE = true;
                var samapVendorObj = mapIndVendorRep.create(mapVendorObj);

                mapIndVendorRep.save(samapVendorObj).then(async (mapIndObj) => {
                    console.log("dvsdfjkkjkjsf")
                    if (req.body.service_types_id) {

                        console.log("jkkjkjsf")
                        req.body.serviceId

                        const interServAmountVendor = await getRepository(INTERNAL_SERVICES_AMOUNT_VENDORS)
                            .createQueryBuilder("interServVendor")
                            .where("interServVendor.IND_SERVICE_ID =  :indServiceId AND interServVendor.SERVICE_TYPE_ID = :indServiceTypeId AND VENDOR_ID = :vendorId ", {
                                indServiceId: req.body.serviceId,
                                indServiceTypeId: req.body.service_types_id,
                                vendorId: vendorId
                            }).getOne().then(async (data) => {

                                console.log(data)

                                var amount = data.AMOUNT;
                                if (req.body.quantity) {

                                    amount = amount * req.body.quantity;
                                }

                                if(indObj.indSerPlan.PLAN == 'Monthly'){
     const indServPlanRep = getRepository(IND_SERVICE_PLANS);
        const indServiceMapSub = await indServPlanRep.findOne(indObj.indSerPlan.IND_SERVICE_PLAN_ID);
       var days =  indServiceMapSub.DAYS;
       indObj.AMOUNT = amount * days;
  
                                }
                                else {

                                    indObj.AMOUNT = amount;
                                    console.log(amount + "amount")
    
                                }

                                var esult = await individualTicketRep.save(indObj);

                                var appUserDetails = req.body.appUserName+'-'+req.body.appUserChildName+'-'+ req.body.appUserGrandchild; 
                                vendorSendMail.vendoIndSendMail(req.body.indTicketId, req.body.description,   vendorObj.VENDOR_EMAIL, req.body.ticket_date, req.body.ticket_time,   req.body.appUserserviceName, req.body.appUserserviceTypeName,req.body.appUserQuantity ,amount,req.body.appUserPlan,req.body.associationName,  appUserDetails)
                                OwnerIndTicketSendMail.IndTicketSendMail(req.body.indTicketId, req.body.description,   req.body.appUserEmail, req.body.ticket_date, req.body.ticket_time,   req.body.appUserserviceName, req.body.appUserserviceTypeName,req.body.appUserQuantity ,amount,req.body.appUserPlan,vendorObj.VENDOR_NAME);

                            }).catch((error) => {
                                console.log(error)
                            });




                    }
                    else {

                        const interServAmountVendor = await getRepository(INTERNAL_SERVICES_AMOUNT_VENDORS)
                            .createQueryBuilder("interServVendor")
                            .where("interServVendor.IND_SERVICE_ID =  :indServiceId  AND VENDOR_ID = :vendorId", {
                                indServiceId: req.body.serviceId,
                                vendorId: vendorId
                            }).getOne().then(async (data) => {
                                var amount = data.AMOUNT;


                                if (req.body.quantity) {

                                    amount = amount * req.body.quantity;
                                }

                                    if(indObj.indSerPlan.PLAN == 'Monthly'){
                                    const indServPlanRep = getRepository(IND_SERVICE_PLANS);
                                    const indServiceMapSub = await indServPlanRep.findOne(indObj.indSerPlan.IND_SERVICE_PLAN_ID);
                                    var days =  indServiceMapSub.DAYS;
                                    indObj.AMOUNT = amount * days;

                                    }
                                    else {

                                    indObj.AMOUNT = amount;
                                    console.log(amount + "amount")

                                    }


                                var esult = await individualTicketRep.save(indObj);

                                var appUserDetails = req.body.appUserName+'-'+req.body.appUserChildName+'-'+ req.body.appUserGrandchild; 
                                vendorSendMail.vendoIndSendMail(req.body.indTicketId, req.body.description,   vendorObj.VENDOR_EMAIL, req.body.ticket_date, req.body.ticket_time,   req.body.appUserserviceName, req.body.appUserserviceTypeName,req.body.appUserQuantity ,amount,req.body.appUserPlan,req.body.associationName,  appUserDetails)
                                          
                                OwnerIndTicketSendMail.IndTicketSendMail(req.body.indTicketId, req.body.description,   req.body.appUserEmail, req.body.ticket_date, req.body.ticket_time,   req.body.appUserserviceName, req.body.appUserserviceTypeName,req.body.appUserQuantity ,amount,req.body.appUserPlan,vendorObj.VENDOR_NAME);
                            }).catch((error) => {
                                console.log(error)
                            });


                    }

                    console.log("working 1")
                    res.send(req.body.data);
                }).catch((error) => {
                    res.status(500).send(error)
                });
            }
            else {
                next()
            }

        }
    }



    //check internal second


    static checkSecondInternalVendor = async (req: Request, res: Response, next) => {

        console.log("seconfdary")
        const mapIndVendorRep = await getRepository(MapIndTicketVendor);
        const vendorRep = await getRepository(VENDOR);
        const individualTicketRep = await getRepository(IndividualTickets);
        var date = "2020-03-23T" + req.body.ticket_time + ".573Z";
        var postTime = getPostTIme(date);
        var preTime = getPreTime(date);
        const prioryOneVendor = await getRepository(VENDOR)
            .createQueryBuilder("vendor")
            .leftJoinAndSelect("vendor.assVendors", "assVendor", "assVendor.vendorInfo = vendor.id")
            .leftJoinAndSelect("vendor.vensServices", "venService", "venService.vendorInfo = vendor.id")
            .leftJoinAndSelect("vendor.mapVendor", "mapVendors", "mapVendors.vendor = vendor.id")
            .leftJoinAndSelect("mapVendors.indTickets", "indTicket", "indTicket.IND_TICKET_ID = mapVendors.IND_TICKET_ID")
            .leftJoinAndSelect("vendor.assVenSerPriority", "assSerPriority", "assSerPriority.vendorInfo = vendor.id")
            .where("assVendor.ASSOCIATION_ID =  :id AND venService.IND_SERVICE_ID = :service  AND indTicket.STATUS = 'Pending'  AND indTicket.TICKET_DATE = :ticket_date AND assSerPriority.PRIORITY = 2  AND  assVendor.IS_INTERNAL_VENDOR = true AND (indTicket.TICKET_TIME   BETWEEN '" + preTime + "' AND '" + postTime + " ')", {
                id: req.body.associationId,
                service: req.body.serviceId,
                ticket_date: req.body.ticket_date,
            }).getOne();


        console.log(prioryOneVendor)
        if (prioryOneVendor) {
            // res.send(req.body.data);
            next()
        }
        else {
            const prioryOneElVendor = await getRepository(VENDOR)
                .createQueryBuilder("vendor")
                .leftJoinAndSelect("vendor.assVendors", "assVendor", "assVendor.vendorInfo = vendor.id")
                .leftJoinAndSelect("vendor.vensServices", "venService", "venService.vendorInfo = vendor.id")
                .leftJoinAndSelect("vendor.mapVendor", "mapVendors", "mapVendors.vendor = vendor.id")
                .leftJoinAndSelect("mapVendors.indTickets", "indTicket", "indTicket.IND_TICKET_ID = mapVendors.IND_TICKET_ID")
                .leftJoinAndSelect("vendor.assVenSerPriority", "assSerPriority", "assSerPriority.vendorInfo = vendor.id")
                .where("assVendor.ASSOCIATION_ID =  :id AND venService.IND_SERVICE_ID = :service   AND assVendor.IS_INTERNAL_VENDOR = true AND    assSerPriority.PRIORITY = 2 ", {
                    id: req.body.associationId,
                    service: req.body.serviceId,
                }).getOne();

            console.log(prioryOneElVendor)

            if (prioryOneElVendor != undefined) {
                var vendorId = prioryOneElVendor.id;

                //if(prioryOneElVendor.id){

                var vendorId = prioryOneElVendor.id;
                //res.send(prioryOneVendor);

                console.log(req.body.indTicketId + "req.body.indTicketId")

                const vendorObj = await vendorRep.findOne(vendorId);
                const indObj = await individualTicketRep.findOne(
                    {
                        where : {
                            IND_TICKET_ID :  req.body.indTicketId
                        },

                        relations : ["indSerPlan"]
                    }
                   
                    )
                console.log(indObj)
                const mapVendorObj = new MapIndTicketVendor();
                mapVendorObj.vendor = vendorObj;
                mapVendorObj.indTickets = indObj;
                mapVendorObj.IS_ACCEPTED = false;
                mapVendorObj.IS_REJECTED = false;
                mapVendorObj.IS_ACTIVE = true;
                var samapVendorObj = mapIndVendorRep.create(mapVendorObj);

                mapIndVendorRep.save(samapVendorObj).then(async (mapIndObj) => {

                    if (req.body.service_types_id) {
                        req.body.serviceId

                        const interServAmountVendor = await getRepository(INTERNAL_SERVICES_AMOUNT_VENDORS)
                            .createQueryBuilder("interServVendor")
                            .where("interServVendor.IND_SERVICE_ID =  :indServiceId AND interServVendor.SERVICE_TYPE_ID = :indServiceTypeId AND VENDOR_ID = :vendorId", {
                                indServiceId: req.body.serviceId,
                                indServiceTypeId: req.body.service_types_id,
                                vendorId: vendorId
                            }).getOne().then(async (data) => {

                                console.log("soaijdiaubsndhsahtsydffsadftrardtdradrsa")
                                console.log(data)
                                var amount = data.AMOUNT;


                                if (req.body.quantity) {


                                    console.log("2soaijdiaubsndhsahtsydffsadftrardtdradrsa")

                                    amount = amount * req.body.quantity;
                                }
                                    if(indObj.indSerPlan.PLAN == 'Monthly'){
                                    const indServPlanRep = getRepository(IND_SERVICE_PLANS);
                                    const indServiceMapSub = await indServPlanRep.findOne(indObj.indSerPlan.IND_SERVICE_PLAN_ID);
                                    var days =  indServiceMapSub.DAYS;
                                    indObj.AMOUNT = amount * days;

                                    }
                                    else {

                                    indObj.AMOUNT = amount;
                                    console.log(amount + "amount")

                                    }

                                var esult = await individualTicketRep.save(indObj);
                                var appUserDetails = req.body.appUserName+'-'+req.body.appUserChildName+'-'+ req.body.appUserGrandchild; 
                                vendorSendMail.vendoIndSendMail(req.body.indTicketId, req.body.description,   vendorObj.VENDOR_EMAIL, req.body.ticket_date, req.body.ticket_time,   req.body.appUserserviceName, req.body.appUserserviceTypeName,req.body.appUserQuantity ,amount,req.body.appUserPlan,req.body.associationName,  appUserDetails)
                                
                                OwnerIndTicketSendMail.IndTicketSendMail(req.body.indTicketId, req.body.description,   req.body.appUserEmail, req.body.ticket_date, req.body.ticket_time,   req.body.appUserserviceName, req.body.appUserserviceTypeName,req.body.appUserQuantity ,amount,req.body.appUserPlan,vendorObj.VENDOR_NAME);
                            }).catch((error) => {
                                console.log(error);
                            });





                    }
                    else {

                        const interServAmountVendor = await getRepository(INTERNAL_SERVICES_AMOUNT_VENDORS)
                            .createQueryBuilder("interServVendor")
                            .where("interServVendor.IND_SERVICE_ID =  :indServiceId  AND VENDOR_ID = :vendorId", {
                                indServiceId: req.body.serviceId,
                                vendorId: vendorId
                            }).getOne().then(async (data) => {
                                var amount = data.AMOUNT;


                                if (req.body.quantity != 'undefined') {

                                    amount = amount * req.body.quantity;
                                }

                                if(indObj.indSerPlan.PLAN == 'Monthly'){
                                    const indServPlanRep = getRepository(IND_SERVICE_PLANS);
                                    const indServiceMapSub = await indServPlanRep.findOne(indObj.indSerPlan.IND_SERVICE_PLAN_ID);
                                    var days =  indServiceMapSub.DAYS;
                                    indObj.AMOUNT = amount * days;

                                    }
                                    else {

                                    indObj.AMOUNT = amount;
                                    console.log(amount + "amount")

                                    }

                                var esult = await individualTicketRep.save(indObj);
                                var appUserDetails = req.body.appUserName+'-'+req.body.appUserChildName+'-'+ req.body.appUserGrandchild; 
                                vendorSendMail.vendoIndSendMail(req.body.indTicketId, req.body.description,   vendorObj.VENDOR_EMAIL, req.body.ticket_date, req.body.ticket_time,   req.body.appUserserviceName, req.body.appUserserviceTypeName,req.body.appUserQuantity ,amount,req.body.appUserPlan,req.body.associationName,  appUserDetails)
                                          
                                OwnerIndTicketSendMail.IndTicketSendMail(req.body.indTicketId, req.body.description,   req.body.appUserEmail, req.body.ticket_date, req.body.ticket_time,   req.body.appUserserviceName, req.body.appUserserviceTypeName,req.body.appUserQuantity ,amount,req.body.appUserPlan,vendorObj.VENDOR_NAME);
                            }).catch((error) => {
                                console.log(error);
                            });
                    }
                    console.log("working 2")
                    res.send(req.body.data);
                }).catch((error) => {
                    res.status(500).send(error)
                });
            }
            else {
                // res.send(req.body.data);
                next();
            }


        }


    }


    static checkThirdInternalVendor = async (req: Request, res: Response, next) => {
        console.log("third")
        const mapIndVendorRep = await getRepository(MapIndTicketVendor);
        const vendorRep = await getRepository(VENDOR);
        const individualTicketRep = await getRepository(IndividualTickets);
        var date = "2020-03-23T" + req.body.ticket_time + ".573Z";
        var postTime = getPostTIme(date);
        var preTime = getPreTime(date);
        const prioryOneVendor = await getRepository(VENDOR)
            .createQueryBuilder("vendor")
            .leftJoinAndSelect("vendor.assVendors", "assVendor", "assVendor.vendorInfo = vendor.id")
            .leftJoinAndSelect("vendor.vensServices", "venService", "venService.vendorInfo = vendor.id")
            .leftJoinAndSelect("vendor.mapVendor", "mapVendors", "mapVendors.vendor = vendor.id")
            .leftJoinAndSelect("mapVendors.indTickets", "indTicket", "indTicket.IND_TICKET_ID = mapVendors.IND_TICKET_ID")
            .leftJoinAndSelect("vendor.assVenSerPriority", "assSerPriority", "assSerPriority.vendorInfo = vendor.id")
            .where("assVendor.ASSOCIATION_ID =  :id AND venService.IND_SERVICE_ID = :service  AND indTicket.STATUS = 'Pending'  AND indTicket.TICKET_DATE = :ticket_date AND assSerPriority.PRIORITY = 3  AND  assVendor.IS_INTERNAL_VENDOR = true AND (indTicket.TICKET_TIME   BETWEEN '" + preTime + "' AND '" + postTime + " ')", {
                id: req.body.associationId,
                service: req.body.serviceId,
                ticket_date: req.body.ticket_date,
            }).getOne();


        console.log(prioryOneVendor)
        if (prioryOneVendor) {
            // res.send(req.body.data);
            next()
        }
        else {
            const prioryOneElVendor = await getRepository(VENDOR)
                .createQueryBuilder("vendor")
                .leftJoinAndSelect("vendor.assVendors", "assVendor", "assVendor.vendorInfo = vendor.id")
                .leftJoinAndSelect("vendor.vensServices", "venService", "venService.vendorInfo = vendor.id")
                .leftJoinAndSelect("vendor.mapVendor", "mapVendors", "mapVendors.vendor = vendor.id")
                .leftJoinAndSelect("mapVendors.indTickets", "indTicket", "indTicket.IND_TICKET_ID = mapVendors.IND_TICKET_ID")
                .leftJoinAndSelect("vendor.assVenSerPriority", "assSerPriority", "assSerPriority.vendorInfo = vendor.id")
                .where("assVendor.ASSOCIATION_ID =  :id AND venService.IND_SERVICE_ID = :service   AND assVendor.IS_INTERNAL_VENDOR = true AND    assSerPriority.PRIORITY = 3 ", {
                    id: req.body.associationId,
                    service: req.body.serviceId,
                }).getOne();

            console.log(prioryOneElVendor)


            if (prioryOneElVendor != undefined) {
                var vendorId = prioryOneElVendor.id;

                // var vendorId = prioryOneElVendor.id;
                //res.send(prioryOneVendor);
                const vendorObj = await vendorRep.findOne(vendorId);
                const indObj = await individualTicketRep.findOne(
                    {
                        where : {
                            IND_TICKET_ID :  req.body.indTicketId
                        },

                        relations : ["indSerPlan"]
                    }
                   
                    )
                const mapVendorObj = new MapIndTicketVendor();
                mapVendorObj.vendor = vendorObj;
                mapVendorObj.indTickets = indObj;
                mapVendorObj.IS_ACCEPTED = false;
                mapVendorObj.IS_REJECTED = false;
                mapVendorObj.IS_ACTIVE = true;
                var samapVendorObj = mapIndVendorRep.create(mapVendorObj);

                mapIndVendorRep.save(samapVendorObj).then(async (mapIndObj) => {

                    if (req.body.service_types_id) {
                        req.body.serviceId

                        const interServAmountVendor = await getRepository(INTERNAL_SERVICES_AMOUNT_VENDORS)
                            .createQueryBuilder("interServVendor")
                            .where("interServVendor.IND_SERVICE_ID =  :indServiceId AND interServVendor.SERVICE_TYPE_ID = :indServiceTypeId AND VENDOR_ID = :vendorId", {
                                indServiceId: req.body.serviceId,
                                indServiceTypeId: req.body.service_types_id,
                                vendorId: vendorId
                            }).getOne().then(async (data) => {
                                var amount = data.AMOUNT;


                                if (req.body.quantity ) {

                                    amount = amount * req.body.quantity;
                                }

                                        if(indObj.indSerPlan.PLAN == 'Monthly'){
                                        const indServPlanRep = getRepository(IND_SERVICE_PLANS);
                                        const indServiceMapSub = await indServPlanRep.findOne(indObj.indSerPlan.IND_SERVICE_PLAN_ID);
                                        var days =  indServiceMapSub.DAYS;
                                        indObj.AMOUNT = amount * days;

                                        }
                                        else {

                                        indObj.AMOUNT = amount;
                                        console.log(amount + "amount")

                                        }

                                var esult = await individualTicketRep.save(indObj);

                                var appUserDetails = req.body.appUserName+'-'+req.body.appUserChildName+'-'+ req.body.appUserGrandchild; 
                                vendorSendMail.vendoIndSendMail(req.body.indTicketId, req.body.description,   vendorObj.VENDOR_EMAIL, req.body.ticket_date, req.body.ticket_time,   req.body.appUserserviceName, req.body.appUserserviceTypeName,req.body.appUserQuantity ,amount,req.body.appUserPlan,req.body.associationName,  appUserDetails)


                                OwnerIndTicketSendMail.IndTicketSendMail(req.body.indTicketId, req.body.description,   req.body.appUserEmail, req.body.ticket_date, req.body.ticket_time,   req.body.appUserserviceName, req.body.appUserserviceTypeName,req.body.appUserQuantity ,amount,req.body.appUserPlan,vendorObj.VENDOR_NAME);
                            }).catch((error) => {
                                console.log(error);
                            });


                    }
                    else {

                        const interServAmountVendor = await getRepository(INTERNAL_SERVICES_AMOUNT_VENDORS)
                            .createQueryBuilder("interServVendor")
                            .where("interServVendor.IND_SERVICE_ID =  :indServiceId  AND VENDOR_ID = :vendorId", {
                                indServiceId: req.body.serviceId,
                                vendorId: vendorId
                            }).getOne().then(async (data) => {
                                var amount = data.AMOUNT;


                                if (req.body.quantity ) {

                                    amount = amount * req.body.quantity;
                                }

                                if(indObj.indSerPlan.PLAN == 'Monthly'){
                                    const indServPlanRep = getRepository(IND_SERVICE_PLANS);
                                    const indServiceMapSub = await indServPlanRep.findOne(indObj.indSerPlan.IND_SERVICE_PLAN_ID);
                                    var days =  indServiceMapSub.DAYS;
                                    indObj.AMOUNT = amount * days;

                                    }
                                    else {

                                    indObj.AMOUNT = amount;
                                    console.log(amount + "amount")

                                    }

                                var esult = await individualTicketRep.save(indObj);
                                         var appUserDetails = req.body.appUserName+'-'+req.body.appUserChildName+'-'+ req.body.appUserGrandchild; 
                                vendorSendMail.vendoIndSendMail(req.body.indTicketId, req.body.description,   vendorObj.VENDOR_EMAIL, req.body.ticket_date, req.body.ticket_time,   req.body.appUserserviceName, req.body.appUserserviceTypeName,req.body.appUserQuantity ,amount,req.body.appUserPlan,req.body.associationName,  appUserDetails)
                                OwnerIndTicketSendMail.IndTicketSendMail(req.body.indTicketId, req.body.description,   req.body.appUserEmail, req.body.ticket_date, req.body.ticket_time,   req.body.appUserserviceName, req.body.appUserserviceTypeName,req.body.appUserQuantity ,amount,req.body.appUserPlan,vendorObj.VENDOR_NAME);
                            }).catch((error) => {
                                console.log(error);
                            });
                    }
                    res.send(req.body.data);
                }).catch((error) => {
                    res.status(500).send(error)
                });
            }
            else {
                //  res.send(req.body.data);
                next();
            }


        }


    }


    static checkFourthInternalVendor = async (req: Request, res: Response, next) => {
        console.log("fourth")
        const mapIndVendorRep = await getRepository(MapIndTicketVendor);
        const vendorRep = await getRepository(VENDOR);
        const individualTicketRep = await getRepository(IndividualTickets);
        var date = "2020-03-23T" + req.body.ticket_time + ".573Z";
        var postTime = getPostTIme(date);
        var preTime = getPreTime(date);
        const prioryOneVendor = await getRepository(VENDOR)
            .createQueryBuilder("vendor")
            .leftJoinAndSelect("vendor.assVendors", "assVendor", "assVendor.vendorInfo = vendor.id")
            .leftJoinAndSelect("vendor.vensServices", "venService", "venService.vendorInfo = vendor.id")
            .leftJoinAndSelect("vendor.mapVendor", "mapVendors", "mapVendors.vendor = vendor.id")
            .leftJoinAndSelect("mapVendors.indTickets", "indTicket", "indTicket.IND_TICKET_ID = mapVendors.IND_TICKET_ID")
            .leftJoinAndSelect("vendor.assVenSerPriority", "assSerPriority", "assSerPriority.vendorInfo = vendor.id")
            .where("assVendor.ASSOCIATION_ID =  :id AND venService.IND_SERVICE_ID = :service   AND indTicket.STATUS = 'Pending' AND indTicket.TICKET_DATE = :ticket_date AND assSerPriority.PRIORITY = 4  AND  assVendor.IS_INTERNAL_VENDOR = true AND (indTicket.TICKET_TIME   BETWEEN '" + preTime + "' AND '" + postTime + " ')", {
                id: req.body.associationId,
                service: req.body.serviceId,
                ticket_date: req.body.ticket_date,
            }).getOne();


        console.log(prioryOneVendor)
        if (prioryOneVendor) {
            //  res.send(req.body.data);
            next()
        }
        else {
            const prioryOneElVendor = await getRepository(VENDOR)
                .createQueryBuilder("vendor")
                .leftJoinAndSelect("vendor.assVendors", "assVendor", "assVendor.vendorInfo = vendor.id")
                .leftJoinAndSelect("vendor.vensServices", "venService", "venService.vendorInfo = vendor.id")
                .leftJoinAndSelect("vendor.mapVendor", "mapVendors", "mapVendors.vendor = vendor.id")
                .leftJoinAndSelect("mapVendors.indTickets", "indTicket", "indTicket.IND_TICKET_ID = mapVendors.IND_TICKET_ID")
                .leftJoinAndSelect("vendor.assVenSerPriority", "assSerPriority", "assSerPriority.vendorInfo = vendor.id")
                .where("assVendor.ASSOCIATION_ID =  :id AND venService.IND_SERVICE_ID = :service   AND assVendor.IS_INTERNAL_VENDOR = true AND    assSerPriority.PRIORITY = 4 ", {
                    id: req.body.associationId,
                    service: req.body.serviceId,
                }).getOne();

            console.log(prioryOneElVendor)

            if (prioryOneElVendor != undefined) {
                var vendorId = prioryOneElVendor.id;

                // var vendorId = prioryOneElVendor.id;
                //res.send(prioryOneVendor);
                const vendorObj = await vendorRep.findOne(vendorId);
                const indObj = await individualTicketRep.findOne(
                    {
                        where : {
                            IND_TICKET_ID :  req.body.indTicketId
                        },

                        relations : ["indSerPlan"]
                    }
                   
                    )
                const mapVendorObj = new MapIndTicketVendor();
                mapVendorObj.vendor = vendorObj;
                mapVendorObj.indTickets = indObj;
                mapVendorObj.IS_ACCEPTED = false;
                mapVendorObj.IS_REJECTED = false;
                mapVendorObj.IS_ACTIVE = true;

                var samapVendorObj = mapIndVendorRep.create(mapVendorObj);

                mapIndVendorRep.save(samapVendorObj).then(async (mapIndObj) => {

                    if (req.body.service_types_id) {
                        req.body.serviceId

                        const interServAmountVendor = await getRepository(INTERNAL_SERVICES_AMOUNT_VENDORS)
                            .createQueryBuilder("interServVendor")
                            .where("interServVendor.IND_SERVICE_ID =  :indServiceId AND interServVendor.SERVICE_TYPE_ID = :indServiceTypeId AND VENDOR_ID = :vendorId", {
                                indServiceId: req.body.serviceId,
                                indServiceTypeId: req.body.service_types_id,
                                vendorId: vendorId
                            }).getOne().then(async (data) => {
                                var amount = data.AMOUNT;


                                if (req.body.quantity ) {

                                    amount = amount * req.body.quantity;
                                }

                            if(indObj.indSerPlan.PLAN == 'Monthly'){
                            const indServPlanRep = getRepository(IND_SERVICE_PLANS);
                            const indServiceMapSub = await indServPlanRep.findOne(indObj.indSerPlan.IND_SERVICE_PLAN_ID);
                            var days =  indServiceMapSub.DAYS;
                            indObj.AMOUNT = amount * days;

                            }
                            else {

                            indObj.AMOUNT = amount;
                            console.log(amount + "amount")

                            }

                                var esult = await individualTicketRep.save(indObj);
                                var appUserDetails = req.body.appUserName+'-'+req.body.appUserChildName+'-'+ req.body.appUserGrandchild; 
                                vendorSendMail.vendoIndSendMail(req.body.indTicketId, req.body.description,   vendorObj.VENDOR_EMAIL, req.body.ticket_date, req.body.ticket_time,   req.body.appUserserviceName, req.body.appUserserviceTypeName,req.body.appUserQuantity ,amount,req.body.appUserPlan,req.body.associationName,  appUserDetails)
                                
                                OwnerIndTicketSendMail.IndTicketSendMail(req.body.indTicketId, req.body.description,   req.body.appUserEmail, req.body.ticket_date, req.body.ticket_time,   req.body.appUserserviceName, req.body.appUserserviceTypeName,req.body.appUserQuantity ,amount,req.body.appUserPlan,vendorObj.VENDOR_NAME);
                            }).catch((error) => {
                                console.log(error);
                            });


                    }
                    else {

                        const interServAmountVendor = await getRepository(INTERNAL_SERVICES_AMOUNT_VENDORS)
                            .createQueryBuilder("interServVendor")
                            .where("interServVendor.IND_SERVICE_ID =  :indServiceId  AND VENDOR_ID = :vendorId", {
                                indServiceId: req.body.serviceId,
                                vendorId: vendorId
                            }).getOne().then(async (data) => {
                                var amount = data.AMOUNT;


                                if (req.body.quantity ) {

                                    amount = amount * req.body.quantity;
                                }

                                if(indObj.indSerPlan.PLAN == 'Monthly'){
                                    const indServPlanRep = getRepository(IND_SERVICE_PLANS);
                                    const indServiceMapSub = await indServPlanRep.findOne(indObj.indSerPlan.IND_SERVICE_PLAN_ID);
                                    var days =  indServiceMapSub.DAYS;
                                    indObj.AMOUNT = amount * days;
        
                                    }
                                    else {
        
                                    indObj.AMOUNT = amount;
                                    console.log(amount + "amount")
        
                                    }

                                var esult = await individualTicketRep.save(indObj);

                                var appUserDetails = req.body.appUserName+'-'+req.body.appUserChildName+'-'+ req.body.appUserGrandchild; 
                                vendorSendMail.vendoIndSendMail(req.body.indTicketId, req.body.description,   vendorObj.VENDOR_EMAIL, req.body.ticket_date, req.body.ticket_time,   req.body.appUserserviceName, req.body.appUserserviceTypeName,req.body.appUserQuantity ,amount,req.body.appUserPlan,req.body.associationName,  appUserDetails)
                                          
                                OwnerIndTicketSendMail.IndTicketSendMail(req.body.indTicketId, req.body.description,   req.body.appUserEmail, req.body.ticket_date, req.body.ticket_time,   req.body.appUserserviceName, req.body.appUserserviceTypeName,req.body.appUserQuantity ,amount,req.body.appUserPlan,vendorObj.VENDOR_NAME);
                            }).catch((error) => {
                                console.log(error);
                            });
                    }
                    res.send(req.body.data);
                }).catch((error) => {
                    res.status(500).send(error)
                });
            }
            else {
                // res.send(req.body.data);
                next();
            }


        }


    }




    static checkFifthInternalVendor = async (req: Request, res: Response, next) => {
        console.log("seconfdary")
        const mapIndVendorRep = await getRepository(MapIndTicketVendor);
        const vendorRep = await getRepository(VENDOR);
        const individualTicketRep = await getRepository(IndividualTickets);
        var date = "2020-03-23T" + req.body.ticket_time + ".573Z";
        var postTime = getPostTIme(date);
        var preTime = getPreTime(date);
        const prioryOneVendor = await getRepository(VENDOR)
            .createQueryBuilder("vendor")
            .leftJoinAndSelect("vendor.assVendors", "assVendor", "assVendor.vendorInfo = vendor.id")
            .leftJoinAndSelect("vendor.vensServices", "venService", "venService.vendorInfo = vendor.id")
            .leftJoinAndSelect("vendor.mapVendor", "mapVendors", "mapVendors.vendor = vendor.id")
            .leftJoinAndSelect("mapVendors.indTickets", "indTicket", "indTicket.IND_TICKET_ID = mapVendors.IND_TICKET_ID")
            .leftJoinAndSelect("vendor.assVenSerPriority", "assSerPriority", "assSerPriority.vendorInfo = vendor.id")
            .where("assVendor.ASSOCIATION_ID =  :id AND venService.IND_SERVICE_ID = :service  AND indTicket.STATUS = 'Pending'  AND indTicket.TICKET_DATE = :ticket_date AND assSerPriority.PRIORITY = 5  AND  assVendor.IS_INTERNAL_VENDOR = true AND (indTicket.TICKET_TIME   BETWEEN '" + preTime + "' AND '" + postTime + " ')", {
                id: req.body.associationId,
                service: req.body.serviceId,
                ticket_date: req.body.ticket_date,
            }).getOne();


        console.log(prioryOneVendor)
        if (prioryOneVendor) {
            // res.send(req.body.data);
            next()
        }
        else {
            const prioryOneElVendor = await getRepository(VENDOR)
                .createQueryBuilder("vendor")
                .leftJoinAndSelect("vendor.assVendors", "assVendor", "assVendor.vendorInfo = vendor.id")
                .leftJoinAndSelect("vendor.vensServices", "venService", "venService.vendorInfo = vendor.id")
                .leftJoinAndSelect("vendor.mapVendor", "mapVendors", "mapVendors.vendor = vendor.id")
                .leftJoinAndSelect("mapVendors.indTickets", "indTicket", "indTicket.IND_TICKET_ID = mapVendors.IND_TICKET_ID")
                .leftJoinAndSelect("vendor.assVenSerPriority", "assSerPriority", "assSerPriority.vendorInfo = vendor.id")
                .where("assVendor.ASSOCIATION_ID =  :id AND venService.IND_SERVICE_ID = :service   AND assVendor.IS_INTERNAL_VENDOR = true AND    assSerPriority.PRIORITY = 5", {
                    id: req.body.associationId,
                    service: req.body.serviceId,
                }).getOne();

            console.log(prioryOneElVendor)


            if (prioryOneElVendor != undefined) {
                var vendorId = prioryOneElVendor.id;
                //     var vendorId = prioryOneElVendor.id;
                //res.send(prioryOneVendor);
                const vendorObj = await vendorRep.findOne(vendorId);
                const indObj = await individualTicketRep.findOne(
                    {
                        where : {
                            IND_TICKET_ID :  req.body.indTicketId
                        },

                        relations : ["indSerPlan"]
                    }
                   
                    )
                const mapVendorObj = new MapIndTicketVendor();
                mapVendorObj.vendor = vendorObj;
                mapVendorObj.indTickets = indObj;
                mapVendorObj.IS_ACCEPTED = false;
                mapVendorObj.IS_REJECTED = false;
                mapVendorObj.IS_ACTIVE = true;

                var samapVendorObj = mapIndVendorRep.create(mapVendorObj);

                mapIndVendorRep.save(samapVendorObj).then(async (mapIndObj) => {

                    if (req.body.service_types_id) {
                        req.body.serviceId

                        const interServAmountVendor = await getRepository(INTERNAL_SERVICES_AMOUNT_VENDORS)
                            .createQueryBuilder("interServVendor")
                            .where("interServVendor.IND_SERVICE_ID =  :indServiceId AND interServVendor.SERVICE_TYPE_ID = :indServiceTypeId AND VENDOR_ID = :vendorId", {
                                indServiceId: req.body.serviceId,
                                indServiceTypeId: req.body.service_types_id,
                                vendorId: vendorId
                            }).getOne().then(async (data) => {
                                var amount = data.AMOUNT;


                                if (req.body.quantity ) {

                                    amount = amount * req.body.quantity;
                                }

                                if(indObj.indSerPlan.PLAN == 'Monthly'){
                                    const indServPlanRep = getRepository(IND_SERVICE_PLANS);
                                       const indServiceMapSub = await indServPlanRep.findOne(indObj.indSerPlan.IND_SERVICE_PLAN_ID);
                                      var days =  indServiceMapSub.DAYS;
                                      indObj.AMOUNT = amount * days;
                                 
                                                               }
                                                               else {
                               
                                                                   indObj.AMOUNT = amount;
                                                                   console.log(amount + "amount")
                                   
                                                               }

                                var esult = await individualTicketRep.save(indObj);
                                var appUserDetails = req.body.appUserName+'-'+req.body.appUserChildName+'-'+ req.body.appUserGrandchild; 
                                vendorSendMail.vendoIndSendMail(req.body.indTicketId, req.body.description,   vendorObj.VENDOR_EMAIL, req.body.ticket_date, req.body.ticket_time,   req.body.appUserserviceName, req.body.appUserserviceTypeName,req.body.appUserQuantity ,amount,req.body.appUserPlan,req.body.associationName,  appUserDetails)
                                
                                OwnerIndTicketSendMail.IndTicketSendMail(req.body.indTicketId, req.body.description,   req.body.appUserEmail, req.body.ticket_date, req.body.ticket_time,   req.body.appUserserviceName, req.body.appUserserviceTypeName,req.body.appUserQuantity ,amount,req.body.appUserPlan,vendorObj.VENDOR_NAME);
                            }).catch((error) => {
                                console.log(error);
                            });


                    }
                    else {

                        const interServAmountVendor = await getRepository(INTERNAL_SERVICES_AMOUNT_VENDORS)
                            .createQueryBuilder("interServVendor")
                            .where("interServVendor.IND_SERVICE_ID =  :indServiceId  AND VENDOR_ID = :vendorId", {
                                indServiceId: req.body.serviceId,
                                vendorId: vendorId
                            }).getOne().then(async (data) => {
                                var amount = data.AMOUNT;


                                if (req.body.quantity ) {

                                    amount = amount * req.body.quantity;
                                }

                            if(indObj.indSerPlan.PLAN == 'Monthly'){
                            const indServPlanRep = getRepository(IND_SERVICE_PLANS);
                            const indServiceMapSub = await indServPlanRep.findOne(indObj.indSerPlan.IND_SERVICE_PLAN_ID);
                            var days =  indServiceMapSub.DAYS;
                            indObj.AMOUNT = amount * days;

                            }
                            else {

                            indObj.AMOUNT = amount;
                            console.log(amount + "amount")

                            }

                                var esult = await individualTicketRep.save(indObj);

                                var appUserDetails = req.body.appUserName+'-'+req.body.appUserChildName+'-'+ req.body.appUserGrandchild; 
                                vendorSendMail.vendoIndSendMail(req.body.indTicketId, req.body.description,   vendorObj.VENDOR_EMAIL, req.body.ticket_date, req.body.ticket_time,   req.body.appUserserviceName, req.body.appUserserviceTypeName,req.body.appUserQuantity ,amount,req.body.appUserPlan,req.body.associationName,  appUserDetails)
                                          
                                OwnerIndTicketSendMail.IndTicketSendMail(req.body.indTicketId, req.body.description,   req.body.appUserEmail, req.body.ticket_date, req.body.ticket_time,   req.body.appUserserviceName, req.body.appUserserviceTypeName,req.body.appUserQuantity ,amount,req.body.appUserPlan,vendorObj.VENDOR_NAME);
                            }).catch((error) => {
                                console.log(error);
                            });
                    }
                    res.send(req.body.data);
                }).catch((error) => {
                    res.status(500).send(error)
                });
            }
            else {
                //  res.send(req.body.data);
                next();
            }


        }


    }




    static checkFirstPriorThirdPartyVendor = async (req: Request, res: Response, next) => {
        console.log("checkFirstPriorThirdPartyVendor");

        console.log(req.body.ticket_date)
        console.log(req.body.ticket_date)

        console.log("check first prior thirs parthey vbendr")
        const mapIndVendorRep = await getRepository(MapIndTicketVendor);
        const vendorRep = await getRepository(VENDOR);
        const individualTicketRep = await getRepository(IndividualTickets);
        var date = "2020-03-23T" + req.body.ticket_time + ".573Z";
        var postTime = getPostTIme(date);
        var preTime = getPreTime(date);
        const prioryOneVendor = await getRepository(VENDOR).query("SELECT * FROM vendor LEFT JOIN map_ind_tickets_vendors ON   map_ind_tickets_vendors.VENDOR_ID = vendor.VENDOR_ID LEFT JOIN individual_service_tickets ON    individual_service_tickets.IND_TICKET_ID = map_ind_tickets_vendors.IND_TICKET_ID LEFT JOIN association_vendors ON   association_vendors.`VENDOR_ASSOCIATION_ID` = vendor.`VENDOR_ASSOCIATION_ID` LEFT JOIN vendor_services ON   vendor_services.`VENDOR_ID` = vendor.`VENDOR_ID` LEFT JOIN association_ven_ind_ser_priority ON   association_ven_ind_ser_priority.`VENDOR_ID` = vendor.`VENDOR_ID` WHERE association_vendors.`ASSOCIATION_ID` = '" + req.body.associationId + "' AND association_ven_ind_ser_priority.`PRIORITY` = 1  AND association_ven_ind_ser_priority.`ASSOCIATION_ID` = '" + req.body.associationId + "' AND vendor_services.IND_SERVICE_ID ='" + req.body.serviceId + "'  AND individual_service_tickets.STATUS = 'Pending' AND individual_service_tickets.TICKET_DATE = '" + req.body.ticket_date + "' AND (individual_service_tickets.TICKET_TIME   BETWEEN '" + preTime + "' AND '" + postTime + "') GROUP BY vendor.`VENDOR_ID`")

        console.log(prioryOneVendor);
        console.log("jasjajb");
        if (prioryOneVendor.length > 0) {
            // res.send(req.body.data);
            next()
        }
        else {
            const prioryOneElVendor = await getRepository(VENDOR).query("SELECT * FROM vendor LEFT JOIN map_ind_tickets_vendors ON   map_ind_tickets_vendors.VENDOR_ID = vendor.VENDOR_ID LEFT JOIN individual_service_tickets ON    individual_service_tickets.IND_TICKET_ID = map_ind_tickets_vendors.IND_TICKET_ID LEFT JOIN association_vendors ON   association_vendors.`VENDOR_ASSOCIATION_ID` = vendor.`VENDOR_ASSOCIATION_ID` LEFT JOIN vendor_services ON   vendor_services.`VENDOR_ID` = vendor.`VENDOR_ID` LEFT JOIN association_ven_ind_ser_priority ON   association_ven_ind_ser_priority.`VENDOR_ID` = vendor.`VENDOR_ID` WHERE association_vendors.`ASSOCIATION_ID` = '" + req.body.associationId + "'  AND vendor_services.IND_SERVICE_ID ='" + req.body.serviceId + "' AND  association_ven_ind_ser_priority.`PRIORITY` = 1  AND association_ven_ind_ser_priority.`ASSOCIATION_ID` = '" + req.body.associationId + "'  GROUP BY vendor.`VENDOR_ID`")

            console.log(prioryOneElVendor)

            if (prioryOneElVendor.length > 0) {
                var vendorId = prioryOneElVendor[0].VENDOR_ID;
                var vendorAssocaitonId = prioryOneElVendor[0].VENDOR_ASSOCIATION_ID;
                console.log(vendorId + "vendorId")
                //res.send(prioryOneVendor);
                const vendorObj = await vendorRep.findOne(vendorId);
                const indObj = await individualTicketRep.findOne(
                    {
                        where : {
                            IND_TICKET_ID :  req.body.indTicketId
                        },

                        relations : ["indSerPlan"]
                    }
                   
                    )
                const mapVendorObj = new MapIndTicketVendor();
                mapVendorObj.vendor = vendorObj;
                mapVendorObj.indTickets = indObj;
                mapVendorObj.IS_ACCEPTED = false;
                mapVendorObj.IS_REJECTED = false;
                mapVendorObj.IS_ACTIVE = true;

                var samapVendorObj = mapIndVendorRep.create(mapVendorObj);

                mapIndVendorRep.save(samapVendorObj).then(async (mapIndObj) => {


                    if (req.body.service_types_id) {
                        req.body.serviceId

                        const interServAmountVendorAssociation = await getRepository(IND_SER_TYPES_VENDOR_ASSOCIATION)
                            .createQueryBuilder("interServAmountVendorAssociation")
                            .where("interServAmountVendorAssociation.IND_SERVICE_ID =  :indServiceId AND interServAmountVendorAssociation.IND_SERVICE_TYPE_ID = :indServiceTypeId AND VENDOR_ASSOCIATION_ID = :vendorAsociationId", {
                                indServiceId: req.body.serviceId,
                                indServiceTypeId: req.body.service_types_id,
                                vendorAsociationId: vendorAssocaitonId
                            }).getOne().then(async (data) => {
                                var amount = data.AMOUNT;


                                if (req.body.quantity ) {

                                    amount = amount * req.body.quantity;
                                }

                                // var esult = await individualTicketRep.save(indObj);

                   
                            if(indObj.indSerPlan.PLAN == 'Monthly'){
                            const indServPlanRep = getRepository(IND_SERVICE_PLANS);
                            const indServiceMapSub = await indServPlanRep.findOne(indObj.indSerPlan.IND_SERVICE_PLAN_ID);
                            var days =  indServiceMapSub.DAYS;
                            indObj.AMOUNT = amount * days;

                            }
                            else {

                            indObj.AMOUNT = amount;
                            console.log(amount + "amount")

                            }

                                var esult = await individualTicketRep.save(indObj);

                                var appUserDetails = req.body.appUserName+'-'+req.body.appUserChildName+'-'+ req.body.appUserGrandchild; 
                                vendorSendMail.vendoIndSendMail(req.body.indTicketId, req.body.description,   vendorObj.VENDOR_EMAIL, req.body.ticket_date, req.body.ticket_time,   req.body.appUserserviceName, req.body.appUserserviceTypeName,req.body.appUserQuantity ,amount,req.body.appUserPlan,req.body.associationName,  appUserDetails)


                                OwnerIndTicketSendMail.IndTicketSendMail(req.body.indTicketId, req.body.description,   req.body.appUserEmail, req.body.ticket_date, req.body.ticket_time,   req.body.appUserserviceName, req.body.appUserserviceTypeName,req.body.appUserQuantity ,amount,req.body.appUserPlan,vendorObj.VENDOR_NAME);
                            }).catch((error) => {
                                console.log(error);
                            });




                    }
                    else {

                        const interServAmountVendorAssociation = await getRepository(IND_SER_TYPES_VENDOR_ASSOCIATION)
                            .createQueryBuilder("interServAmountVendorAssociation")
                            .where("interServAmountVendorAssociation.IND_SERVICE_ID =  :indServiceId AND VENDOR_ASSOCIATION_ID = :vendorAsociationId", {
                                indServiceId: req.body.serviceId,
                                vendorAsociationId: vendorAssocaitonId
                            }).getOne().then(async (data) => {
                                var amount = data.AMOUNT;


                                if (req.body.quantity ) {

                                    amount = amount * req.body.quantity;
                                }

                                // var esult = await individualTicketRep.save(indObj);

                           
                                        if(indObj.indSerPlan.PLAN == 'Monthly'){
                                        const indServPlanRep = getRepository(IND_SERVICE_PLANS);
                                        const indServiceMapSub = await indServPlanRep.findOne(indObj.indSerPlan.IND_SERVICE_PLAN_ID);
                                        var days =  indServiceMapSub.DAYS;
                                        indObj.AMOUNT = amount * days;

                                        }
                                        else {

                                        indObj.AMOUNT = amount;
                                        console.log(amount + "amount")

                                        }

                                var esult = await individualTicketRep.save(indObj);

                                            var appUserDetails = req.body.appUserName+'-'+req.body.appUserChildName+'-'+ req.body.appUserGrandchild; 
                                vendorSendMail.vendoIndSendMail(req.body.indTicketId, req.body.description,   vendorObj.VENDOR_EMAIL, req.body.ticket_date, req.body.ticket_time,   req.body.appUserserviceName, req.body.appUserserviceTypeName,req.body.appUserQuantity ,amount,req.body.appUserPlan,req.body.associationName,  appUserDetails)      
                                OwnerIndTicketSendMail.IndTicketSendMail(req.body.indTicketId, req.body.description,   req.body.appUserEmail, req.body.ticket_date, req.body.ticket_time,   req.body.appUserserviceName, req.body.appUserserviceTypeName,req.body.appUserQuantity ,amount,req.body.appUserPlan,vendorObj.VENDOR_NAME);
                            }).catch((error) => {
                                console.log(error);
                            });

                    }
                    res.send(req.body.data);
                }).catch((error) => {
                    res.status(500).send(error)
                });
            }

            else {
                next()
            }


        }


    }


    static checkSecondPriorThirdPartyVendor = async (req: Request, res: Response, next) => {

        console.log("checkSecondPriorThirdPartyVendor");
        const mapIndVendorRep = await getRepository(MapIndTicketVendor);
        const vendorRep = await getRepository(VENDOR);
        const individualTicketRep = await getRepository(IndividualTickets);
        var date = "2020-03-23T" + req.body.ticket_time + ".573Z";
        var postTime = getPostTIme(date);
        var preTime = getPreTime(date);
        const prioryOneVendor = await getRepository(VENDOR).query("SELECT * FROM vendor LEFT JOIN map_ind_tickets_vendors ON   map_ind_tickets_vendors.VENDOR_ID = vendor.VENDOR_ID LEFT JOIN individual_service_tickets ON    individual_service_tickets.IND_TICKET_ID = map_ind_tickets_vendors.IND_TICKET_ID LEFT JOIN association_vendors ON   association_vendors.`VENDOR_ASSOCIATION_ID` = vendor.`VENDOR_ASSOCIATION_ID` LEFT JOIN vendor_services ON   vendor_services.`VENDOR_ID` = vendor.`VENDOR_ID` LEFT JOIN association_ven_ind_ser_priority ON   association_ven_ind_ser_priority.`VENDOR_ID` = vendor.`VENDOR_ID` WHERE association_vendors.`ASSOCIATION_ID` = '" + req.body.associationId + "'  AND vendor_services.IND_SERVICE_ID ='" + req.body.serviceId + "' AND association_ven_ind_ser_priority.`PRIORITY` = 2  AND individual_service_tickets.STATUS = 'Pending'  AND association_ven_ind_ser_priority.`ASSOCIATION_ID` = '" + req.body.associationId + "' AND individual_service_tickets.TICKET_DATE = '" + req.body.ticket_date + "' AND (individual_service_tickets.TICKET_TIME   BETWEEN '" + preTime + "' AND '" + postTime + "') GROUP BY vendor.`VENDOR_ID`")

        console.log(prioryOneVendor.length + "prioryOneVendor.length")
        if (prioryOneVendor.length > 0) {
            // res.send(req.body.data);
            next()
        }
        else {


            const prioryOneElVendor = await getRepository(VENDOR).query("SELECT * FROM vendor LEFT JOIN map_ind_tickets_vendors ON   map_ind_tickets_vendors.VENDOR_ID = vendor.VENDOR_ID LEFT JOIN individual_service_tickets ON    individual_service_tickets.IND_TICKET_ID = map_ind_tickets_vendors.IND_TICKET_ID LEFT JOIN association_vendors ON   association_vendors.`VENDOR_ASSOCIATION_ID` = vendor.`VENDOR_ASSOCIATION_ID` LEFT JOIN vendor_services ON   vendor_services.`VENDOR_ID` = vendor.`VENDOR_ID` LEFT JOIN association_ven_ind_ser_priority ON   association_ven_ind_ser_priority.`VENDOR_ID` = vendor.`VENDOR_ID` WHERE association_vendors.`ASSOCIATION_ID` = '" + req.body.associationId + "'  AND vendor_services.IND_SERVICE_ID ='" + req.body.serviceId + "' AND association_ven_ind_ser_priority.`PRIORITY` = 2  AND association_ven_ind_ser_priority.`ASSOCIATION_ID` = '" + req.body.associationId + "'  GROUP BY vendor.`VENDOR_ID`")

            if (prioryOneElVendor.length > 0) {


                console.log("Primasdhj asdfadda")
                var vendorId = prioryOneElVendor[0].VENDOR_ID;
                var vendorAssocaitonId = prioryOneElVendor[0].VENDOR_ASSOCIATION_ID;

                // var vendorId = prioryOneElVendor.VENDOR_ID;
                //res.send(prioryOneVendor);
                const vendorObj = await vendorRep.findOne(vendorId);
                const indObj = await individualTicketRep.findOne(
                    {
                        where : {
                            IND_TICKET_ID :  req.body.indTicketId
                        },

                        relations : ["indSerPlan"]
                    }
                   
                    )

                const mapVendorObj = new MapIndTicketVendor();
                mapVendorObj.vendor = vendorObj;
                mapVendorObj.indTickets = indObj;
                mapVendorObj.IS_ACCEPTED = false;
                mapVendorObj.IS_REJECTED = false;
                mapVendorObj.IS_ACTIVE = true;

                var samapVendorObj = mapIndVendorRep.create(mapVendorObj);

                mapIndVendorRep.save(samapVendorObj).then(async (mapIndObj) => {


                    if (req.body.service_types_id) {
                        req.body.serviceId

                        const interServAmountVendorAssociation = await getRepository(IND_SER_TYPES_VENDOR_ASSOCIATION)
                            .createQueryBuilder("interServAmountVendorAssociation")
                            .where("interServAmountVendorAssociation.IND_SERVICE_ID =  :indServiceId AND interServAmountVendorAssociation.IND_SERVICE_TYPE_ID = :indServiceTypeId AND VENDOR_ASSOCIATION_ID = :vendorAsociationId ", {
                                indServiceId: req.body.serviceId,
                                indServiceTypeId: req.body.service_types_id,
                                vendorAsociationId: vendorAssocaitonId
                            }).getOne().then(async (data) => {
                                console.log("auidnb")
                                console.log(data)
                                var amount = data.AMOUNT;


                                if (req.body.quantity) {

                                    amount = amount * req.body.quantity;
                                }



           if(indObj.indSerPlan.PLAN == 'Monthly'){
     const indServPlanRep = getRepository(IND_SERVICE_PLANS);
        const indServiceMapSub = await indServPlanRep.findOne(indObj.indSerPlan.IND_SERVICE_PLAN_ID);
       var days =  indServiceMapSub.DAYS;
       indObj.AMOUNT = amount * days;
  
                                }
                                else {

                                    indObj.AMOUNT = amount;
                                    console.log(amount + "amount")
    
                                }


                                var esult = await individualTicketRep.save(indObj);

                                var appUserDetails = req.body.appUserName+'-'+req.body.appUserChildName+'-'+ req.body.appUserGrandchild; 
                                vendorSendMail.vendoIndSendMail(req.body.indTicketId, req.body.description,   vendorObj.VENDOR_EMAIL, req.body.ticket_date, req.body.ticket_time,   req.body.appUserserviceName, req.body.appUserserviceTypeName,req.body.appUserQuantity ,amount,req.body.appUserPlan,req.body.associationName,  appUserDetails)
                                
                                
                                OwnerIndTicketSendMail.IndTicketSendMail(req.body.indTicketId, req.body.description,   req.body.appUserEmail, req.body.ticket_date, req.body.ticket_time,   req.body.appUserserviceName, req.body.appUserserviceTypeName,req.body.appUserQuantity ,amount,req.body.appUserPlan,vendorObj.VENDOR_NAME);
                            }).catch((error) => {
                                console.log(error);
                            });



                    }
                    else {

                        const interServAmountVendorAssociation = await getRepository(IND_SER_TYPES_VENDOR_ASSOCIATION)
                            .createQueryBuilder("interServAmountVendorAssociation")
                            .where("interServAmountVendorAssociation.IND_SERVICE_ID =  :indServiceId AND VENDOR_ASSOCIATION_ID = :vendorAsociationId", {
                                indServiceId: req.body.serviceId,
                                vendorAsociationId: vendorAssocaitonId
                            }).getOne().then(async (data) => {
                                var amount = data.AMOUNT;


                                if (req.body.quantity ) {

                                    amount = amount * req.body.quantity;
                                }

                                //  var esult = await individualTicketRep.save(indObj);

                             
           if(indObj.indSerPlan.PLAN == 'Monthly'){
            const indServPlanRep = getRepository(IND_SERVICE_PLANS);
               const indServiceMapSub = await indServPlanRep.findOne(indObj.indSerPlan.IND_SERVICE_PLAN_ID);
              var days =  indServiceMapSub.DAYS;
              indObj.AMOUNT = amount * days;
         
                                       }
                                       else {
       
                                           indObj.AMOUNT = amount;
                                           console.log(amount + "amount")
           
                                       }
       

                                var esult = await individualTicketRep.save(indObj);
                                var appUserDetails = req.body.appUserName+'-'+req.body.appUserChildName+'-'+ req.body.appUserGrandchild; 
                                vendorSendMail.vendoIndSendMail(req.body.indTicketId, req.body.description,   vendorObj.VENDOR_EMAIL, req.body.ticket_date, req.body.ticket_time,   req.body.appUserserviceName, req.body.appUserserviceTypeName,req.body.appUserQuantity ,amount,req.body.appUserPlan,req.body.associationName,  appUserDetails)
                                          
                                OwnerIndTicketSendMail.IndTicketSendMail(req.body.indTicketId, req.body.description,   req.body.appUserEmail, req.body.ticket_date, req.body.ticket_time,   req.body.appUserserviceName, req.body.appUserserviceTypeName,req.body.appUserQuantity ,amount,req.body.appUserPlan,vendorObj.VENDOR_NAME);
                            }).catch((error) => {
                                console.log(error);
                            });

                    }
                    res.send(req.body.data);
                }).catch((error) => {
                    res.status(500).send(error)
                });
            }

            else {
                next()
            }


        }

    }




    static checkThirdPriorThirdPartyVendor = async (req: Request, res: Response, next) => {

        console.log("checkThirdPriorThirdPartyVendor");
        const mapIndVendorRep = await getRepository(MapIndTicketVendor);
        const vendorRep = await getRepository(VENDOR);
        const individualTicketRep = await getRepository(IndividualTickets);
        var date = "2020-03-23T" + req.body.ticket_time + ".573Z";
        var postTime = getPostTIme(date);
        var preTime = getPreTime(date);
        const prioryOneVendor = await getRepository(VENDOR).query("SELECT * FROM vendor LEFT JOIN map_ind_tickets_vendors ON   map_ind_tickets_vendors.VENDOR_ID = vendor.VENDOR_ID LEFT JOIN individual_service_tickets ON    individual_service_tickets.IND_TICKET_ID = map_ind_tickets_vendors.IND_TICKET_ID LEFT JOIN association_vendors ON   association_vendors.`VENDOR_ASSOCIATION_ID` = vendor.`VENDOR_ASSOCIATION_ID` LEFT JOIN vendor_services ON   vendor_services.`VENDOR_ID` = vendor.`VENDOR_ID` LEFT JOIN association_ven_ind_ser_priority ON   association_ven_ind_ser_priority.`VENDOR_ID` = vendor.`VENDOR_ID` WHERE association_vendors.`ASSOCIATION_ID` = '" + req.body.associationId + "'  AND vendor_services.IND_SERVICE_ID ='" + req.body.serviceId + "' AND association_ven_ind_ser_priority.`PRIORITY` = 3  AND  association_ven_ind_ser_priority.`ASSOCIATION_ID` = '" + req.body.associationId + "'  AND individual_service_tickets.STATUS = 'Pending' AND individual_service_tickets.TICKET_DATE = '" + req.body.ticket_date + "' AND (individual_service_tickets.TICKET_TIME   BETWEEN '" + preTime + "' AND '" + postTime + "') GROUP BY vendor.`VENDOR_ID`")
        if (prioryOneVendor.length > 0) {
            // res.send(req.body.data);
            next()
        }
        else {


            const prioryOneElVendor = await getRepository(VENDOR).query("SELECT * FROM vendor LEFT JOIN map_ind_tickets_vendors ON   map_ind_tickets_vendors.VENDOR_ID = vendor.VENDOR_ID LEFT JOIN individual_service_tickets ON    individual_service_tickets.IND_TICKET_ID = map_ind_tickets_vendors.IND_TICKET_ID LEFT JOIN association_vendors ON   association_vendors.`VENDOR_ASSOCIATION_ID` = vendor.`VENDOR_ASSOCIATION_ID` LEFT JOIN vendor_services ON   vendor_services.`VENDOR_ID` = vendor.`VENDOR_ID` LEFT JOIN association_ven_ind_ser_priority ON   association_ven_ind_ser_priority.`VENDOR_ID` = vendor.`VENDOR_ID` WHERE association_vendors.`ASSOCIATION_ID` = '" + req.body.associationId + "'  AND vendor_services.IND_SERVICE_ID ='" + req.body.serviceId + "' AND association_ven_ind_ser_priority.`PRIORITY` = 3  AND association_ven_ind_ser_priority.`ASSOCIATION_ID` = '" + req.body.associationId + "'  GROUP BY vendor.`VENDOR_ID`")


            if (prioryOneElVendor.length > 0) {

                var vendorId = prioryOneElVendor[0].VENDOR_ID;
                var vendorAssocaitonId = prioryOneElVendor[0].VENDOR_ASSOCIATION_ID;
                //  var vendorId = prioryOneElVendor.VENDOR_ID;
                //res.send(prioryOneVendor);
                const vendorObj = await vendorRep.findOne(vendorId);
                const indObj = await individualTicketRep.findOne(
                    {
                        where : {
                            IND_TICKET_ID :  req.body.indTicketId
                        },

                        relations : ["indSerPlan"]
                    }
                   
                    )
                const mapVendorObj = new MapIndTicketVendor();
                mapVendorObj.vendor = vendorObj;
                mapVendorObj.indTickets = indObj;
                mapVendorObj.IS_ACCEPTED = false;
                mapVendorObj.IS_REJECTED = false;
                mapVendorObj.IS_ACTIVE = true;

                var samapVendorObj = mapIndVendorRep.create(mapVendorObj);

                mapIndVendorRep.save(samapVendorObj).then(async (mapIndObj) => {

                    if (req.body.service_types_id) {
                        req.body.serviceId

                        const interServAmountVendorAssociation = await getRepository(IND_SER_TYPES_VENDOR_ASSOCIATION)
                            .createQueryBuilder("interServAmountVendorAssociation")
                            .where("interServAmountVendorAssociation.IND_SERVICE_ID =  :indServiceId AND interServAmountVendorAssociation.IND_SERVICE_TYPE_ID = :indServiceTypeId AND VENDOR_ASSOCIATION_ID = :vendorAsociationId ", {
                                indServiceId: req.body.serviceId,
                                indServiceTypeId: req.body.service_types_id,
                                vendorAsociationId: vendorAssocaitonId
                            }).getOne().then(async (data) => {
                                var amount = data.AMOUNT;


                                if (req.body.quantity ) {

                                    amount = amount * req.body.quantity;
                                }

                                if(indObj.indSerPlan.PLAN == 'Monthly'){
                                const indServPlanRep = getRepository(IND_SERVICE_PLANS);
                                const indServiceMapSub = await indServPlanRep.findOne(indObj.indSerPlan.IND_SERVICE_PLAN_ID);
                                var days =  indServiceMapSub.DAYS;
                                indObj.AMOUNT = amount * days;

                                }
                                else {

                                indObj.AMOUNT = amount;
                                console.log(amount + "amount")

                                }

                                // var esult = await individualTicketRep.save(indObj);

                                indObj.AMOUNT = amount;

                                var esult = await individualTicketRep.save(indObj);

                                var appUserDetails = req.body.appUserName+'-'+req.body.appUserChildName+'-'+ req.body.appUserGrandchild; 
                                vendorSendMail.vendoIndSendMail(req.body.indTicketId, req.body.description,   vendorObj.VENDOR_EMAIL, req.body.ticket_date, req.body.ticket_time,   req.body.appUserserviceName, req.body.appUserserviceTypeName,req.body.appUserQuantity ,amount,req.body.appUserPlan,req.body.associationName,  appUserDetails)
                                OwnerIndTicketSendMail.IndTicketSendMail(req.body.indTicketId, req.body.description,   req.body.appUserEmail, req.body.ticket_date, req.body.ticket_time,   req.body.appUserserviceName, req.body.appUserserviceTypeName,req.body.appUserQuantity ,amount,req.body.appUserPlan,vendorObj.VENDOR_NAME);
                            }).catch((error) => {
                                console.log(error);
                            });



                    }
                    else {

                        const interServAmountVendorAssociation = await getRepository(IND_SER_TYPES_VENDOR_ASSOCIATION)
                            .createQueryBuilder("interServAmountVendorAssociation")
                            .where("interServAmountVendorAssociation.IND_SERVICE_ID =  :indServiceId AND VENDOR_ASSOCIATION_ID = :vendorAsociationId", {
                                indServiceId: req.body.serviceId,
                                vendorAsociationId: vendorAssocaitonId
                            }).getOne().then(async (data) => {
                                var amount = data.AMOUNT;


                                if (req.body.quantity) {

                                    amount = amount * req.body.quantity;
                                }


                                if(indObj.indSerPlan.PLAN == 'Monthly'){
                                    const indServPlanRep = getRepository(IND_SERVICE_PLANS);
                                    const indServiceMapSub = await indServPlanRep.findOne(indObj.indSerPlan.IND_SERVICE_PLAN_ID);
                                    var days =  indServiceMapSub.DAYS;
                                    indObj.AMOUNT = amount * days;
    
                                    }
                                    else {
    
                                    indObj.AMOUNT = amount;
                                    console.log(amount + "amount")
    
                                    }

                                //  var esult = await individualTicketRep.save(indObj);

                                indObj.AMOUNT = amount;

                                var esult = await individualTicketRep.save(indObj);

                                var appUserDetails = req.body.appUserName+'-'+req.body.appUserChildName+'-'+ req.body.appUserGrandchild; 
                                vendorSendMail.vendoIndSendMail(req.body.indTicketId, req.body.description,   vendorObj.VENDOR_EMAIL, req.body.ticket_date, req.body.ticket_time,   req.body.appUserserviceName, req.body.appUserserviceTypeName,req.body.appUserQuantity ,amount,req.body.appUserPlan,req.body.associationName,  appUserDetails)
                                OwnerIndTicketSendMail.IndTicketSendMail(req.body.indTicketId, req.body.description,   req.body.appUserEmail, req.body.ticket_date, req.body.ticket_time,   req.body.appUserserviceName, req.body.appUserserviceTypeName,req.body.appUserQuantity ,amount,req.body.appUserPlan,vendorObj.VENDOR_NAME);
                            }).catch((error) => {
                                console.log(error);
                            });

                    }
                    res.send(req.body.data);
                }).catch((error) => {
                    res.status(500).send(error)
                });
            }

            else {
                next()
            }


        }


    }




    static checkFourthPriorThirdPartyVendor = async (req: Request, res: Response, next) => {
        console.log("checkFourthPriorThirdPartyVendor");
        const mapIndVendorRep = await getRepository(MapIndTicketVendor);
        const vendorRep = await getRepository(VENDOR);
        const individualTicketRep = await getRepository(IndividualTickets);
        var date = "2020-03-23T" + req.body.ticket_time + ".573Z";
        var postTime = getPostTIme(date);
        var preTime = getPreTime(date);
        const prioryOneVendor = await getRepository(VENDOR).query("SELECT * FROM vendor LEFT JOIN map_ind_tickets_vendors ON   map_ind_tickets_vendors.VENDOR_ID = vendor.VENDOR_ID LEFT JOIN individual_service_tickets ON    individual_service_tickets.IND_TICKET_ID = map_ind_tickets_vendors.IND_TICKET_ID LEFT JOIN association_vendors ON   association_vendors.`VENDOR_ASSOCIATION_ID` = vendor.`VENDOR_ASSOCIATION_ID` LEFT JOIN vendor_services ON   vendor_services.`VENDOR_ID` = vendor.`VENDOR_ID` LEFT JOIN association_ven_ind_ser_priority ON   association_ven_ind_ser_priority.`VENDOR_ID` = vendor.`VENDOR_ID` WHERE association_vendors.`ASSOCIATION_ID` = '" + req.body.associationId + "'  AND vendor_services.IND_SERVICE_ID ='" + req.body.serviceId + "' AND association_ven_ind_ser_priority.`PRIORITY` = 4  AND association_ven_ind_ser_priority.`ASSOCIATION_ID` = '" + req.body.associationId + "'  AND individual_service_tickets.STATUS = 'Pending' AND individual_service_tickets.TICKET_DATE = '" + req.body.ticket_date + "' AND (individual_service_tickets.TICKET_TIME   BETWEEN '" + preTime + "' AND '" + postTime + "') GROUP BY vendor.`VENDOR_ID`")
        if (prioryOneVendor.length > 0) {
            // res.send(req.body.data);
            next()
        }
        else {


            const prioryOneElVendor = await getRepository(VENDOR).query("SELECT * FROM vendor LEFT JOIN map_ind_tickets_vendors ON   map_ind_tickets_vendors.VENDOR_ID = vendor.VENDOR_ID LEFT JOIN individual_service_tickets ON    individual_service_tickets.IND_TICKET_ID = map_ind_tickets_vendors.IND_TICKET_ID LEFT JOIN association_vendors ON   association_vendors.`VENDOR_ASSOCIATION_ID` = vendor.`VENDOR_ASSOCIATION_ID` LEFT JOIN vendor_services ON   vendor_services.`VENDOR_ID` = vendor.`VENDOR_ID` LEFT JOIN association_ven_ind_ser_priority ON   association_ven_ind_ser_priority.`VENDOR_ID` = vendor.`VENDOR_ID` WHERE association_vendors.`ASSOCIATION_ID` = '" + req.body.associationId + "'  AND vendor_services.IND_SERVICE_ID ='" + req.body.serviceId + "' AND association_ven_ind_ser_priority.`PRIORITY` = 4  AND association_ven_ind_ser_priority.`ASSOCIATION_ID` = '" + req.body.associationId + "'  GROUP BY vendor.`VENDOR_ID`")


            if (prioryOneElVendor.length > 0) {

                var vendorId = prioryOneElVendor[0].VENDOR_ID;
                var vendorAssocaitonId = prioryOneElVendor[0].VENDOR_ASSOCIATION_ID;

                // var vendorId = prioryOneElVendor.VENDOR_ID;
                //res.send(prioryOneVendor);
                const vendorObj = await vendorRep.findOne(vendorId);
                const indObj = await individualTicketRep.findOne(
                    {
                        where : {
                            IND_TICKET_ID :  req.body.indTicketId
                        },

                        relations : ["indSerPlan"]
                    }
                   
                    )
                const mapVendorObj = new MapIndTicketVendor();
                mapVendorObj.vendor = vendorObj;
                mapVendorObj.indTickets = indObj;
                mapVendorObj.IS_ACCEPTED = false;
                mapVendorObj.IS_REJECTED = false;
                mapVendorObj.IS_ACTIVE = true;

                var samapVendorObj = mapIndVendorRep.create(mapVendorObj);

                mapIndVendorRep.save(samapVendorObj).then(async (mapIndObj) => {

                    if (req.body.service_types_id) {
                        req.body.serviceId

                        const interServAmountVendorAssociation = await getRepository(IND_SER_TYPES_VENDOR_ASSOCIATION)
                            .createQueryBuilder("interServAmountVendorAssociation")
                            .where("interServAmountVendorAssociation.IND_SERVICE_ID =  :indServiceId AND interServAmountVendorAssociation.IND_SERVICE_TYPE_ID = :indServiceTypeId AND VENDOR_ASSOCIATION_ID = :vendorAsociationId ", {
                                indServiceId: req.body.serviceId,
                                indServiceTypeId: req.body.service_types_id,
                                vendorAsociationId: vendorAssocaitonId
                            }).getOne().then(async (data) => {
                                var amount = data.AMOUNT;

                                if (req.body.quantity ) {

                                    amount = amount * req.body.quantity;
                                }

                                //  var esult = await individualTicketRep.save(indObj);

                                    if(indObj.indSerPlan.PLAN == 'Monthly'){
                                    const indServPlanRep = getRepository(IND_SERVICE_PLANS);
                                    const indServiceMapSub = await indServPlanRep.findOne(indObj.indSerPlan.IND_SERVICE_PLAN_ID);
                                    var days =  indServiceMapSub.DAYS;
                                    indObj.AMOUNT = amount * days;

                                    }
                                    else {

                                        indObj.AMOUNT = amount;
                                        console.log(amount + "amount")

                                    }

                                var esult = await individualTicketRep.save(indObj);

                                var appUserDetails = req.body.appUserName+'-'+req.body.appUserChildName+'-'+ req.body.appUserGrandchild; 
                                vendorSendMail.vendoIndSendMail(req.body.indTicketId, req.body.description,   vendorObj.VENDOR_EMAIL, req.body.ticket_date, req.body.ticket_time,   req.body.appUserserviceName, req.body.appUserserviceTypeName,req.body.appUserQuantity ,amount,req.body.appUserPlan,req.body.associationName,  appUserDetails)
                                OwnerIndTicketSendMail.IndTicketSendMail(req.body.indTicketId, req.body.description,   req.body.appUserEmail, req.body.ticket_date, req.body.ticket_time,   req.body.appUserserviceName, req.body.appUserserviceTypeName,req.body.appUserQuantity ,amount,req.body.appUserPlan,vendorObj.VENDOR_NAME);
                            }).catch((error) => {
                                console.log(error);
                            });


                    }
                    else {

                        const interServAmountVendorAssociation = await getRepository(IND_SER_TYPES_VENDOR_ASSOCIATION)
                            .createQueryBuilder("interServAmountVendorAssociation")
                            .where("interServAmountVendorAssociation.IND_SERVICE_ID =  :indServiceId AND VENDOR_ASSOCIATION_ID = :vendorAsociationId", {
                                indServiceId: req.body.serviceId,
                                vendorAsociationId: vendorAssocaitonId
                            }).getOne().then(async (data) => {
                                var amount = data.AMOUNT;


                                if (req.body.quantity ) {

                                    amount = amount * req.body.quantity;
                                }

                                // var esult = await individualTicketRep.save(indObj);
                                if(indObj.indSerPlan.PLAN == 'Monthly'){
                                const indServPlanRep = getRepository(IND_SERVICE_PLANS);
                                const indServiceMapSub = await indServPlanRep.findOne(indObj.indSerPlan.IND_SERVICE_PLAN_ID);
                                var days =  indServiceMapSub.DAYS;
                                indObj.AMOUNT = amount * days;

                                }
                                else {

                                indObj.AMOUNT = amount;
                                console.log(amount + "amount")

                                }

                                var esult = await individualTicketRep.save(indObj);
                                var appUserDetails = req.body.appUserName+'-'+req.body.appUserChildName+'-'+ req.body.appUserGrandchild; 
                                vendorSendMail.vendoIndSendMail(req.body.indTicketId, req.body.description,   vendorObj.VENDOR_EMAIL, req.body.ticket_date, req.body.ticket_time,   req.body.appUserserviceName, req.body.appUserserviceTypeName,req.body.appUserQuantity ,amount,req.body.appUserPlan,req.body.associationName,  appUserDetails)    
                                OwnerIndTicketSendMail.IndTicketSendMail(req.body.indTicketId, req.body.description,   req.body.appUserEmail, req.body.ticket_date, req.body.ticket_time,   req.body.appUserserviceName, req.body.appUserserviceTypeName,req.body.appUserQuantity ,amount,req.body.appUserPlan,vendorObj.VENDOR_NAME);
                            }).catch((error) => {
                                console.log(error);
                            });

                    }
                    res.send(req.body.data);
                }).catch((error) => {
                    res.status(500).send(error)
                });
            }

            else {
                next()
            }


        }



    }






    static checkFifthPriorThirdPartyVendor = async (req: Request, res: Response, next) => {

        console.log("checkFifthPriorThirdPartyVendor");

        const mapIndVendorRep = await getRepository(MapIndTicketVendor);
        const vendorRep = await getRepository(VENDOR);
        const individualTicketRep = await getRepository(IndividualTickets);
        var date = "2020-03-23T" + req.body.ticket_time + ".573Z";
        var postTime = getPostTIme(date);
        var preTime = getPreTime(date);
        const prioryOneVendor = await getRepository(VENDOR).query("SELECT * FROM vendor LEFT JOIN map_ind_tickets_vendors ON   map_ind_tickets_vendors.VENDOR_ID = vendor.VENDOR_ID LEFT JOIN individual_service_tickets ON    individual_service_tickets.IND_TICKET_ID = map_ind_tickets_vendors.IND_TICKET_ID LEFT JOIN association_vendors ON   association_vendors.`VENDOR_ASSOCIATION_ID` = vendor.`VENDOR_ASSOCIATION_ID` LEFT JOIN vendor_services ON   vendor_services.`VENDOR_ID` = vendor.`VENDOR_ID` LEFT JOIN association_ven_ind_ser_priority ON   association_ven_ind_ser_priority.`VENDOR_ID` = vendor.`VENDOR_ID` WHERE association_vendors.`ASSOCIATION_ID` = '" + req.body.associationId + "' AND association_ven_ind_ser_priority.`PRIORITY` = 5  AND association_ven_ind_ser_priority.`ASSOCIATION_ID` = '" + req.body.associationId + "'  AND vendor_services.IND_SERVICE_ID ='" + req.body.serviceId + "' AND individual_service_tickets.STATUS = 'Pending' AND individual_service_tickets.TICKET_DATE = '" + req.body.ticket_date + "' AND (individual_service_tickets.TICKET_TIME   BETWEEN '" + preTime + "' AND '" + postTime + "') GROUP BY vendor.`VENDOR_ID`")
        if (prioryOneVendor.lenght > 0) {
            assSendMail.associationIndSendMail(req.body.indTicketId, req.body.description, req.body.associationEmail, req.body.ticket_date, req.body.ticket_time,   req.body.appUserserviceName, req.body.appUserserviceTypeName,req.body.appUserQuantity ,null,req.body.appUserPlan,'Not Assigned')
          
            OwnerIndTicketSendMail.IndTicketSendMail(req.body.indTicketId, req.body.description,   req.body.appUserEmail, req.body.ticket_date, req.body.ticket_time,   req.body.appUserserviceName, req.body.appUserserviceTypeName,req.body.appUserQuantity ,null,req.body.appUserPlan,'Not Assigned');
            res.send(req.body.data);
            //next()
        }
        else {


            const prioryOneElVendor = await getRepository(VENDOR).query("SELECT * FROM vendor LEFT JOIN map_ind_tickets_vendors ON   map_ind_tickets_vendors.VENDOR_ID = vendor.VENDOR_ID LEFT JOIN individual_service_tickets ON    individual_service_tickets.IND_TICKET_ID = map_ind_tickets_vendors.IND_TICKET_ID LEFT JOIN association_vendors ON   association_vendors.`VENDOR_ASSOCIATION_ID` = vendor.`VENDOR_ASSOCIATION_ID` LEFT JOIN vendor_services ON   vendor_services.`VENDOR_ID` = vendor.`VENDOR_ID` LEFT JOIN association_ven_ind_ser_priority ON   association_ven_ind_ser_priority.`VENDOR_ID` = vendor.`VENDOR_ID` WHERE association_vendors.`ASSOCIATION_ID` = '" + req.body.associationId + "'  AND vendor_services.IND_SERVICE_ID ='" + req.body.serviceId + "' AND association_ven_ind_ser_priority.`PRIORITY` = 5  AND association_ven_ind_ser_priority.`ASSOCIATION_ID` = '" + req.body.associationId + "'  GROUP BY vendor.`VENDOR_ID`")


            if (prioryOneElVendor.length > 0) {

                var vendorId = prioryOneElVendor[0].VENDOR_ID;
                var vendorAssocaitonId = prioryOneElVendor[0].VENDOR_ASSOCIATION_ID;
                // var vendorId = prioryOneElVendor.VENDOR_ID;
                //res.send(prioryOneVendor);
                const vendorObj = await vendorRep.findOne(vendorId);
                const indObj = await individualTicketRep.findOne(
                    {
                        where : {
                            IND_TICKET_ID :  req.body.indTicketId
                        },

                        relations : ["indSerPlan"]
                    }
                   
                    )

                const mapVendorObj = new MapIndTicketVendor();
                mapVendorObj.vendor = vendorObj;
                mapVendorObj.indTickets = indObj;
                mapVendorObj.IS_ACCEPTED = false;
                mapVendorObj.IS_REJECTED = false;
                mapVendorObj.IS_ACTIVE = true;

                var samapVendorObj = mapIndVendorRep.create(mapVendorObj);

                mapIndVendorRep.save(samapVendorObj).then(async (mapIndObj) => {
                    if (req.body.service_types_id) {
                        req.body.serviceId

                        const interServAmountVendorAssociation = await getRepository(IND_SER_TYPES_VENDOR_ASSOCIATION)
                            .createQueryBuilder("interServAmountVendorAssociation")
                            .where("interServAmountVendorAssociation.IND_SERVICE_ID =  :indServiceId AND interServAmountVendorAssociation.IND_SERVICE_TYPE_ID = :indServiceTypeId AND VENDOR_ASSOCIATION_ID = :vendorAsociationId ", {
                                indServiceId: req.body.serviceId,
                                indServiceTypeId: req.body.service_types_id,
                                vendorAsociationId: vendorAssocaitonId
                            }).getOne().then(async (data) => {
                                var amount = data.AMOUNT;


                                if (req.body.quantity ) {

                                    amount = amount * req.body.quantity;
                                }

                                //   var esult = await individualTicketRep.save(indObj);

                                if(indObj.indSerPlan.PLAN == 'Monthly'){
                                const indServPlanRep = getRepository(IND_SERVICE_PLANS);
                                const indServiceMapSub = await indServPlanRep.findOne(indObj.indSerPlan.IND_SERVICE_PLAN_ID);
                                var days =  indServiceMapSub.DAYS;
                                indObj.AMOUNT = amount * days;

                                }
                                else {

                                    indObj.AMOUNT = amount;
                                    console.log(amount + "amount")

                                }

                                var esult = await individualTicketRep.save(indObj);

                                var appUserDetails = req.body.appUserName+'-'+req.body.appUserChildName+'-'+ req.body.appUserGrandchild; 
                                vendorSendMail.vendoIndSendMail(req.body.indTicketId, req.body.description,   vendorObj.VENDOR_EMAIL, req.body.ticket_date, req.body.ticket_time,   req.body.appUserserviceName, req.body.appUserserviceTypeName,req.body.appUserQuantity ,amount,req.body.appUserPlan,req.body.associationName,  appUserDetails)
                                OwnerIndTicketSendMail.IndTicketSendMail(req.body.indTicketId, req.body.description,   req.body.appUserEmail, req.body.ticket_date, req.body.ticket_time,   req.body.appUserserviceName, req.body.appUserserviceTypeName,req.body.appUserQuantity ,amount,req.body.appUserPlan,vendorObj.VENDOR_NAME);
                            }).catch((error) => {
                                console.log(error);
                            });



                    }
                    else {

                        const interServAmountVendorAssociation = await getRepository(IND_SER_TYPES_VENDOR_ASSOCIATION)
                            .createQueryBuilder("interServAmountVendorAssociation")
                            .where("interServAmountVendorAssociation.IND_SERVICE_ID =  :indServiceId AND VENDOR_ASSOCIATION_ID = :vendorAsociationId", {
                                indServiceId: req.body.serviceId,
                                vendorAsociationId: vendorAssocaitonId
                            }).getOne().then(async (data) => {
                                var amount = data.AMOUNT;


                                if (req.body.quantity) {

                                    amount = amount * req.body.quantity;
                                }

                                    if(indObj.indSerPlan.PLAN == 'Monthly'){
                                    const indServPlanRep = getRepository(IND_SERVICE_PLANS);
                                    const indServiceMapSub = await indServPlanRep.findOne(indObj.indSerPlan.IND_SERVICE_PLAN_ID);
                                    var days =  indServiceMapSub.DAYS;
                                    indObj.AMOUNT = amount * days;

                                    }
                                    else {

                                    indObj.AMOUNT = amount;
                                    console.log(amount + "amount")

                                    }

                                var esult = await individualTicketRep.save(indObj);

                                var appUserDetails = req.body.appUserName+'-'+req.body.appUserChildName+'-'+ req.body.appUserGrandchild; 
                                vendorSendMail.vendoIndSendMail(req.body.indTicketId, req.body.description,   vendorObj.VENDOR_EMAIL, req.body.ticket_date, req.body.ticket_time,   req.body.appUserserviceName, req.body.appUserserviceTypeName,req.body.appUserQuantity ,amount,req.body.appUserPlan,req.body.associationName,  appUserDetails) 
                                OwnerIndTicketSendMail.IndTicketSendMail(req.body.indTicketId, req.body.description,   req.body.appUserEmail, req.body.ticket_date, req.body.ticket_time,   req.body.appUserserviceName, req.body.appUserserviceTypeName,req.body.appUserQuantity ,amount,req.body.appUserPlan,vendorObj.VENDOR_NAME);
                            }).catch((error) => {
                                console.log(error);
                            });

                    }
                    res.send(req.body.data);
                }).catch((error) => {
                    res.status(500).send(error)
                });
            }

            else {
                res.send(req.body.data);
            }


        }


    }



    static checkInternalVendors = async (req: Request, res: Response, next) => {
        const mapIndVendorRep = await getRepository(MapIndTicketVendor);
        const vendorRep = await getRepository(VENDOR);
        const individualTicketRep = await getRepository(IndividualTickets);
        var date = "2020-03-23T" + req.body.ticket_time + ".573Z";
        var postTime = getPostTIme(date);
        var preTime = getPreTime(date);
        const prioryOneVendor = await getRepository(VENDOR)
            .createQueryBuilder("vendor")
            .leftJoinAndSelect("vendor.assVendors", "assVendor", "assVendor.vendorInfo = vendor.id")
            .leftJoinAndSelect("vendor.vensServices", "venService", "venService.vendorInfo = vendor.id")
            .leftJoinAndSelect("vendor.mapVendor", "mapVendors", "mapVendors.vendor = vendor.id")
            .leftJoinAndSelect("mapVendors.indTickets", "indTicket", "indTicket.IND_TICKET_ID = mapVendors.IND_TICKET_ID")
            .leftJoinAndSelect("vendor.assVenSerPriority", "assSerPriority", "assSerPriority.vendorInfo = vendor.id")
            .where("assVendor.ASSOCIATION_ID =  :id AND venService.IND_SERVICE_ID = :service  AND indTicket.TICKET_DATE = :ticket_date AND assSerPriority.PRIORITY = 1  AND  assVendor.IS_INTERNAL_VENDOR = true AND  assVendor.IS_THIRD_PARTY_VENDOR = false AND (indTicket.TICKET_TIME   BETWEEN '" + preTime + "' AND '" + postTime + " ')", {
                id: req.body.associationId,
                service: req.body.serviceId,
                ticket_date: req.body.ticket_date,
            }).getOne();
        console.log("primary vendor");
        console.log(prioryOneVendor)
        if (prioryOneVendor) {
            //second internla vendor
            const prioryTwoVendor = await getRepository(VENDOR)
                .createQueryBuilder("vendor")
                .leftJoinAndSelect("vendor.assVendors", "assVendor", "assVendor.vendorInfo = vendor.id")
                .leftJoinAndSelect("vendor.vensServices", "venService", "venService.vendorInfo = vendor.id")
                .leftJoinAndSelect("vendor.mapVendor", "mapVendors", "mapVendors.vendor = vendor.id")
                .leftJoinAndSelect("mapVendors.indTickets", "indTicket", "indTicket.IND_TICKET_ID = mapVendors.IND_TICKET_ID")
                .leftJoinAndSelect("vendor.assVenSerPriority", "assSerPriority", "assSerPriority.vendorInfo = vendor.id")
                .where("assVendor.ASSOCIATION_ID =  :id AND assVendor.IS_INTERNAL_VENDOR = true AND assVendor.IS_THIRD_PARTY_VENDOR = false AND venService.IND_SERVICE_ID = :service  AND indTicket.TICKET_DATE = :ticket_date AND assSerPriority.PRIORITY = 2  AND (indTicket.TICKET_TIME   BETWEEN '" + preTime + "' AND '" + postTime + " ')", {
                    id: req.body.associationId,
                    service: req.body.serviceId,
                    ticket_date: req.body.ticket_date,
                }).getOne();


            if (prioryTwoVendor) {

                //check 3ed prority

                const prioryThreeVendor = await getRepository(VENDOR)
                    .createQueryBuilder("vendor")
                    .leftJoinAndSelect("vendor.assVendors", "assVendor", "assVendor.vendorInfo = vendor.id")
                    .leftJoinAndSelect("vendor.vensServices", "venService", "venService.vendorInfo = vendor.id")
                    .leftJoinAndSelect("vendor.mapVendor", "mapVendors", "mapVendors.vendor = vendor.id")
                    .leftJoinAndSelect("mapVendors.indTickets", "indTicket", "indTicket.IND_TICKET_ID = mapVendors.IND_TICKET_ID")
                    .leftJoinAndSelect("vendor.assVenSerPriority", "assSerPriority", "assSerPriority.vendorInfo = vendor.id")
                    .where("assVendor.ASSOCIATION_ID =  :id AND assVendor.IS_INTERNAL_VENDOR = true AND assVendor.IS_THIRD_PARTY_VENDOR = false AND venService.IND_SERVICE_ID = :service  AND indTicket.TICKET_DATE = :ticket_date AND assSerPriority.PRIORITY = 3  AND (indTicket.TICKET_TIME   BETWEEN '" + preTime + "' AND '" + postTime + " ')", {
                        id: req.body.associationId,
                        service: req.body.serviceId,
                        ticket_date: req.body.ticket_date,
                    }).getOne();


                if (prioryThreeVendor) {

                    //4th vendor

                    const prioryFourVendor = await getRepository(VENDOR)
                        .createQueryBuilder("vendor")
                        .leftJoinAndSelect("vendor.assVendors", "assVendor", "assVendor.vendorInfo = vendor.id")
                        .leftJoinAndSelect("vendor.vensServices", "venService", "venService.vendorInfo = vendor.id")
                        .leftJoinAndSelect("vendor.mapVendor", "mapVendors", "mapVendors.vendor = vendor.id")
                        .leftJoinAndSelect("mapVendors.indTickets", "indTicket", "indTicket.IND_TICKET_ID = mapVendors.IND_TICKET_ID")
                        .leftJoinAndSelect("vendor.assVenSerPriority", "assSerPriority", "assSerPriority.vendorInfo = vendor.id")
                        .where("assVendor.ASSOCIATION_ID =  :id AND assVendor.IS_INTERNAL_VENDOR = true AND  assVendor.IS_THIRD_PARTY_VENDOR = false AND venService.IND_SERVICE_ID = :service  AND indTicket.TICKET_DATE = :ticket_date AND assSerPriority.PRIORITY = 4  AND (indTicket.TICKET_TIME   BETWEEN '" + preTime + "' AND '" + postTime + " ')", {
                            id: req.body.associationId,
                            service: req.body.serviceId,
                            ticket_date: req.body.ticket_date,
                        }).getOne();

                    if (prioryFourVendor) {
                        //prior five vendor
                        const prioryFiveVendor = await getRepository(VENDOR)
                            .createQueryBuilder("vendor")
                            .leftJoinAndSelect("vendor.assVendors", "assVendor", "assVendor.vendorInfo = vendor.id")
                            .leftJoinAndSelect("vendor.vensServices", "venService", "venService.vendorInfo = vendor.id")
                            .leftJoinAndSelect("vendor.mapVendor", "mapVendors", "mapVendors.vendor = vendor.id")
                            .leftJoinAndSelect("mapVendors.indTickets", "indTicket", "indTicket.IND_TICKET_ID = mapVendors.IND_TICKET_ID")
                            .leftJoinAndSelect("vendor.assVenSerPriority", "assSerPriority", "assSerPriority.vendorInfo = vendor.id")
                            .where("assVendor.ASSOCIATION_ID =  :id AND venService.IND_SERVICE_ID = :service  AND indTicket.TICKET_DATE = :ticket_date AND assSerPriority.PRIORITY = 5  AND assVendor.IS_INTERNAL_VENDOR = true AND  assVendor.IS_THIRD_PARTY_VENDOR = false AND (indTicket.TICKET_TIME   BETWEEN '" + preTime + "' AND '" + postTime + " ')", {
                                id: req.body.associationId,
                                service: req.body.serviceId,
                                ticket_date: req.body.ticket_date,
                            }).getOne();

                        if (prioryFiveVendor) {
                            next();

                        }
                        else {
                            const prioryFiveElVendor = await getRepository(VENDOR)
                                .createQueryBuilder("vendor")
                                .leftJoinAndSelect("vendor.assVendors", "assVendor", "assVendor.vendorInfo = vendor.id")
                                .leftJoinAndSelect("vendor.vensServices", "venService", "venService.vendorInfo = vendor.id")
                                .leftJoinAndSelect("vendor.mapVendor", "mapVendors", "mapVendors.vendor = vendor.id")
                                .leftJoinAndSelect("mapVendors.indTickets", "indTicket", "indTicket.IND_TICKET_ID = mapVendors.IND_TICKET_ID")
                                .leftJoinAndSelect("vendor.assVenSerPriority", "assSerPriority", "assSerPriority.vendorInfo = vendor.id")
                                .where("assVendor.ASSOCIATION_ID =  :id AND assVendor.IS_INTERNAL_VENDOR = true AND  assVendor.IS_THIRD_PARTY_VENDOR = false AND venService.IND_SERVICE_ID = :service   AND assSerPriority.PRIORITY = 5", {
                                    id: req.body.associationId,
                                    service: req.body.serviceId,

                                }).getOne();

                            var vendorId = prioryFiveElVendor.id;
                            //res.send(prioryOneVendor);
                            const vendorObj = await vendorRep.findOne(vendorId);
                            const indObj = await individualTicketRep.findOne(req.body.indTicketId)
                            const mapVendorObj = new MapIndTicketVendor();
                            mapVendorObj.vendor = vendorObj;
                            mapVendorObj.indTickets = indObj;
                            mapVendorObj.IS_ACCEPTED = false;
                            mapVendorObj.IS_REJECTED = false;
                            mapVendorObj.IS_ACTIVE = true;

                            var samapVendorObj = mapIndVendorRep.create(mapVendorObj);

                            mapIndVendorRep.save(samapVendorObj).then((mapIndObj) => {
                                res.send(req.body.data);
                            }).catch((error) => {
                                res.status(500).send(error)
                            });

                            // res.send(prioryFiveElVendor)


                        }



                    }
                    else {
                        const prioryFourVendor = await getRepository(VENDOR)
                            .createQueryBuilder("vendor")
                            .leftJoinAndSelect("vendor.assVendors", "assVendor", "assVendor.vendorInfo = vendor.id")
                            .leftJoinAndSelect("vendor.vensServices", "venService", "venService.vendorInfo = vendor.id")
                            .leftJoinAndSelect("vendor.mapVendor", "mapVendors", "mapVendors.vendor = vendor.id")
                            .leftJoinAndSelect("mapVendors.indTickets", "indTicket", "indTicket.IND_TICKET_ID = mapVendors.IND_TICKET_ID")
                            .leftJoinAndSelect("vendor.assVenSerPriority", "assSerPriority", "assSerPriority.vendorInfo = vendor.id")
                            .where("assVendor.ASSOCIATION_ID =  :id AND assVendor.IS_INTERNAL_VENDOR = true AND  assVendor.IS_THIRD_PARTY_VENDOR = false AND venService.IND_SERVICE_ID = :service   AND assSerPriority.PRIORITY = 4 ", {
                                id: req.body.associationId,
                                service: req.body.serviceId,

                            }).getOne();

                        var vendorId = prioryFourVendor.id;
                        //res.send(prioryOneVendor);
                        const vendorObj = await vendorRep.findOne(vendorId);
                        const indObj = await individualTicketRep.findOne(req.body.indTicketId)
                        const mapVendorObj = new MapIndTicketVendor();
                        mapVendorObj.vendor = vendorObj;
                        mapVendorObj.indTickets = indObj;
                        mapVendorObj.IS_ACCEPTED = false;
                        mapVendorObj.IS_REJECTED = false;
                        mapVendorObj.IS_ACTIVE = true;

                        var samapVendorObj = mapIndVendorRep.create(mapVendorObj);

                        mapIndVendorRep.save(samapVendorObj).then((mapIndObj) => {
                            res.send(req.body.data);
                        }).catch((error) => {
                            res.status(500).send(error)
                        });

                        //  res.send(prioryFourVendor);


                    }







                }
                else {

                    const prioryThreeElVendor = await getRepository(VENDOR)
                        .createQueryBuilder("vendor")
                        .leftJoinAndSelect("vendor.assVendors", "assVendor", "assVendor.vendorInfo = vendor.id")
                        .leftJoinAndSelect("vendor.vensServices", "venService", "venService.vendorInfo = vendor.id")
                        .leftJoinAndSelect("vendor.mapVendor", "mapVendors", "mapVendors.vendor = vendor.id")
                        .leftJoinAndSelect("mapVendors.indTickets", "indTicket", "indTicket.IND_TICKET_ID = mapVendors.IND_TICKET_ID")
                        .leftJoinAndSelect("vendor.assVenSerPriority", "assSerPriority", "assSerPriority.vendorInfo = vendor.id")
                        .where("assVendor.ASSOCIATION_ID =  :id AND assVendor.IS_INTERNAL_VENDOR = true AND  assVendors.IS_THIRD_PARTY_VENDOR = false AND venService.IND_SERVICE_ID = :service  AND assSerPriority.PRIORITY = 3", {
                            id: req.body.associationId,
                            service: req.body.serviceId,
                            //  ticket_date: req.body.ticket_date,
                        }).getOne();


                    var vendorId = prioryThreeElVendor.id;
                    //res.send(prioryOneVendor);
                    const vendorObj = await vendorRep.findOne(vendorId);
                    const indObj = await individualTicketRep.findOne(req.body.indTicketId)
                    const mapVendorObj = new MapIndTicketVendor();
                    mapVendorObj.vendor = vendorObj;
                    mapVendorObj.indTickets = indObj;
                    mapVendorObj.IS_ACCEPTED = false;
                    mapVendorObj.IS_REJECTED = false;
                    mapVendorObj.IS_ACTIVE = true;

                    var samapVendorObj = mapIndVendorRep.create(mapVendorObj);

                    mapIndVendorRep.save(samapVendorObj).then((mapIndObj) => {
                        res.send(req.body.data);
                    }).catch((error) => {
                        res.status(500).send(error)
                    });


                    //  res.send(prioryThreeElVendor);

                }



            }
            else {
                const prioryTwoElVendor = await getRepository(VENDOR)
                    .createQueryBuilder("vendor")
                    .leftJoinAndSelect("vendor.assVendors", "assVendor", "assVendor.vendorInfo = vendor.id")
                    .leftJoinAndSelect("vendor.vensServices", "venService", "venService.vendorInfo = vendor.id")
                    .leftJoinAndSelect("vendor.mapVendor", "mapVendors", "mapVendors.vendor = vendor.id")
                    .leftJoinAndSelect("mapVendors.indTickets", "indTicket", "indTicket.IND_TICKET_ID = mapVendors.IND_TICKET_ID")
                    .leftJoinAndSelect("vendor.assVenSerPriority", "assSerPriority", "assSerPriority.vendorInfo = vendor.id")
                    .where("assVendor.ASSOCIATION_ID =  :id AND assVendor.IS_INTERNAL_VENDOR = true AND  assVendor.IS_THIRD_PARTY_VENDOR = false AND venService.IND_SERVICE_ID = :service  AND assSerPriority.PRIORITY = 2 ", {
                        id: req.body.associationId,
                        service: req.body.serviceId,
                        //  ticket_date: req.body.ticket_date,
                    }).getOne();


                var vendorId = prioryTwoElVendor.id;
                //res.send(prioryOneVendor);
                const vendorObj = await vendorRep.findOne(vendorId);
                const indObj = await individualTicketRep.findOne(req.body.indTicketId)
                const mapVendorObj = new MapIndTicketVendor();
                mapVendorObj.vendor = vendorObj;
                mapVendorObj.indTickets = indObj;
                mapVendorObj.IS_ACCEPTED = false;
                mapVendorObj.IS_REJECTED = false;
                mapVendorObj.IS_ACTIVE = true;

                var samapVendorObj = mapIndVendorRep.create(mapVendorObj);

                mapIndVendorRep.save(samapVendorObj).then((mapIndObj) => {
                    res.send(req.body.data);
                }).catch((error) => {
                    res.status(500).send(error)
                });

                //    res.send(prioryTwoElVendor);


            }








            // next()
            //res.send("vendor Busy");
        }
        else {

            const prioryOneElVendor = await getRepository(VENDOR)
                .createQueryBuilder("vendor")
                .leftJoinAndSelect("vendor.assVendors", "assVendor", "assVendor.vendorInfo = vendor.id")
                .leftJoinAndSelect("vendor.vensServices", "venService", "venService.vendorInfo = vendor.id")
                .leftJoinAndSelect("vendor.mapVendor", "mapVendors", "mapVendors.vendor = vendor.id")
                .leftJoinAndSelect("mapVendors.indTickets", "indTicket", "indTicket.IND_TICKET_ID = mapVendors.IND_TICKET_ID")
                .leftJoinAndSelect("vendor.assVenSerPriority", "assSerPriority", "assSerPriority.vendorInfo = vendor.id")
                .where("assVendor.ASSOCIATION_ID =  :id AND venService.IND_SERVICE_ID = :service   AND assVendor.IS_INTERNAL_VENDOR = true AND  assVendor.IS_THIRD_PARTY_VENDOR = false AND  assSerPriority.PRIORITY = 1 ", {
                    id: req.body.associationId,
                    service: req.body.serviceId,
                }).getOne();

            console.log(prioryOneElVendor)

            var vendorId = prioryOneElVendor.id;
            //res.send(prioryOneVendor);
            const vendorObj = await vendorRep.findOne(vendorId);
            const indObj = await individualTicketRep.findOne(req.body.indTicketId)
            const mapVendorObj = new MapIndTicketVendor();
            mapVendorObj.vendor = vendorObj;
            mapVendorObj.indTickets = indObj;
            mapVendorObj.IS_ACCEPTED = false;
            mapVendorObj.IS_REJECTED = false;
            mapVendorObj.IS_ACTIVE = true;

            var samapVendorObj = mapIndVendorRep.create(mapVendorObj);

            mapIndVendorRep.save(samapVendorObj).then((mapIndObj) => {
                res.send(req.body.data);
            }).catch((error) => {
                res.status(500).send(error)
            });

            // res.send(prioryOneElVendor);
        }
        //  res.send(prioryOneVendor);

    }

    static checkThirdPartyVendors = async (req: Request, res: Response, next) => {
        const mapIndVendorRep = await getRepository(MapIndTicketVendor);
        const vendorRep = await getRepository(VENDOR);
        const individualTicketRep = await getRepository(IndividualTickets);
        var date = "2020-03-23T" + req.body.ticket_time + ".573Z";
        var postTime = getPostTIme(date);
        var preTime = getPreTime(date);
        const prioryOneVendor = await getRepository(VENDOR)
            .createQueryBuilder("vendor")
            .leftJoinAndSelect("vendor.assVendors", "assVendor", "assVendor.vendorInfo = vendor.id")
            .leftJoinAndSelect("vendor.vensServices", "venService", "venService.vendorInfo = vendor.id")
            .leftJoinAndSelect("vendor.mapVendor", "mapVendors", "mapVendors.vendor = vendor.id")
            .leftJoinAndSelect("mapVendors.indTickets", "indTicket", "indTicket.IND_TICKET_ID = mapVendors.IND_TICKET_ID")
            .leftJoinAndSelect("vendor.assVenSerPriority", "assSerPriority", "assSerPriority.vendorInfo = vendor.id")
            .where("assVendor.ASSOCIATION_ID =  :id AND venService.IND_SERVICE_ID = :service  AND indTicket.TICKET_DATE = :ticket_date AND assSerPriority.PRIORITY = 1  AND  assVendor.IS_INTERNAL_VENDOR = false AND  assVendor.IS_THIRD_PARTY_VENDOR = true AND  (indTicket.TICKET_TIME   BETWEEN '" + preTime + "' AND '" + postTime + " ')", {
                id: req.body.associationId,
                service: req.body.serviceId,
                ticket_date: req.body.ticket_date,
            }).getOne();

        if (prioryOneVendor) {
            //second internla vendor
            const prioryTwoVendor = await getRepository(VENDOR)
                .createQueryBuilder("vendor")
                .leftJoinAndSelect("vendor.assVendors", "assVendor", "assVendor.vendorInfo = vendor.id")
                .leftJoinAndSelect("vendor.vensServices", "venService", "venService.vendorInfo = vendor.id")
                .leftJoinAndSelect("vendor.mapVendor", "mapVendors", "mapVendors.vendor = vendor.id")
                .leftJoinAndSelect("mapVendors.indTickets", "indTicket", "indTicket.IND_TICKET_ID = mapVendors.IND_TICKET_ID")
                .leftJoinAndSelect("vendor.assVenSerPriority", "assSerPriority", "assSerPriority.vendorInfo = vendor.id")
                .where("assVendor.ASSOCIATION_ID =  :id AND assVendor.IS_INTERNAL_VENDOR = false AND  assVendor.IS_THIRD_PARTY_VENDOR = true AND venService.IND_SERVICE_ID = :service  AND indTicket.TICKET_DATE = :ticket_date AND assSerPriority.PRIORITY = 2  AND (indTicket.TICKET_TIME   BETWEEN '" + preTime + "' AND '" + postTime + " ')", {
                    id: req.body.associationId,
                    service: req.body.serviceId,
                    ticket_date: req.body.ticket_date,
                }).getOne();


            if (prioryTwoVendor) {

                //check 3ed prority

                const prioryThreeVendor = await getRepository(VENDOR)
                    .createQueryBuilder("vendor")
                    .leftJoinAndSelect("vendor.assVendors", "assVendor", "assVendor.vendorInfo = vendor.id")
                    .leftJoinAndSelect("vendor.vensServices", "venService", "venService.vendorInfo = vendor.id")
                    .leftJoinAndSelect("vendor.mapVendor", "mapVendors", "mapVendors.vendor = vendor.id")
                    .leftJoinAndSelect("mapVendors.indTickets", "indTicket", "indTicket.IND_TICKET_ID = mapVendors.IND_TICKET_ID")
                    .leftJoinAndSelect("vendor.assVenSerPriority", "assSerPriority", "assSerPriority.vendorInfo = vendor.id")
                    .where("assVendor.ASSOCIATION_ID =  :id AND assVendor.IS_INTERNAL_VENDOR = false AND  assVendor.IS_THIRD_PARTY_VENDOR = true AND venService.IND_SERVICE_ID = :service  AND indTicket.TICKET_DATE = :ticket_date AND assSerPriority.PRIORITY = 3  AND (indTicket.TICKET_TIME   BETWEEN '" + preTime + "' AND '" + postTime + " ')", {
                        id: req.body.associationId,
                        service: req.body.serviceId,
                        ticket_date: req.body.ticket_date,
                    }).getOne();


                if (prioryThreeVendor) {

                    //4th vendor

                    const prioryFourVendor = await getRepository(VENDOR)
                        .createQueryBuilder("vendor")
                        .leftJoinAndSelect("vendor.assVendors", "assVendor", "assVendor.vendorInfo = vendor.id")
                        .leftJoinAndSelect("vendor.vensServices", "venService", "venService.vendorInfo = vendor.id")
                        .leftJoinAndSelect("vendor.mapVendor", "mapVendors", "mapVendors.vendor = vendor.id")
                        .leftJoinAndSelect("mapVendors.indTickets", "indTicket", "indTicket.IND_TICKET_ID = mapVendors.IND_TICKET_ID")
                        .leftJoinAndSelect("vendor.assVenSerPriority", "assSerPriority", "assSerPriority.vendorInfo = vendor.id")
                        .where("assVendor.ASSOCIATION_ID =  :id AND assVendor.IS_INTERNAL_VENDOR = false AND  assVendor.IS_THIRD_PARTY_VENDOR = true AND venService.IND_SERVICE_ID = :service  AND indTicket.TICKET_DATE = :ticket_date AND assSerPriority.PRIORITY = 4  AND (indTicket.TICKET_TIME   BETWEEN '" + preTime + "' AND '" + postTime + " ')", {
                            id: req.body.associationId,
                            service: req.body.serviceId,
                            ticket_date: req.body.ticket_date,
                        }).getOne();

                    if (prioryFourVendor) {
                        //prior five vendor
                        const prioryFiveVendor = await getRepository(VENDOR)
                            .createQueryBuilder("vendor")
                            .leftJoinAndSelect("vendor.assVendors", "assVendor", "assVendor.vendorInfo = vendor.id")
                            .leftJoinAndSelect("vendor.vensServices", "venService", "venService.vendorInfo = vendor.id")
                            .leftJoinAndSelect("vendor.mapVendor", "mapVendors", "mapVendors.vendor = vendor.id")
                            .leftJoinAndSelect("mapVendors.indTickets", "indTicket", "indTicket.IND_TICKET_ID = mapVendors.IND_TICKET_ID")
                            .leftJoinAndSelect("vendor.assVenSerPriority", "assSerPriority", "assSerPriority.vendorInfo = vendor.id")
                            .where("assVendor.ASSOCIATION_ID =  :id AND venService.IND_SERVICE_ID = :service  AND indTicket.TICKET_DATE = :ticket_date AND assSerPriority.PRIORITY = 5  AND assVendor.IS_INTERNAL_VENDOR = false AND  assVendor.IS_THIRD_PARTY_VENDOR = true AND (indTicket.TICKET_TIME   BETWEEN '" + preTime + "' AND '" + postTime + " ')", {
                                id: req.body.associationId,
                                service: req.body.serviceId,
                                ticket_date: req.body.ticket_date,
                            }).getOne();

                        if (prioryFiveVendor) {
                            res.send("unable to assign vendor");
                            //next();

                        }
                        else {
                            const prioryFiveElVendor = await getRepository(VENDOR)
                                .createQueryBuilder("vendor")
                                .leftJoinAndSelect("vendor.assVendors", "assVendor", "assVendor.vendorInfo = vendor.id")
                                .leftJoinAndSelect("vendor.vensServices", "venService", "venService.vendorInfo = vendor.id")
                                .leftJoinAndSelect("vendor.mapVendor", "mapVendors", "mapVendors.vendor = vendor.id")
                                .leftJoinAndSelect("mapVendors.indTickets", "indTicket", "indTicket.IND_TICKET_ID = mapVendors.IND_TICKET_ID")
                                .leftJoinAndSelect("vendor.assVenSerPriority", "assSerPriority", "assSerPriority.vendorInfo = vendor.id")
                                .where("assVendor.ASSOCIATION_ID =  :id AND assVendor.IS_INTERNAL_VENDOR = false AND  assVendor.IS_THIRD_PARTY_VENDOR = true AND venService.IND_SERVICE_ID = :service   AND assSerPriority.PRIORITY = 5", {
                                    id: req.body.associationId,
                                    service: req.body.serviceId,

                                }).getOne();

                            // res.send(prioryFiveElVendor)

                            var vendorId = prioryFiveElVendor.id;
                            //res.send(prioryOneVendor);
                            const vendorObj = await vendorRep.findOne(vendorId);
                            const indObj = await individualTicketRep.findOne(req.body.indTicketId)
                            const mapVendorObj = new MapIndTicketVendor();
                            mapVendorObj.vendor = vendorObj;
                            mapVendorObj.indTickets = indObj;
                            mapVendorObj.IS_ACCEPTED = false;
                            mapVendorObj.IS_REJECTED = false;
                            mapVendorObj.IS_ACTIVE = true;

                            var samapVendorObj = mapIndVendorRep.create(mapVendorObj);

                            mapIndVendorRep.save(samapVendorObj).then((mapIndObj) => {
                                res.send(req.body.data);
                            }).catch((error) => {
                                res.status(500).send(error)
                            });


                        }



                    }
                    else {
                        const prioryFourVendor = await getRepository(VENDOR)
                            .createQueryBuilder("vendor")
                            .leftJoinAndSelect("vendor.assVendors", "assVendor", "assVendor.vendorInfo = vendor.id")
                            .leftJoinAndSelect("vendor.vensServices", "venService", "venService.vendorInfo = vendor.id")
                            .leftJoinAndSelect("vendor.mapVendor", "mapVendors", "mapVendors.vendor = vendor.id")
                            .leftJoinAndSelect("mapVendors.indTickets", "indTicket", "indTicket.IND_TICKET_ID = mapVendors.IND_TICKET_ID")
                            .leftJoinAndSelect("vendor.assVenSerPriority", "assSerPriority", "assSerPriority.vendorInfo = vendor.id")
                            .where("assVendor.ASSOCIATION_ID =  :id AND assVendor.IS_INTERNAL_VENDOR = false AND  assVendor.IS_THIRD_PARTY_VENDOR = true AND venService.IND_SERVICE_ID = :service   AND assSerPriority.PRIORITY = 4 ", {
                                id: req.body.associationId,
                                service: req.body.serviceId,

                            }).getOne();



                        var vendorId = prioryFourVendor.id;
                        //res.send(prioryOneVendor);
                        const vendorObj = await vendorRep.findOne(vendorId);
                        const indObj = await individualTicketRep.findOne(req.body.indTicketId)
                        const mapVendorObj = new MapIndTicketVendor();
                        mapVendorObj.vendor = vendorObj;
                        mapVendorObj.indTickets = indObj;
                        mapVendorObj.IS_ACCEPTED = false;
                        mapVendorObj.IS_REJECTED = false;
                        mapVendorObj.IS_ACTIVE = true;
                        var samapVendorObj = mapIndVendorRep.create(mapVendorObj);

                        mapIndVendorRep.save(samapVendorObj).then((mapIndObj) => {
                            res.send(req.body.data);
                        }).catch((error) => {
                            res.status(500).send(error)
                        });

                        //  res.send(prioryFourVendor);


                    }







                }
                else {

                    const prioryThreeElVendor = await getRepository(VENDOR)
                        .createQueryBuilder("vendor")
                        .leftJoinAndSelect("vendor.assVendors", "assVendor", "assVendor.vendorInfo = vendor.id")
                        .leftJoinAndSelect("vendor.vensServices", "venService", "venService.vendorInfo = vendor.id")
                        .leftJoinAndSelect("vendor.mapVendor", "mapVendors", "mapVendors.vendor = vendor.id")
                        .leftJoinAndSelect("mapVendors.indTickets", "indTicket", "indTicket.IND_TICKET_ID = mapVendors.IND_TICKET_ID")
                        .leftJoinAndSelect("vendor.assVenSerPriority", "assSerPriority", "assSerPriority.vendorInfo = vendor.id")
                        .where("assVendor.ASSOCIATION_ID =  :id AND assVendor.IS_INTERNAL_VENDOR = false AND  assVendor.IS_THIRD_PARTY_VENDOR = true AND venService.IND_SERVICE_ID = :service  AND assSerPriority.PRIORITY = 3", {
                            id: req.body.associationId,
                            service: req.body.serviceId,
                            //  ticket_date: req.body.ticket_date,
                        }).getOne();


                    var vendorId = prioryThreeElVendor.id;
                    //res.send(prioryOneVendor);
                    const vendorObj = await vendorRep.findOne(vendorId);
                    const indObj = await individualTicketRep.findOne(req.body.indTicketId)
                    const mapVendorObj = new MapIndTicketVendor();
                    mapVendorObj.vendor = vendorObj;
                    mapVendorObj.indTickets = indObj;
                    mapVendorObj.IS_ACCEPTED = false;
                    mapVendorObj.IS_REJECTED = false;
                    mapVendorObj.IS_ACTIVE = true;
                    var samapVendorObj = mapIndVendorRep.create(mapVendorObj);

                    mapIndVendorRep.save(samapVendorObj).then((mapIndObj) => {
                        res.send(req.body.data);
                    }).catch((error) => {
                        res.status(500).send(error)
                    });


                    // res.send(prioryThreeElVendor);

                }



            }
            else {
                const prioryTwoElVendor = await getRepository(VENDOR)
                    .createQueryBuilder("vendor")
                    .leftJoinAndSelect("vendor.assVendors", "assVendor", "assVendor.vendorInfo = vendor.id")
                    .leftJoinAndSelect("vendor.vensServices", "venService", "venService.vendorInfo = vendor.id")
                    .leftJoinAndSelect("vendor.mapVendor", "mapVendors", "mapVendors.vendor = vendor.id")
                    .leftJoinAndSelect("mapVendors.indTickets", "indTicket", "indTicket.IND_TICKET_ID = mapVendors.IND_TICKET_ID")
                    .leftJoinAndSelect("vendor.assVenSerPriority", "assSerPriority", "assSerPriority.vendorInfo = vendor.id")
                    .where("assVendor.ASSOCIATION_ID =  :id AND assVendor.IS_INTERNAL_VENDOR = false AND  assVendor.IS_THIRD_PARTY_VENDOR = true AND venService.IND_SERVICE_ID = :service  AND assSerPriority.PRIORITY = 2 ", {
                        id: req.body.associationId,
                        service: req.body.serviceId,
                        //  ticket_date: req.body.ticket_date,
                    }).getOne();



                var vendorId = prioryTwoElVendor.id;
                //res.send(prioryOneVendor);
                const vendorObj = await vendorRep.findOne(vendorId);
                const indObj = await individualTicketRep.findOne(req.body.indTicketId)
                const mapVendorObj = new MapIndTicketVendor();
                mapVendorObj.vendor = vendorObj;
                mapVendorObj.indTickets = indObj;
                mapVendorObj.IS_ACCEPTED = false;
                mapVendorObj.IS_REJECTED = false;
                mapVendorObj.IS_ACTIVE = true;
                var samapVendorObj = mapIndVendorRep.create(mapVendorObj);

                mapIndVendorRep.save(samapVendorObj).then((mapIndObj) => {
                    res.send(req.body.data);
                }).catch((error) => {
                    res.status(500).send(error)
                });

                //    res.send(prioryTwoElVendor);


            }








            // next()
            //res.send("vendor Busy");
        }
        else {

            const prioryOneElVendor = await getRepository(VENDOR)
                .createQueryBuilder("vendor")
                .leftJoinAndSelect("vendor.assVendors", "assVendor", "assVendor.vendorInfo = vendor.id")
                .leftJoinAndSelect("vendor.vensServices", "venService", "venService.vendorInfo = vendor.id")
                .leftJoinAndSelect("vendor.mapVendor", "mapVendors", "mapVendors.vendor = vendor.id")
                .leftJoinAndSelect("mapVendors.indTickets", "indTicket", "indTicket.IND_TICKET_ID = mapVendors.IND_TICKET_ID")
                .leftJoinAndSelect("vendor.assVenSerPriority", "assSerPriority", "assSerPriority.vendorInfo = vendor.id")
                .where("assVendor.ASSOCIATION_ID =  :id AND  assVendor.IS_INTERNAL_VENDOR = false AND  assVendor.IS_THIRD_PARTY_VENDOR = true AND venService.IND_SERVICE_ID = :service  AND assSerPriority.PRIORITY = 1 ", {
                    id: req.body.associationId,
                    service: req.body.serviceId,
                }).getOne();



            var vendorId = prioryOneElVendor.id;
            //res.send(prioryOneVendor);
            const vendorObj = await vendorRep.findOne(vendorId);
            const indObj = await individualTicketRep.findOne(req.body.indTicketId)
            const mapVendorObj = new MapIndTicketVendor();
            mapVendorObj.vendor = vendorObj;
            mapVendorObj.indTickets = indObj;
            mapVendorObj.IS_ACCEPTED = false;
            mapVendorObj.IS_REJECTED = false;
            mapVendorObj.IS_ACTIVE = true;
            var samapVendorObj = mapIndVendorRep.create(mapVendorObj);

            mapIndVendorRep.save(samapVendorObj).then((mapIndObj) => {
                res.send(req.body.data);
            }).catch((error) => {
                res.status(500).send(error)
            });
            //  res.send(prioryOneElVendor);
        }
        //  res.send(prioryOneVendor);

    }


    static indTicketAssignSecondaryVendor = async (req: Request, res: Response, next) => {
        const mapIndVendorRep = await getRepository(MapIndTicketVendor);
        const vendorRep = await getRepository(VENDOR);
        const individualTicketRep = await getRepository(IndividualTickets);
        var date = "2020-03-23T" + req.body.ticket_time + ".573Z";
        var postTime = getPostTIme(date);
        var preTime = getPreTime(date);
        const prioryOneVendor = await getRepository(VENDOR)
            .createQueryBuilder("vendor")
            .leftJoinAndSelect("vendor.assVendors", "assVendor", "assVendor.vendorInfo = vendor.id")
            .leftJoinAndSelect("vendor.vensServices", "venService", "venService.vendorInfo = vendor.id")
            .leftJoinAndSelect("vendor.mapVendor", "mapVendors", "mapVendors.vendor = vendor.id")
            .leftJoinAndSelect("mapVendors.indTickets", "indTicket", "indTicket.IND_TICKET_ID = mapVendors.IND_TICKET_ID")
            .where("assVendor.ASSOCIATION_ID =  :id AND venService.IND_SERVICE_ID = :service    AND indTicket.TICKET_DATE = :ticket_date  AND (indTicket.TICKET_TIME  BETWEEN '" + preTime + "' AND '" + postTime + "')", {
                id: req.body.associationId,
                service: req.body.serviceId,
                ticket_date: req.body.ticket_date,
            })
            .getOne();

        if (prioryOneVendor) {
            next();
        }
        else {
            const prioryVendor = await getRepository(VENDOR)
                .createQueryBuilder("vendor")
                .leftJoinAndSelect("vendor.assVendors", "assVendor", "assVendor.vendorInfo = vendor.id")
                .leftJoinAndSelect("vendor.vensServices", "venService", "venService.vendorInfo = vendor.id")
                .leftJoinAndSelect("vendor.mapVendor", "mapVendors", "mapVendors.vendor = vendor.id")
                .where("assVendor.ASSOCIATION_ID =  :id AND venService.IND_SERVICE_ID = :service  ", {
                    id: req.body.associationId,
                    service: req.body.serviceId,
                    //  ticket_date: req.body.ticket_date,
                })
                .getOne();
            var vendorId = prioryVendor.id;

            const vendorObj = await vendorRep.findOne(vendorId);
            const indObj = await individualTicketRep.findOne(req.body.indTicketId)

            const mapVendorObj = new MapIndTicketVendor();
            mapVendorObj.vendor = vendorObj;
            mapVendorObj.indTickets = indObj;
            mapVendorObj.IS_ACCEPTED = false;
            mapVendorObj.IS_REJECTED = false;
            mapVendorObj.IS_ACTIVE = true;
            var samapVendorObj = mapIndVendorRep.create(mapVendorObj);

            mapIndVendorRep.save(samapVendorObj).then((mapIndObj) => {
                res.send(req.body.data);
            }).catch((error) => {
                res.status(500).send(error)
            });


        }


    }

    static vendorIndividualTickets = async (req: Request, res: Response, next) => {
        const prioryVendor = await getRepository(MapIndTicketVendor)
            .createQueryBuilder("mapIndVendor")
            .leftJoinAndSelect("mapIndVendor.indTickets", "individualTickeMap", "individualTickeMap.IND_TICKET_ID = mapIndVendor.indTickets")
            .leftJoinAndSelect("individualTickeMap.serviceId", "indService", "indService.IND_SERVICE_ID = individualTickeMap.serviceId")
            .leftJoinAndSelect("individualTickeMap.association", "indAssociation", "indAssociation.id = individualTickeMap.association")
            .leftJoinAndSelect("individualTickeMap.indTicketOtp", "indTicketOtpMap", "indTicketOtpMap.indTickets = mapIndVendor.indTickets")
            .where("mapIndVendor.vendor =  :id AND mapIndVendor.IS_ACTIVE = true AND  individualTickeMap.STATUS = 'Pending'", {
                id: req.params.id,
            })
            .getMany();
        res.send(prioryVendor);

        //  const mapIndVendorRep = await getRepository(MapIndTicketVendor);

    }

    static vendorIndividualTicketsPendingCompleted = async (req: Request, res: Response, next) => {
        var data = [];
        const pending = await getRepository(MapIndTicketVendor)
            .createQueryBuilder("mapIndVendor")
            .leftJoinAndSelect("mapIndVendor.indTickets", "individualTickeMap", "individualTickeMap.IND_TICKET_ID = mapIndVendor.indTickets")
            .leftJoinAndSelect("individualTickeMap.serviceId", "indService", "indService.IND_SERVICE_ID = individualTickeMap.serviceId")
            .leftJoinAndSelect("individualTickeMap.association", "indAssociation", "indAssociation.id = individualTickeMap.association")
            .leftJoinAndSelect("individualTickeMap.indTicketOtp", "indTicketOtpMap", "indTicketOtpMap.indTickets = mapIndVendor.indTickets")
            .where("mapIndVendor.vendor =  :id AND mapIndVendor.IS_ACTIVE = true AND  individualTickeMap.STATUS = 'Pending' ", {
                id: req.params.id,
            })
            .getCount();

        data.push(pending);

        const completed = await getRepository(MapIndTicketVendor)
            .createQueryBuilder("mapIndVendor")
            .leftJoinAndSelect("mapIndVendor.indTickets", "individualTickeMap", "individualTickeMap.IND_TICKET_ID = mapIndVendor.indTickets")
            .leftJoinAndSelect("individualTickeMap.serviceId", "indService", "indService.IND_SERVICE_ID = individualTickeMap.serviceId")
            .leftJoinAndSelect("individualTickeMap.association", "indAssociation", "indAssociation.id = individualTickeMap.association")
            .leftJoinAndSelect("individualTickeMap.indTicketOtp", "indTicketOtpMap", "indTicketOtpMap.indTickets = mapIndVendor.indTickets")
            .where("mapIndVendor.vendor =  :id AND mapIndVendor.IS_ACTIVE = true AND  individualTickeMap.STATUS = 'Completed' ", {
                id: req.params.id,
            })
            .getCount();
        data.push(completed);

        res.send(data);

        //  const mapIndVendorRep = await getRepository(MapIndTicketVendor);

    }

    static getindTicketsByMonthAll = async (req: Request, res: Response, next) => {

        var crDate = new Date();
        var year = crDate.getFullYear();
        const monthLabels = await getRepository(IndividualTickets).query("SELECT COUNT(o.`IND_TICKET_ID`) AS code_count FROM cal_month m LEFT JOIN individual_service_tickets  o ON m.`index` = MONTH(o.`CREATED_DATE`)   AND  YEAR(o.CREATED_DATE)= '" + req.params.year + "' GROUP BY  m.`name` ORDER BY m.index ASC  ");
        res.json({
            monthLabels: monthLabels,
            year: year
        });


        //res.send(monthLabels)
    }
    static getindTicketsByMonthVendorAssociation = async (req: Request, res: Response, next) => {
        console.log("indTicketx")
        var crDate = new Date();
        var year = crDate.getFullYear();
        const monthLabels = await getRepository(IndividualTickets).query("SELECT COUNT(o.`IND_TICKET_ID`) AS code_count,m.index AS indexpo FROM cal_month m LEFT JOIN individual_service_tickets  o ON m.`index` = MONTH(o.`CREATED_DATE`) AND(YEAR(o.CREATED_DATE)= '" + req.params.year + "')   INNER JOIN map_ind_tickets_vendors  ma ON ma.`IND_TICKET_ID` = o.`IND_TICKET_ID` AND (ma.`IS_ACTIVE` = TRUE ) INNER JOIN vendor  ven ON  ma.`VENDOR_ID`  = ven.`VENDOR_ID`AND(ven.`ASSOCIATION_ID` = '" + req.params.id + "') GROUP BY  m.`name` ORDER BY m.index ASC ");
        console.log(monthLabels)

        res.send(monthLabels)
    }

    static getindTicketsByMonth = async (req: Request, res: Response, next) => {

        console.log("indTicket")
        var crDate = new Date();
        var year = crDate.getFullYear();
        const monthLabels = await getRepository(IndividualTickets).query("SELECT COUNT(o.`IND_TICKET_ID`) AS code_count FROM cal_month m LEFT JOIN individual_service_tickets  o ON m.`index` = MONTH(o.`CREATED_DATE`)  AND o.`ASSOCIATION_ID` = '" + req.params.id + "' AND  YEAR(o.CREATED_DATE)= '" + req.params.year + "' GROUP BY  m.`name` ORDER BY m.index ASC  ");
        res.json({
            monthLabels: monthLabels,
            year: year
        });
        // .then((data)=>{
        //     res.json()
        // }).catch((error)=>{
        //     res.status(500).send(error)
        // })

    }

    static getindTicketsByAssociation = async (req: Request, res: Response, next) => {

        const pending = await getRepository(IndividualTickets).query("SELECT * FROM individual_service_tickets WHERE individual_service_tickets.`ASSOCIATION_ID` = '" + req.params.id + "'  AND individual_service_tickets.STATUS = 'Pending'");
        const completed = await getRepository(IndividualTickets).query("SELECT * FROM individual_service_tickets WHERE individual_service_tickets.`ASSOCIATION_ID` = '" + req.params.id + "'  AND individual_service_tickets.STATUS = 'Completed'");


        res.json({
            completed: completed.length,
            pending: pending.length
        });
        // .then((data)=>{
        //     res.json()
        // }).catch((error)=>{
        //     res.status(500).send(error)
        // })

    }


    static vendorIndividualTicketsCompleted = async (req: Request, res: Response, next) => {
        const prioryVendor = await getRepository(MapIndTicketVendor)
            .createQueryBuilder("mapIndVendor")
            .leftJoinAndSelect("mapIndVendor.indTickets", "individualTickeMap", "individualTickeMap.IND_TICKET_ID = mapIndVendor.indTickets")
            .leftJoinAndSelect("individualTickeMap.serviceId", "indService", "indService.IND_SERVICE_ID = individualTickeMap.serviceId")
            .leftJoinAndSelect("individualTickeMap.association", "indAssociation", "indAssociation.id = individualTickeMap.association")
            .leftJoinAndSelect("individualTickeMap.indTicketOtp", "indTicketOtpMap", "indTicketOtpMap.indTickets = mapIndVendor.indTickets")
            .where("mapIndVendor.vendor =  :id AND mapIndVendor.IS_ACTIVE = true AND  individualTickeMap.STATUS = 'Completed' ", {
                id: req.params.id,
            })
            .getMany();
        res.send(prioryVendor);

        //  const mapIndVendorRep = await getRepository(MapIndTicketVendor);

    }
    static appUserIndividualTickets = async (req: Request, res: Response, next) => {
        console.log(req.params.id);
        const indTicketVendor = await getRepository(IndividualTickets)
        const indTicketObj = await indTicketVendor.find({
            where: {
                appUserDetails: req.params.id
            },
            relations: ['indTicketOtp', 'mapIndVendor', 'indServiceTypes', 'indSerPlan', 'indAddtionAmount'],
            order: {
                IND_TICKET_ID: "DESC"
            }
        })

        res.send(indTicketObj);

        //  const mapIndVendorRep = await getRepository(MapIndTicketVendor);

    }

    static vendorIndActionAccept = async (req: Request, res: Response, next) => {
        const mapIndTicketVendorRep = await getRepository(MapIndTicketVendor);
        const mapIndTicketVendor = await getRepository(MapIndTicketVendor)
            .createQueryBuilder("mapIndVendor")
            .where("mapIndVendor.vendor =  :vendorid AND mapIndVendor.indTickets = :ticketId", {
                vendorid: req.params.vendorID,
                ticketId: req.params.id,
            })
            .getOne();

        mapIndTicketVendor.IS_ACCEPTED = true;

        mapIndTicketVendorRep.save(mapIndTicketVendor).then((data) => {
            res.send(data)
        }).catch((error) => {
            res.send(error)
        })

    }
    static vendorIndActionReject = async (req: Request, res: Response, next) => {

        const appUserDetailRep = getRepository(AppUserDetails);

        const indTicketRepository = getRepository(IndividualTickets);

        const flatOwnersRep = getRepository(FlatOwners);
        const flatTenantsRep = getRepository(FlatTenants);
        const vendorRep = await getRepository(VENDOR);
   

       const indOpObj = await indTicketRepository.findOne({
            where : {
                IND_TICKET_ID : req.body.indTicketId
            },

            relations: ["appUserDetails","association","appUserDetails.flatOwner","appUserDetails.flatTenant","serviceId","indSerPlan","indServiceTypes"]
        })


        const appUser = await appUserDetailRep.findOne(

            {
                where: {
                    APP_USER_ID: indOpObj.appUserDetails.APP_USER_ID
                },
                relations: ["flatOwner", "flatTenant"]
            }

            // req.body.appUserId
        );



        var email;
        req.body.associationName = indOpObj.association.ASSOCIATION_NAME;
        req.body.associationEmail = indOpObj.association.EMAIL;
        if (appUser.flatOwner) {
            console.log("flatOwner")
            var flatOwnerId = appUser.flatOwner.FLAT_OWNER_ID;
            const flatOwnerObj = await flatOwnersRep.findOne({
                where : {
                    FLAT_OWNER_ID  : flatOwnerId
                },
                relations : ["associationChildEntity","associationGrandChildEntity"]
            });
            email = flatOwnerObj.OWNER_EMAIL;
            req.body.appUserEmail = email;
            req.body.appUserName =   flatOwnerObj.OWNER_FIRST_NAME ;
            req.body.appUserChildName =   flatOwnerObj.associationChildEntity.CHILD_NAME ;
            req.body.appUserGrandchild =  flatOwnerObj.associationGrandChildEntity.GARND_CHILD_NAME;

        }

        if (appUser.flatTenant) {
            console.log("flatTenant")
            var flatTenantId = appUser.flatTenant.TENANT_ID;
            const flatTenantObj = await flatTenantsRep.findOne({
                where : {
                    TENANT_ID : flatTenantId
                },
                relations : ["associationChildEntity","associationGrandChildEntity"]
            });
            email = flatTenantObj.TENANT_EMAIL;
            req.body.appUserEmail = email;
            req.body.appUserName =   flatTenantObj.TENANT_FIRST_NAME ;
            req.body.appUserChildName =   flatTenantObj.associationChildEntity.CHILD_NAME ;
            req.body.appUserGrandchild =  flatTenantObj.associationGrandChildEntity.GARND_CHILD_NAME;

            

        }

        req.body.appUserserviceName = indOpObj.serviceId.IND_SERVICE_NAME;
        req.body.appUserserviceTypeName = indOpObj.indServiceTypes.TYPE;
        req.body.appUserQuantity = indOpObj.QUANTITY;
        req.body.appUserPlan = indOpObj.indSerPlan.PLAN;
        req.body.service_types_id = indOpObj.indServiceTypes.IND_SERVICE_TYPE_ID;
        req.body.quantity = indOpObj.QUANTITY;
        const mapIndTicketVendorRep = await getRepository(MapIndTicketVendor);
        const mapIndTicketVendor = await getRepository(MapIndTicketVendor)
            .createQueryBuilder("mapIndVendor")
            .where("mapIndVendor.vendor =  :vendorid AND mapIndVendor.indTickets = :ticketId", {
                //  vendorid:   req.params.vendorID,
                // ticketId:   req.params.id,
                vendorid: req.body.vendorId,
                ticketId: req.body.indTicketId,
            })
            .getOne();

        console.log(mapIndTicketVendor);
        mapIndTicketVendor.IS_ACTIVE = false;
        mapIndTicketVendor.IS_ACCEPTED = false;
        mapIndTicketVendor.IS_REJECTED = true;
        mapIndTicketVendorRep.save(mapIndTicketVendor).then(async (data) => {


const vendorObj = await vendorRep.findOne(req.body.vendorId);

var appUserDetails = req.body.appUserName+'-'+req.body.appUserChildName+'-'+ req.body.appUserGrandchild; 

vendorSendMail.vendoIndRejectSendMail(req.body.indTicketId, req.body.description,   vendorObj.VENDOR_EMAIL, req.body.ticket_date, req.body.ticket_time,   req.body.appUserserviceName, req.body.appUserserviceTypeName,req.body.appUserQuantity ,indOpObj.AMOUNT,req.body.appUserPlan,req.body.associationName,  appUserDetails)
OwnerIndTicketSendMail.IndTicketRejectSendMail(req.body.indTicketId, req.body.description,   req.body.appUserEmail, req.body.ticket_date, req.body.ticket_time,   req.body.appUserserviceName, req.body.appUserserviceTypeName,req.body.appUserQuantity ,indOpObj.AMOUNT,req.body.appUserPlan,vendorObj.VENDOR_NAME);

            req.body.data = data;
            next()
            // res.send(data)
        }).catch((error) => {
            res.send(error)
        })

    }


    static vendorWorkCompleted = async (req: Request, res: Response, next) => {
        //     const mapIndTicketOtpVendor = await getRepository(MapIndTicketVendor);
        //    const mapVendorRep = await  mapIndTicketOtpVendor.findOne(req.params.id);
        //    mapIndTicketOtpVendor.save(mapVendorRep).then((data)=>{
        //        res.send(data);
        //    }).catch((error)=>{
        //     res.status(500).send(error);
        //    })

        const mapIndTicketOtpVendorRep = await getRepository(INDIVIDUAL_TICKET_OTP);
        const mapIndTicketOtpVendor = await getRepository(INDIVIDUAL_TICKET_OTP)
            .createQueryBuilder("indOtp")
            .where("indOtp.indTickets = :ticketId   AND indOtp.OTP_DATE =:otpDate && indOtp.IS_ACTIVE = true && indOtp.IS_VERIFIED = true", {
                ticketId: req.body.indTicketId,
                otpDate: req.body.otpDate,


            })
            .getOne();

        if (mapIndTicketOtpVendor) {
            mapIndTicketOtpVendor.IS_WORK_COMPLETED = true;
            mapIndTicketOtpVendorRep.save(mapIndTicketOtpVendor).then((data) => {
                res.send(true)
            }).catch((error) => {

                console.log(error)
                res.send(null)
            })
            //  res.send(mapIndTicketOtpVendor);  
        }
        else {
            console.log('sdjkf')
            res.send(null);
        }


    }

    static mailTest = async (req: Request, res: Response, next) => {
        OwnerIndTicketSendMail.IndTicketSendMail(1, "asdasdasd",   "sharukh.smart.9@gmail.com", "22-23-1255", "34",   "dasdasda", "serviceTyooe","1" ,"234","3313","vendorName");
        res.send("ok")
    }

    static getTicketDetails = async (req: Request, res: Response, next) => {

        const pending = await getRepository(IndividualTickets).query("SELECT COUNT(*) AS pendingCount FROM individual_service_tickets WHERE individual_service_tickets.`STATUS` = 'Pending'")
        const completed = await getRepository(IndividualTickets).query("SELECT COUNT(*) AS completedCount FROM individual_service_tickets WHERE individual_service_tickets.`STATUS` = 'Completed'")

        res.json({
            pending: pending,
            completed: completed
        })
    }


    static getIndivdulaTIcketByVendorAssociation = async (req: Request, res: Response, next) => {

        const pending = await getRepository(IndividualTickets).query("SELECT COUNT(*) AS pendingCount FROM individual_service_tickets INNER JOIN map_ind_tickets_vendors ON `map_ind_tickets_vendors`.`IND_TICKET_ID` =individual_service_tickets.`IND_TICKET_ID` INNER JOIN vendor ON  `vendor`.`VENDOR_ID` = map_ind_tickets_vendors.`VENDOR_ID` WHERE map_ind_tickets_vendors.`IS_ACTIVE` = TRUE AND vendor.`VENDOR_ASSOCIATION_ID` = " + req.params.id + " AND individual_service_tickets.`STATUS` = 'Pending'")
        const completed = await getRepository(IndividualTickets).query("SELECT COUNT(*) AS completeCount FROM individual_service_tickets INNER JOIN map_ind_tickets_vendors ON `map_ind_tickets_vendors`.`IND_TICKET_ID` =individual_service_tickets.`IND_TICKET_ID` INNER JOIN vendor ON  `vendor`.`VENDOR_ID` = map_ind_tickets_vendors.`VENDOR_ID` WHERE map_ind_tickets_vendors.`IS_ACTIVE` = TRUE AND vendor.`VENDOR_ASSOCIATION_ID` = " + req.params.id + " AND individual_service_tickets.`STATUS` = 'Completed'")

        res.json({
            pending: pending,
            completed: completed
        })


    }


    static resendOtp = async (req: Request, res: Response, next) => {

        const indTicketOtp = await getRepository(INDIVIDUAL_TICKET_OTP)
            .createQueryBuilder("indTikOtp")
            .where("indTikOtp.IND_TICKET_ID = :ticketId AND indTikOtp.OTP_DATE =  :otp_date AND indTikOtp.IS_VERIFIED = true  AND indTikOtp.IS_WORK_COMPLETED = true  ", {
                ticketId: req.body.ticketId,
                otp_date: req.body.otpDate,
            }).getOne();


        console.log("indTIcketOtp")
        console.log(indTicketOtp)

        if (indTicketOtp) {

            res.json({ msg: "Work Already Completed" })

        }
        else {
            console.log("dsfsadasd")
            const indTicketRepository = getRepository(IndividualTickets);
            const indTicketOtpRep = getRepository(INDIVIDUAL_TICKET_OTP);
            var indTicketObj = await indTicketRepository.findOne(req.body.ticketId);
            var randomNumber = Math.floor(100000 + Math.random() * 900000) + '';
            const newIndTicketOtp = new INDIVIDUAL_TICKET_OTP();

            newIndTicketOtp.OTP_DATE = req.body.otpDate;
            newIndTicketOtp.OTP_NUMBER = randomNumber;
            newIndTicketOtp.IS_ACTIVE = true;
            newIndTicketOtp.IS_VERIFIED = false;
            newIndTicketOtp.IS_VERIFIED = false;
            newIndTicketOtp.IS_USER_VERIFIED = false;
            newIndTicketOtp.IS_WORK_COMPLETED = false;
            newIndTicketOtp.indTickets = indTicketObj;

            const indTIcketObj = await indTicketOtpRep.save(newIndTicketOtp).then((data) => {
                console.log("oksdfs")
                console.log(data)

                res.send(data)
            }).catch((error) => {

                console.log(error)
                res.status(500).send(error)
            })
        }


    }

    static ownerWorkCompleted = async (req: Request, res: Response, next) => {

        //     const mapIndTicketOtpVendor = await getRepository(MapIndTicketVendor);
        //     const indTickets = await getRepository(IndividualTickets);
        const indTicketsRep = await getRepository(IndividualTickets);
        const plansRep = await getRepository(Plans);
        const indTickets = await getRepository(IndividualTickets);
        const mapIndTicketOtpVendorRep = await getRepository(INDIVIDUAL_TICKET_OTP);



        const IndticketObj = await indTicketsRep.findOne(req.body.ticketId);
        //    const mapVendorRep = await  mapIndTicketOtpVendor.findOne(req.params.id);
        //    const IndticketObj = await  indTicketsRep.findOne(req.params.ticketId);

        //    IndticketObj.STATUS = "Completed";
        //   indTickets.save(IndticketObj).then((indObj)=>{

        //     mapIndTicketOtpVendor.save(mapVendorRep).then((data)=>{
        //         res.send(data);
        //     }).catch((error)=>{
        //      res.status(500).send(error);
        //     })
        //   })

        if (req.body.plan == 'Monthly') {

            const mapIndTicketOtpVendor = await getRepository(INDIVIDUAL_TICKET_OTP)
                .createQueryBuilder("indOtp")
                .where("indOtp.indTickets = :ticketId   AND indOtp.OTP_DATE =:otpDate && indOtp.IS_ACTIVE = true AND indOtp.IS_VERIFIED = true AND  indOtp.IS_WORK_COMPLETED = true", {
                    ticketId: req.body.ticketId,
                    otpDate: req.body.otpDate,
                })
                .getOne();
            console.log(":akjdjka")
            console.log(mapIndTicketOtpVendor)

            if (mapIndTicketOtpVendor) {
                mapIndTicketOtpVendor.IS_USER_VERIFIED = true;
                mapIndTicketOtpVendorRep.save(mapIndTicketOtpVendor).then(async (data) => {



                    const indServicePlan = await getRepository(IND_SERVICE_PLANS)
                        .createQueryBuilder("servPlan")
                        .where("servPlan.PLAN = :plan", {
                            plan: req.body.plan,
                        })
                        .getOne();

                    var days = indServicePlan.DAYS;

                    const count = await getRepository(INDIVIDUAL_TICKET_OTP)
                        .createQueryBuilder("indOtp")
                        .where("indOtp.indTickets = :ticketId AND   indOtp.IS_WORK_COMPLETED = true", {
                            ticketId: req.body.ticketId
                        })
                        .getCount();

                    console.log(count + "+ console.log(count)");

                    if (count >= days) {
                        console.log("plan Over")

                        IndticketObj.STATUS = "Completed";
                        indTickets.save(IndticketObj).then((indObj) => {

                        })

                    }

                    res.send(true)
                }).catch((error) => {

                    console.log(error)
                    res.send(null)
                })
                //  res.send(mapIndTicketOtpVendor);  
            }
            else {
                console.log('sdjkf')
                res.send(null);
            }









        }
        else {

            const mapIndTicketOtpVendor = await getRepository(INDIVIDUAL_TICKET_OTP)
                .createQueryBuilder("indOtp")
                .where("indOtp.indTickets = :ticketId   AND indOtp.OTP_DATE =:otpDate && indOtp.IS_ACTIVE = true AND indOtp.IS_VERIFIED = true AND  indOtp.IS_WORK_COMPLETED = true", {
                    ticketId: req.body.ticketId,
                    otpDate: req.body.otpDate,
                })
                .getOne();


            if (mapIndTicketOtpVendor) {
                mapIndTicketOtpVendor.IS_USER_VERIFIED = true;
                mapIndTicketOtpVendorRep.save(mapIndTicketOtpVendor).then((data) => {




                    IndticketObj.STATUS = "Completed";
                    indTickets.save(IndticketObj).then((indObj) => {

                    })

                    res.send(true)
                }).catch((error) => {

                    console.log(error)
                    res.send(null)
                })
                //  res.send(mapIndTicketOtpVendor);  
            }
            else {
                console.log('sdjkf')
                res.send(null);
            }



        }


    }

    static indTicketsOtpVerify = async (req: Request, res: Response, next) => {

        const mapIndTicketOtpVendorRep = await getRepository(INDIVIDUAL_TICKET_OTP);
        const mapIndTicketOtpVendor = await getRepository(INDIVIDUAL_TICKET_OTP)
            .createQueryBuilder("indOtp")
            .where("indOtp.indTickets = :ticketId  AND indOtp.OTP_NUMBER =:otpNumber AND indOtp.OTP_DATE =:otpDate && indOtp.IS_ACTIVE = true && indOtp.IS_VERIFIED = false", {
                vendorid: req.body.vendorID,
                ticketId: req.body.ticketid,
                otpDate: req.body.ticketDate,
                otpNumber: req.body.otpNumber

            })
            .getOne();

        if (mapIndTicketOtpVendor) {
            mapIndTicketOtpVendor.IS_VERIFIED = true;
            mapIndTicketOtpVendorRep.save(mapIndTicketOtpVendor).then((data) => {
                res.send(true)
            }).catch((error) => {

                console.log(error)
                res.send(null)
            })
            //  res.send(mapIndTicketOtpVendor);  
        }
        else {
            console.log('sdjkf')
            res.send(null);
        }





    }

    static indTicketAssignTeritaryVendor = async (req: Request, res: Response, next) => {
        const mapIndVendorRep = await getRepository(MapIndTicketVendor);
        const vendorRep = await getRepository(VENDOR);
        const individualTicketRep = await getRepository(IndividualTickets);
        var date = "2020-03-23T" + req.body.ticket_time + ".573Z";
        console.log(date);
        var postTime = getPostTIme(date);
        var preTime = getPreTime(date);
        //checking priority 1 
        const prioryOneVendor = await getRepository(VENDOR)
            .createQueryBuilder("vendor")
            .leftJoinAndSelect("vendor.assVendors", "assVendor", "assVendor.vendorInfo = vendor.id")
            .leftJoinAndSelect("vendor.vensServices", "venService", "venService.vendorInfo = vendor.id")
            .leftJoinAndSelect("vendor.mapVendor", "mapVendors", "mapVendors.vendor = vendor.id")
            .leftJoinAndSelect("mapVendors.indTickets", "indTicket", "indTicket.IND_TICKET_ID = mapVendors.IND_TICKET_ID")
            .where("assVendor.ASSOCIATION_ID =  :id AND venService.IND_SERVICE_ID = :service    AND indTicket.TICKET_DATE = :ticket_date  AND (indTicket.TICKET_TIME  BETWEEN '" + preTime + "' AND '" + postTime + "')", {
                id: req.body.associationId,
                service: req.body.serviceId,
                ticket_date: req.body.ticket_date,
            })
            .getOne();

        if (prioryOneVendor) {

            next();

            //res.send(prioryOneVendor);
        }
        else {
            const prioryVendor = await getRepository(VENDOR)
                .createQueryBuilder("vendor")
                .leftJoinAndSelect("vendor.assVendors", "assVendor", "assVendor.vendorInfo = vendor.id")
                .leftJoinAndSelect("vendor.vensServices", "venService", "venService.vendorInfo = vendor.id")
                .leftJoinAndSelect("vendor.mapVendor", "mapVendors", "mapVendors.vendor = vendor.id")
                .where("assVendor.ASSOCIATION_ID =  :id AND venService.IND_SERVICE_ID = :service  ", {
                    id: req.body.associationId,
                    service: req.body.serviceId,
                    //  ticket_date: req.body.ticket_date,
                })
                .getOne();
            var vendorId = prioryVendor.id;

            const vendorObj = await vendorRep.findOne(vendorId);
            const indObj = await individualTicketRep.findOne(req.body.indTicketId)

            const mapVendorObj = new MapIndTicketVendor();
            mapVendorObj.vendor = vendorObj;
            mapVendorObj.indTickets = indObj;
            mapVendorObj.IS_ACCEPTED = false;
            mapVendorObj.IS_REJECTED = false;
            mapVendorObj.IS_ACTIVE = true;
            var samapVendorObj = mapIndVendorRep.create(mapVendorObj);

            mapIndVendorRep.save(samapVendorObj).then((mapIndObj) => {
                res.send(req.body.data);
            }).catch((error) => {
                res.status(500).send(error)
            });


        }


    }


    static checkRejectionPrimaryInternalVendor = async (req: Request, res: Response, next) => {
        console.log(req.body.ticket_date);
        console.log("Debubbageble Date")
        console.log(req.body.ticket_time);
        console.log("Debubbageble time")
        const mapIndVendorRep = await getRepository(MapIndTicketVendor);
        const vendorRep = await getRepository(VENDOR);
        const individualTicketRep = await getRepository(IndividualTickets);

        var date = "2020-03-23T" + req.body.ticket_time + ".573Z";
        var postTime = getPostTIme(date);
        var preTime = getPreTime(date);
        console.log(req.body.ticket_date)
        const prioryOneVendor = await getRepository(VENDOR)
            .createQueryBuilder("vendor")
            .leftJoinAndSelect("vendor.assVendors", "assVendor", "assVendor.vendorInfo = vendor.id")
            .leftJoinAndSelect("vendor.vensServices", "venService", "venService.vendorInfo = vendor.id")
            .leftJoinAndSelect("vendor.mapVendor", "mapVendors", "mapVendors.vendor = vendor.id")
            .leftJoinAndSelect("mapVendors.indTickets", "indTicket", "indTicket.IND_TICKET_ID = mapVendors.IND_TICKET_ID")
            .leftJoinAndSelect("vendor.assVenSerPriority", "assSerPriority", "assSerPriority.vendorInfo = vendor.id")
            .where("assVendor.ASSOCIATION_ID =  :id AND venService.IND_SERVICE_ID = :service  AND assSerPriority.PRIORITY = 1  AND  assVendor.IS_INTERNAL_VENDOR = true AND indTicket.STATUS = 'Pending'   AND   indTicket.TICKET_DATE = :ticket_date AND (indTicket.TICKET_TIME   BETWEEN '" + preTime + "' AND '" + postTime + " ') ", {
                id: req.body.associationId,
                service: req.body.serviceId,
                ticket_date: req.body.ticket_date,
            }).getOne();

        if (prioryOneVendor) {
            next()
        }
        else {

            const prioryOneElVendor = await getRepository(VENDOR)
                .createQueryBuilder("vendor")
                .leftJoinAndSelect("vendor.assVendors", "assVendor", "assVendor.vendorInfo = vendor.id")
                .leftJoinAndSelect("vendor.vensServices", "venService", "venService.vendorInfo = vendor.id")
                .leftJoinAndSelect("vendor.mapVendor", "mapVendors", "mapVendors.vendor = vendor.id")
                .leftJoinAndSelect("mapVendors.indTickets", "indTicket", "indTicket.IND_TICKET_ID = mapVendors.IND_TICKET_ID")
                .leftJoinAndSelect("vendor.assVenSerPriority", "assSerPriority", "assSerPriority.vendorInfo = vendor.id")
                .where("assVendor.ASSOCIATION_ID =  :id AND venService.IND_SERVICE_ID = :service   AND assVendor.IS_INTERNAL_VENDOR = true AND   assSerPriority.PRIORITY = 1 ", {
                    id: req.body.associationId,
                    service: req.body.serviceId,
                }).getOne();

            console.log(prioryOneElVendor)
            if (prioryOneElVendor != undefined) {
                var vendorId = prioryOneElVendor.id;

                //if(vendorId){
                //res.send(prioryOneVendor);
                const vendorObj = await vendorRep.findOne(vendorId);
                const indObj = await individualTicketRep.findOne(req.body.indTicketId)
                const mapVendorObj = new MapIndTicketVendor();
                mapVendorObj.vendor = vendorObj;
                mapVendorObj.indTickets = indObj;
                mapVendorObj.IS_ACCEPTED = false;
                mapVendorObj.IS_REJECTED = false;
                mapVendorObj.IS_ACTIVE = true;
                var samapVendorObj = mapIndVendorRep.create(mapVendorObj);

                mapIndVendorRep.save(samapVendorObj).then(async (mapIndObj) => {

                    if (req.body.service_types_id) {
                        req.body.serviceId
                        const interServAmountVendor = await getRepository(INTERNAL_SERVICES_AMOUNT_VENDORS)
                            .createQueryBuilder("interServVendor")
                            .where("interServVendor.IND_SERVICE_ID =  :indServiceId AND interServVendor.SERVICE_TYPE_ID = :indServiceTypeId AND VENDOR_ID = :vendorId ", {
                                indServiceId: req.body.serviceId,
                                indServiceTypeId: req.body.service_types_id,
                                vendorId: vendorId
                            }).getOne().then(async (data) => {
                                var amount = data.AMOUNT;
                                if (req.body.quantity) {
                                    amount = amount * req.body.quantity;
                                }
                                indObj.AMOUNT = amount;
                                console.log(amount + "amount")

                                var esult = await individualTicketRep.save(indObj);
                            }).catch((error) => {
                                console.log(error)
                            });

                    }
                    else {

                        const interServAmountVendor = await getRepository(INTERNAL_SERVICES_AMOUNT_VENDORS)
                            .createQueryBuilder("interServVendor")
                            .where("interServVendor.IND_SERVICE_ID =  :indServiceId  AND VENDOR_ID = :vendorId", {
                                indServiceId: req.body.serviceId,
                                vendorId: vendorId
                            }).getOne().then(async (data) => {
                                var amount = data.AMOUNT;


                                if (req.body.quantity) {

                                    amount = amount * req.body.quantity;
                                }

                                indObj.AMOUNT = amount;

                                var esult = await individualTicketRep.save(indObj);
                            }).catch((error) => {
                                console.log(error)
                            });


                    }



                    res.send(req.body.data);
                }).catch((error) => {
                    res.status(500).send(error)
                });
            }
            else {
                next()
            }

        }
    }













}

function getPostTIme(date) {
    var dt = new Date(date);
    dt.setMinutes(dt.getMinutes() + 30).toString();
    var addedDate = dt.toISOString();
    var splittedTIme = addedDate.split('T');
    var newAddedDtime = splittedTIme[1];
    var postTimeSplit = newAddedDtime.split('.');
    var postTIme = postTimeSplit[0];
    return postTIme;
}
function getPreTime(date) {
    var dt2 = new Date(date);
    dt2.setMinutes(dt2.getMinutes() - 30).toString();
    var RemovedDate = dt2.toISOString();
    var spllited2TIme = RemovedDate.split('T');
    var newRemovbedDtime = spllited2TIme[1];
    var preTimeSplit = newRemovbedDtime.split('.');
    var preTIme = preTimeSplit[0];

    return preTIme;


}




export default IndividualTicketController;