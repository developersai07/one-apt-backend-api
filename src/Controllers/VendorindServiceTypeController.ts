import { Request, Response } from "express";
import * as jwt from "jsonwebtoken";
import { getRepository } from "typeorm";
import { IND_SER_TYPES_VENDOR_ASSOCIATION } from "../entity/IndSerTypesVendorAssociation";
import { IND_SERVICE_TYPES } from "../entity/indServiceType";
import { IndividualAreaServices } from "../entity/IndividualAreaServices";
import {  VENDORASSOCIATION} from "../entity/VendorAssociation";

class VendorindServiceTypeController {
    static addIndServiceType = async (req: Request, res: Response, next) => {
        const indServceRepVendorAss = getRepository(IND_SER_TYPES_VENDOR_ASSOCIATION);
        const indServiceRep = getRepository(IndividualAreaServices);
        const indServiceTypeRep = getRepository(IND_SERVICE_TYPES);
        const vAssociationRep = getRepository(VENDORASSOCIATION);  
      const indServiceObj = await  indServiceRep.findOne(req.body.serviceid);
      const indServiceTypeObj = await  indServiceTypeRep.findOne(req.body.serviceTypeid);
      const vendorAssociationObj = await  vAssociationRep.findOne(req.body.vendorAssociationId);
        const serType = new IND_SER_TYPES_VENDOR_ASSOCIATION();
        serType.AMOUNT = req.body.amount;
        serType.indServices = indServiceObj;
        serType.indServicetypes = indServiceTypeObj;
        serType.vassociation = vendorAssociationObj;
        serType.CREATED_BY = req.body.created_by;
        serType.UPDATED_BY = req.body.created_by;
        const serTypeObj = indServceRepVendorAss.create(serType);
        indServceRepVendorAss.save(serTypeObj).then((data) => {
           res.send(data)
        }
        ).catch((error) => {
            res.send(error)
        });
    }

    static getAll = async (req: Request, res: Response, next) => {

    }
  

}
export default VendorindServiceTypeController;