import { Request, Response } from "express";
import { getRepository } from "typeorm";
import { FamilyRelations } from "../entity/FamilyRelations";
import { AppUserDetails } from "../entity/AppUserDetails";
import { ASSOCIATION } from "../entity/Associations";
import { AssociationFamilyRelations } from "../entity/AssociationFamilyRelations";


class AssociationFamilyRelationsController {
    
    static addAssociationFamilyRelations = async( req: Request, res: Response, next ) => {
        console.log("addAssociationFamilyRelations");
        const  familyRelationRepository = getRepository(FamilyRelations);
        const appUserDetailsRepository = getRepository(AppUserDetails);
        const associationRepository = getRepository(ASSOCIATION);
        const assFamilyRelationsRepository = getRepository(AssociationFamilyRelations);

        const familyRelationObj = await familyRelationRepository.findOne(req.body.frId); 
        const appUserDetailsObj = await appUserDetailsRepository.findOne(req.body.appUserId); 
        const associationObj = await associationRepository.findOne(req.body.associationId);
        
        const assfamilyRelations = new AssociationFamilyRelations();
        assfamilyRelations.RELATIVE_NAME = req.body.relative_name;
        assfamilyRelations.PHONE_NUMBER = req.body.phone_number;
        assfamilyRelations.EMAIL_ID = req.body.email_id;
        assfamilyRelations.AGE = req.body.age;
        assfamilyRelations.CREATED_BY = req.body.created_by;
        assfamilyRelations.UPDATED_BY = req.body.created_by;
        assfamilyRelations.familyRelationsEntity = familyRelationObj;
        assfamilyRelations.appUserDetailsEntity = appUserDetailsObj;
        assfamilyRelations.associationEntity = associationObj;

        const assFamilyRelationsObj = await assFamilyRelationsRepository.create(assfamilyRelations);
        assFamilyRelationsRepository.save(assFamilyRelationsObj).then((data) => {
            req.body.data = data;
            next();
            console.log(data);
            res.send(data);

        }).catch((error) => {
            console.log(error);
            res.send(error);
        });

    }

    static addAppAssociationFamilyRelations = async (req: Request, res: Response, next) => {
        console.log("addAssociationFamilyRelations");
        const familyRelationRepository = getRepository(FamilyRelations);
        const appUserDetailsRepository = getRepository(AppUserDetails);
        const associationRepository = getRepository(ASSOCIATION);
        const assFamilyRelationsRepository = getRepository(AssociationFamilyRelations);

        const familyRelationObj = await familyRelationRepository.findOne(req.body.relation_id);
        const appUserDetailsObj = await appUserDetailsRepository.findOne(req.body.app_user_id);
        const associationObj = await associationRepository.findOne(req.body.association_id);

        const assfamilyRelations = new AssociationFamilyRelations();
        assfamilyRelations.RELATIVE_NAME = req.body.relative_name;
        assfamilyRelations.PHONE_NUMBER = req.body.phone_number;
        assfamilyRelations.EMAIL_ID = req.body.email_id;
        assfamilyRelations.AGE = req.body.age;
        assfamilyRelations.CREATED_BY = req.body.created_by;
        assfamilyRelations.UPDATED_BY = req.body.updated_by;
        assfamilyRelations.familyRelationsEntity = familyRelationObj;
        assfamilyRelations.appUserDetailsEntity = appUserDetailsObj;
        assfamilyRelations.associationEntity = associationObj;
        

        console.log('-------------------------------------------------');
        console.log(req.body.relation_id);

        console.log(assfamilyRelations);

        const assFamilyRelationsObj = await assFamilyRelationsRepository.create(assfamilyRelations);
        assFamilyRelationsRepository.save(assFamilyRelationsObj).then((data) => {
            req.body.data = data;
            next();
            console.log(data);
            res.send(data);

        }).catch((error) => {
            console.log(error);
            res.send(error);
        });

    }

    static getAllAppAssociationFamilyRelations = async (req: Request, res: Response) => {
        console.log('--------------------');
        console.log('getAllAppAssociationFamilyRelations');
        const assFamily = getRepository(AssociationFamilyRelations);
        const result = await assFamily.find({
            relations: ['familyRelationsEntity', 'appUserDetailsEntity', 'associationEntity'],
        }).then((data) => {
            console.log(data);
            res.send(data)
        }
        ).catch((error) => {
            console.log(error);
            res.send(error)
        });
    }

    static updateAppAssociationFamilyRelations = async (req: Request, res: Response) => {
        console.log("updateAppAssociationFamilyRelations");
        const familyRelationRepository = getRepository(FamilyRelations);
        const appUserDetailsRepository = getRepository(AppUserDetails);
        const associationRepository = getRepository(ASSOCIATION);
        const assFamilyRelationsRepository = getRepository(AssociationFamilyRelations);

        const familyRelationObj = await familyRelationRepository.findOne(req.body.relation_id);
        const appUserDetailsObj = await appUserDetailsRepository.findOne(req.body.app_user_id);
        const associationObj = await associationRepository.findOne(req.body.association_id);

        // const assfamilyRelations = new AssociationFamilyRelations();
        const assFamilyRelationsObj = await assFamilyRelationsRepository.findOne(req.body.id);
        assFamilyRelationsObj.RELATIVE_NAME = req.body.relative_name;
        assFamilyRelationsObj.PHONE_NUMBER = req.body.phone_number;
        assFamilyRelationsObj.EMAIL_ID = req.body.email_id;
        assFamilyRelationsObj.AGE = req.body.age;
        assFamilyRelationsObj.UPDATED_BY = req.body.updated_by;
        assFamilyRelationsObj.familyRelationsEntity = familyRelationObj;
        assFamilyRelationsObj.appUserDetailsEntity = appUserDetailsObj;
        assFamilyRelationsObj.associationEntity = associationObj;

        assFamilyRelationsRepository.save(assFamilyRelationsObj).then((data) => {
            req.body.data = data;
            // next();
            console.log(data);
            res.send(data);

        }).catch((error) => {
            console.log(error);
            res.send(error);
        });
    }

    static deleteAppAssociationFamilyRelations = async (req: Request, res: Response) => {
        console.log("delete: " + req.body.id);
        const assFamilyRelationsRepository = getRepository(AssociationFamilyRelations);
        const results = await assFamilyRelationsRepository.delete(req.body.id);
        res.send(results);
    }
}

export default AssociationFamilyRelationsController;