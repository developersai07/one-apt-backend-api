import { Request, Response } from "express";
import { getRepository } from "typeorm";
import { Roles } from "../entity/Roles";

class RolesController {

    static rolesgetall = async (req: Request, res: Response) => {
        console.log("roles");
        const rolesrep = getRepository(Roles);
        const results = await rolesrep.find().then(
            data => {
                console.log(data);
                res.send(data);
            }).catch(err => {
                console.log(err);
            });
    }

}

export default RolesController;