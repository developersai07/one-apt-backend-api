import { Request, Response } from "express";
import { getRepository } from "typeorm";
import { IndividualServiceSubscriptions } from "../entity/IndServiceSubscriptions";


class IndServiceSubscriptionController {

    static addIndServiceSubscription = async (req: Request, res: Response, next) => {
        console.log("addIndServiceSubscription");
        const indServiceSubscriptionRepository = getRepository(IndividualServiceSubscriptions);
        const subscription = new IndividualServiceSubscriptions();
        subscription.SERVICE_SUBSCRIPTION_NAME = req.body.sub_service_name;
        subscription.IS_TIMES = req.body.is_times;
        subscription.IS_ACTIVE = req.body.is_active;
        subscription.TIMES = req.body.times;
        subscription.CREATED_BY = req.body.created_by;
        subscription.UPDATED_BY = req.body.created_by;
        const subObj = indServiceSubscriptionRepository.create(subscription);
        indServiceSubscriptionRepository.save(subObj).then((data) => {
            req.body.subId = data.SERVICE_SUBSCRIPTION_ID;
            req.body.data = data;
            console.log(data);
            res.send(data);
        }).catch((error) => {
            console.log(error);
            res.send(error)
        });

    }

    static updateIndServiceSubscription = async (req: Request, res: Response) => {
        console.log("upupdateIndServiceSubscriptiondate")
        const indServiceSubscriptionRepository = getRepository(IndividualServiceSubscriptions);
        const subscription = await indServiceSubscriptionRepository.findOne(req.body.subId);
        subscription.SERVICE_SUBSCRIPTION_NAME = req.body.sub_service_name;
        subscription.IS_TIMES = req.body.is_times;
        subscription.IS_ACTIVE = req.body.is_active;
        subscription.TIMES = req.body.times;
        subscription.UPDATED_BY = req.body.last_updated_by;
        indServiceSubscriptionRepository.save(subscription).then(data => {
            console.log(data);
            res.send(data);
        }
        ).catch(err => {
            console.log(err);
        });
     }

    static deleteIndServiceSubscription = async (req: Request, res: Response) => {
        console.log("delete: " + req.body.subId);
        const indServiceSubscriptionRepository = getRepository(IndividualServiceSubscriptions);
        const results = await indServiceSubscriptionRepository.delete(req.body.subId);
        res.send(results);
    }

    static getAllIndServiceSubscription = async (req: Request, res: Response) => {
        console.log("getAllIndServiceSubscription");
        const indServiceSubscriptionRepository = getRepository(IndividualServiceSubscriptions);
        const results = await indServiceSubscriptionRepository.find({}).then(
            data => {
                res.send(data);
            }
        ).catch(err => {
            console.log(err);
        });
    }
}
export default IndServiceSubscriptionController;