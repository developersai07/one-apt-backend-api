import { Request, Response } from "express";
import * as jwt from "jsonwebtoken";
import { getRepository, AdvancedConsoleLogger } from "typeorm";
import { IndividualAreaServices } from "../entity/IndividualAreaServices";
import { IndividualServiceSubscriptions } from "../entity/IndServiceSubscriptions";
import { IndividualServiceMapSubscriptions } from "../entity/IndServiceMapSubscriptions";
import { ASSOCIATION_IND_SERVICES } from "../entity/AssociationIndServices";
import { Like } from "typeorm";
import { VendorServices } from "../entity/VendorServices";
import { ASSOCIATION_VENDORS } from "../entity/AssociationVendors";
import { ASSOCIATION_VEN_SER_PRIORITY } from '../entity/AssociationVenServPriority';
import { VENDOR } from "../entity/Vendors";
import { ASSOCIATION } from "../entity/Associations";
import { MAP_IND_SERVICE_PLANS } from '../entity/MapIndServicePlan';
import { IND_SERVICE_PLANS } from '../entity/indServicePlan';

class IndividualServiceController {

    static addService = async (req: Request, res: Response, next) => {
        const indServiceRepository = getRepository(IndividualAreaServices);
        const mapIndServicePlanRep = getRepository(MAP_IND_SERVICE_PLANS);
        const indServicePlanRep = getRepository(IND_SERVICE_PLANS);

        //chaithanya start

        indServiceRepository.find({
            where: {
                IND_SERVICE_NAME: req.body.serviceName
            }
        }).then((data) => {

        console.log(data)
            if (data.length > 0) {
                console.log(data.length);
                console.log(data);
                res.status(201).send({
                    message: "Service Already Available",
                    obj: data
                })
            } else {
                console.log("---------- else");
                const indService = new IndividualAreaServices();
                var indServicePlan = [];

                indService.IND_SERVICE_NAME = req.body.serviceName;
                indService.DESCRIPTION = req.body.description;
                indService.CREATED_BY = req.body.created_by;
                indService.UPDATED_BY = req.body.created_by;

                indServicePlan = JSON.parse("[" + req.body.subscriptionsPlan + "]");
                console.log(indServicePlan.length);


                const indSObj = indServiceRepository.create(indService);
                indServiceRepository.save(indSObj).then(async (data) => {

                    var indServiceId = data.IND_SERVICE_ID;
                    var indServviceObj = await indServiceRepository.findOne(indServiceId)

                    for (var i = 0; i < indServicePlan.length; i++) {
                        var planId = indServicePlan[i];
                        console.log(planId + "planId")
                        var indSerPlanRep = await indServicePlanRep.findOne(planId);
                        var map_ind_servicePlan = new MAP_IND_SERVICE_PLANS();
                        map_ind_servicePlan.indServices = indServviceObj;
                        map_ind_servicePlan.indServicesPlan = indSerPlanRep;
                        var mapIndServicePlan = await mapIndServicePlanRep.create(map_ind_servicePlan);
                        var mapIndServicePlanOnj = await mapIndServicePlanRep.save(mapIndServicePlan).then(data => {
                            console.log(data)
                        }).catch((error) => {
                            console.log(error)
                        })

                    }

                    req.body.indServiceId = data.IND_SERVICE_ID;
                    req.body.data = data;

                    next();
                    // res.send(data)
                }
                ).catch((error) => {
                    res.send(error)
                });
            }
        }).catch((error) => {
            res.send(error);
        })


    }

    static updateFileName = async (req: Request, res: Response, next) => {
        const indServiceRepository = getRepository(IndividualAreaServices);
        const indService = await indServiceRepository.findOne(req.body.indServiceId);
        if (req.body.ind_image_name) {
            indService.IND_IMAGE_NAME = req.body.ind_image_name;
        }
        if (req.body.ind_icon_name) {
            indService.IND_ICON_NAME = req.body.ind_icon_name;
        }


        indServiceRepository.save(indService).then(data => {
            res.send(data);
        }).catch((error) => {
            res.status(500).send(error)
        });
    }
    static getAllService = async (req: Request, res: Response) => {
        const indServiceRepository = getRepository(IndividualAreaServices);
        const results = await indServiceRepository.find({}).then(
            data => {
                res.status(200).send(data);
            }
        ).catch(err => {
            res.send(err);
        });
    }

    static getAllIndService = async (req: Request, res: Response) => {
        const indServiceRepository = getRepository(IndividualAreaServices);
        const results = await indServiceRepository.find({}).then(
            data => {
                res.status(200).send(data);
            }
        ).catch(err => {
            res.send(err);
        });
    }
    static getAllIndServiceAssociation = async (req: Request, res: Response) => {
        const associationindServiceRepository = getRepository(ASSOCIATION_IND_SERVICES);
        const results = await associationindServiceRepository.find({
            relations: ['indServices']
        }).then(
            data => {
                res.status(200).send(data);
            }
        ).catch(err => {
            res.send(err);
        });
    }

    static associationVendorByServiceInternal = async (req: Request, res: Response) => {
        const vendorRep = await getRepository(VENDOR).query("SELECT *, vendor.VENDOR_ID AS vendorId,association_ven_ind_ser_priority.PRIORITY AS prior  FROM vendor  INNER JOIN association_vendors ON association_vendors.ASSOCIATION_ID = vendor.ASSOCIATION_ID LEFT JOIN association_ven_ind_ser_priority ON association_ven_ind_ser_priority.VENDOR_ID = vendor.VENDOR_ID INNER JOIN vendor_services ON vendor_services.vendor_id = vendor.VENDOR_ID  WHERE association_vendors.ASSOCIATION_ID = " + req.params.associationId + " AND association_vendors.IS_INTERNAL_VENDOR = TRUE AND vendor_services.IND_SERVICE_ID = " + req.params.serviceId + "  OR ( association_ven_ind_ser_priority.ASSOCIATION_ID = " + req.params.associationId + " AND association_ven_ind_ser_priority.IND_SERVICE_ID = " + req.params.serviceId + " ) GROUP BY vendorId")
        res.send(vendorRep);
    }

    static associationVendorByServiceThirdparty = async (req: Request, res: Response) => {

        // const vendorRep = await getRepository(VENDOR).query("SELECT *,vendor.VENDOR_ID as vendorId  FROM vendor  INNER JOIN association_vendors ON association_vendors.VENDOR_ASSOCIATION_ID = vendor.VENDOR_ASSOCIATION_ID INNER JOIN vendor_association ON vendor_association.VENDOR_ASSOCIATION_ID = vendor.VENDOR_ASSOCIATION_ID   INNER JOIN vendor_services ON vendor_services.vendor_id = vendor.VENDOR_ID     WHERE association_vendors.ASSOCIATION_ID = "+req.params.associationId+" AND association_vendors.IS_THIRD_PARTY_ASSOCIATION = true AND vendor_services.IND_SERVICE_ID = "+req.params.serviceId+"" )
        const vendorRep = await getRepository(VENDOR).query("SELECT *, vendor.VENDOR_ID AS vendorId,association_ven_ind_ser_priority.PRIORITY AS prior  FROM vendor  INNER JOIN association_vendors ON association_vendors.VENDOR_ASSOCIATION_ID = vendor.VENDOR_ASSOCIATION_ID INNER JOIN vendor_association ON vendor_association.VENDOR_ASSOCIATION_ID = vendor.VENDOR_ASSOCIATION_ID LEFT JOIN association_ven_ind_ser_priority ON association_ven_ind_ser_priority.VENDOR_ID = vendor.VENDOR_ID INNER JOIN vendor_services ON vendor_services.vendor_id = vendor.VENDOR_ID   WHERE association_vendors.ASSOCIATION_ID = " + req.params.associationId + " AND association_vendors.IS_THIRD_PARTY_ASSOCIATION = TRUE AND vendor_services.IND_SERVICE_ID = " + req.params.serviceId + "  OR ( association_ven_ind_ser_priority.ASSOCIATION_ID = " + req.params.associationId + " AND association_ven_ind_ser_priority.IND_SERVICE_ID = " + req.params.serviceId + " ) GROUP BY vendorId")
        res.send(vendorRep);

    }




    static associationVendorPriorityByServiceExternal = async (req: Request, res: Response) => {
        const assVendorserPr = getRepository(ASSOCIATION_VEN_SER_PRIORITY);
        const vendorRep = getRepository(VENDOR);
        const associationRep = getRepository(ASSOCIATION);
        const indServiceRep = getRepository(IndividualAreaServices);
        const indServiceObj = await indServiceRep.findOne(req.body.serviceId);
        const associationObj = await associationRep.findOne(req.body.associationId);
        const vendorObj = await vendorRep.findOne(req.body.vendorId);
        const venIndSupirioty = await getRepository(VENDOR).query("SELECT * FROM vendor LEFT JOIN association_vendors ON   association_vendors.`VENDOR_ASSOCIATION_ID` = vendor.`VENDOR_ASSOCIATION_ID` INNER JOIN vendor_services ON vendor_services.vendor_id = vendor.VENDOR_ID  LEFT JOIN association_ven_ind_ser_priority ON   association_ven_ind_ser_priority.`VENDOR_ID` = vendor.`VENDOR_ID` WHERE association_vendors.`ASSOCIATION_ID` = '" + req.body.associationId + "' AND vendor_services.IND_SERVICE_ID = '" + req.body.serviceId + "' AND association_ven_ind_ser_priority.`PRIORITY` = '" + req.body.priority + "'  AND association_ven_ind_ser_priority.`ASSOCIATION_ID` = '" + req.body.associationId + "'")
        if (venIndSupirioty.length > 0) {
            res.status(200).json({
                msg: "priority is already assigned"
            })
        }
        else {

            const vendorOpObj = await getRepository(ASSOCIATION_VEN_SER_PRIORITY)
.createQueryBuilder("assVendSuper")
.where("assVendSuper.ASSOCIATION_ID =  :id AND assVendSuper.IND_SERVICE_ID = :service   AND assVendSuper.VENDOR_ID = :vendorId ", {
    id: req.body.associationId,
    service: req.body.serviceId,
    vendorId : vendorObj.id
}).getOne();
console.log("sadadad")
console.log(vendorOpObj)

if(vendorOpObj){
console.log('if')
    vendorOpObj.PRIORITY =  req.body.priority;
    const assVendorSupObj = await assVendorserPr.save(vendorOpObj);

    res.send(vendorOpObj)
}
else {


    const assVendorSurPr = new ASSOCIATION_VEN_SER_PRIORITY();
    assVendorSurPr.CREATED_BY = req.body.created_by;
    assVendorSurPr.UPDATED_BY = req.body.created_by;
    assVendorSurPr.association = associationObj;
    assVendorSurPr.indService = indServiceObj;
    assVendorSurPr.vendorInfo = vendorObj;
    assVendorSurPr.PRIORITY = req.body.priority;
    assVendorSurPr.CREATED_DATE = new Date();
    assVendorSurPr.UPDATED_DATE = new Date();
    const assVendorSupObj = await assVendorserPr.create(assVendorSurPr);
    assVendorserPr.save(assVendorSupObj).then((data) => {
        res.send(data)
    }).catch((error) => {
        res.status(500).send(error)
    });
}


}


    }

    static associationVendorPriorityByService = async (req: Request, res: Response) => {
        const assVendorserPr = getRepository(ASSOCIATION_VEN_SER_PRIORITY);
        const vendorRep = getRepository(VENDOR);
        const associationRep = getRepository(ASSOCIATION);
        const indServiceRep = getRepository(IndividualAreaServices);
        const indServiceObj = await indServiceRep.findOne(req.body.serviceId);
        const associationObj = await associationRep.findOne(req.body.associationId);
        const vendorObj = await vendorRep.findOne(req.body.vendorId);
        const venIndSupirioty = await getRepository(VENDOR)
            .createQueryBuilder("vendor")
            .leftJoinAndSelect("vendor.assVendors", "assVendor", "assVendor.vendorInfo = vendor.id")
            .leftJoinAndSelect("vendor.vensServices", "venService", "venService.vendorInfo = vendor.id")
            .leftJoinAndSelect("vendor.mapVendor", "mapVendors", "mapVendors.vendor = vendor.id")
            .leftJoinAndSelect("mapVendors.indTickets", "indTicket", "indTicket.IND_TICKET_ID = mapVendors.IND_TICKET_ID")
            .leftJoinAndSelect("vendor.assVenSerPriority", "assSerPriority", "assSerPriority.vendorInfo = vendor.id")
            .where("assVendor.ASSOCIATION_ID =  :id AND venService.IND_SERVICE_ID = :service   AND assVendor.IS_INTERNAL_VENDOR = true AND   assSerPriority.PRIORITY = '" + req.body.priority + "' ", {
                id: req.body.associationId,
                service: req.body.serviceId,
            }).getMany();

            console.log("req.body.priority"+req.body.priority)
            console.log("req.body.associationId"+req.body.associationId)
            console.log("req.body.service"+req.body.serviceId)
        console.log(venIndSupirioty.length + ".length ")
        if (venIndSupirioty.length > 0) {
            res.status(200).json({
                msg: "priority is already assigned"
            })


        }
        else {
console.log("new prority")

// const vendorOpObj = await getRepository(VENDOR)
// .createQueryBuilder("vendor")
// .leftJoinAndSelect("vendor.assVendors", "assVendor", "assVendor.vendorInfo = vendor.id")
// .leftJoinAndSelect("vendor.vensServices", "venService", "venService.vendorInfo = vendor.id")
// .leftJoinAndSelect("vendor.mapVendor", "mapVendors", "mapVendors.vendor = vendor.id")
// .leftJoinAndSelect("mapVendors.indTickets", "indTicket", "indTicket.IND_TICKET_ID = mapVendors.IND_TICKET_ID")
// .leftJoinAndSelect("vendor.assVenSerPriority", "assSerPriority", "assSerPriority.vendorInfo = vendor.id")
// .where("assVendor.ASSOCIATION_ID =  :id AND venService.IND_SERVICE_ID = :service   AND assVendor.IS_INTERNAL_VENDOR = true ", {
//     id: req.body.associationId,
//     service: req.body.serviceId,
// }).getOne();

const vendorOpObj = await getRepository(ASSOCIATION_VEN_SER_PRIORITY)
.createQueryBuilder("assVendSuper")
.where("assVendSuper.ASSOCIATION_ID =  :id AND assVendSuper.IND_SERVICE_ID = :service   AND assVendSuper.VENDOR_ID = :vendorId ", {
    id: req.body.associationId,
    service: req.body.serviceId,
    vendorId : vendorObj.id
}).getOne();
console.log("sadadad")
console.log(vendorOpObj)

if(vendorOpObj){
console.log('if')
    vendorOpObj.PRIORITY =  req.body.priority;
    const assVendorSupObj = await assVendorserPr.save(vendorOpObj);

    res.send(vendorOpObj)
}
else {
    console.log('fails')
//const vendorOpObj = await getRepository(ASSOCIATION_VEN_SER_PRIORITY).query()
const assVendorSurPr = new ASSOCIATION_VEN_SER_PRIORITY();
assVendorSurPr.CREATED_BY = req.body.created_by;
assVendorSurPr.UPDATED_BY = req.body.created_by;
assVendorSurPr.association = associationObj;
assVendorSurPr.indService = indServiceObj;
assVendorSurPr.vendorInfo = vendorObj;
assVendorSurPr.PRIORITY = req.body.priority;
assVendorSurPr.CREATED_DATE = new Date();
assVendorSurPr.UPDATED_DATE = new Date();



const assVendorSupObj = await assVendorserPr.create(assVendorSurPr);
assVendorserPr.save(assVendorSupObj).then((data) => {
    res.send(data)
}).catch((error) => {
    res.status(500).send(error)
});
}


        }

    }

    associationVendorPriorityByServiceExternal
    static searchService = async (req: Request, res: Response) => {
        const associationindServiceRepository = getRepository(ASSOCIATION_IND_SERVICES);
        const loadedPost = await getRepository(ASSOCIATION_IND_SERVICES)
            .createQueryBuilder("association_services")
            .leftJoinAndSelect("association_services.indServices", "indService", "indService.IND_SERVICE_ID = association_services.indServices")
            .where("IND_SERVICE_NAME like :id", { id: '%' + req.params.id + '%' })
            .getMany();
        console.log("Ojksad" + req.params.id);
        console.log(loadedPost);
        res.status(200).send(loadedPost);
    }
    static updateService = async (req: Request, res: Response, next) => {
        const indServiceRepository = getRepository(IndividualAreaServices);
        const indService = await indServiceRepository.findOne(req.body.serviceId);
        // const mergedUser = indServiceRepository.merge(user,req.body);
        var isUnit;
        if (req.body.is_unit_wise == 'true') {
            isUnit = true;
        }
        else {
            isUnit = false;
        }
        console.log(indService);
        console.log(req.body.serviceName);
        indService.IND_SERVICE_NAME = req.body.serviceName;
        indService.UPDATED_BY = req.body.last_updated_by;
        indService.DESCRIPTION = req.body.description;
        //  indService.IS_UNTI_WISE = isUnit;
        indServiceRepository.save(indService).then(data => {
            // res.send(data);
            req.body.indServiceId = data.IND_SERVICE_ID;
            next()
        }).catch((error) => {
            res.status(500).send(error)
        });
    }
    static deleteService = async (req: Request, res: Response) => {
        const indServiceRepository = getRepository(IndividualAreaServices);
        const results = await indServiceRepository.delete(req.body.id);
        res.send(results);
    }
    static addindServicePlans = async (req: Request, res: Response) => {
        const indServiceRepository = getRepository(IndividualAreaServices);
        const indServiceSubRepository = getRepository(IndividualServiceSubscriptions);
        const individualServiceMapSubscriptions = getRepository(IndividualServiceMapSubscriptions);
        const servSubObj = await indServiceSubRepository.findOne(req.body.subId);
        const IndserviceObj = await indServiceRepository.findOne(req.body.serviceId);
        const serviceMapSubscriptions = new IndividualServiceMapSubscriptions();
        serviceMapSubscriptions.IND_SERVICE_ID = IndserviceObj;
        serviceMapSubscriptions.SERVICE_SUBSCRIPTION_ID = servSubObj;
        serviceMapSubscriptions.AMOUNT = req.body.amount;
        serviceMapSubscriptions.IS_ACTIVE = true;
        serviceMapSubscriptions.CREATED_BY = req.body.created_by;
        serviceMapSubscriptions.UPDATED_BY = req.body.created_by;

        individualServiceMapSubscriptions.save(serviceMapSubscriptions).then((data) => {
            res.send(data)
        }
        ).catch((error) => {
            console.log(error);
            res.status(500).send(error)
        });
    }
    static getindServiceSubPlans = async (req: Request, res: Response) => {
        const individualServiceMapSubscriptions = getRepository(IndividualServiceMapSubscriptions);
        const servSubObj = await individualServiceMapSubscriptions.find(
            {
                relations: ['SERVICE_SUBSCRIPTION_ID', 'IND_SERVICE_ID',],
                where: {
                    IND_SERVICE_ID: req.params.id
                }
            }
        )
        res.send(servSubObj);
    }

    

    static getService = async (req: Request, res: Response) => {
        const individualAreasServices = getRepository(IndividualAreaServices);
        const servSubObj = await individualAreasServices.findOne(
            {
                relations: ['mapSub','mapSub.SERVICE_SUBSCRIPTION_ID'],
                where: {
                    IND_SERVICE_ID: req.params.id,
                    mapSub: {
                        IS_ACTIVE: true
                    }
                }
            }
        )
        res.send(servSubObj);
    }
    static getindServiceParamSubPlans = async (req: Request, res: Response) => {
        const individualAreasServices = getRepository(IndividualAreaServices);
        const servSubObj = await individualAreasServices.findOne(
            {
                relations: ['mapSub', 'mapSub.SERVICE_SUBSCRIPTION_ID'],
                where: {
                    IND_SERVICE_ID: req.params.id,
                    mapSub: {
                        IS_ACTIVE: true
                    }
                }
            }
        )
        res.send(servSubObj);
    }



    static deleteSub = async (req: Request, res: Response) => {
        const individualServiceMapSubscriptions = getRepository(IndividualServiceMapSubscriptions);
        const results = await individualServiceMapSubscriptions.delete(req.body.id);
        res.send(results);

    }
    static updateindServiceSubPlans = async (req: Request, res: Response) => {
        const indServiceRepository = getRepository(IndividualAreaServices);
        const indServiceSubRepository = getRepository(IndividualServiceSubscriptions);
        const individualServiceMapSubscriptions = getRepository(IndividualServiceMapSubscriptions);
        const indServicemap = await individualServiceMapSubscriptions.findOne(req.body.id);
        const servSubObj = await indServiceSubRepository.findOne(req.body.subId);
        const IndserviceObj = await indServiceRepository.findOne(req.body.serviceId);
        //const serviceMapSubscriptions = new IndividualServiceMapSubscriptions();
        indServicemap.IND_SERVICE_ID = IndserviceObj;
        indServicemap.SERVICE_SUBSCRIPTION_ID = servSubObj;
        indServicemap.AMOUNT = req.body.amount;
        indServicemap.IS_ACTIVE = true;
        indServicemap.UPDATED_BY = req.body.updated_by;
        console.log(indServicemap);
        individualServiceMapSubscriptions.save(indServicemap).then((data) => {
            res.send(data)
        }
        ).catch((error) => {
            console.log(error);
            res.status(500).send(error)
        });

    }
    static indServicePlans = async (req: Request, res: Response) => {
        const indServiceSubRepository = getRepository(IndividualServiceSubscriptions);
        const results = await indServiceSubRepository.find({
        });


        // const users = await connection.manager.find(User);
        res.send(results);
    }


}

export default IndividualServiceController;