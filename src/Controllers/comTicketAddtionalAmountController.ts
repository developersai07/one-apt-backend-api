import { Request, Response } from "express";
import * as jwt from "jsonwebtoken";
import { getRepository } from "typeorm";
import { COM_TICKETS_ADDITIONAL_AMOUNTS } from "../entity/ComTicketAdditionAmount";
import { CommonAreaTickets } from "../entity/CommonAreaTickets";
import { VENDOR } from "../entity/Vendors";

class comTicketAddtionalAmountController {
    static addTicketAmount = async (req: Request, res: Response) => {
        const  comTicketAdditionalAmount = getRepository(COM_TICKETS_ADDITIONAL_AMOUNTS);
        const  comTicketRep = getRepository(CommonAreaTickets);
        const  vendorRep = getRepository(VENDOR);
     const comTicketObj = await   comTicketRep.findOne(req.body.comTicketId);
     const vendorObj = await   vendorRep.findOne(req.body.vendorId);
     const comTicketAmount    = new COM_TICKETS_ADDITIONAL_AMOUNTS();
     comTicketAmount.AMOUNT = req.body.amount;
     comTicketAmount.REASON = req.body.reason;
     comTicketAmount.CREATED_BY = req.body.created_by;
     comTicketAmount.UPDATED_BY = req.body.created_by;
     comTicketAmount.comTickets =comTicketObj;
     comTicketAmount.vendor = vendorObj;

     comTicketAdditionalAmount.save(comTicketAmount).then((data)=>{
         res.send(data)
     }).catch((error)=>{

console.log(error)
         res.status(500).send(error)
     })



        
    }



}

export default comTicketAddtionalAmountController;