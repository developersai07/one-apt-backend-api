import { Request, Response } from "express";
import * as jwt from "jsonwebtoken";
import { getRepository } from "typeorm";
import { CommonAreaTickets } from "../entity/CommonAreaTickets";
import { APP_USER_COM_TICKETS } from "../entity/AppUserComTicket";
import { IND_SERVICE_PLANS } from '../entity/indServicePlan';
import { IND_SERVICE_TYPES } from '../entity/indServiceType';
import { AppUserDetails } from "../entity/AppUserDetails";
import { IndividualAreaServices } from "../entity/IndividualAreaServices";
import { CommonAreaServices } from "../entity/CommonAreaService";
import { ASSOCIATION } from "../entity/Associations";
import { MAP_ASS_COM_TICKET } from "../entity/MapAssociationComTIcket";
import { FlatOwners } from "../entity/FlatOwners";
import { MapIndTicketVendor } from '../entity/MapIndTicketsVendor'
import { VENDOR } from "../entity/Vendors";
import { MapComTicketVendor } from "../entity/MapComTIcketVendor";
import { INTERNAL_SERVICES_AMOUNT_VENDORS } from '../entity/InternalVendorServiceAmount';
import { IND_SER_TYPES_VENDOR_ASSOCIATION } from '../entity/IndSerTypesVendorAssociation';
import ownerComTicketSendMail from "../Mails/OwnerComTicketSendMail";
import vendorSendMail from "../Mails/VendorSendMail";
class CommonAreaTicketController {
    static raiseCommonTicket = async (req: Request, res: Response, next) => {

        const associationRepository = getRepository(ASSOCIATION);
        const commonAreaServiceRep = getRepository(CommonAreaServices);
        const appComTicketRep = getRepository(APP_USER_COM_TICKETS);
        const individualAreaServiceRepository = getRepository(IndividualAreaServices);
        const indServPlanRep = getRepository(IND_SERVICE_PLANS);
        const indSerTypeRep = getRepository(IND_SERVICE_TYPES);
        const appUserDetailRep = getRepository(AppUserDetails);
        const commonAreaTicketsRep = getRepository(CommonAreaTickets);
        const appUser = await appUserDetailRep.findOne(req.body.appUserId);
        const indSerTypeSub = await indSerTypeRep.findOne(req.body.service_types_id);
        const indServiceMapSub = await indServPlanRep.findOne(req.body.service_subscription_id);
        const indService = await individualAreaServiceRepository.findOne(req.body.serviceId);
        const comService = await commonAreaServiceRep.findOne(req.body.comServiceId);
        const association = await associationRepository.findOne(req.body.associationId);


        const appComTicketObj = await getRepository(CommonAreaTickets)
            .createQueryBuilder("comTicket")
            .where("comTicket.STATUS =  'Pending' AND comTicket.ASSOCIATION_ID = '" + req.body.associationId + "' AND comTicket.IND_SERVICE_ID = '" + req.body.serviceId + "' AND comTicket.COM_SERVICE_ID = '" + req.body.comServiceId + "' ").getOne();

        console.log("appComTicketObj")
        console.log(appComTicketObj)

        if (appComTicketObj) {
            console.log("if")
            const appUserComTIcket = new APP_USER_COM_TICKETS();
            appUserComTIcket.comTicket = appComTicketObj;
            appUserComTIcket.appUser = appUser;
            appUserComTIcket.IS_ACTIVE = true;
            appComTicketRep.save(appUserComTIcket).then((data) => {
                res.send(appComTicketObj)
            });

        }
        else {
            console.log("else")

            //         var spliitedDate =   req.body.ticket_date.split('T');
            //         var spliDate =spliitedDate[0];
            //         req.body.ticket_date = spliDate;
            //         var spliitedTIme =   req.body.ticket_time.split('T');
            //         var newDtime = spliitedTIme[1];
            //         var newtime =newDtime.split('.');      
            // var ticketTime =  newtime[0];
            //sdf
            var spliitedDate = req.body.ticket_date.split('T');
            var spliDate = spliitedDate[0];
            req.body.ticket_date = spliDate;
            var spliitedTIme = req.body.ticket_time.split('T');
            var newDtime = spliitedTIme[1];
            var newtime = newDtime.split('.');
            var ticketTime = newtime[0];
            const comTicketObj = new CommonAreaTickets();
            comTicketObj.TICKET_DESCRIPTION = req.body.description;
            comTicketObj.STATUS = 'Pending';
            comTicketObj.TICKET_DATE = spliDate;
            comTicketObj.TICKET_TIME = ticketTime;

            if(req.body.quantity != 'undefined'){

                comTicketObj.QUANTITY = req.body.quantity;
            }
            else {
                req.body.quantity = null
            }
        
            // comTicketObj.appUserDetails =appUser;
            comTicketObj.CREATED_BY = req.body.created_by;
            comTicketObj.UPDATED_BY = req.body.created_by;
            comTicketObj.indSerPlan = indServiceMapSub;
            comTicketObj.indServiceTypes = indSerTypeSub;
            comTicketObj.serviceId = indService;
            comTicketObj.IS_ACTIVE = true;
            comTicketObj.IS_ASSOCIATION_APPROVED = false;
            comTicketObj.comService = comService;
            comTicketObj.association = association;

            commonAreaTicketsRep.save(comTicketObj).then(async (data) => {
                const appUserComTIcket = new APP_USER_COM_TICKETS();
                appUserComTIcket.comTicket = data;
                appUserComTIcket.appUser = appUser;
                appUserComTIcket.IS_ACTIVE = true;
                const Obj = await appComTicketRep.save(appUserComTIcket);

                req.body.data = data;

                req.body.comTicketId = data.COM_TICKET_ID
                next()
                //res.send(data)
            }).catch((error) => {
                console.log(error)
                res.status(500).send(error)
            })

        }


    }
    static appUserComTicket = async (req: Request, res: Response, next) => {
        const appComTicketRep = getRepository(APP_USER_COM_TICKETS);

        appComTicketRep.find({
            where: {
                appUser: req.params.id
            },
            relations: ["comTicket", "comTicket.serviceId", "comTicket.association", "comTicket.comService", "comTicket.indSerPlan", "comTicket.indServiceTypes"],
            order: {
                APP_USER_COM_TICKET_ID: "DESC"
            }

        }).then((data) => {
            res.send(data)
        }).catch((error) => {
            res.status(500).send(error);
        })


    }
    static getcomTicketsByMonthVendorAssociation = async (req: Request, res: Response, next) => {
        console.log("indTicketx")
        var crDate = new Date();
        var year = crDate.getFullYear();
        const monthLabels = await getRepository(CommonAreaTickets).query("SELECT COUNT(o.`COM_TICKET_ID`) AS code_count,m.index AS indexpo FROM cal_month m LEFT JOIN common_area_tickets  o ON m.`index` = MONTH(o.`CREATED_DATE`) AND(YEAR(o.CREATED_DATE)= '"+req.params.year+"')   INNER JOIN map_com_tickets_vendors  ma ON ma.`COM_TICKET_ID` = o.`COM_TICKET_ID` AND (ma.`IS_ACTIVE` = TRUE ) INNER JOIN vendor  ven ON  ma.`VENDOR_ID`  = ven.`VENDOR_ID`AND(ven.`VENDOR_ASSOCIATION_ID` = '"+req.params.id+"') GROUP BY  m.`name` ORDER BY m.index ASC ");
     console.log(monthLabels)

     res.send(monthLabels)



    }
    static getcomTicketsByMonthAll = async (req: Request, res: Response, next) => {

        var crDate = new Date();
        var year = crDate.getFullYear();
        const monthLabels = await getRepository(CommonAreaTickets).query("SELECT COUNT(o.`COM_TICKET_ID`) AS code_count FROM cal_month m LEFT JOIN common_area_tickets  o ON m.`index` = MONTH(o.`CREATED_DATE`)   AND  YEAR(o.CREATED_DATE)= '"+req.params.year+"' GROUP BY  m.`name` ORDER BY m.index ASC  ");
     

        res.json({
            monthLabels : monthLabels,
            year : year
        });

    }

    static getcomTicketsByMonth = async (req: Request, res: Response, next) => {

        var crDate = new Date();
        var year = crDate.getFullYear();
        const monthLabels = await getRepository(CommonAreaTickets).query("SELECT COUNT(o.`COM_TICKET_ID`) AS code_count FROM cal_month m LEFT JOIN common_area_tickets  o ON m.`index` = MONTH(o.`CREATED_DATE`)  AND o.`ASSOCIATION_ID` = '"+req.params.id+"' AND  YEAR(o.CREATED_DATE)= '"+req.params.year+"' GROUP BY  m.`name` ORDER BY m.index ASC  ");
     

        res.json({
            monthLabels : monthLabels,
            year : year
        });

    }

    static appVendorComTicketCompleted = async (req: Request, res: Response, next) => {

        const prioryVendor = await getRepository(MapComTicketVendor)
            .createQueryBuilder("mapComVendor")
            .leftJoinAndSelect("mapComVendor.comTickets", "comTicketRep", "comTicketRep.COM_TICKET_ID = mapComVendor.comTickets")
            .leftJoinAndSelect("comTicketRep.serviceId", "indService", "indService.IND_SERVICE_ID = comTicketRep.serviceId")
            .leftJoinAndSelect("comTicketRep.association", "indAssociation", "indAssociation.id = comTicketRep.association")
            .where("mapComVendor.vendor =  :id AND mapComVendor.IS_ACTIVE = true AND  comTicketRep.STATUS = 'Completed' ", {
                id: req.params.id,
            })
            .getMany();
        res.send(prioryVendor);

    }

    static appVendorComTicket = async (req: Request, res: Response, next) => {


        const prioryVendor = await getRepository(MapComTicketVendor)
            .createQueryBuilder("mapComVendor")
            .leftJoinAndSelect("mapComVendor.comTickets", "comTicketRep", "comTicketRep.COM_TICKET_ID = mapComVendor.comTickets")
            .leftJoinAndSelect("comTicketRep.serviceId", "indService", "indService.IND_SERVICE_ID = comTicketRep.serviceId")
            .leftJoinAndSelect("comTicketRep.association", "indAssociation", "indAssociation.id = comTicketRep.association")
            .where("mapComVendor.vendor =  :id AND mapComVendor.IS_ACTIVE = true AND  comTicketRep.STATUS = 'Pending' ", {
                id: req.params.id,
            })
            .getMany();
        res.send(prioryVendor);
    }

    static appVendorPendingComTicket = async (req: Request, res: Response, next) => {

        var data = [];
        const pending = await getRepository(MapComTicketVendor)
            .createQueryBuilder("mapComVendor")
            .leftJoinAndSelect("mapComVendor.comTickets", "comTicketRep", "comTicketRep.COM_TICKET_ID = mapComVendor.comTickets")
            .leftJoinAndSelect("comTicketRep.serviceId", "indService", "indService.IND_SERVICE_ID = comTicketRep.serviceId")
            .leftJoinAndSelect("comTicketRep.association", "indAssociation", "indAssociation.id = comTicketRep.association")
            .where("mapComVendor.vendor =  :id AND mapComVendor.IS_ACTIVE = true AND  comTicketRep.STATUS = 'Pending' ", {
                id: req.params.id,
            })
            .getCount();
        data.push(pending);
        const completed = await getRepository(MapComTicketVendor)
            .createQueryBuilder("mapComVendor")
            .leftJoinAndSelect("mapComVendor.comTickets", "comTicketRep", "comTicketRep.COM_TICKET_ID = mapComVendor.comTickets")
            .leftJoinAndSelect("comTicketRep.serviceId", "indService", "indService.IND_SERVICE_ID = comTicketRep.serviceId")
            .leftJoinAndSelect("comTicketRep.association", "indAssociation", "indAssociation.id = comTicketRep.association")
            .where("mapComVendor.vendor =  :id AND mapComVendor.IS_ACTIVE = true AND  comTicketRep.STATUS = 'Completed' ", {
                id: req.params.id,
            })
            .getCount();
        data.push(completed);
        res.send(data);
    }

    static updateFileName = async (req: Request, res: Response, next) => {

        console.log("update file name")
        const comTicketRep = getRepository(CommonAreaTickets);
        const comTicket = await comTicketRep.findOne(req.body.comTicketId);
        if (req.body.comTicketImg) {
            comTicket.IMG_LOCATION = req.body.comTicketImg;
        }
        comTicketRep.save(comTicket).then(data => {
            //  next()
            res.send(data);
        }).catch((error) => {
            res.status(500).send(error)
        });
    }


    static getcomTicketsByAssociation = async (req: Request, res: Response, next) => {
        const pending = await getRepository(CommonAreaTickets).query("SELECT * FROM common_area_tickets WHERE common_area_tickets.`ASSOCIATION_ID` = '"+req.params.id+"'  AND common_area_tickets.STATUS = 'Pending'");
        const completed = await getRepository(CommonAreaTickets).query("SELECT * FROM common_area_tickets WHERE common_area_tickets.`ASSOCIATION_ID` = '"+req.params.id+"'  AND common_area_tickets.STATUS = 'Completed'");
        res.json({
            completed : completed.length,
            pending : pending.length
        });
    }

    static getCommonAreaTicketsByVendorAssociation = async (req: Request, res: Response, next) => {
        const pending = await getRepository(CommonAreaTickets).query("SELECT COUNT(*) AS pendingCount FROM `common_area_tickets` INNER JOIN map_com_tickets_vendors ON `map_com_tickets_vendors`.`COM_TICKET_ID` =common_area_tickets.`COM_TICKET_ID` INNER JOIN vendor ON  `vendor`.`VENDOR_ID` = map_com_tickets_vendors.`VENDOR_ID` WHERE map_com_tickets_vendors.`IS_ACTIVE` = TRUE AND vendor.`VENDOR_ASSOCIATION_ID` = '"+req.params.id+"' AND common_area_tickets.`STATUS` = 'Pending'");
        const completed = await getRepository(CommonAreaTickets).query("SELECT COUNT(*) AS completedCount FROM `common_area_tickets` INNER JOIN map_com_tickets_vendors ON `map_com_tickets_vendors`.`COM_TICKET_ID` =common_area_tickets.`COM_TICKET_ID` INNER JOIN vendor ON  `vendor`.`VENDOR_ID` = map_com_tickets_vendors.`VENDOR_ID` WHERE map_com_tickets_vendors.`IS_ACTIVE` = TRUE AND vendor.`VENDOR_ASSOCIATION_ID` = '"+req.params.id+"' AND common_area_tickets.`STATUS` = 'Completed'");
        res.json({
            pending : pending,
            completed : completed
        });
    }

        
    static getTicketDetails = async (req: Request, res: Response,next) => {

        const pending = await getRepository(CommonAreaTickets).query("SELECT COUNT(*) AS pendingCount FROM common_area_tickets WHERE common_area_tickets.`STATUS` = 'Pending'")
        const completed = await getRepository(CommonAreaTickets).query("SELECT COUNT(*) AS completedCount FROM common_area_tickets WHERE common_area_tickets.`STATUS` = 'Completed'")

        res.json({
            pending : pending,
            completed : completed
           })
    }

    static workCompleteVerify = async (req: Request, res: Response, next) => {

        console.log("workCompleteVerify")
        const appUserRep = getRepository(AppUserDetails);
        const comTicketRep = getRepository(CommonAreaTickets);
        const comTicket = await comTicketRep.findOne(req.params.id);
        const appUserRepObj = await appUserRep.findOne(req.params.userid);
        comTicket.STATUS = 'Completed';
        comTicket.IS_USER_VERIFIED = true;
        comTicket.vappUserDetails = appUserRepObj;
        comTicketRep.save(comTicket).then(data => {
            //  next()
            res.send(data);
        }).catch((error) => {
            res.status(500).send(error)
        });
    }

    static getComTicketByAsscoiation = async (req: Request, res: Response, next) => {
        const comTicketRep = getRepository(CommonAreaTickets);

        comTicketRep.find({
            where: {
                association: req.params.associationId,
            },
            relations: ['association', 'serviceId', 'indSerPlan', 'indServiceTypes', 'mapAssCom', 'mapAssCom.appUser']
        }).then((data) => {
            res.send(data)
        }).catch((error) => {
            res.status(500).send(error)
        })

    }


    static mapCommonVendorAction = async (req: Request, res: Response, next) => {
        const mapcomTicketRep = getRepository(MapComTicketVendor);
        const mapComVenObj = await mapcomTicketRep.findOne(
            {
             where : {
                  id : req.body.id 
                },
                relations : ["vendor","comTickets"]
            });
            const commonTIcketRep = await getRepository(CommonAreaTickets);
            
    const comObjAss= await commonTIcketRep.findOne({
        where : {
           COM_TICKET_ID : mapComVenObj.comTickets.COM_TICKET_ID
        },
       relations : ["association","serviceId","comService","indSerPlan","indServiceTypes"]
   })
        if (req.body.status == '1') {
            mapComVenObj.IS_ACCEPTED = true
            mapComVenObj.IS_REJECTED = false
        }
        else {
            mapComVenObj.IS_ACCEPTED = false
            mapComVenObj.IS_REJECTED = true
        }
        req.body.quantity =   comObjAss.QUANTITY;


        console.log(mapComVenObj);
        mapcomTicketRep.save(mapComVenObj).then((data) => {

            if (req.body.status == '1') {
                res.send(data)
            }
            else {

                vendorSendMail.vendoComRejectSendMail(comObjAss.COM_TICKET_ID,comObjAss.TICKET_DESCRIPTION,mapComVenObj.vendor.VENDOR_EMAIL,comObjAss.TICKET_DATE,comObjAss.TICKET_TIME,comObjAss.comService.COM_SERVICE_NAME,comObjAss.serviceId.IND_SERVICE_NAME, comObjAss.indServiceTypes.TYPE,comObjAss.QUANTITY,comObjAss.AMOUNT,comObjAss.indSerPlan.PLAN,comObjAss.association.ASSOCIATION_NAME)
                ownerComTicketSendMail.ComTicketRejectSendMail(comObjAss.COM_TICKET_ID,comObjAss.TICKET_DESCRIPTION,comObjAss.association.EMAIL,comObjAss.TICKET_DATE,comObjAss.TICKET_TIME,comObjAss.comService.COM_SERVICE_NAME,comObjAss.serviceId.IND_SERVICE_NAME,comObjAss.indServiceTypes.TYPE,comObjAss.QUANTITY,comObjAss.AMOUNT,comObjAss.indSerPlan.PLAN, mapComVenObj.vendor.VENDOR_NAME)

                next()
            }

        }).catch((error) => {
            res.status(500).send(error);
        })



    }


    static actionWorkComTicketByassociation = async (req: Request, res: Response, next) => {

        const mapAssComTicketRep = getRepository(MapComTicketVendor);
        const commonTicketsRep = getRepository(CommonAreaTickets);

        const mapAssComOb = await mapAssComTicketRep.findOne(
            {
                where: {
                    id: req.params.id
                },
                relations: ["comTickets"]
            }
        );

        if (req.params.status == "workStarted") {
            mapAssComOb.IS_WORK_STARTED = true;
            mapAssComOb.IS_WORK_COMPLETED = false;
        }
        else {
            mapAssComOb.IS_WORK_COMPLETED = true;
            mapAssComOb.IS_WORK_COMPLETED = true;

        }
        console.log(mapAssComOb)
        const mapObj = await mapAssComTicketRep.save(mapAssComOb);

        if (req.params.status != "workStarted") {
            var comTIicketId = mapAssComOb.comTickets.COM_TICKET_ID;

            var comTicketObj = await commonTicketsRep.findOne(comTIicketId);
            comTicketObj.IS_WORK_COMPLETED = true;

            var resultObj = await commonTicketsRep.save(comTicketObj);
            res.send(mapObj);

        }
        else {
            res.send(mapObj);
        }



    }

    static actionComTicketByassociation = async (req: Request, res: Response, next) => {

        console.log("okgoood 234234")
        const mapAssComTicketRep = getRepository(MAP_ASS_COM_TICKET);
        const appUserRep = getRepository(AppUserDetails);
        const comTicketsRep = getRepository(CommonAreaTickets);
        
        const comObjAss = await comTicketsRep.findOne({
            where : {
               COM_TICKET_ID : req.body.comTicketId
            },
           relations : ["association","serviceId","comService","indSerPlan","indServiceTypes"]
       })
        const appUserObj = await appUserRep.findOne(req.params.appUserid);
        const comTicketObj = await comTicketsRep.findOne(
            {
                where: {
                    COM_TICKET_ID: req.params.comticketid
                },
                relations: ["association", "serviceId", "comService", "indSerPlan", "indServiceTypes"]
            }
        );

        console.log(comTicketObj);
        const flatownerObj = await getRepository(FlatOwners)
            .createQueryBuilder("flatOwner")
            .where("flatOwner.ASSOCIATION_ID =  :id AND flatOwner.IS_ASSOCIATION = true ", {
                id: req.params.associationId,
            })
            .getMany();
console.log("flatOwnerObj")
        console.log(flatownerObj)
        const mapAssComTicket = new MAP_ASS_COM_TICKET();
        if (req.params.status == '1') {
            mapAssComTicket.IS_ACCEPTED = true;
            mapAssComTicket.IS_REJECTED = false;
        }
        else if (req.params.status == '2') {
            mapAssComTicket.IS_ACCEPTED = false;
            mapAssComTicket.IS_REJECTED = true;
        }

        mapAssComTicket.comTickets = comTicketObj;
        mapAssComTicket.appUser = appUserObj;
        req.body.comTicketId = comTicketObj.COM_TICKET_ID;
        req.body.ticket_date = comTicketObj.TICKET_DATE;
        req.body.ticket_time = comTicketObj.TICKET_TIME;
        req.body.serviceId = comTicketObj.serviceId.IND_SERVICE_ID;

        console.log("re.body")

        console.log(req.body)

        console.log(comTicketObj.indServiceTypes)
        req.body.service_types_id = comTicketObj.indServiceTypes.IND_SERVICE_TYPE_ID;

        req.body.associationId = comTicketObj.association.id;

        mapAssComTicketRep.save(mapAssComTicket).then(async (data) => {

            const mapAssComTicket = await getRepository(MAP_ASS_COM_TICKET)
                .createQueryBuilder("mapAssCom")
                .where("mapAssCom.comTickets =  :id AND mapAssCom.IS_ACCEPTED = true ", {
                    id: req.params.comticketid,
                })
                .getMany();
            var mapAssCount = mapAssComTicket.length;
            var flatOwnerLength = flatownerObj.length;
            if (mapAssCount >= flatOwnerLength) {

                comTicketObj.IS_ASSOCIATION_APPROVED = true;

              const resulComRep = await   comTicketsRep.save(comTicketObj);






                next()

                // console.log("assgigning ticket")
                // res.send(data)
            }
            else {
                res.send(data)
            }

            //  appUserRep.findOne()

            //     res.send(data)
        }).catch((error) => {
            res.status(500).send(error)
        })



    }


    static checkPrimaryInternalVendor = async (req: Request, res: Response, next) => {
        console.log("data" + req.body.ticket_date);
        console.log("time" + req.body.ticket_time);
        const mapComVendorRep = await getRepository(MapComTicketVendor);
        const vendorRep = await getRepository(VENDOR);
        const commonTIcketRep = await getRepository(CommonAreaTickets);

        const comObjAss = await commonTIcketRep.findOne({
            where : {
               COM_TICKET_ID : req.body.comTicketId
            },
           relations : ["association","serviceId","comService","indSerPlan","indServiceTypes"]
       })
        var date = "2020-03-23T" + req.body.ticket_time + ".573Z";
        var postTime = getPostTIme(date);
        var preTime = getPreTime(date);
        console.log(req.body.ticket_date)
        console.log("eror query")
        const prioryOneVendor = await getRepository(VENDOR)
            .createQueryBuilder("vendor")
            .leftJoinAndSelect("vendor.assVendors", "assVendor", "assVendor.vendorInfo = vendor.id")
            .leftJoinAndSelect("vendor.vensServices", "venService", "venService.vendorInfo = vendor.id")
            .leftJoinAndSelect("vendor.mapComVendor", "mapComVendors", "mapComVendors.vendor = vendor.id")
            .leftJoinAndSelect("mapComVendors.comTickets", "comTicket", "comTicket.COM_TICKET_ID = mapComVendors.COM_TICKET_ID")
            .leftJoinAndSelect("vendor.assVenSerPriority", "assSerPriority", "assSerPriority.vendorInfo = vendor.id")
            .where("assVendor.ASSOCIATION_ID =  :id AND venService.IND_SERVICE_ID = :service  AND assSerPriority.PRIORITY = 1  AND  assVendor.IS_INTERNAL_VENDOR = true AND comTicket.STATUS = 'Pending'   AND   comTicket.TICKET_DATE = :ticket_date AND (comTicket.TICKET_TIME   BETWEEN '" + preTime + "' AND '" + postTime + " ') ", {
                id: req.body.associationId,
                service: req.body.serviceId,
                ticket_date: req.body.ticket_date,
            }).getOne();
        console.log("next")

        if (prioryOneVendor) {
            console.log("if")
            next()
        }
        else {

            console.log("else")

            const prioryOneElVendor = await getRepository(VENDOR)
                .createQueryBuilder("vendor")
                .leftJoinAndSelect("vendor.assVendors", "assVendor", "assVendor.vendorInfo = vendor.id")
                .leftJoinAndSelect("vendor.vensServices", "venService", "venService.vendorInfo = vendor.id")
                .leftJoinAndSelect("vendor.mapVendor", "mapVendors", "mapVendors.vendor = vendor.id")
                .leftJoinAndSelect("vendor.assVenSerPriority", "assSerPriority", "assSerPriority.vendorInfo = vendor.id")
                .where("assVendor.ASSOCIATION_ID =  :id AND venService.IND_SERVICE_ID = :service   AND assVendor.IS_INTERNAL_VENDOR = true AND   assSerPriority.PRIORITY = 1 ", {
                    id: req.body.associationId,
                    service: req.body.serviceId,
                }).getOne();

            console.log(prioryOneElVendor)
            if (prioryOneElVendor != undefined) {
                var vendorId = prioryOneElVendor.id;

                console.log("req.body.indTicketId" + req.body.indTicketId)

                //if(vendorId){
                //res.send(prioryOneVendor);
                const vendorObj = await vendorRep.findOne(vendorId);
                // const comObj = await commonTIcketRep.findOne(req.body.comTicketId,{
                //     relations : ["association","serviceId","comService","indSerPlan","indServiceTypes"]
                // })
                 const comObj = await commonTIcketRep.findOne({
                     where : {
                        COM_TICKET_ID : req.body.comTicketId
                     },
                    relations : ["association","serviceId","comService","indSerPlan","indServiceTypes"]
                })
                console.log(comObj)
                const mapComVendorObj = new MapComTicketVendor();
                mapComVendorObj.vendor = vendorObj;
                mapComVendorObj.comTickets = comObj;
                mapComVendorObj.IS_ACCEPTED = false;
                mapComVendorObj.IS_REJECTED = false;
                mapComVendorObj.IS_ACTIVE = true;
                var samapComVendorObj = mapComVendorRep.create(mapComVendorObj);

                mapComVendorRep.save(samapComVendorObj).then(async (mapIndObj) => {
                    console.log("dvsdfjkkjkjsf")
                    if (req.body.service_types_id) {
                        req.body.serviceId

                        const interServAmountVendor = await getRepository(INTERNAL_SERVICES_AMOUNT_VENDORS)
                            .createQueryBuilder("interServVendor")
                            .where("interServVendor.IND_SERVICE_ID =  :indServiceId AND interServVendor.SERVICE_TYPE_ID = :indServiceTypeId AND VENDOR_ID = :vendorId ", {
                                indServiceId: req.body.serviceId,
                                indServiceTypeId: req.body.service_types_id,
                                vendorId: vendorId
                            }).getOne().then(async (data) => {

                                var amount = data.AMOUNT;


                                if (req.body.quantity) {

                                    amount = amount * req.body.quantity;
                                }

                                comObj.AMOUNT = amount;
                                console.log(amount + "amount")

                                var esult = await commonTIcketRep.save(comObj);


                                vendorSendMail.vendoComSendMail(comObjAss.COM_TICKET_ID,comObjAss.TICKET_DESCRIPTION,vendorObj.VENDOR_EMAIL,comObjAss.TICKET_DATE,comObjAss.TICKET_TIME,comObjAss.comService.COM_SERVICE_NAME,comObjAss.serviceId.IND_SERVICE_NAME,
                                    comObjAss.indServiceTypes.TYPE,comObjAss.QUANTITY,comObjAss.AMOUNT,comObjAss.indSerPlan.PLAN,comObjAss.association.ASSOCIATION_NAME)

                                ownerComTicketSendMail.ComTicketSendMail(comObjAss.COM_TICKET_ID,comObjAss.TICKET_DESCRIPTION,comObjAss.association.EMAIL,comObjAss.TICKET_DATE,comObjAss.TICKET_TIME,comObjAss.comService.COM_SERVICE_NAME,comObjAss.serviceId.IND_SERVICE_NAME,comObjAss.indSerPlan.PLAN,
                                    comObjAss.indServiceTypes.TYPE,comObjAss.QUANTITY,comObjAss.AMOUNT,vendorObj.VENDOR_NAME)

                                

                            }).catch((error) => {
                                console.log(error)
                            });




                    }
                    else {

                        const interServAmountVendor = await getRepository(INTERNAL_SERVICES_AMOUNT_VENDORS)
                            .createQueryBuilder("interServVendor")
                            .where("interServVendor.IND_SERVICE_ID =  :indServiceId  AND VENDOR_ID = :vendorId", {
                                indServiceId: req.body.serviceId,
                                vendorId: vendorId
                            }).getOne().then(async (data) => {
                                var amount = data.AMOUNT;


                                if (req.body.quantity) {

                                    amount = amount * req.body.quantity;
                                }

                                comObj.AMOUNT = amount;

                                var esult = await commonTIcketRep.save(comObj);

                                vendorSendMail.vendoComSendMail(comObjAss.COM_TICKET_ID,comObjAss.TICKET_DESCRIPTION,vendorObj.VENDOR_EMAIL,comObjAss.TICKET_DATE,comObjAss.TICKET_TIME,comObjAss.comService.COM_SERVICE_NAME,comObjAss.serviceId.IND_SERVICE_NAME,
                                    comObjAss.indServiceTypes.TYPE,comObjAss.QUANTITY,comObjAss.AMOUNT,comObjAss.indSerPlan.PLAN,comObjAss.association.ASSOCIATION_NAME)

                                ownerComTicketSendMail.ComTicketSendMail(comObjAss.COM_TICKET_ID,comObjAss.TICKET_DESCRIPTION,comObjAss.association.EMAIL,comObjAss.TICKET_DATE,comObjAss.TICKET_TIME,comObjAss.comService.COM_SERVICE_NAME,comObjAss.serviceId.IND_SERVICE_NAME,comObjAss.indSerPlan.PLAN,
                                    comObjAss.indServiceTypes.TYPE,comObjAss.QUANTITY,comObjAss.AMOUNT,vendorObj.VENDOR_NAME)



                            }).catch((error) => {
                                console.log(error)
                            });


                    }

                    console.log("working 1")
                    res.send(req.body);
                }).catch((error) => {
                    res.status(500).send(error)
                });
            }
            else {
                next()
            }

        }
    }




    static checkSecondaryInternalVendor = async (req: Request, res: Response, next) => {
        console.log("data" + req.body.ticket_date);
        console.log("time" + req.body.ticket_time);
        const mapComVendorRep = await getRepository(MapComTicketVendor);
        const vendorRep = await getRepository(VENDOR);
        const commonTIcketRep = await getRepository(CommonAreaTickets);
        const comObjAss= await commonTIcketRep.findOne({
            where : {
               COM_TICKET_ID : req.body.comTicketId
            },
           relations : ["association","serviceId","comService","indSerPlan","indServiceTypes"]
       })

        const comObj = await commonTIcketRep.findOne({
            where : {
               COM_TICKET_ID : req.body.comTicketId
            },
           relations : ["association","serviceId","comService","indSerPlan","indServiceTypes"]
       })
        var date = "2020-03-23T" + req.body.ticket_time + ".573Z";
        var postTime = getPostTIme(date);
        var preTime = getPreTime(date);
        console.log(req.body.ticket_date)
        console.log("eror query")
        const prioryOneVendor = await getRepository(VENDOR)
            .createQueryBuilder("vendor")
            .leftJoinAndSelect("vendor.assVendors", "assVendor", "assVendor.vendorInfo = vendor.id")
            .leftJoinAndSelect("vendor.vensServices", "venService", "venService.vendorInfo = vendor.id")
            .leftJoinAndSelect("vendor.mapComVendor", "mapComVendors", "mapComVendors.vendor = vendor.id")
            .leftJoinAndSelect("mapComVendors.comTickets", "comTicket", "comTicket.COM_TICKET_ID = mapComVendors.COM_TICKET_ID")
            .leftJoinAndSelect("vendor.assVenSerPriority", "assSerPriority", "assSerPriority.vendorInfo = vendor.id")
            .where("assVendor.ASSOCIATION_ID =  :id AND venService.IND_SERVICE_ID = :service  AND assSerPriority.PRIORITY = 2  AND  assVendor.IS_INTERNAL_VENDOR = true AND comTicket.STATUS = 'Pending'   AND   comTicket.TICKET_DATE = :ticket_date AND (comTicket.TICKET_TIME   BETWEEN '" + preTime + "' AND '" + postTime + " ') ", {
                id: req.body.associationId,
                service: req.body.serviceId,
                ticket_date: req.body.ticket_date,
            }).getOne();
        console.log("next")

        if (prioryOneVendor) {
            console.log("if")
            next()
        }
        else {

            console.log("else")

            const prioryOneElVendor = await getRepository(VENDOR)
                .createQueryBuilder("vendor")
                .leftJoinAndSelect("vendor.assVendors", "assVendor", "assVendor.vendorInfo = vendor.id")
                .leftJoinAndSelect("vendor.vensServices", "venService", "venService.vendorInfo = vendor.id")
                .leftJoinAndSelect("vendor.mapVendor", "mapVendors", "mapVendors.vendor = vendor.id")
                .leftJoinAndSelect("vendor.assVenSerPriority", "assSerPriority", "assSerPriority.vendorInfo = vendor.id")
                .where("assVendor.ASSOCIATION_ID =  :id AND venService.IND_SERVICE_ID = :service   AND assVendor.IS_INTERNAL_VENDOR = true AND   assSerPriority.PRIORITY = 2 ", {
                    id: req.body.associationId,
                    service: req.body.serviceId,
                }).getOne();

            console.log(prioryOneElVendor)
            if (prioryOneElVendor != undefined) {
                var vendorId = prioryOneElVendor.id;

                console.log("req.body.indTicketId" + req.body.indTicketId)

                //if(vendorId){
                //res.send(prioryOneVendor);
                const vendorObj = await vendorRep.findOne(vendorId);
                const comObj = await commonTIcketRep.findOne(req.body.comTicketId)
                console.log(comObj)
                const mapComVendorObj = new MapComTicketVendor();
                mapComVendorObj.vendor = vendorObj;
                mapComVendorObj.comTickets = comObj;
                mapComVendorObj.IS_ACCEPTED = false;
                mapComVendorObj.IS_REJECTED = false;
                mapComVendorObj.IS_ACTIVE = true;
                var samapComVendorObj = mapComVendorRep.create(mapComVendorObj);

                mapComVendorRep.save(samapComVendorObj).then(async (mapIndObj) => {
                    console.log("dvsdfjkkjkjsf")
                    if (req.body.service_types_id) {
                        req.body.serviceId

                        const interServAmountVendor = await getRepository(INTERNAL_SERVICES_AMOUNT_VENDORS)
                            .createQueryBuilder("interServVendor")
                            .where("interServVendor.IND_SERVICE_ID =  :indServiceId AND interServVendor.SERVICE_TYPE_ID = :indServiceTypeId AND VENDOR_ID = :vendorId ", {
                                indServiceId: req.body.serviceId,
                                indServiceTypeId: req.body.service_types_id,
                                vendorId: vendorId
                            }).getOne().then(async (data) => {

                                var amount = data.AMOUNT;


                                if (req.body.quantity) {

                                    amount = amount * req.body.quantity;
                                }

                                comObj.AMOUNT = amount;
                                console.log(amount + "amount")

                                var esult = await commonTIcketRep.save(comObj);

                                vendorSendMail.vendoComSendMail(comObjAss.COM_TICKET_ID,comObjAss.TICKET_DESCRIPTION,vendorObj.VENDOR_EMAIL,comObjAss.TICKET_DATE,comObjAss.TICKET_TIME,comObjAss.comService.COM_SERVICE_NAME,comObjAss.serviceId.IND_SERVICE_NAME,
                                    comObjAss.indServiceTypes.TYPE,comObjAss.QUANTITY,comObjAss.AMOUNT,comObjAss.indSerPlan.PLAN,comObjAss.association.ASSOCIATION_NAME)

                                ownerComTicketSendMail.ComTicketSendMail(comObjAss.COM_TICKET_ID,comObjAss.TICKET_DESCRIPTION,comObjAss.association.EMAIL,comObjAss.TICKET_DATE,comObjAss.TICKET_TIME,comObjAss.comService.COM_SERVICE_NAME,comObjAss.serviceId.IND_SERVICE_NAME,comObjAss.indSerPlan.PLAN,
                                    comObjAss.indServiceTypes.TYPE,comObjAss.QUANTITY,comObjAss.AMOUNT,vendorObj.VENDOR_NAME)
                            }).catch((error) => {
                                console.log(error)
                            });




                    }
                    else {

                        const interServAmountVendor = await getRepository(INTERNAL_SERVICES_AMOUNT_VENDORS)
                            .createQueryBuilder("interServVendor")
                            .where("interServVendor.IND_SERVICE_ID =  :indServiceId  AND VENDOR_ID = :vendorId", {
                                indServiceId: req.body.serviceId,
                                vendorId: vendorId
                            }).getOne().then(async (data) => {
                                var amount = data.AMOUNT;


                                if (req.body.quantity) {

                                    amount = amount * req.body.quantity;
                                }

                                comObj.AMOUNT = amount;

                                var esult = await commonTIcketRep.save(comObj);

                                vendorSendMail.vendoComSendMail(comObjAss.COM_TICKET_ID,comObjAss.TICKET_DESCRIPTION,vendorObj.VENDOR_EMAIL,comObjAss.TICKET_DATE,comObjAss.TICKET_TIME,comObjAss.comService.COM_SERVICE_NAME,comObjAss.serviceId.IND_SERVICE_NAME,
                                    comObjAss.indServiceTypes.TYPE,comObjAss.QUANTITY,comObjAss.AMOUNT,comObjAss.indSerPlan.PLAN,comObjAss.association.ASSOCIATION_NAME)
                                ownerComTicketSendMail.ComTicketSendMail(comObjAss.COM_TICKET_ID,comObjAss.TICKET_DESCRIPTION,comObjAss.association.EMAIL,comObjAss.TICKET_DATE,comObjAss.TICKET_TIME,comObjAss.comService.COM_SERVICE_NAME,comObjAss.serviceId.IND_SERVICE_NAME,comObjAss.indSerPlan.PLAN,
                                    comObjAss.indServiceTypes.TYPE,comObjAss.QUANTITY,comObjAss.AMOUNT,vendorObj.VENDOR_NAME)
                            }).catch((error) => {
                                console.log(error)
                            });


                    }

                    res.send(req.body);
                }).catch((error) => {
                    res.status(500).send(error)
                });
            }
            else {
                next()
            }

        }
    }

    static checkThirdInternalVendor = async (req: Request, res: Response, next) => {
        console.log("data" + req.body.ticket_date);
        console.log("time" + req.body.ticket_time);
        const mapComVendorRep = await getRepository(MapComTicketVendor);
        const vendorRep = await getRepository(VENDOR);
        const commonTIcketRep = await getRepository(CommonAreaTickets);

        const comObjAss= await commonTIcketRep.findOne({
            where : {
               COM_TICKET_ID : req.body.comTicketId
            },
           relations : ["association","serviceId","comService","indSerPlan","indServiceTypes"]
       })
        var date = "2020-03-23T" + req.body.ticket_time + ".573Z";
        var postTime = getPostTIme(date);
        var preTime = getPreTime(date);
        console.log(req.body.ticket_date)
        console.log("eror query")
        const prioryOneVendor = await getRepository(VENDOR)
            .createQueryBuilder("vendor")
            .leftJoinAndSelect("vendor.assVendors", "assVendor", "assVendor.vendorInfo = vendor.id")
            .leftJoinAndSelect("vendor.vensServices", "venService", "venService.vendorInfo = vendor.id")
            .leftJoinAndSelect("vendor.mapComVendor", "mapComVendors", "mapComVendors.vendor = vendor.id")
            .leftJoinAndSelect("mapComVendors.comTickets", "comTicket", "comTicket.COM_TICKET_ID = mapComVendors.COM_TICKET_ID")
            .leftJoinAndSelect("vendor.assVenSerPriority", "assSerPriority", "assSerPriority.vendorInfo = vendor.id")
            .where("assVendor.ASSOCIATION_ID =  :id AND venService.IND_SERVICE_ID = :service  AND assSerPriority.PRIORITY = 3  AND  assVendor.IS_INTERNAL_VENDOR = true AND comTicket.STATUS = 'Pending'   AND   comTicket.TICKET_DATE = :ticket_date AND (comTicket.TICKET_TIME   BETWEEN '" + preTime + "' AND '" + postTime + " ') ", {
                id: req.body.associationId,
                service: req.body.serviceId,
                ticket_date: req.body.ticket_date,
            }).getOne();
        console.log("next")

        if (prioryOneVendor) {
            console.log("if")
            next()
        }
        else {

            console.log("else")

            const prioryOneElVendor = await getRepository(VENDOR)
                .createQueryBuilder("vendor")
                .leftJoinAndSelect("vendor.assVendors", "assVendor", "assVendor.vendorInfo = vendor.id")
                .leftJoinAndSelect("vendor.vensServices", "venService", "venService.vendorInfo = vendor.id")
                .leftJoinAndSelect("vendor.mapVendor", "mapVendors", "mapVendors.vendor = vendor.id")
                .leftJoinAndSelect("vendor.assVenSerPriority", "assSerPriority", "assSerPriority.vendorInfo = vendor.id")
                .where("assVendor.ASSOCIATION_ID =  :id AND venService.IND_SERVICE_ID = :service   AND assVendor.IS_INTERNAL_VENDOR = true AND   assSerPriority.PRIORITY = 3 ", {
                    id: req.body.associationId,
                    service: req.body.serviceId,
                }).getOne();

            console.log(prioryOneElVendor)
            if (prioryOneElVendor != undefined) {
                var vendorId = prioryOneElVendor.id;

                console.log("req.body.indTicketId" + req.body.indTicketId)

                //if(vendorId){
                //res.send(prioryOneVendor);
                const vendorObj = await vendorRep.findOne(vendorId);
                const comObj = await commonTIcketRep.findOne(req.body.comTicketId)
                console.log(comObj)
                const mapComVendorObj = new MapComTicketVendor();
                mapComVendorObj.vendor = vendorObj;
                mapComVendorObj.comTickets = comObj;
                mapComVendorObj.IS_ACCEPTED = false;
                mapComVendorObj.IS_REJECTED = false;
                mapComVendorObj.IS_ACTIVE = true;
                var samapComVendorObj = mapComVendorRep.create(mapComVendorObj);

                mapComVendorRep.save(samapComVendorObj).then(async (mapIndObj) => {
                    console.log("dvsdfjkkjkjsf")
                    if (req.body.service_types_id) {
                        req.body.serviceId

                        const interServAmountVendor = await getRepository(INTERNAL_SERVICES_AMOUNT_VENDORS)
                            .createQueryBuilder("interServVendor")
                            .where("interServVendor.IND_SERVICE_ID =  :indServiceId AND interServVendor.SERVICE_TYPE_ID = :indServiceTypeId AND VENDOR_ID = :vendorId ", {
                                indServiceId: req.body.serviceId,
                                indServiceTypeId: req.body.service_types_id,
                                vendorId: vendorId
                            }).getOne().then(async (data) => {

                                var amount = data.AMOUNT;


                                if (req.body.quantity) {

                                    amount = amount * req.body.quantity;
                                }

                                comObj.AMOUNT = amount;
                                console.log(amount + "amount")

                                var esult = await commonTIcketRep.save(comObj);

                                vendorSendMail.vendoComSendMail(comObjAss.COM_TICKET_ID,comObjAss.TICKET_DESCRIPTION,vendorObj.VENDOR_EMAIL,comObjAss.TICKET_DATE,comObjAss.TICKET_TIME,comObjAss.comService.COM_SERVICE_NAME,comObjAss.serviceId.IND_SERVICE_NAME,
                                    comObjAss.indServiceTypes.TYPE,comObjAss.QUANTITY,comObjAss.AMOUNT,comObjAss.indSerPlan.PLAN,comObjAss.association.ASSOCIATION_NAME)
                                ownerComTicketSendMail.ComTicketSendMail(comObjAss.COM_TICKET_ID,
                                    comObjAss.TICKET_DESCRIPTION,comObjAss.association.EMAIL,comObjAss.TICKET_DATE,
                                    comObjAss.TICKET_TIME,comObjAss.comService.COM_SERVICE_NAME,comObjAss.serviceId.IND_SERVICE_NAME,
                                    comObjAss.indSerPlan.PLAN,comObjAss.indServiceTypes.TYPE,
                                    comObjAss.QUANTITY,comObjAss.AMOUNT,vendorObj.VENDOR_NAME)
                            }).catch((error) => {
                                console.log(error)
                            });




                    }
                    else {

                        const interServAmountVendor = await getRepository(INTERNAL_SERVICES_AMOUNT_VENDORS)
                            .createQueryBuilder("interServVendor")
                            .where("interServVendor.IND_SERVICE_ID =  :indServiceId  AND VENDOR_ID = :vendorId", {
                                indServiceId: req.body.serviceId,
                                vendorId: vendorId
                            }).getOne().then(async (data) => {
                                var amount = data.AMOUNT;


                                if (req.body.quantity) {

                                    amount = amount * req.body.quantity;
                                }

                                comObj.AMOUNT = amount;

                                var esult = await commonTIcketRep.save(comObj);

                                vendorSendMail.vendoComSendMail(comObjAss.COM_TICKET_ID,comObjAss.TICKET_DESCRIPTION,vendorObj.VENDOR_EMAIL,comObjAss.TICKET_DATE,comObjAss.TICKET_TIME,comObjAss.comService.COM_SERVICE_NAME,comObjAss.serviceId.IND_SERVICE_NAME,
                                    comObjAss.indServiceTypes.TYPE,comObjAss.QUANTITY,comObjAss.AMOUNT,comObjAss.indSerPlan.PLAN,comObjAss.association.ASSOCIATION_NAME)

                                ownerComTicketSendMail.ComTicketSendMail(comObjAss.COM_TICKET_ID,
                                    comObjAss.TICKET_DESCRIPTION,comObjAss.association.EMAIL,comObjAss.TICKET_DATE,
                                    comObjAss.TICKET_TIME,comObjAss.comService.COM_SERVICE_NAME,comObjAss.serviceId.IND_SERVICE_NAME,
                                    comObjAss.indSerPlan.PLAN,comObjAss.indServiceTypes.TYPE,
                                    comObjAss.QUANTITY,comObjAss.AMOUNT,vendorObj.VENDOR_NAME)
                            }).catch((error) => {
                                console.log(error)
                            });


                    }

                    console.log("working 1")
                    res.send(req.body);
                }).catch((error) => {
                    res.status(500).send(error)
                });
            }
            else {
                next()
            }

        }
    }


    static checkFourthInternalVendor = async (req: Request, res: Response, next) => {
        console.log("data" + req.body.ticket_date);
        console.log("time" + req.body.ticket_time);
        const mapComVendorRep = await getRepository(MapComTicketVendor);
        const vendorRep = await getRepository(VENDOR);
        const commonTIcketRep = await getRepository(CommonAreaTickets);

        const comObjAss= await commonTIcketRep.findOne({
            where : {
               COM_TICKET_ID : req.body.comTicketId
            },
           relations : ["association","serviceId","comService","indSerPlan","indServiceTypes"]
       })
        
    const comObj = await commonTIcketRep.findOne({
        where : {
           COM_TICKET_ID : req.body.comTicketId
        },
       relations : ["association","serviceId","comService","indSerPlan","indServiceTypes"]
   })

        var date = "2020-03-23T" + req.body.ticket_time + ".573Z";
        var postTime = getPostTIme(date);
        var preTime = getPreTime(date);
        console.log(req.body.ticket_date)
        console.log("eror query")
        const prioryOneVendor = await getRepository(VENDOR)
            .createQueryBuilder("vendor")
            .leftJoinAndSelect("vendor.assVendors", "assVendor", "assVendor.vendorInfo = vendor.id")
            .leftJoinAndSelect("vendor.vensServices", "venService", "venService.vendorInfo = vendor.id")
            .leftJoinAndSelect("vendor.mapComVendor", "mapComVendors", "mapComVendors.vendor = vendor.id")
            .leftJoinAndSelect("mapComVendors.comTickets", "comTicket", "comTicket.COM_TICKET_ID = mapComVendors.COM_TICKET_ID")
            .leftJoinAndSelect("vendor.assVenSerPriority", "assSerPriority", "assSerPriority.vendorInfo = vendor.id")
            .where("assVendor.ASSOCIATION_ID =  :id AND venService.IND_SERVICE_ID = :service  AND assSerPriority.PRIORITY = 4  AND  assVendor.IS_INTERNAL_VENDOR = true AND comTicket.STATUS = 'Pending'   AND   comTicket.TICKET_DATE = :ticket_date AND (comTicket.TICKET_TIME   BETWEEN '" + preTime + "' AND '" + postTime + " ') ", {
                id: req.body.associationId,
                service: req.body.serviceId,
                ticket_date: req.body.ticket_date,
            }).getOne();
        console.log("next")

        if (prioryOneVendor) {
            console.log("if")
            next()
        }
        else {

            console.log("else")

            const prioryOneElVendor = await getRepository(VENDOR)
                .createQueryBuilder("vendor")
                .leftJoinAndSelect("vendor.assVendors", "assVendor", "assVendor.vendorInfo = vendor.id")
                .leftJoinAndSelect("vendor.vensServices", "venService", "venService.vendorInfo = vendor.id")
                .leftJoinAndSelect("vendor.mapVendor", "mapVendors", "mapVendors.vendor = vendor.id")
                .leftJoinAndSelect("vendor.assVenSerPriority", "assSerPriority", "assSerPriority.vendorInfo = vendor.id")
                .where("assVendor.ASSOCIATION_ID =  :id AND venService.IND_SERVICE_ID = :service   AND assVendor.IS_INTERNAL_VENDOR = true AND   assSerPriority.PRIORITY = 4 ", {
                    id: req.body.associationId,
                    service: req.body.serviceId,
                }).getOne();

            console.log(prioryOneElVendor)
            if (prioryOneElVendor != undefined) {
                var vendorId = prioryOneElVendor.id;

                console.log("req.body.indTicketId" + req.body.indTicketId)

                //if(vendorId){
                //res.send(prioryOneVendor);
                const vendorObj = await vendorRep.findOne(vendorId);
                const comObj = await commonTIcketRep.findOne(req.body.comTicketId)
                console.log(comObj)
                const mapComVendorObj = new MapComTicketVendor();
                mapComVendorObj.vendor = vendorObj;
                mapComVendorObj.comTickets = comObj;
                mapComVendorObj.IS_ACCEPTED = false;
                mapComVendorObj.IS_REJECTED = false;
                mapComVendorObj.IS_ACTIVE = true;
                var samapComVendorObj = mapComVendorRep.create(mapComVendorObj);

                mapComVendorRep.save(samapComVendorObj).then(async (mapIndObj) => {
                    console.log("dvsdfjkkjkjsf")
                    if (req.body.service_types_id) {
                        req.body.serviceId

                        const interServAmountVendor = await getRepository(INTERNAL_SERVICES_AMOUNT_VENDORS)
                            .createQueryBuilder("interServVendor")
                            .where("interServVendor.IND_SERVICE_ID =  :indServiceId AND interServVendor.SERVICE_TYPE_ID = :indServiceTypeId AND VENDOR_ID = :vendorId ", {
                                indServiceId: req.body.serviceId,
                                indServiceTypeId: req.body.service_types_id,
                                vendorId: vendorId
                            }).getOne().then(async (data) => {

                                var amount = data.AMOUNT;


                                if (req.body.quantity) {

                                    amount = amount * req.body.quantity;
                                }

                                comObj.AMOUNT = amount;
                                console.log(amount + "amount")

                                var esult = await commonTIcketRep.save(comObj);

                                vendorSendMail.vendoComSendMail(comObjAss.COM_TICKET_ID,comObjAss.TICKET_DESCRIPTION,vendorObj.VENDOR_EMAIL,comObjAss.TICKET_DATE,comObjAss.TICKET_TIME,comObjAss.comService.COM_SERVICE_NAME,comObjAss.serviceId.IND_SERVICE_NAME,
                                    comObjAss.indServiceTypes.TYPE,comObjAss.QUANTITY,comObjAss.AMOUNT,comObjAss.indSerPlan.PLAN,comObjAss.association.ASSOCIATION_NAME)
                                ownerComTicketSendMail.ComTicketSendMail(comObjAss.COM_TICKET_ID,
                                    comObjAss.TICKET_DESCRIPTION,comObjAss.association.EMAIL,comObjAss.TICKET_DATE,
                                    comObjAss.TICKET_TIME,comObjAss.comService.COM_SERVICE_NAME,comObjAss.serviceId.IND_SERVICE_NAME,
                                    comObjAss.indSerPlan.PLAN,comObjAss.indServiceTypes.TYPE,
                                    comObjAss.QUANTITY,comObjAss.AMOUNT,vendorObj.VENDOR_NAME)
                            }).catch((error) => {
                                console.log(error)
                            });




                    }
                    else {

                        const interServAmountVendor = await getRepository(INTERNAL_SERVICES_AMOUNT_VENDORS)
                            .createQueryBuilder("interServVendor")
                            .where("interServVendor.IND_SERVICE_ID =  :indServiceId  AND VENDOR_ID = :vendorId", {
                                indServiceId: req.body.serviceId,
                                vendorId: vendorId
                            }).getOne().then(async (data) => {
                                var amount = data.AMOUNT;


                                if (req.body.quantity) {

                                    amount = amount * req.body.quantity;
                                }

                                comObj.AMOUNT = amount;

                                var esult = await commonTIcketRep.save(comObj);

                                vendorSendMail.vendoComSendMail(comObjAss.COM_TICKET_ID,comObjAss.TICKET_DESCRIPTION,vendorObj.VENDOR_EMAIL,comObjAss.TICKET_DATE,comObjAss.TICKET_TIME,comObjAss.comService.COM_SERVICE_NAME,comObjAss.serviceId.IND_SERVICE_NAME,
                                    comObjAss.indServiceTypes.TYPE,comObjAss.QUANTITY,comObjAss.AMOUNT,comObjAss.indSerPlan.PLAN,comObjAss.association.ASSOCIATION_NAME)

                                ownerComTicketSendMail.ComTicketSendMail(comObjAss.COM_TICKET_ID,
                                    comObjAss.TICKET_DESCRIPTION,comObjAss.association.EMAIL,comObjAss.TICKET_DATE,
                                    comObjAss.TICKET_TIME,comObjAss.comService.COM_SERVICE_NAME,comObjAss.serviceId.IND_SERVICE_NAME,
                                    comObjAss.indSerPlan.PLAN,comObjAss.indServiceTypes.TYPE,
                                    comObjAss.QUANTITY,comObjAss.AMOUNT,vendorObj.VENDOR_NAME)
                            }).catch((error) => {
                                console.log(error)
                            });


                    }

                    console.log("working 1")
                    res.send(req.body);
                }).catch((error) => {
                    res.status(500).send(error)
                });
            }
            else {
                next()
            }

        }
    }

    static checkFifthInternalVendor = async (req: Request, res: Response, next) => {
        console.log("data" + req.body.ticket_date);
        console.log("time" + req.body.ticket_time);
        const mapComVendorRep = await getRepository(MapComTicketVendor);
        const vendorRep = await getRepository(VENDOR);
        const commonTIcketRep = await getRepository(CommonAreaTickets);

        const comObjAss= await commonTIcketRep.findOne({
            where : {
               COM_TICKET_ID : req.body.comTicketId
            },
           relations : ["association","serviceId","comService","indSerPlan","indServiceTypes"]
       })
    const comObj = await commonTIcketRep.findOne({
        where : {
           COM_TICKET_ID : req.body.comTicketId
        },
       relations : ["association","serviceId","comService","indSerPlan","indServiceTypes"]
   })
        var date = "2020-03-23T" + req.body.ticket_time + ".573Z";
        var postTime = getPostTIme(date);
        var preTime = getPreTime(date);
        console.log(req.body.ticket_date)
        console.log("eror query")
        const prioryOneVendor = await getRepository(VENDOR)
            .createQueryBuilder("vendor")
            .leftJoinAndSelect("vendor.assVendors", "assVendor", "assVendor.vendorInfo = vendor.id")
            .leftJoinAndSelect("vendor.vensServices", "venService", "venService.vendorInfo = vendor.id")
            .leftJoinAndSelect("vendor.mapComVendor", "mapComVendors", "mapComVendors.vendor = vendor.id")
            .leftJoinAndSelect("mapComVendors.comTickets", "comTicket", "comTicket.COM_TICKET_ID = mapComVendors.COM_TICKET_ID")
            .leftJoinAndSelect("vendor.assVenSerPriority", "assSerPriority", "assSerPriority.vendorInfo = vendor.id")
            .where("assVendor.ASSOCIATION_ID =  :id AND venService.IND_SERVICE_ID = :service  AND assSerPriority.PRIORITY = 5  AND  assVendor.IS_INTERNAL_VENDOR = true AND comTicket.STATUS = 'Pending'   AND   comTicket.TICKET_DATE = :ticket_date AND (comTicket.TICKET_TIME   BETWEEN '" + preTime + "' AND '" + postTime + " ') ", {
                id: req.body.associationId,
                service: req.body.serviceId,
                ticket_date: req.body.ticket_date,
            }).getOne();
        console.log("next")

        if (prioryOneVendor) {
            console.log("if")
            next()
        }
        else {

            console.log("else")

            const prioryOneElVendor = await getRepository(VENDOR)
                .createQueryBuilder("vendor")
                .leftJoinAndSelect("vendor.assVendors", "assVendor", "assVendor.vendorInfo = vendor.id")
                .leftJoinAndSelect("vendor.vensServices", "venService", "venService.vendorInfo = vendor.id")
                .leftJoinAndSelect("vendor.mapVendor", "mapVendors", "mapVendors.vendor = vendor.id")
                .leftJoinAndSelect("vendor.assVenSerPriority", "assSerPriority", "assSerPriority.vendorInfo = vendor.id")
                .where("assVendor.ASSOCIATION_ID =  :id AND venService.IND_SERVICE_ID = :service   AND assVendor.IS_INTERNAL_VENDOR = true AND   assSerPriority.PRIORITY = 5 ", {
                    id: req.body.associationId,
                    service: req.body.serviceId,
                }).getOne();

            console.log(prioryOneElVendor)
            if (prioryOneElVendor != undefined) {
                var vendorId = prioryOneElVendor.id;

                console.log("req.body.indTicketId" + req.body.indTicketId)

                //if(vendorId){
                //res.send(prioryOneVendor);
                const vendorObj = await vendorRep.findOne(vendorId);
                const comObj = await commonTIcketRep.findOne(req.body.comTicketId)
                console.log(comObj)
                const mapComVendorObj = new MapComTicketVendor();
                mapComVendorObj.vendor = vendorObj;
                mapComVendorObj.comTickets = comObj;
                mapComVendorObj.IS_ACCEPTED = false;
                mapComVendorObj.IS_REJECTED = false;
                mapComVendorObj.IS_ACTIVE = true;
                var samapComVendorObj = mapComVendorRep.create(mapComVendorObj);

                mapComVendorRep.save(samapComVendorObj).then(async (mapIndObj) => {
                    console.log("dvsdfjkkjkjsf")
                    if (req.body.service_types_id) {
                        req.body.serviceId

                        const interServAmountVendor = await getRepository(INTERNAL_SERVICES_AMOUNT_VENDORS)
                            .createQueryBuilder("interServVendor")
                            .where("interServVendor.IND_SERVICE_ID =  :indServiceId AND interServVendor.SERVICE_TYPE_ID = :indServiceTypeId AND VENDOR_ID = :vendorId ", {
                                indServiceId: req.body.serviceId,
                                indServiceTypeId: req.body.service_types_id,
                                vendorId: vendorId
                            }).getOne().then(async (data) => {

                                var amount = data.AMOUNT;


                                if (req.body.quantity) {

                                    amount = amount * req.body.quantity;
                                }

                                comObj.AMOUNT = amount;
                                console.log(amount + "amount")

                                var esult = await commonTIcketRep.save(comObj);

                                vendorSendMail.vendoComSendMail(comObjAss.COM_TICKET_ID,comObjAss.TICKET_DESCRIPTION,vendorObj.VENDOR_EMAIL,comObjAss.TICKET_DATE,comObjAss.TICKET_TIME,comObjAss.comService.COM_SERVICE_NAME,comObjAss.serviceId.IND_SERVICE_NAME,
                                    comObjAss.indServiceTypes.TYPE,comObjAss.QUANTITY,comObjAss.AMOUNT,comObjAss.indSerPlan.PLAN,comObjAss.association.ASSOCIATION_NAME)

                                ownerComTicketSendMail.ComTicketSendMail(comObjAss.COM_TICKET_ID,
                                    comObjAss.TICKET_DESCRIPTION,comObjAss.association.EMAIL,comObjAss.TICKET_DATE,
                                    comObjAss.TICKET_TIME,comObjAss.comService.COM_SERVICE_NAME,comObjAss.serviceId.IND_SERVICE_NAME,
                                    comObjAss.indSerPlan.PLAN,comObjAss.indServiceTypes.TYPE,
                                    comObjAss.QUANTITY,comObjAss.AMOUNT,vendorObj.VENDOR_NAME)
                            }).catch((error) => {
                                console.log(error)
                            });




                    }
                    else {

                        const interServAmountVendor = await getRepository(INTERNAL_SERVICES_AMOUNT_VENDORS)
                            .createQueryBuilder("interServVendor")
                            .where("interServVendor.IND_SERVICE_ID =  :indServiceId  AND VENDOR_ID = :vendorId", {
                                indServiceId: req.body.serviceId,
                                vendorId: vendorId
                            }).getOne().then(async (data) => {
                                var amount = data.AMOUNT;


                                if (req.body.quantity) {

                                    amount = amount * req.body.quantity;
                                }

                                comObj.AMOUNT = amount;

                                var esult = await commonTIcketRep.save(comObj);

                                vendorSendMail.vendoComSendMail(comObjAss.COM_TICKET_ID,comObjAss.TICKET_DESCRIPTION,vendorObj.VENDOR_EMAIL,comObjAss.TICKET_DATE,comObjAss.TICKET_TIME,comObjAss.comService.COM_SERVICE_NAME,comObjAss.serviceId.IND_SERVICE_NAME,
                                    comObjAss.indServiceTypes.TYPE,comObjAss.QUANTITY,comObjAss.AMOUNT,comObjAss.indSerPlan.PLAN,comObjAss.association.ASSOCIATION_NAME)

                                ownerComTicketSendMail.ComTicketSendMail(comObjAss.COM_TICKET_ID,
                                    comObjAss.TICKET_DESCRIPTION,comObjAss.association.EMAIL,comObjAss.TICKET_DATE,
                                    comObjAss.TICKET_TIME,comObjAss.comService.COM_SERVICE_NAME,comObjAss.serviceId.IND_SERVICE_NAME,
                                    comObjAss.indSerPlan.PLAN,comObjAss.indServiceTypes.TYPE,
                                    comObjAss.QUANTITY,comObjAss.AMOUNT,vendorObj.VENDOR_NAME)
                            }).catch((error) => {
                                console.log(error)
                            });


                    }

                    console.log("working 1")
                    res.send(req.body);
                }).catch((error) => {
                    res.status(500).send(error)
                });
            }
            else {
                next()
            }

        }
    }


    static mapAssociationComTicketAction = async (req: Request, res: Response, next) => {
        const comTicketRep = getRepository(CommonAreaTickets);

        comTicketRep.find({
            where: {
                association: req.params.associationId
            },
            relations: ['appUserDetails', 'association', 'serviceId', 'indSerPlan', 'indServiceTypes', 'appUserDetails.flatOwner', 'appUserDetails.flatTenant',]
        }).then((data) => {
            res.send(data)
        }).catch((error) => {
            res.status(500).send(error)
        })

    }

    static checkFirstPriorThirdPartyVendor = async (req: Request, res: Response, next) => {

        console.log(req.body.ticket_date)
        console.log(req.body.ticket_date)

        console.log("check first prior thirs parthey vbendr")
        const mapIndVendorRep = await getRepository(MapIndTicketVendor);
        const vendorRep = await getRepository(VENDOR);
        const commonTIcketRep = await getRepository(CommonAreaTickets);
        const comObjAss= await commonTIcketRep.findOne({
            where : {
               COM_TICKET_ID : req.body.comTicketId
            },
           relations : ["association","serviceId","comService","indSerPlan","indServiceTypes"]
       })
        const comObj = await commonTIcketRep.findOne({
            where : {
               COM_TICKET_ID : req.body.comTicketId
            },
           relations : ["association","serviceId","comService","indSerPlan","indServiceTypes"]
       })
        const mapComVendorRep = await getRepository(MapComTicketVendor);
        var date = "2020-03-23T" + req.body.ticket_time + ".573Z";
        var postTime = getPostTIme(date);
        var preTime = getPreTime(date);
        const prioryOneVendor = await getRepository(VENDOR).query("SELECT * FROM vendor LEFT JOIN map_com_tickets_vendors ON   map_com_tickets_vendors.VENDOR_ID = vendor.VENDOR_ID LEFT JOIN common_area_tickets ON    common_area_tickets.COM_TICKET_ID = map_com_tickets_vendors.COM_TICKET_ID LEFT JOIN association_vendors ON   association_vendors.`VENDOR_ASSOCIATION_ID` = vendor.`VENDOR_ASSOCIATION_ID` LEFT JOIN vendor_services ON   vendor_services.`VENDOR_ID` = vendor.`VENDOR_ID` LEFT JOIN association_ven_ind_ser_priority ON   association_ven_ind_ser_priority.`VENDOR_ID` = vendor.`VENDOR_ID` WHERE association_vendors.`ASSOCIATION_ID` = '" + req.body.associationId + "' AND association_ven_ind_ser_priority.`PRIORITY` = 1  AND association_ven_ind_ser_priority.`ASSOCIATION_ID` = '" + req.body.associationId + "'  AND common_area_tickets.STATUS = 'Pending' AND common_area_tickets.TICKET_DATE = '" + req.body.ticket_date + "' AND (common_area_tickets.TICKET_TIME   BETWEEN '" + preTime + "' AND '" + postTime + "') GROUP BY vendor.`VENDOR_ID`")

        console.log(prioryOneVendor);
        console.log("jasjajb");
        if (prioryOneVendor.length > 0) {
            // res.send(req.body.data);
            next()
        }
        else {
            const prioryOneElVendor = await getRepository(VENDOR).query("SELECT * FROM vendor LEFT JOIN map_com_tickets_vendors ON   map_com_tickets_vendors.VENDOR_ID = vendor.VENDOR_ID LEFT JOIN common_area_tickets ON    common_area_tickets.COM_TICKET_ID = map_com_tickets_vendors.COM_TICKET_ID LEFT JOIN association_vendors ON   association_vendors.`VENDOR_ASSOCIATION_ID` = vendor.`VENDOR_ASSOCIATION_ID` LEFT JOIN vendor_services ON   vendor_services.`VENDOR_ID` = vendor.`VENDOR_ID` LEFT JOIN association_ven_ind_ser_priority ON   association_ven_ind_ser_priority.`VENDOR_ID` = vendor.`VENDOR_ID` WHERE association_vendors.`ASSOCIATION_ID` = '" + req.body.associationId + "' AND association_ven_ind_ser_priority.`PRIORITY` = 1  AND association_ven_ind_ser_priority.`ASSOCIATION_ID` = '" + req.body.associationId + "'  GROUP BY vendor.`VENDOR_ID`")

            console.log(prioryOneElVendor)

            if (prioryOneElVendor.length > 0) {
                var vendorId = prioryOneElVendor[0].VENDOR_ID;
                var vendorAssocaitonId = prioryOneElVendor[0].VENDOR_ASSOCIATION_ID;
                console.log(vendorId + "vendorId")
                //res.send(prioryOneVendor);
                const vendorObj = await vendorRep.findOne(vendorId);
                const comObj = await commonTIcketRep.findOne(req.body.comTicketId)
                console.log(comObj)
                const mapComVendorObj = new MapComTicketVendor();
                mapComVendorObj.vendor = vendorObj;
                mapComVendorObj.comTickets = comObj;
                mapComVendorObj.IS_ACCEPTED = false;
                mapComVendorObj.IS_REJECTED = false;
                mapComVendorObj.IS_ACTIVE = true;
                var samapComVendorObj = mapComVendorRep.create(mapComVendorObj);

                mapIndVendorRep.save(samapComVendorObj).then(async (mapIndObj) => {


                    if (req.body.service_types_id) {
                        req.body.serviceId

                        const interServAmountVendorAssociation = await getRepository(IND_SER_TYPES_VENDOR_ASSOCIATION)
                            .createQueryBuilder("interServAmountVendorAssociation")
                            .where("interServAmountVendorAssociation.IND_SERVICE_ID =  :indServiceId AND interServAmountVendorAssociation.IND_SERVICE_TYPE_ID = :indServiceTypeId AND VENDOR_ASSOCIATION_ID = :vendorAsociationId", {
                                indServiceId: req.body.serviceId,
                                indServiceTypeId: req.body.service_types_id,
                                vendorAsociationId: vendorAssocaitonId
                            }).getOne().then(async (data) => {
                                var amount = data.AMOUNT;


                                if (req.body.quantity) {

                                    amount = amount * req.body.quantity;
                                }

                                // var esult = await individualTicketRep.save(indObj);

                                comObj.AMOUNT = amount;

                                var esult = await commonTIcketRep.save(comObj);


                                vendorSendMail.vendoComSendMail(comObjAss.COM_TICKET_ID,comObjAss.TICKET_DESCRIPTION,vendorObj.VENDOR_EMAIL,comObjAss.TICKET_DATE,comObjAss.TICKET_TIME,comObjAss.comService.COM_SERVICE_NAME,comObjAss.serviceId.IND_SERVICE_NAME,
                                    comObjAss.indServiceTypes.TYPE,comObjAss.QUANTITY,comObjAss.AMOUNT,comObjAss.indSerPlan.PLAN,comObjAss.association.ASSOCIATION_NAME)

                                ownerComTicketSendMail.ComTicketSendMail(comObjAss.COM_TICKET_ID,
                                    comObjAss.TICKET_DESCRIPTION,comObjAss.association.EMAIL,comObjAss.TICKET_DATE,
                                    comObjAss.TICKET_TIME,comObjAss.comService.COM_SERVICE_NAME,comObjAss.serviceId.IND_SERVICE_NAME,
                                    comObjAss.indSerPlan.PLAN,comObjAss.indServiceTypes.TYPE,
                                    comObjAss.QUANTITY,comObjAss.AMOUNT,vendorObj.VENDOR_NAME)
                            }).catch((error) => {
                                console.log(error);
                            });




                    }
                    else {

                        const interServAmountVendorAssociation = await getRepository(IND_SER_TYPES_VENDOR_ASSOCIATION)
                            .createQueryBuilder("interServAmountVendorAssociation")
                            .where("interServAmountVendorAssociation.IND_SERVICE_ID =  :indServiceId AND VENDOR_ASSOCIATION_ID = :vendorAsociationId", {
                                indServiceId: req.body.serviceId,
                                vendorAsociationId: vendorAssocaitonId
                            }).getOne().then(async (data) => {
                                var amount = data.AMOUNT;


                                if (req.body.quantity) {

                                    amount = amount * req.body.quantity;
                                }

                                // var esult = await individualTicketRep.save(indObj);

                                comObj.AMOUNT = amount;

                                var esult = await commonTIcketRep.save(comObj);

                                vendorSendMail.vendoComSendMail(comObjAss.COM_TICKET_ID,comObjAss.TICKET_DESCRIPTION,vendorObj.VENDOR_EMAIL,comObjAss.TICKET_DATE,comObjAss.TICKET_TIME,comObjAss.comService.COM_SERVICE_NAME,comObjAss.serviceId.IND_SERVICE_NAME,
                                    comObjAss.indServiceTypes.TYPE,comObjAss.QUANTITY,comObjAss.AMOUNT,comObjAss.indSerPlan.PLAN,comObjAss.association.ASSOCIATION_NAME)

                                ownerComTicketSendMail.ComTicketSendMail(comObjAss.COM_TICKET_ID,
                                    comObjAss.TICKET_DESCRIPTION,comObjAss.association.EMAIL,comObjAss.TICKET_DATE,
                                    comObjAss.TICKET_TIME,comObjAss.comService.COM_SERVICE_NAME,comObjAss.serviceId.IND_SERVICE_NAME,
                                    comObjAss.indSerPlan.PLAN,comObjAss.indServiceTypes.TYPE,
                                    comObjAss.QUANTITY,comObjAss.AMOUNT,vendorObj.VENDOR_NAME)
                            }).catch((error) => {
                                console.log(error);
                            });

                    }
                    res.send(req.body);
                }).catch((error) => {
                    res.status(500).send(error)
                });
            }

            else {
                next()
            }


        }


    }

    static checkSecondPriorThirdPartyVendor = async (req: Request, res: Response, next) => {

        console.log(req.body.ticket_date)
        console.log(req.body.ticket_date)

        console.log("check first prior thirs parthey vbendr")
        const mapIndVendorRep = await getRepository(MapIndTicketVendor);
        const vendorRep = await getRepository(VENDOR);
        const commonTIcketRep = await getRepository(CommonAreaTickets);

        

        const comObjAss= await commonTIcketRep.findOne({
            where : {
               COM_TICKET_ID : req.body.comTicketId
            },
           relations : ["association","serviceId","comService","indSerPlan","indServiceTypes"]
       })
    const comObj = await commonTIcketRep.findOne({
        where : {
           COM_TICKET_ID : req.body.comTicketId
        },
       relations : ["association","serviceId","comService","indSerPlan","indServiceTypes"]
   })
        const mapComVendorRep = await getRepository(MapComTicketVendor);
        var date = "2020-03-23T" + req.body.ticket_time + ".573Z";
        var postTime = getPostTIme(date);
        var preTime = getPreTime(date);
        const prioryOneVendor = await getRepository(VENDOR).query("SELECT * FROM vendor LEFT JOIN map_com_tickets_vendors ON   map_com_tickets_vendors.VENDOR_ID = vendor.VENDOR_ID LEFT JOIN common_area_tickets ON    common_area_tickets.COM_TICKET_ID = map_com_tickets_vendors.COM_TICKET_ID LEFT JOIN association_vendors ON   association_vendors.`VENDOR_ASSOCIATION_ID` = vendor.`VENDOR_ASSOCIATION_ID` LEFT JOIN vendor_services ON   vendor_services.`VENDOR_ID` = vendor.`VENDOR_ID` LEFT JOIN association_ven_ind_ser_priority ON   association_ven_ind_ser_priority.`VENDOR_ID` = vendor.`VENDOR_ID` WHERE association_vendors.`ASSOCIATION_ID` = '" + req.body.associationId + "' AND association_ven_ind_ser_priority.`PRIORITY` = 2  AND association_ven_ind_ser_priority.`ASSOCIATION_ID` = '" + req.body.associationId + "'  AND common_area_tickets.STATUS = 'Pending' AND common_area_tickets.TICKET_DATE = '" + req.body.ticket_date + "' AND (common_area_tickets.TICKET_TIME   BETWEEN '" + preTime + "' AND '" + postTime + "') GROUP BY vendor.`VENDOR_ID`")

        console.log(prioryOneVendor);
        console.log("jasjajb");
        if (prioryOneVendor.length > 0) {
            // res.send(req.body.data);
            next()
        }
        else {
            const prioryOneElVendor = await getRepository(VENDOR).query("SELECT * FROM vendor LEFT JOIN map_com_tickets_vendors ON   map_com_tickets_vendors.VENDOR_ID = vendor.VENDOR_ID LEFT JOIN common_area_tickets ON    common_area_tickets.COM_TICKET_ID = map_com_tickets_vendors.COM_TICKET_ID LEFT JOIN association_vendors ON   association_vendors.`VENDOR_ASSOCIATION_ID` = vendor.`VENDOR_ASSOCIATION_ID` LEFT JOIN vendor_services ON   vendor_services.`VENDOR_ID` = vendor.`VENDOR_ID` LEFT JOIN association_ven_ind_ser_priority ON   association_ven_ind_ser_priority.`VENDOR_ID` = vendor.`VENDOR_ID` WHERE association_vendors.`ASSOCIATION_ID` = '" + req.body.associationId + "' AND association_ven_ind_ser_priority.`PRIORITY` = 2  AND association_ven_ind_ser_priority.`ASSOCIATION_ID` = '" + req.body.associationId + "'  GROUP BY vendor.`VENDOR_ID`")

            console.log(prioryOneElVendor)

            if (prioryOneElVendor.length > 0) {
                var vendorId = prioryOneElVendor[0].VENDOR_ID;
                var vendorAssocaitonId = prioryOneElVendor[0].VENDOR_ASSOCIATION_ID;
                console.log(vendorId + "vendorId")
                //res.send(prioryOneVendor);
                const vendorObj = await vendorRep.findOne(vendorId);
                const comObj = await commonTIcketRep.findOne(req.body.comTicketId)
                console.log(comObj)
                const mapComVendorObj = new MapComTicketVendor();
                mapComVendorObj.vendor = vendorObj;
                mapComVendorObj.comTickets = comObj;
                mapComVendorObj.IS_ACCEPTED = false;
                mapComVendorObj.IS_REJECTED = false;
                mapComVendorObj.IS_ACTIVE = true;
                var samapComVendorObj = mapComVendorRep.create(mapComVendorObj);

                mapIndVendorRep.save(samapComVendorObj).then(async (mapIndObj) => {


                    if (req.body.service_types_id) {
                        req.body.serviceId

                        const interServAmountVendorAssociation = await getRepository(IND_SER_TYPES_VENDOR_ASSOCIATION)
                            .createQueryBuilder("interServAmountVendorAssociation")
                            .where("interServAmountVendorAssociation.IND_SERVICE_ID =  :indServiceId AND interServAmountVendorAssociation.IND_SERVICE_TYPE_ID = :indServiceTypeId AND VENDOR_ASSOCIATION_ID = :vendorAsociationId", {
                                indServiceId: req.body.serviceId,
                                indServiceTypeId: req.body.service_types_id,
                                vendorAsociationId: vendorAssocaitonId
                            }).getOne().then(async (data) => {
                                var amount = data.AMOUNT;


                                if (req.body.quantity) {

                                    amount = amount * req.body.quantity;
                                }

                                // var esult = await individualTicketRep.save(indObj);

                                comObj.AMOUNT = amount;

                                var esult = await commonTIcketRep.save(comObj);

                                vendorSendMail.vendoComSendMail(comObjAss.COM_TICKET_ID,comObjAss.TICKET_DESCRIPTION,vendorObj.VENDOR_EMAIL,comObjAss.TICKET_DATE,comObjAss.TICKET_TIME,comObjAss.comService.COM_SERVICE_NAME,comObjAss.serviceId.IND_SERVICE_NAME,
                                    comObjAss.indServiceTypes.TYPE,comObjAss.QUANTITY,comObjAss.AMOUNT,comObjAss.indSerPlan.PLAN,comObjAss.association.ASSOCIATION_NAME)

                                ownerComTicketSendMail.ComTicketSendMail(comObjAss.COM_TICKET_ID,
                                    comObjAss.TICKET_DESCRIPTION,comObjAss.association.EMAIL,comObjAss.TICKET_DATE,
                                    comObjAss.TICKET_TIME,comObjAss.comService.COM_SERVICE_NAME,comObjAss.serviceId.IND_SERVICE_NAME,
                                    comObjAss.indSerPlan.PLAN,comObjAss.indServiceTypes.TYPE,
                                    comObjAss.QUANTITY,comObjAss.AMOUNT,vendorObj.VENDOR_NAME)
                            }).catch((error) => {
                                console.log(error);
                            });




                    }
                    else {

                        const interServAmountVendorAssociation = await getRepository(IND_SER_TYPES_VENDOR_ASSOCIATION)
                            .createQueryBuilder("interServAmountVendorAssociation")
                            .where("interServAmountVendorAssociation.IND_SERVICE_ID =  :indServiceId AND VENDOR_ASSOCIATION_ID = :vendorAsociationId", {
                                indServiceId: req.body.serviceId,
                                vendorAsociationId: vendorAssocaitonId
                            }).getOne().then(async (data) => {
                                var amount = data.AMOUNT;


                                if (req.body.quantity) {

                                    amount = amount * req.body.quantity;
                                }

                                // var esult = await individualTicketRep.save(indObj);

                                comObj.AMOUNT = amount;

                                var esult = await commonTIcketRep.save(comObj);

                                vendorSendMail.vendoComSendMail(comObjAss.COM_TICKET_ID,comObjAss.TICKET_DESCRIPTION,vendorObj.VENDOR_EMAIL,comObjAss.TICKET_DATE,comObjAss.TICKET_TIME,comObjAss.comService.COM_SERVICE_NAME,comObjAss.serviceId.IND_SERVICE_NAME,
                                    comObjAss.indServiceTypes.TYPE,comObjAss.QUANTITY,comObjAss.AMOUNT,comObjAss.indSerPlan.PLAN,comObjAss.association.ASSOCIATION_NAME)
                                ownerComTicketSendMail.ComTicketSendMail(comObjAss.COM_TICKET_ID,
                                    comObjAss.TICKET_DESCRIPTION,comObjAss.association.EMAIL,comObjAss.TICKET_DATE,
                                    comObjAss.TICKET_TIME,comObjAss.comService.COM_SERVICE_NAME,comObjAss.serviceId.IND_SERVICE_NAME,
                                    comObjAss.indSerPlan.PLAN,comObjAss.indServiceTypes.TYPE,
                                    comObjAss.QUANTITY,comObjAss.AMOUNT,vendorObj.VENDOR_NAME)
                            }).catch((error) => {
                                console.log(error);
                            });

                    }
                    res.send(req.body);
                }).catch((error) => {
                    res.status(500).send(error)
                });
            }

            else {
                next()
            }


        }


    }

    static checkThirdPriorThirdPartyVendor = async (req: Request, res: Response, next) => {

        console.log(req.body.ticket_date)
        console.log(req.body.ticket_date)

        console.log("check first prior thirs parthey vbendr")
        const mapIndVendorRep = await getRepository(MapIndTicketVendor);
        const vendorRep = await getRepository(VENDOR);
        const commonTIcketRep = await getRepository(CommonAreaTickets);

        
        const comObjAss= await commonTIcketRep.findOne({
            where : {
               COM_TICKET_ID : req.body.comTicketId
            },
           relations : ["association","serviceId","comService","indSerPlan","indServiceTypes"]
       })
        const mapComVendorRep = await getRepository(MapComTicketVendor);
        var date = "2020-03-23T" + req.body.ticket_time + ".573Z";
        var postTime = getPostTIme(date);
        var preTime = getPreTime(date);
        const prioryOneVendor = await getRepository(VENDOR).query("SELECT * FROM vendor LEFT JOIN map_com_tickets_vendors ON   map_com_tickets_vendors.VENDOR_ID = vendor.VENDOR_ID LEFT JOIN common_area_tickets ON    common_area_tickets.COM_TICKET_ID = map_com_tickets_vendors.COM_TICKET_ID LEFT JOIN association_vendors ON   association_vendors.`VENDOR_ASSOCIATION_ID` = vendor.`VENDOR_ASSOCIATION_ID` LEFT JOIN vendor_services ON   vendor_services.`VENDOR_ID` = vendor.`VENDOR_ID` LEFT JOIN association_ven_ind_ser_priority ON   association_ven_ind_ser_priority.`VENDOR_ID` = vendor.`VENDOR_ID` WHERE association_vendors.`ASSOCIATION_ID` = '" + req.body.associationId + "' AND association_ven_ind_ser_priority.`PRIORITY` = 3  AND association_ven_ind_ser_priority.`ASSOCIATION_ID` = '" + req.body.associationId + "'  AND common_area_tickets.STATUS = 'Pending' AND common_area_tickets.TICKET_DATE = '" + req.body.ticket_date + "' AND (common_area_tickets.TICKET_TIME   BETWEEN '" + preTime + "' AND '" + postTime + "') GROUP BY vendor.`VENDOR_ID`")

        console.log(prioryOneVendor);
        console.log("jasjajb");
        if (prioryOneVendor.length > 0) {
            // res.send(req.body.data);
            next()
        }
        else {
            const prioryOneElVendor = await getRepository(VENDOR).query("SELECT * FROM vendor LEFT JOIN map_com_tickets_vendors ON   map_com_tickets_vendors.VENDOR_ID = vendor.VENDOR_ID LEFT JOIN common_area_tickets ON    common_area_tickets.COM_TICKET_ID = map_com_tickets_vendors.COM_TICKET_ID LEFT JOIN association_vendors ON   association_vendors.`VENDOR_ASSOCIATION_ID` = vendor.`VENDOR_ASSOCIATION_ID` LEFT JOIN vendor_services ON   vendor_services.`VENDOR_ID` = vendor.`VENDOR_ID` LEFT JOIN association_ven_ind_ser_priority ON   association_ven_ind_ser_priority.`VENDOR_ID` = vendor.`VENDOR_ID` WHERE association_vendors.`ASSOCIATION_ID` = '" + req.body.associationId + "' AND association_ven_ind_ser_priority.`PRIORITY` = 3  AND association_ven_ind_ser_priority.`ASSOCIATION_ID` = '" + req.body.associationId + "'  GROUP BY vendor.`VENDOR_ID`")

            console.log(prioryOneElVendor)

            if (prioryOneElVendor.length > 0) {
                var vendorId = prioryOneElVendor[0].VENDOR_ID;
                var vendorAssocaitonId = prioryOneElVendor[0].VENDOR_ASSOCIATION_ID;
                console.log(vendorId + "vendorId")
                //res.send(prioryOneVendor);
                const vendorObj = await vendorRep.findOne(vendorId);
                const comObj = await commonTIcketRep.findOne(req.body.comTicketId)
                console.log(comObj)
                const mapComVendorObj = new MapComTicketVendor();
                mapComVendorObj.vendor = vendorObj;
                mapComVendorObj.comTickets = comObj;
                mapComVendorObj.IS_ACCEPTED = false;
                mapComVendorObj.IS_REJECTED = false;
                mapComVendorObj.IS_ACTIVE = true;
                var samapComVendorObj = mapComVendorRep.create(mapComVendorObj);

                mapIndVendorRep.save(samapComVendorObj).then(async (mapIndObj) => {


                    if (req.body.service_types_id) {
                        req.body.serviceId

                        const interServAmountVendorAssociation = await getRepository(IND_SER_TYPES_VENDOR_ASSOCIATION)
                            .createQueryBuilder("interServAmountVendorAssociation")
                            .where("interServAmountVendorAssociation.IND_SERVICE_ID =  :indServiceId AND interServAmountVendorAssociation.IND_SERVICE_TYPE_ID = :indServiceTypeId AND VENDOR_ASSOCIATION_ID = :vendorAsociationId", {
                                indServiceId: req.body.serviceId,
                                indServiceTypeId: req.body.service_types_id,
                                vendorAsociationId: vendorAssocaitonId
                            }).getOne().then(async (data) => {
                                var amount = data.AMOUNT;


                                if (req.body.quantity) {

                                    amount = amount * req.body.quantity;
                                }

                                // var esult = await individualTicketRep.save(indObj);

                                comObj.AMOUNT = amount;

                                var esult = await commonTIcketRep.save(comObj);

                                vendorSendMail.vendoComSendMail(comObjAss.COM_TICKET_ID,comObjAss.TICKET_DESCRIPTION,vendorObj.VENDOR_EMAIL,comObjAss.TICKET_DATE,comObjAss.TICKET_TIME,comObjAss.comService.COM_SERVICE_NAME,comObjAss.serviceId.IND_SERVICE_NAME,
                                    comObjAss.indServiceTypes.TYPE,comObjAss.QUANTITY,comObjAss.AMOUNT,comObjAss.indSerPlan.PLAN,comObjAss.association.ASSOCIATION_NAME)

                                ownerComTicketSendMail.ComTicketSendMail(comObjAss.COM_TICKET_ID,
                                    comObjAss.TICKET_DESCRIPTION,comObjAss.association.EMAIL,comObjAss.TICKET_DATE,
                                    comObjAss.TICKET_TIME,comObjAss.comService.COM_SERVICE_NAME,comObjAss.serviceId.IND_SERVICE_NAME,
                                    comObjAss.indSerPlan.PLAN,comObjAss.indServiceTypes.TYPE,
                                    comObjAss.QUANTITY,comObjAss.AMOUNT,vendorObj.VENDOR_NAME)
                            }).catch((error) => {
                                console.log(error);
                            });




                    }
                    else {

                        const interServAmountVendorAssociation = await getRepository(IND_SER_TYPES_VENDOR_ASSOCIATION)
                            .createQueryBuilder("interServAmountVendorAssociation")
                            .where("interServAmountVendorAssociation.IND_SERVICE_ID =  :indServiceId AND VENDOR_ASSOCIATION_ID = :vendorAsociationId", {
                                indServiceId: req.body.serviceId,
                                vendorAsociationId: vendorAssocaitonId
                            }).getOne().then(async (data) => {
                                var amount = data.AMOUNT;


                                if (req.body.quantity) {

                                    amount = amount * req.body.quantity;
                                }

                                // var esult = await individualTicketRep.save(indObj);

                                comObj.AMOUNT = amount;

                                var esult = await commonTIcketRep.save(comObj);

                                vendorSendMail.vendoComSendMail(comObjAss.COM_TICKET_ID,comObjAss.TICKET_DESCRIPTION,vendorObj.VENDOR_EMAIL,comObjAss.TICKET_DATE,comObjAss.TICKET_TIME,comObjAss.comService.COM_SERVICE_NAME,comObjAss.serviceId.IND_SERVICE_NAME,
                                    comObjAss.indServiceTypes.TYPE,comObjAss.QUANTITY,comObjAss.AMOUNT,comObjAss.indSerPlan.PLAN,comObjAss.association.ASSOCIATION_NAME)

                                ownerComTicketSendMail.ComTicketSendMail(comObjAss.COM_TICKET_ID,
                                    comObjAss.TICKET_DESCRIPTION,comObjAss.association.EMAIL,comObjAss.TICKET_DATE,
                                    comObjAss.TICKET_TIME,comObjAss.comService.COM_SERVICE_NAME,comObjAss.serviceId.IND_SERVICE_NAME,
                                    comObjAss.indSerPlan.PLAN,comObjAss.indServiceTypes.TYPE,
                                    comObjAss.QUANTITY,comObjAss.AMOUNT,vendorObj.VENDOR_NAME)
                            }).catch((error) => {
                                console.log(error);
                            });

                    }
                    res.send(req.body);
                }).catch((error) => {
                    res.status(500).send(error)
                });
            }

            else {
                next()
            }


        }


    }


    static checkFourthPriorThirdPartyVendor = async (req: Request, res: Response, next) => {

        console.log(req.body.ticket_date)
        console.log(req.body.ticket_date)

        console.log("check first prior thirs parthey vbendr")
        const mapIndVendorRep = await getRepository(MapIndTicketVendor);
        const vendorRep = await getRepository(VENDOR);
        const commonTIcketRep = await getRepository(CommonAreaTickets);

        const comObjAss= await commonTIcketRep.findOne({
            where : {
               COM_TICKET_ID : req.body.comTicketId
            },
           relations : ["association","serviceId","comService","indSerPlan","indServiceTypes"]
       })
        const mapComVendorRep = await getRepository(MapComTicketVendor);
        var date = "2020-03-23T" + req.body.ticket_time + ".573Z";
        var postTime = getPostTIme(date);
        var preTime = getPreTime(date);
        const prioryOneVendor = await getRepository(VENDOR).query("SELECT * FROM vendor LEFT JOIN map_com_tickets_vendors ON   map_com_tickets_vendors.VENDOR_ID = vendor.VENDOR_ID LEFT JOIN common_area_tickets ON    common_area_tickets.COM_TICKET_ID = map_com_tickets_vendors.COM_TICKET_ID LEFT JOIN association_vendors ON   association_vendors.`VENDOR_ASSOCIATION_ID` = vendor.`VENDOR_ASSOCIATION_ID` LEFT JOIN vendor_services ON   vendor_services.`VENDOR_ID` = vendor.`VENDOR_ID` LEFT JOIN association_ven_ind_ser_priority ON   association_ven_ind_ser_priority.`VENDOR_ID` = vendor.`VENDOR_ID` WHERE association_vendors.`ASSOCIATION_ID` = '" + req.body.associationId + "' AND association_ven_ind_ser_priority.`PRIORITY` = 4  AND association_ven_ind_ser_priority.`ASSOCIATION_ID` = '" + req.body.associationId + "'  AND common_area_tickets.STATUS = 'Pending' AND common_area_tickets.TICKET_DATE = '" + req.body.ticket_date + "' AND (common_area_tickets.TICKET_TIME   BETWEEN '" + preTime + "' AND '" + postTime + "') GROUP BY vendor.`VENDOR_ID`")

        console.log(prioryOneVendor);
        console.log("jasjajb");
        if (prioryOneVendor.length > 0) {
            // res.send(req.body.data);
            next()
        }
        else {
            const prioryOneElVendor = await getRepository(VENDOR).query("SELECT * FROM vendor LEFT JOIN map_com_tickets_vendors ON   map_com_tickets_vendors.VENDOR_ID = vendor.VENDOR_ID LEFT JOIN common_area_tickets ON    common_area_tickets.COM_TICKET_ID = map_com_tickets_vendors.COM_TICKET_ID LEFT JOIN association_vendors ON   association_vendors.`VENDOR_ASSOCIATION_ID` = vendor.`VENDOR_ASSOCIATION_ID` LEFT JOIN vendor_services ON   vendor_services.`VENDOR_ID` = vendor.`VENDOR_ID` LEFT JOIN association_ven_ind_ser_priority ON   association_ven_ind_ser_priority.`VENDOR_ID` = vendor.`VENDOR_ID` WHERE association_vendors.`ASSOCIATION_ID` = '" + req.body.associationId + "' AND association_ven_ind_ser_priority.`PRIORITY` = 4  AND association_ven_ind_ser_priority.`ASSOCIATION_ID` = '" + req.body.associationId + "'  GROUP BY vendor.`VENDOR_ID`")

            console.log(prioryOneElVendor)

            if (prioryOneElVendor.length > 0) {
                var vendorId = prioryOneElVendor[0].VENDOR_ID;
                var vendorAssocaitonId = prioryOneElVendor[0].VENDOR_ASSOCIATION_ID;
                console.log(vendorId + "vendorId")
                //res.send(prioryOneVendor);
                const vendorObj = await vendorRep.findOne(vendorId);
                const comObj = await commonTIcketRep.findOne(req.body.comTicketId)
                console.log(comObj)
                const mapComVendorObj = new MapComTicketVendor();
                mapComVendorObj.vendor = vendorObj;
                mapComVendorObj.comTickets = comObj;
                mapComVendorObj.IS_ACCEPTED = false;
                mapComVendorObj.IS_REJECTED = false;
                mapComVendorObj.IS_ACTIVE = true;
                var samapComVendorObj = mapComVendorRep.create(mapComVendorObj);

                mapIndVendorRep.save(samapComVendorObj).then(async (mapIndObj) => {


                    if (req.body.service_types_id) {
                        req.body.serviceId

                        const interServAmountVendorAssociation = await getRepository(IND_SER_TYPES_VENDOR_ASSOCIATION)
                            .createQueryBuilder("interServAmountVendorAssociation")
                            .where("interServAmountVendorAssociation.IND_SERVICE_ID =  :indServiceId AND interServAmountVendorAssociation.IND_SERVICE_TYPE_ID = :indServiceTypeId AND VENDOR_ASSOCIATION_ID = :vendorAsociationId", {
                                indServiceId: req.body.serviceId,
                                indServiceTypeId: req.body.service_types_id,
                                vendorAsociationId: vendorAssocaitonId
                            }).getOne().then(async (data) => {
                                var amount = data.AMOUNT;


                                if (req.body.quantity) {

                                    amount = amount * req.body.quantity;
                                }

                                // var esult = await individualTicketRep.save(indObj);

                                comObj.AMOUNT = amount;

                                var esult = await commonTIcketRep.save(comObj);

                                vendorSendMail.vendoComSendMail(comObjAss.COM_TICKET_ID,comObjAss.TICKET_DESCRIPTION,vendorObj.VENDOR_EMAIL,comObjAss.TICKET_DATE,comObjAss.TICKET_TIME,comObjAss.comService.COM_SERVICE_NAME,comObjAss.serviceId.IND_SERVICE_NAME,
                                    comObjAss.indServiceTypes.TYPE,comObjAss.QUANTITY,comObjAss.AMOUNT,comObjAss.indSerPlan.PLAN,comObjAss.association.ASSOCIATION_NAME)

                                ownerComTicketSendMail.ComTicketSendMail(comObjAss.COM_TICKET_ID,
                                    comObjAss.TICKET_DESCRIPTION,comObjAss.association.EMAIL,comObjAss.TICKET_DATE,
                                    comObjAss.TICKET_TIME,comObjAss.comService.COM_SERVICE_NAME,comObjAss.serviceId.IND_SERVICE_NAME,
                                    comObjAss.indSerPlan.PLAN,comObjAss.indServiceTypes.TYPE,
                                    comObjAss.QUANTITY,comObjAss.AMOUNT,vendorObj.VENDOR_NAME)
                            }).catch((error) => {
                                console.log(error);
                            });




                    }
                    else {

                        const interServAmountVendorAssociation = await getRepository(IND_SER_TYPES_VENDOR_ASSOCIATION)
                            .createQueryBuilder("interServAmountVendorAssociation")
                            .where("interServAmountVendorAssociation.IND_SERVICE_ID =  :indServiceId AND VENDOR_ASSOCIATION_ID = :vendorAsociationId", {
                                indServiceId: req.body.serviceId,
                                vendorAsociationId: vendorAssocaitonId
                            }).getOne().then(async (data) => {
                                var amount = data.AMOUNT;


                                if (req.body.quantity) {

                                    amount = amount * req.body.quantity;
                                }

                                // var esult = await individualTicketRep.save(indObj);

                                comObj.AMOUNT = amount;

                                var esult = await commonTIcketRep.save(comObj);


                                vendorSendMail.vendoComSendMail(comObjAss.COM_TICKET_ID,comObjAss.TICKET_DESCRIPTION,vendorObj.VENDOR_EMAIL,comObjAss.TICKET_DATE,comObjAss.TICKET_TIME,comObjAss.comService.COM_SERVICE_NAME,comObjAss.serviceId.IND_SERVICE_NAME,
                                    comObjAss.indServiceTypes.TYPE,comObjAss.QUANTITY,comObjAss.AMOUNT,comObjAss.indSerPlan.PLAN,comObjAss.association.ASSOCIATION_NAME)

                                ownerComTicketSendMail.ComTicketSendMail(comObjAss.COM_TICKET_ID,
                                    comObjAss.TICKET_DESCRIPTION,comObjAss.association.EMAIL,comObjAss.TICKET_DATE,
                                    comObjAss.TICKET_TIME,comObjAss.comService.COM_SERVICE_NAME,comObjAss.serviceId.IND_SERVICE_NAME,
                                    comObjAss.indSerPlan.PLAN,comObjAss.indServiceTypes.TYPE,
                                    comObjAss.QUANTITY,comObjAss.AMOUNT,vendorObj.VENDOR_NAME)
                            }).catch((error) => {
                                console.log(error);
                            });

                    }
                    res.send(req.body);
                }).catch((error) => {
                    res.status(500).send(error)
                });
            }

            else {
                next()
            }


        }


    }


    static checkFifthPriorThirdPartyVendor = async (req: Request, res: Response, next) => {

        console.log(req.body.ticket_date)
        console.log(req.body.ticket_date)

        console.log("check first prior thirs parthey vbendr")
        const mapIndVendorRep = await getRepository(MapIndTicketVendor);
        const vendorRep = await getRepository(VENDOR);
        const commonTIcketRep = await getRepository(CommonAreaTickets);

        const comObjAss= await commonTIcketRep.findOne({
            where : {
               COM_TICKET_ID : req.body.comTicketId
            },
           relations : ["association","serviceId","comService","indSerPlan","indServiceTypes"]
       })
        const mapComVendorRep = await getRepository(MapComTicketVendor);
        var date = "2020-03-23T" + req.body.ticket_time + ".573Z";
        var postTime = getPostTIme(date);
        var preTime = getPreTime(date);
        const prioryOneVendor = await getRepository(VENDOR).query("SELECT * FROM vendor LEFT JOIN map_com_tickets_vendors ON   map_com_tickets_vendors.VENDOR_ID = vendor.VENDOR_ID LEFT JOIN common_area_tickets ON    common_area_tickets.COM_TICKET_ID = map_com_tickets_vendors.COM_TICKET_ID LEFT JOIN association_vendors ON   association_vendors.`VENDOR_ASSOCIATION_ID` = vendor.`VENDOR_ASSOCIATION_ID` LEFT JOIN vendor_services ON   vendor_services.`VENDOR_ID` = vendor.`VENDOR_ID` LEFT JOIN association_ven_ind_ser_priority ON   association_ven_ind_ser_priority.`VENDOR_ID` = vendor.`VENDOR_ID` WHERE association_vendors.`ASSOCIATION_ID` = '" + req.body.associationId + "' AND association_ven_ind_ser_priority.`PRIORITY` = 5  AND association_ven_ind_ser_priority.`ASSOCIATION_ID` = '" + req.body.associationId + "'  AND common_area_tickets.STATUS = 'Pending' AND common_area_tickets.TICKET_DATE = '" + req.body.ticket_date + "' AND (common_area_tickets.TICKET_TIME   BETWEEN '" + preTime + "' AND '" + postTime + "') GROUP BY vendor.`VENDOR_ID`")

        console.log(prioryOneVendor);
        console.log("jasjajb");
        if (prioryOneVendor.length > 0) {

            ownerComTicketSendMail.ComTicketSendMail(comObjAss.COM_TICKET_ID,
                comObjAss.TICKET_DESCRIPTION,comObjAss.association.EMAIL,comObjAss.TICKET_DATE,
                comObjAss.TICKET_TIME,comObjAss.comService.COM_SERVICE_NAME,comObjAss.serviceId.IND_SERVICE_NAME,
                comObjAss.indSerPlan.PLAN,comObjAss.indServiceTypes.TYPE,
                comObjAss.QUANTITY,comObjAss.AMOUNT,'Not Assigned')
            res.send(req.body);
            //next()
        }
        else {
            const prioryOneElVendor = await getRepository(VENDOR).query("SELECT * FROM vendor LEFT JOIN map_com_tickets_vendors ON   map_com_tickets_vendors.VENDOR_ID = vendor.VENDOR_ID LEFT JOIN common_area_tickets ON    common_area_tickets.COM_TICKET_ID = map_com_tickets_vendors.COM_TICKET_ID LEFT JOIN association_vendors ON   association_vendors.`VENDOR_ASSOCIATION_ID` = vendor.`VENDOR_ASSOCIATION_ID` LEFT JOIN vendor_services ON   vendor_services.`VENDOR_ID` = vendor.`VENDOR_ID` LEFT JOIN association_ven_ind_ser_priority ON   association_ven_ind_ser_priority.`VENDOR_ID` = vendor.`VENDOR_ID` WHERE association_vendors.`ASSOCIATION_ID` = '" + req.body.associationId + "' AND association_ven_ind_ser_priority.`PRIORITY` = 5  AND association_ven_ind_ser_priority.`ASSOCIATION_ID` = '" + req.body.associationId + "'  GROUP BY vendor.`VENDOR_ID`")

            console.log(prioryOneElVendor)

            if (prioryOneElVendor.length > 0) {
                var vendorId = prioryOneElVendor[0].VENDOR_ID;
                var vendorAssocaitonId = prioryOneElVendor[0].VENDOR_ASSOCIATION_ID;
                console.log(vendorId + "vendorId")
                //res.send(prioryOneVendor);
                const vendorObj = await vendorRep.findOne(vendorId);
                const comObj = await commonTIcketRep.findOne(req.body.comTicketId)
                console.log(comObj)
                const mapComVendorObj = new MapComTicketVendor();
                mapComVendorObj.vendor = vendorObj;
                mapComVendorObj.comTickets = comObj;
                mapComVendorObj.IS_ACCEPTED = false;
                mapComVendorObj.IS_REJECTED = false;
                mapComVendorObj.IS_ACTIVE = true;
                var samapComVendorObj = mapComVendorRep.create(mapComVendorObj);

                mapIndVendorRep.save(samapComVendorObj).then(async (mapIndObj) => {


                    if (req.body.service_types_id) {
                        req.body.serviceId

                        const interServAmountVendorAssociation = await getRepository(IND_SER_TYPES_VENDOR_ASSOCIATION)
                            .createQueryBuilder("interServAmountVendorAssociation")
                            .where("interServAmountVendorAssociation.IND_SERVICE_ID =  :indServiceId AND interServAmountVendorAssociation.IND_SERVICE_TYPE_ID = :indServiceTypeId AND VENDOR_ASSOCIATION_ID = :vendorAsociationId", {
                                indServiceId: req.body.serviceId,
                                indServiceTypeId: req.body.service_types_id,
                                vendorAsociationId: vendorAssocaitonId
                            }).getOne().then(async (data) => {
                                var amount = data.AMOUNT;


                                if (req.body.quantity) {

                                    amount = amount * req.body.quantity;
                                }

                                // var esult = await individualTicketRep.save(indObj);

                                comObj.AMOUNT = amount;

                                var esult = await commonTIcketRep.save(comObj);

                                vendorSendMail.vendoComSendMail(comObjAss.COM_TICKET_ID,comObjAss.TICKET_DESCRIPTION,vendorObj.VENDOR_EMAIL,comObjAss.TICKET_DATE,comObjAss.TICKET_TIME,comObjAss.comService.COM_SERVICE_NAME,comObjAss.serviceId.IND_SERVICE_NAME,
                                    comObjAss.indServiceTypes.TYPE,comObjAss.QUANTITY,comObjAss.AMOUNT,comObjAss.indSerPlan.PLAN,comObjAss.association.ASSOCIATION_NAME)
                                ownerComTicketSendMail.ComTicketSendMail(comObjAss.COM_TICKET_ID,
                                    comObjAss.TICKET_DESCRIPTION,comObjAss.association.EMAIL,comObjAss.TICKET_DATE,
                                    comObjAss.TICKET_TIME,comObjAss.comService.COM_SERVICE_NAME,comObjAss.serviceId.IND_SERVICE_NAME,
                                    comObjAss.indSerPlan.PLAN,comObjAss.indServiceTypes.TYPE,
                                    comObjAss.QUANTITY,comObjAss.AMOUNT,vendorObj.VENDOR_NAME)
                            }).catch((error) => {
                                console.log(error);
                            });




                    }
                    else {

                        const interServAmountVendorAssociation = await getRepository(IND_SER_TYPES_VENDOR_ASSOCIATION)
                            .createQueryBuilder("interServAmountVendorAssociation")
                            .where("interServAmountVendorAssociation.IND_SERVICE_ID =  :indServiceId AND VENDOR_ASSOCIATION_ID = :vendorAsociationId", {
                                indServiceId: req.body.serviceId,
                                vendorAsociationId: vendorAssocaitonId
                            }).getOne().then(async (data) => {
                                var amount = data.AMOUNT;


                                if (req.body.quantity) {

                                    amount = amount * req.body.quantity;
                                }

                                // var esult = await individualTicketRep.save(indObj);

                                comObj.AMOUNT = amount;

                                var esult = await commonTIcketRep.save(comObj);

                                vendorSendMail.vendoComSendMail(comObjAss.COM_TICKET_ID,comObjAss.TICKET_DESCRIPTION,vendorObj.VENDOR_EMAIL,comObjAss.TICKET_DATE,comObjAss.TICKET_TIME,comObjAss.comService.COM_SERVICE_NAME,comObjAss.serviceId.IND_SERVICE_NAME,
                                    comObjAss.indServiceTypes.TYPE,comObjAss.QUANTITY,comObjAss.AMOUNT,comObjAss.indSerPlan.PLAN,comObjAss.association.ASSOCIATION_NAME)

                                ownerComTicketSendMail.ComTicketSendMail(comObjAss.COM_TICKET_ID,
                                    comObjAss.TICKET_DESCRIPTION,comObjAss.association.EMAIL,comObjAss.TICKET_DATE,
                                    comObjAss.TICKET_TIME,comObjAss.comService.COM_SERVICE_NAME,comObjAss.serviceId.IND_SERVICE_NAME,
                                    comObjAss.indSerPlan.PLAN,comObjAss.indServiceTypes.TYPE,
                                    comObjAss.QUANTITY,comObjAss.AMOUNT,vendorObj.VENDOR_NAME)
                            }).catch((error) => {
                                console.log(error);
                            });

                    }
                    res.send(req.body);
                }).catch((error) => {
                    res.status(500).send(error)
                });
            }

            else {
                res.send(req.body);
            }


        }


    }










}
function getPostTIme(date) {
    var dt = new Date(date);
    dt.setMinutes(dt.getMinutes() + 30).toString();
    var addedDate = dt.toISOString();
    var splittedTIme = addedDate.split('T');
    var newAddedDtime = splittedTIme[1];
    var postTimeSplit = newAddedDtime.split('.');
    var postTIme = postTimeSplit[0];
    return postTIme;
}
function getPreTime(date) {
    var dt2 = new Date(date);
    dt2.setMinutes(dt2.getMinutes() - 30).toString();
    var RemovedDate = dt2.toISOString();
    var spllited2TIme = RemovedDate.split('T');
    var newRemovbedDtime = spllited2TIme[1];
    var preTimeSplit = newRemovbedDtime.split('.');
    var preTIme = preTimeSplit[0];

    return preTIme;


}


export default CommonAreaTicketController;