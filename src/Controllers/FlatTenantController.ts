import { Request, Response } from "express";
import * as jwt from "jsonwebtoken";
import { getRepository } from "typeorm";
import { FlatTenants } from "../entity/FlatTenants";
import { Appartment } from "../entity/AppartmentEntity";
import { ASSOCIATION } from "../entity/Associations";
import { APPARTMENT_CHILD_ENTITIES } from "../entity/AppartmentChildEntites";
import { APPARTMENT_GRAND_CHILD_ENTITIES } from "../entity/AppartmentGrandChildEntity";
import { AppUserDetails } from "../entity/AppUserDetails";
import { Roles } from "../entity/Roles";
import { UserRoles } from "../entity/UserRoles";
import FlatTenantSendMail from "../Mails/FlatTenantsSendMail";
import { error } from "util";



class FlatTenantController {

  static tenantAdd = async (req: Request, res: Response, next) => {
    const rolesRepository = getRepository(Roles);
    const userRolesRepository = getRepository(UserRoles);
    const appUserDetailsRepository = getRepository(AppUserDetails);
    const flatenantRepository = getRepository(FlatTenants);
    const appartmentRepository = getRepository(Appartment);
    const associationRepository = getRepository(ASSOCIATION);
    const appartmentChildRepository = getRepository(APPARTMENT_CHILD_ENTITIES);
    const appartmentGrandChildRepository = getRepository(APPARTMENT_GRAND_CHILD_ENTITIES);


    const flatTenantRepositoryObject = await flatenantRepository
      .createQueryBuilder("flatTenant")
     
      .where("flatTenant.associationEntity = :associationEntity AND flatTenant.associationChildEntity = :associationChildEntity AND flatTenant.associationGrandChildEntity = :associationGrandChildEntity ", {
        associationEntity: req.body.associationId,
        associationChildEntity: req.body.appartmentChildId,
        associationGrandChildEntity: req.body.appartmentGrandChildId,
      }).getOne();
    console.log("flatTenantRepository: ");
    console.log(flatTenantRepositoryObject);

    const flatTenantEmailRepositoryObject = await flatenantRepository
    .createQueryBuilder("flatTenant")
   
    .where("flatTenant.TENANT_EMAIL = :email", {
      email:  req.body.tenant_email,
  
    }).getOne();

console.log( req.body.tenant_email)
console.log(flatTenantEmailRepositoryObject)

    if (flatTenantRepositoryObject) {
      res.json({
        msg: "Tenant Already Exists",
        data: flatTenantRepositoryObject
      });
    
    } 
    else if (flatTenantEmailRepositoryObject){
      res.json({
        msg: "Tenant Email Exists",
        data: flatTenantEmailRepositoryObject
    });
    }
    
    else {
      const appartmentObj = await appartmentRepository.findOne(req.body.appartmentId);
      const associationObj = await associationRepository.findOne(req.body.associationId);
      const appartmentChildObj = await appartmentChildRepository.findOne(req.body.appartmentChildId);
      const appartmentGrandChildObj = await appartmentGrandChildRepository.findOne(req.body.appartmentGrandChildId);
      // const planObj = await plansRepository.findOne(req.body.id);
      // const mergedUser = indServiceRepository.merge(user,req.body);
      const flatTenant = new FlatTenants();
      flatTenant.TENANT_FIRST_NAME = req.body.tenant_first_name;
      flatTenant.TENANT_LAST_NAME = req.body.tenant_last_name;
      flatTenant.TENANT_EMAIL = req.body.tenant_email;
      flatTenant.TENANT_MOBILE_NUMBER = req.body.tenant_mobile_number;
      var isFinancialAcceess ;
            if(req.body.is_financial_access == 'true'){
              isFinancialAcceess = true
            }
            else {
              isFinancialAcceess = false
            }
      flatTenant.IS_FINANCIAL_ACCEESS = isFinancialAcceess;
      flatTenant.TENANT_PIC = req.body.tenant_pic;
      flatTenant.TENANT_ADDRESS = req.body.tenant_address;
      flatTenant.IS_ACTIVE = true;
      flatTenant.appartmentEntiy = appartmentObj;
      flatTenant.associationEntity = associationObj;
      flatTenant.associationChildEntity = appartmentChildObj;
      flatTenant.associationGrandChildEntity = appartmentGrandChildObj;
      flatTenant.CREATED_BY = req.body.created_by;
      flatTenant.UPDATED_BY = req.body.updated_by;
      const flatTenantObj = await flatenantRepository.create(flatTenant);

      flatenantRepository.save(flatTenantObj).then((data) => {
        var flatTenantResObj = data;
        var assTring = req.body.tenant_first_name.charAt(0);
        var appTring = req.body.tenant_last_name.charAt(0);
        var username = data.TENANT_ID + '' + appTring + '' + assTring + '' + req.body.associationId + '' + req.body.appartmentChildId + '' + req.body.appartmentGrandChildId + '';
        var password = Math.floor(100000000 + Math.random() * 900000000) + '';
        const appUser = new AppUserDetails();

        appUser.USER_NAME = username;
        appUser.PASSWORD = password;
        appUser.flatTenant = data;
        appUser.IS_ACTIVE = true;
        const appUserObj = appUserDetailsRepository.create(appUser);

        appUserDetailsRepository.save(appUserObj).then((async data => {


          const appUsrUpobj = await appUserDetailsRepository.findOne(data.APP_USER_ID);
          appUsrUpobj.USER_NAME = data.USER_NAME + '' + data.APP_USER_ID + '';
          appUserDetailsRepository.save(appUsrUpobj);

          var appUserObjres = data;
          const rolesObj = await rolesRepository.findOne(5);

          const userRoleObj = new UserRoles();
          userRoleObj.appUserInfo = appUserObj;
          userRoleObj.role = rolesObj;
          const UserRoleObj = await userRolesRepository.create(userRoleObj);
          userRolesRepository.save(UserRoleObj).then((data) => {
            FlatTenantSendMail.flatTenantEnrollmentSendMail(req.body.tenant_first_name,username, password, req.body.tenant_email);
            // res.status(200).json({
            //   flatTenant: flatTenantResObj,
            //   data: appUserObjres,
            //   roleObj: data,
            // })
            req.body.id = flatTenantResObj.TENANT_ID;
            req.body.flatTenantResObj = flatTenantResObj;
            req.body.appUserObjres = appUserObjres;
            req.body.data = data;
            next();

          }).catch((err) => {
            res.status(500).send(err);
          });

        })).catch((err) => {
          res.status(500).send(err);
        });

        //  res.status(200).send(data)
      }).catch((err) => {
        res.status(500).send(err);
      });
    }



  }

  static flatTenantImage = async (req: Request, res: Response, next) => {
    const flatTenantRepository = getRepository(FlatTenants);

    const flatTenantObj = await flatTenantRepository.findOne(req.body.id);
    if (req.body.tenant_pic_name) {
      flatTenantObj.TENANT_PIC = req.body.tenant_pic_name;
    }

    console.log("updating file in different")
    flatTenantRepository.save(flatTenantObj).then((data) => {
      var flatTenantResObj = data;
      res.status(200).json({
        flatTenant: flatTenantResObj
      })
    }).catch((error) => {
      res.status(500).send(error);
    })
  }

  static flatTenantgetAll = async (req: Request, res: Response) => {
    const flatTenats = getRepository(FlatTenants);
    flatTenats.find({
      where : {
        associationEntity : req.params.id
      },
      relations: ['appartmentEntiy', 'associationEntity', 'associationChildEntity', 'associationGrandChildEntity'],
    }).then((data) => {
      res.send(data)
    }
    ).catch((error) => {
      res.send(error)
    });
  }

  static flatTenantgetById = async (req: Request, res: Response) => {
    const flatTenats = getRepository(FlatTenants);
    flatTenats.findOne({
      where: {
        TENANT_ID: req.params.id
      },
      relations: ['appartmentEntiy', 'associationEntity', 'associationChildEntity', 'associationGrandChildEntity'],
    }).then((data) => {
      res.send(data);
    }
    ).catch((error) => {
      res.send(error)
    });
  }

  static tenantUpdate = async (req: Request, res: Response, next) => {

    console.log("tenant update")
    // const appartmentChildEntity = getRepository(APPARTMENT_CHILD_ENTITIES);
    // const appGrandChildEntity = getRepository(APPARTMENT_GRAND_CHILD_ENTITIES);
    const flatTenantRepository = getRepository(FlatTenants);
    const appartmentRepository = getRepository(Appartment);
    const associationRepository = getRepository(ASSOCIATION);
    const appartmentChildRepository = getRepository(APPARTMENT_CHILD_ENTITIES);
    const appartmentGrandChildRepository = getRepository(APPARTMENT_GRAND_CHILD_ENTITIES);

    const flatTenantsObj = await flatTenantRepository.findOne(req.body.id);
    const appartmentObj = await appartmentRepository.findOne(req.body.appartmentId);
    const associationObj = await associationRepository.findOne(req.body.associationId);
    const appartmentChildObj = await appartmentChildRepository.findOne(req.body.appartmentChildId);
    const appartmentGrandChildObj = await appartmentGrandChildRepository.findOne(req.body.appartmentGrandChildId);
    flatTenantsObj.TENANT_FIRST_NAME = req.body.tenant_first_name;
    flatTenantsObj.TENANT_LAST_NAME = req.body.tenant_last_name;
    //flatTenantsObj.TENANT_EMAIL = req.body.tenant_email;
    flatTenantsObj.TENANT_MOBILE_NUMBER = req.body.tenant_mobile_number;
    var isFinancialAcceess ;
    if(req.body.is_financial_access == 'true'){
      isFinancialAcceess = true
    }
    else {
      isFinancialAcceess = false
    }
    flatTenantsObj.IS_FINANCIAL_ACCEESS = isFinancialAcceess;
    flatTenantsObj.TENANT_PIC = req.body.tenant_pic;
    flatTenantsObj.TENANT_ADDRESS = req.body.tenant_address;
    flatTenantsObj.IS_ACTIVE = true;
    flatTenantsObj.appartmentEntiy = appartmentObj;
    flatTenantsObj.associationEntity = associationObj;
    flatTenantsObj.associationChildEntity = appartmentChildObj;
    flatTenantsObj.associationGrandChildEntity = appartmentGrandChildObj;
    flatTenantsObj.UPDATED_BY = req.body.created_by;
    flatTenantRepository.save(flatTenantsObj).then(async (data) => {
      // res.send(data)
      req.body.id = flatTenantsObj.TENANT_ID;
      req.body.flatTenantsObj = flatTenantsObj;
      req.body.data = data;
      next();
    }
    ).catch((error) => {
      res.send(error)
    });
  }
  static tenantAppUpdate = async (req: Request, res: Response, next) => {
    // const appartmentChildEntity = getRepository(APPARTMENT_CHILD_ENTITIES);
    // const appGrandChildEntity = getRepository(APPARTMENT_GRAND_CHILD_ENTITIES);
    const flatTenantRepository = getRepository(FlatTenants);
    // const appartmentRepository = getRepository(Appartment);
    const associationRepository = getRepository(ASSOCIATION);
    // const appartmentChildRepository = getRepository(APPARTMENT_CHILD_ENTITIES);
    // const appartmentGrandChildRepository = getRepository(APPARTMENT_GRAND_CHILD_ENTITIES);

    const flatTenantsObj = await flatTenantRepository.findOne(req.body.id);
    // const appartmentObj = await appartmentRepository.findOne(req.body.appartmentId);
    const associationObj = await associationRepository.findOne(req.body.associationId);
    // const appartmentChildObj = await appartmentChildRepository.findOne(req.body.appartmentChildId);
    // const appartmentGrandChildObj = await appartmentGrandChildRepository.findOne(req.body.appartmentGrandChildId);
    flatTenantsObj.TENANT_FIRST_NAME = req.body.first_name;
    flatTenantsObj.TENANT_LAST_NAME = req.body.last_name;
    flatTenantsObj.TENANT_EMAIL = req.body.email;
    flatTenantsObj.TENANT_MOBILE_NUMBER = req.body.mobile_number;
    // flatTenantsObj.IS_FINANCIAL_ACCEESS = req.body.is_financial_access;
    // flatTenantsObj.TENANT_PIC = req.body.tenant_pic;
    // flatTenantsObj.TENANT_ADDRESS = req.body.address;
    flatTenantsObj.IS_ACTIVE = true;
    // flatTenantsObj.appartmentEntiy = appartmentObj;
    flatTenantsObj.associationEntity = associationObj;
    // flatTenantsObj.associationChildEntity = appartmentChildObj;
    // flatTenantsObj.associationGrandChildEntity = appartmentGrandChildObj;
    flatTenantsObj.UPDATED_BY = req.body.updated_by;
    flatTenantRepository.save(flatTenantsObj).then(async (data) => {
    //  res.send(data);
      // req.body.id = flatTenantsObj.TENANT_ID;
      // req.body.flatTenantsObj = flatTenantsObj;
       req.body.data = data;
       next();
    }
    ).catch((error) => {
      res.send(error)
    });
  }


  static flatTenantDelete = async (req: Request, res: Response) => {

    const flatTenantRepository = getRepository(FlatTenants);

    // const result = await flatTenantRepository.query('DELETE FROM FLAT_TENANTS WHERE TENANT_ID = ' + req.body.id + '');
    // res.send(result);
    await flatTenantRepository.query('DELETE FROM FLAT_TENANTS WHERE TENANT_ID = ' + req.body.id + '').then(async (data) => {
      res.send(data);
    }).catch((error) => {
      res.send(error);
    });


  }

}

export default FlatTenantController;