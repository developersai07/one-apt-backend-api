import { Request, Response, response } from "express";
// const express = require('express');
// import * as jwt from "jsonwebtoken";
import { getRepository, AdvancedConsoleLogger } from "typeorm";
import { ASSOCIATION } from "../entity/Associations";
import { COUNTRIES } from "../entity/Countries";
import { STATES } from "../entity/States";
import { CITIES } from "../entity/Cities";
import { Appartment } from "../entity/AppartmentEntity";
import { AppUserDetails } from '../entity/AppUserDetails';
import { Roles } from '../entity/Roles';
import { UserRoles } from "../entity/UserRoles";
import { Plans } from "../entity/Plans";
import { AssociationSubscription } from "../entity/AssociationSubscription";
import AssociationSendMail from "../Mails/AssociationSendMail";


import * as fileUpload from "express-fileupload";
const fs = require('fs');
import AssociationUploadFiles from "../AssocaitionRegistrayionFIle/AssociationFile"
import { FlatOwners } from "../entity/FlatOwners";
import { FlatTenants } from "../entity/FlatTenants";
import { APPARTMENT_GRAND_CHILD_ENTITIES } from "../entity/AppartmentGrandChildEntity";

class AssociationController {
  static associationEnrollment = async (req: Request, res: Response, next) => {
    const countriesrep = getRepository(COUNTRIES);
    const statesrep = getRepository(STATES);
    const citiessrep = getRepository(CITIES);
    const appartmentrep = getRepository(Appartment);
    const assorep = getRepository(ASSOCIATION);
    const appUserRepository = getRepository(AppUserDetails);
    const rolesrep = getRepository(Roles);
    const userRolesRepository = getRepository(UserRoles);
    const plansRepository = getRepository(Plans);
    const associationSubscriptionrep = getRepository(AssociationSubscription);
    const contryobj = await countriesrep.findOne(req.body.country_id);
    const statesobj = await statesrep.findOne(req.body.states_id);
    const citiesobj = await citiessrep.findOne(req.body.cities_id);
    const appartment = new Appartment();
    appartment.APPARTMENT_NAME = req.body.appartment_name;
    appartment.DISTRICT = req.body.distrcit;
    appartment.LAND_MARK = req.body.landmark;
    appartment.PINCODE = req.body.pin_code;
    appartment.ADDRESS = req.body.address;
    appartment.APPARTMENT_UNIQUE_NUMBER = "123123214";

    if(req.body.app_registration_number != 'undefined'){
      appartment.REGISTRATION_NUMBER=  req.body.app_registration_number; 
    }
   // appartment.REGISTRATION_NUMBER = req.body.app_registration_number;
    appartment.IS_REGISTERED = req.body.is_registration_number;
    appartment.REGISTRATION_FILE_LOC = 'asdad';
    appartment.CREATED_BY = 'User';
    appartment.UPDATED_BY = 'User';
    appartment.country = contryobj;
    appartment.states = statesobj;
    appartment.cities = citiesobj;
    const apprtmentObj = appartmentrep.create(appartment);
    appartmentrep.save(apprtmentObj).then(async (data: any) => {
      var appartmentId = data.id;
      const upAppartmentObj: any = await appartmentrep.findOne(data.id);
      var today = new Date();
      var dd = String(today.getDate()).padStart(2, '0');
      var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
      var yyyy = today.getFullYear();
      var hrs = today.getHours();
      var minut = today.getMinutes();
      var seconds = today.getSeconds();
      var stringDate = 'OP' + mm + '' + dd + '' + yyyy + '' + hrs + '' + minut + '' + seconds + '' + data.id;
      upAppartmentObj.APPARTMENT_UNIQUE_NUMBER = stringDate;

      var obj = await appartmentrep.save(upAppartmentObj);

      const associationobj = new ASSOCIATION();
      associationobj.ASSOCIATION_NAME = req.body.association_name;
      associationobj.ASSOCIATION_UNIQUE_NUMBER = "2131231223";
      associationobj.CONTACT_PERSON_NAME = req.body.contact_person_name;
      associationobj.PHONE_NUMBER = req.body.phone_number;
      associationobj.EMAIL = req.body.email;
      associationobj.NUMBER_OF_FLATS_HOUSES = req.body.number_flats_houses;
      associationobj.PRESIDENT_NAME = req.body.presiden_name;
      associationobj.PRESIDENT_EMAIL = req.body.president_email;
      associationobj.appatmentInfo = upAppartmentObj;
      associationobj.ZIP_CODE = req.body.pin_code;
      associationobj.PRESIDENT_PHONE_NUMBER = req.body.president_phone_number;
      associationobj.PRESIDENT_ADDRESS = req.body.prseident_address;
      associationobj.NUMBER_OF_PARKING_SLOTS = req.body.number_of_parking_slots;
      associationobj.TWO_WHELLER_PARKING_SLOTS = req.body.two_wheeler_parking_slots;
      associationobj.FOUR_WHELLER_PARKING_SLOTS = req.body.four_wheeler_parking_slots;
      associationobj.AMBULANCE_PARKING = req.body.ambulance_parking;

      if(req.body.ass_registraion_number != 'undefined'){

        console.log("ok fails")

        console.log(req.body)
        associationobj.REGISTRATION_NUMBER= req.body.ass_registraion_number; 
      }
      associationobj.IS_REGISTERED = req.body.is_registered;
     /// associationobj.REGISTRATION_NUMBER = req.body.ass_registraion_number;
      associationobj.REGISTRATION_FILE_LOC = req.body.ass_registraion_file;
      associationobj.VISITOR_VEHICLE_ALLOWED = req.body.visitor_vehicle;
      //
      associationobj.DATA_RANGE = req.body.data_range;
      associationobj.NUMBER_REPORT_CARDS = req.body.no_of_report_cards;
      associationobj.NUMBER_FLAT_HOUSES = req.body.number_flats_houses;
      associationobj.NUMBER_EMPLOYEES_LOGIN = req.body.no_of_employee_login;
      associationobj.EMAIL_MODE = req.body.email_mode;
      associationobj.SMS_MODE = req.body.sms_mode;
      associationobj.WHATSAPP_MODE = req.body.whatsApp_mode;
      associationobj.ACT_REQ_COMMUNICATION = req.body.act_req_communication;
      associationobj.ASS_PAYMENT = req.body.ass_payment;


      associationobj.CREATED_BY = 'User';
      associationobj.UPDATED_BY = 'User';
      const associationNewE = await assorep.create(associationobj);
      await assorep.save(associationNewE).then((async data => {
        var associationIdmentId = data.id;

        var todayA = new Date();
        var dda = String(today.getDate()).padStart(2, '0');
        var mma = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyya = today.getFullYear();
        var hrsa = today.getHours();
        var minuta = today.getMinutes();
        var secondsa = today.getSeconds();
        var stringDatea = 'OP' + mma + '' + dda + '' + yyyya + '' + hrsa + '' + minuta + '' + secondsa + '' + data.id;
        // upAppartmentObj.APPARTMENT_UNIQUE_NUMBER = stringDate;
        const upAssocObj: any = await assorep.findOne(data.id);
        upAssocObj.ASSOCIATION_UNIQUE_NUMBER = stringDatea;
        assorep.save(upAssocObj).then((newdata => {

          var assTring = req.body.association_name.charAt(0);
          var appTring = req.body.appartment_name.charAt(0);
          var username = appartmentId + '' + appTring + '' + assTring + '' + associationIdmentId + '' + req.body.presiden_name;
          var password = Math.floor(100000000 + Math.random() * 900000000) + '';

          const appUser = new AppUserDetails();
          appUser.IS_ACTIVE = true;
          appUser.USER_NAME = username;
          appUser.PASSWORD = password;
          appUser.adminInfo = null;
          appUser.association = newdata;
          const appuserObj = appUserRepository.create(appUser);
          appUserRepository.save(appuserObj).then(async data => {
            const appUserResObj = data;
            var roleId = 2;
            const roleObj = await rolesrep.findOne(roleId);
            //  const roleObj =   rolesrep.findOne(2);
            const userRole = new UserRoles();
            userRole.role = roleObj;
            userRole.appUserInfo = data;
            const userObj = userRolesRepository.create(userRole);
            userRolesRepository.save(userObj).then(async (roleObj) => {
              const planObj = await plansRepository.findOne(req.body.plan);
              var Cr_date = new Date();
              var date = Cr_date.setDate(Cr_date.getDate() + planObj.DAYS);
              var subEndDate = new Date(date)
              const assocSub = new AssociationSubscription();
              assocSub.plan = planObj;
              assocSub.appUser = appUserResObj;
              assocSub.IS_ASSOCIATION = true;
              assocSub.IS_VENDOR = false;
              assocSub.IS_ACTIVE = true;
              assocSub.SUCBSCRIPTION_START_DATE = new Date();
              assocSub.SUBSCRIPTION_CREATED_DATE = new Date();
              assocSub.SUCBSCRIPTION_END_DATE = subEndDate;
              assocSub.UPDATED_BY = 'User';
              assocSub.CREATED_BY = 'User';
              assocSub.UPDATED_BY = 'User';
              const assSubObj = associationSubscriptionrep.create(assocSub);
              associationSubscriptionrep.save(assSubObj).then((assObj) => {
                AssociationSendMail.associationEnrollmentSendMail(req.body.association_name,username, password, req.body.email);
                req.body.obj = obj;
                req.body.data = newdata;
                req.body.userDetaild = data;
                req.body.data = newdata;
                req.body.roleObj = roleObj;
                next();
              }).catch((error) => {
                res.status(500).send(error)
              })

            }).catch((error) => {
              res.status(500).send(error)
            })

          }).catch((error) => {
            res.status(500).send(error)
          });

        }))
      })

      ).catch((error) => {
        console.log(error);
        res.status(500).send(error)
      });
    }
    ).catch((error) => {

      res.send(error)
    });



  };

  static updateAssociationEnrollment = async (req:Request, res: Response, next) => {
    console.log("updateAssociationEnrollment");
    const appartmentRep = getRepository(Appartment);
    const associationRep = getRepository(ASSOCIATION);
    const countriesrep = getRepository(COUNTRIES);
    const statesrep = getRepository(STATES);
    const citiessrep = getRepository(CITIES);

    const assObj = await associationRep.findOne(req.body.ass_id);
    const aptObj = await appartmentRep.findOne(req.body.apt_id);
    const contryobj = await countriesrep.findOne(req.body.country);
    const statesobj = await statesrep.findOne(req.body.state);
    const citiesobj = await citiessrep.findOne(req.body.city);
    aptObj.APPARTMENT_NAME = req.body.apt_name;
    aptObj.ADDRESS = req.body.address;
    aptObj.cities = citiesobj;
    aptObj.states = statesobj;
    aptObj.country = contryobj;
    aptObj.DISTRICT = req.body.distrcit;
    aptObj.LAND_MARK = req.body.landmark;
    aptObj.UPDATED_BY = req.body.updated_by;
    aptObj.UPDATED_DATE = new Date();
    appartmentRep.save(aptObj).then(async (data) => {
      var appartmentId = data.id;
      console.log(appartmentId);
      const upAppartmentObj: any = await appartmentRep.findOne(data.id);
      assObj.ASSOCIATION_NAME = req.body.ass_name;
      assObj.CONTACT_PERSON_NAME = req.body.ass_contact_person_name;
      assObj.PHONE_NUMBER = req.body.ass_phone_number;
      assObj.EMAIL = req.body.ass_email;
      assObj.NUMBER_OF_FLATS_HOUSES = req.body.num_of_flats;
      assObj.PRESIDENT_NAME = req.body.ass_president_name;
      assObj.PRESIDENT_EMAIL = req.body.ass_president_email;
      assObj.appatmentInfo = upAppartmentObj;
      assObj.ZIP_CODE = req.body.pincode;
      assObj.PRESIDENT_PHONE_NUMBER = req.body.ass_president_mobile;
      assObj.PRESIDENT_ADDRESS = req.body.ass_president_address;
      assObj.NUMBER_OF_PARKING_SLOTS = req.body.number_of_parking_slots;
      assObj.TWO_WHELLER_PARKING_SLOTS = req.body.number_of_two_wheeler_parking_slots;
      assObj.FOUR_WHELLER_PARKING_SLOTS = req.body.number_of_Four_Wheeler_parking_slots;
      assObj.AMBULANCE_PARKING = req.body.ambulance_parking;
      // assObj.IS_REGISTERED = req.body.is_registered;
      // assObj.REGISTRATION_NUMBER = req.body.ass_registraion_number;
      // assObj.REGISTRATION_FILE_LOC = req.body.ass_registraion_file;
      assObj.VISITOR_VEHICLE_ALLOWED = req.body.visitor_parking;
      //
      assObj.DATA_RANGE = req.body.data_range;
      assObj.NUMBER_REPORT_CARDS = req.body.number_of_report_cards;
      assObj.NUMBER_FLAT_HOUSES = req.body.num_of_registered_flats;
      assObj.NUMBER_EMPLOYEES_LOGIN = req.body.num_emp_login;
      assObj.EMAIL_MODE = req.body.com_email;
      assObj.SMS_MODE = req.body.sms;
      assObj.WHATSAPP_MODE = req.body.whats_app;
      assObj.ACT_REQ_COMMUNICATION = req.body.act_req_communication;
      // associationobj.ASS_PAYMENT = req.body.ass_payment;
      assObj.UPDATED_BY = req.body.updated_by;
      assObj.UPDATED_DATE = new Date();
      associationRep.save(assObj).then(async (data) => {
        res.send(data);

      }).catch((error) => {
        res.send(error);
      });

    }).catch((error)=>{
      res.send(error);
    })

  }


  static added = async (req: Request, res: Response, next) => {
    console.log("added");
    // console.log("okwrwr");

    // console.log(req.files);

    AssociationUploadFiles.uploadFile(req, res);
    req.body.id = "12234";

    next();
    // res.send("ok");

  }




  static updateFileName = async (req: Request, res: Response, next) => {

    console.log("Stept by Step Woerkinsdf");
    res.send("All Are Done Ok");

  }


  static updateAppartmentFileLoc = async (req: Request, res: Response, next) => {

    if (req.body.appartment_file_loc) {

      const appartmentrep = getRepository(Appartment);

      const appartmentObj = await appartmentrep.findOne(req.body.obj.id);
      appartmentObj.REGISTRATION_FILE_LOC = req.body.appartment_file_loc;

      appartmentrep.save(appartmentObj);
      next();
    }
    else {
      next();
    }



  }

  static updateassociationFileLoc = async (req: Request, res: Response, next) => {

    if (req.body.association_file_loc) {
      const associationrep = getRepository(ASSOCIATION);
      const assocaitionObj = await associationrep.findOne(req.body.data.id);
      assocaitionObj.REGISTRATION_FILE_LOC = req.body.association_file_loc;
      const fObj = await associationrep.save(assocaitionObj);
      res.status(200).json({
        obj: req.body.obj,
        data: req.body.newdata,
        userDetaild: req.body.data,
        roleObj: req.body.roleObj,
        assObj: req.body.assObj
      })
    }
    else {
      res.status(200).json({
        obj: req.body.obj,
        data: req.body.newdata,
        userDetaild: req.body.data,
        roleObj: req.body.roleObj,
        assObj: req.body.assObj
      })
    }



  }

  static associationInfo = async (req: Request, res: Response, next) => {
    const assorep = getRepository(ASSOCIATION);

    assorep.findOne({
      where: {
        id: req.params.id
      },
      relations: ['appatmentInfo', 'appatmentInfo.cities', 'appatmentInfo.states', 'appatmentInfo.country']
    }).then((data) => {
      console.log(data);
      res.send(data);
    }).catch((error) => {
      res.status(500).send(error);
    })

  }

  
  // static associationGrandChildEntites = async (req: Request, res: Response, next) => {
  //   const grandChildRep = getRepository(APPARTMENT_GRAND_CHILD_ENTITIES);

  //   grandChildRep.find({
  //     where: {
  //       id: req.params.id
  //     },
  //     relations: ['appatmentInfo', 'appatmentInfo.cities', 'appatmentInfo.states', 'appatmentInfo.country']
  //   }).then((data) => {
  //     console.log(data);
  //     res.send(data);
  //   }).catch((error) => {
  //     res.status(500).send(error);
  //   })

  // }

  static associationInformation = async (req: Request, res: Response, next) => {
    const assorep = getRepository(ASSOCIATION);

    assorep.find({
      where: {
        id: req.params.id
      }
    }).then((data) => {
      console.log(data);
      res.send(data);
    }).catch((error) => {
      res.status(500).send(error);
    })

  }


  static associationByZipCode = async (req: Request, res: Response, next) => {
    const assorep = getRepository(ASSOCIATION);
    assorep.find({
      where: {
        ZIP_CODE: req.params.zipcode
      },
      // relations :['appatmentInfo']
    }).then((data) => {
      console.log(data);
      res.send(data);
    }).catch((error) => {
      res.status(500).send(error);
    })

  }

  static getAssociationData = async (req: Request, res: Response, next) => {
        const associationCount = await getRepository(ASSOCIATION).query("SELECT COUNT(*) as associationCount From association");
        const flatOwnerCount = await getRepository(FlatOwners).query("SELECT COUNT(*) as flatOwnerCount From flat_owners");
     const flatTenantCount = await getRepository(FlatTenants).query("SELECT COUNT(*) as flatTenantCount From flat_tenants");
     const vendorCount = await getRepository(FlatTenants).query("SELECT  COUNT(*) as vendorCount FROM vendor WHERE `vendor`.`ASSOCIATION_ID` IS NOT NULL  ");
     res.json({
      associationCount : associationCount,
      flatOwnerCount : flatOwnerCount,
      flatTenantCount  : flatTenantCount,
      vendorCount : vendorCount
     })

  }

}
export default AssociationController;