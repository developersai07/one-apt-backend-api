import { Request, Response } from "express";
import * as jwt from "jsonwebtoken";
import { getRepository } from "typeorm";
import { STATES } from "../entity/States";

class StatesController {
    static statesgetall = async (req: Request, res: Response) => {

        const statesrep = getRepository(STATES);
        const results = await statesrep.find({
            relations: ['country']
        }).then(
            data => {
                console.log(data);
                res.send(data);
            }).catch(err => {
                console.log(err);
            });
    }

    static getStates = async (req: Request, res: Response) => {
        const statesrep = getRepository(STATES);
        const result = await statesrep.find({
            // relations: ['country'],
            where: { country : req.params.id }                
            }
        )
       res.send(result);
    }



}

export default StatesController;