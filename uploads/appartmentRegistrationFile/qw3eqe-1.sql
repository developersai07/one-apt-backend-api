/*
SQLyog Professional v12.4.1 (64 bit)
MySQL - 8.0.13-4 : Database - 6SdzIkLA0y
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`6SdzIkLA0y` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;

USE `6SdzIkLA0y`;

/*Table structure for table `promotional_code` */

DROP TABLE IF EXISTS `promotional_code`;

CREATE TABLE `promotional_code` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `promotional_code` varchar(40) DEFAULT NULL,
  `userId` varchar(10) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `created_by` varchar(10) DEFAULT NULL,
  `last_updated_by` varchar(10) DEFAULT NULL,
  `last_updated_date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

/*Data for the table `promotional_code` */

insert  into `promotional_code`(`id`,`promotional_code`,`userId`,`created_date`,`created_by`,`last_updated_by`,`last_updated_date`) values 
(11,'MS2020024','90','2020-02-26','90','90','2020-02-26'),
(10,'MS2020023-11','89','2020-02-25','89','89','2020-02-25'),
(9,'MS2020024-11','88','2020-02-25','88','88','2020-02-25'),
(12,'1582098995','91','2020-02-26','91','91','2020-02-26');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
