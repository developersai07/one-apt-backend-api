import { Router } from "express";
import AssociationComServiceController  from "../Controllers/AssociationComServiceController";
import { checkJwt } from "../middlewares/checkJwt";
//import { checkJwt } from "../middlewares/checkJwt";
const router = Router();
//Login route
router.post("/addService",[checkJwt], AssociationComServiceController.addService);

 router.get("/allAssociationServices/:id",[checkJwt], AssociationComServiceController.allAssociationServices);
 router.post("/delete",[checkJwt], AssociationComServiceController.delete);


export default router;